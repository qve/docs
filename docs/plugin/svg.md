# SVG

矢量图形（Scalable Vector Graphics，SVG），用于描述二维的矢量图形，基于 XML 的标记语言。

## quick-svg 组件

- [Markdown 示例展示](../plugin/mindmap.md)
- [quick-svg 更新库](https://www.npmjs.com/package/quick-svg)
- [SVG 文档](https://developer.mozilla.org/zh-CN/docs/Web/SVG/Element)
- [SVG 入门](https://zhuanlan.zhihu.com/p/351659629)

## edge foreignObject

注意火狐与 edge 浏览器显示存在差异

## Mindmap 脑图组件、

可全局引入，当前为局部引入使用

```vue
<template>
  <div>
    <Mindmap :config="the.config" :data="the.data" @onEvent="mindmapEvent" />
  </div>
</template>
<script>
// 引入样式
import 'quick-svg/styles/mindmap.less';
// 引入组件
import { Mindmap } from 'quick-svg';
export default {
  components: { Mindmap },
  setup() {
    const the = {
      config: {
        line: 2,
        lineColor: '#9299bb'
      },
      data: [
        {
          title: '脑图节点',
          shape: 'rect',
          children: [
            { title: '初始配置' },
            {
              title: '<b>传入<b>参数',
              color: '#00bb00',
              lineColor: '#00bb00',
              children: [
                {
                  lineColor: '#00bb00',
                  title: 'config 显示配置ui'
                },
                {
                  lineColor: '#00bb00',
                  title: 'data 脑图数组',
                  children: [
                    { title: 'title 标题' },
                    { title: 'children 子节点' }
                  ]
                }
              ]
            },
            {
              title: 'onEvent 事件指令cmd',
              color: 'white',
              shape: 'rect',
              line: 3,
              lineColor: 'red',
              bgColor: 'red',
              children: [
                {
                  title: 'init 初始化返回',
                  color: 'red',
                  line: 3,
                  lineColor: 'red',
                  children: [
                    { title: 'cmd 事件指令' },
                    { title: 'data 脑图结构数组' },
                    { title: 'bind 回调更新脑图', lineColor: 'red' }
                  ]
                },
                { title: 'dot 节点点击事件' },
                { title: 'text 标题点击事件' }
              ]
            }
          ]
        }
      ]
    };

    /**
     * 递归取出对象当前对象
     * @param {*} key 节点规则 数组.序号
     * @returns
     */
    const getItemByKey = (key, data) => {
      return key.split('.').reduce((o, i) => {
        //  console.log(i, o.title)
        if (o.children) {
          // 从子节点查询
          return o.children[i];
        } else {
          // 直接取出
          return o[i];
        }
      }, data);
    };

    /** 脑图更新事件 */
    let mindmapBind;

    /**
     * 脑图事件
     */
    const mindmapEvent = (resp) => {
      // console.log(resp.cmd, resp.node)
      switch (resp.cmd) {
        case 'init': //组件初始化
          // 缓存脑图回调绑定方法
          mindmapBind = resp.bind;
          break;
        case 'dot': //节点事件
          const _item = getItemByKey(resp.node.key, the.data);
          // 显示或隐藏
          _item.hide = !_item.hide;
          // 整体更新
          mindmapBind();
          break;
        case 'text': //标题点击触发
          {
            const _item = getItemByKey(resp.node.key, the.data);
            if (_item) {
              // 编辑标题
              _item.title = 'musss';
              // 只更新指定序号的脑图标题
              mindmapBind({
                cmd: resp.cmd,
                index: resp.node.index,
                data: { title: _item.title }
              });
            }
          }
          break;
      }
    };

    return { the, mindmapEvent };
  }
};
</script>
```

### props 传入

| 属性   | 说明             | 类型   | 默认值  |
| ------ | ---------------- | ------ | ------- |
| named  | 组件命名         | String | mindmap |
| config | 组件 ui 参数配置 | Object |         |
| data   | 脑图 json 数据   | Array  |         |

- config ui 参数

```json
{
  /** 文字外框形状 path,rect */
  "shape": "path",
  /** 字体大小 */
  "font": 14,
  /** 节点数圆形半径 */
  "dotR": 20,
  /** 背景颜色 */
  "bgColor": "#9299bb",
  /** 连线颜色 */
  "lineColor": "#9299bb",
  /** 线宽度 */
  "line": 2,
  /** 连接线弯曲度 */
  "bent": 20,
  /** 节点内部间距 */
  "padding": 8,
  /** 节点外部上下间距 */
  "margin": 8,
  /** 节点左右间隔大小 */
  "interval": 25,
  /** 节点默认宽度 */
  "width": 100,
  /** 节点高度 */
  "height": 0
}
```

### onEvent 事件

- cmd init 初始化事件

| 属性 | 说明             | 类型     | 默认值 |
| ---- | ---------------- | -------- | ------ |
| cmd  | 事件指令         | String   | init   |
| bind | 脑图回调更新方法 | function |        |
| data | 脑图结构数据列表 | Array    |        |

- data

```json
[
  {
    "index": 0,
    "key": "0",
    "depth": 1,
    "sort": 0,
    "w": 93.2,
    "x": 6,
    "child": 3,
    "title": "脑图节点",
    "shape": "rect",
    "y": 4.25
  },
  {
    "index": 1,
    "key": "0.0",
    "depth": 2,
    "sort": 0,
    "w": 93.2,
    "x": 130.2,
    "child": 0,
    "title": "初始配置",
    "y": 1,
    "parent": {
      "index": 0,
      "key": "0",
      "depth": 1,
      "sort": 0,
      "w": 93.2,
      "x": 6,
      "child": 3,
      "title": "脑图节点",
      "shape": "rect",
      "y": 4.25
    }
  },
  {
    "index": 2,
    "key": "0.1",
    "depth": 2,
    "sort": 1,
    "w": 93.2,
    "x": 130.2,
    "child": 2,
    "title": "<b>传入<b>参数",
    "color": "#00bb00",
    "lineColor": "#00bb00",
    "y": 2.75,
    "parent": {
      "index": 0,
      "key": "0",
      "depth": 1,
      "sort": 0,
      "w": 93.2,
      "x": 6,
      "child": 3,
      "title": "脑图节点",
      "shape": "rect",
      "y": 4.25
    }
  }
]
```

- cmd text 标题点击

| 属性  | 说明               | 类型   | 默认值  |
| ----- | ------------------ | ------ | ------- |
| cmd   | 事件指令           | String | text    |
| named | 组件命名           | String | mindmap |
| event | 点击对象           | dom    |         |
| node  | 脑图点击的节点数据 | Json   |         |
