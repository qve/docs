# mindmap 脑图组件

支持在 Markdown 中显示脑图

- [mindmap 组件](./svg.md)

## Mindmap 脑图插件

<Mindmap auto>

```json
[
  {
    "title": "脑图节点",
    "shape": "rect",
    "children": [
      { "title": "初始配置" },
      {
        "title": "<b>传入<b>参数",
        "color": "#00bb00",
        "lineColor": "#00bb00",
        "children": [
          {
            "lineColor": "#00bb00",
            "title": "config 显示配置ui"
          },
          {
            "lineColor": "#00bb00",
            "title": "data 脑图数组",
            "children": [
              { "title": "title 标题" },
              { "title": "children 子节点" },
              { "title": "hide 不展开子节点" }
            ]
          }
        ]
      },
      {
        "title": "onEvent 事件指令cmd",
        "color": "white",
        "shape": "rect",
        "line": 3,
        "lineColor": "red",
        "bgColor": "red",
        "children": [
          {
            "title": "init 初始化返回",
            "color": "red",
            "line": 3,
            "lineColor": "red",
            "hide": true,
            "children": [
              { "title": "cmd 事件指令" },
              { "title": "data 脑图结构数组" },
              { "title": "bind 回调更新脑图", "lineColor": "red" }
            ]
          },
          { "title": "dot 节点点击事件" },
          { "title": "text 标题点击事件" }
        ]
      }
    ]
  }
]
```

</Mindmap>

## 使用方法

用标签内部写入 json `<Mindmap auto> </Mindmap>`
