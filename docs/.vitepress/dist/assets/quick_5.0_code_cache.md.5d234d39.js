import{bm as n,aS as s,ap as a,at as p}from"./plugin-vue_export-helper.ec0df64b.js";const y='{"title":"QuickCache \u7F13\u5B58","description":"","frontmatter":{},"headers":[{"level":2,"title":"QuickCache \u672C\u5730\u7F13\u5B58","slug":"quickcache-\u672C\u5730\u7F13\u5B58"},{"level":2,"title":"\u6309\u89C4\u5219\u589E\u5220\u67E5","slug":"\u6309\u89C4\u5219\u589E\u5220\u67E5"},{"level":2,"title":"QuickCache \u7F13\u5B58","slug":"quickcache-\u7F13\u5B58-1"},{"level":2,"title":"\u8FC7\u671F\u5220\u9664\u56DE\u8C03","slug":"\u8FC7\u671F\u5220\u9664\u56DE\u8C03"},{"level":2,"title":"\u5185\u5B58\u7F13\u5B58 qm","slug":"\u5185\u5B58\u7F13\u5B58-qm"}],"relativePath":"quick/5.0/code/cache.md","lastUpdated":1637162758785}',e={},t=p(`<h1 id="quickcache-\u7F13\u5B58" tabindex="-1">QuickCache \u7F13\u5B58 <a class="header-anchor" href="#quickcache-\u7F13\u5B58" aria-hidden="true">#</a></h1><p>MemoryCache <a href="http://xn--0iv.Net" target="_blank" rel="noopener noreferrer">\u662F.Net</a> Framework 4.0 \u5F00\u59CB\u63D0\u4F9B\u7684\u5185\u5B58\u7F13\u5B58\u7C7B\uFF0C\u4F7F\u7528\u8BE5\u7C7B\u578B\u53EF\u4EE5\u65B9\u4FBF\u7684\u5728\u7A0B\u5E8F\u5185\u90E8\u7F13\u5B58\u6570\u636E\u5E76\u5BF9\u4E8E\u6570\u636E\u7684\u6709\u6548\u6027\u8FDB\u884C\u65B9\u4FBF\u7684\u7BA1\u7406, \u5B83\u901A\u8FC7\u5728\u5185\u5B58\u4E2D\u7F13\u5B58\u6570\u636E\u548C\u5BF9\u8C61\u6765\u51CF\u5C11\u8BFB\u53D6\u6570\u636E\u5E93\u7684\u6B21\u6570\uFF0C\u4ECE\u800C\u51CF\u8F7B\u6570\u636E\u5E93\u8D1F\u8F7D\uFF0C\u52A0\u5FEB\u6570\u636E\u8BFB\u53D6\u901F\u5EA6\uFF0C\u63D0\u5347\u7CFB\u7EDF\u7684\u6027\u80FD\u3002</p><p>NuGet \u5F15\u7528 Microsoft.Extensions.Caching.Memory \u5305</p><h2 id="quickcache-\u672C\u5730\u7F13\u5B58" tabindex="-1">QuickCache \u672C\u5730\u7F13\u5B58 <a class="header-anchor" href="#quickcache-\u672C\u5730\u7F13\u5B58" aria-hidden="true">#</a></h2><ul><li>\u5168\u5C40\u542F\u52A8\u914D\u7F6E <code>Startup.cs</code></li></ul><div class="language-csharp line-numbers-mode"><pre><code><span class="token keyword">using</span> <span class="token namespace">Quick</span><span class="token punctuation">;</span>

<span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">void</span></span> <span class="token function">ConfigureServices</span><span class="token punctuation">(</span><span class="token class-name">IServiceCollection</span> services<span class="token punctuation">)</span>
<span class="token punctuation">{</span>

  <span class="token preprocessor property">#<span class="token directive keyword">region</span> \u4E2D\u95F4\u4EF6</span>
    <span class="token comment">// \u6CE8\u518CQuickCache \u7F13\u5B58</span>
    QuickCache<span class="token punctuation">.</span>Memory <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token constructor-invocation class-name">QuickMemoryProvider</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>


            <span class="token comment">// \u81EA\u5B9A\u4E49\u914D\u7F6E\u53C2\u8003\uFF0Cusing Microsoft.Extensions.Caching.Memory;</span>
            <span class="token comment">//QuickCache.Memory = new QuickMemoryProvider(new MemoryCacheOptions()</span>
            <span class="token comment">//{</span>
            <span class="token comment">//    //\u7F13\u5B58\u6700\u5927\u4E3A100\u4EFD</span>
            <span class="token comment">//    //##\u6CE8\u610Fnetcore\u4E2D\u7684\u7F13\u5B58\u662F\u6CA1\u6709\u5355\u4F4D\u7684\uFF0C\u7F13\u5B58\u9879\u548C\u7F13\u5B58\u7684\u76F8\u5BF9\u5173\u7CFB</span>
            <span class="token comment">//    SizeLimit = 100,</span>
            <span class="token comment">//    //\u7F13\u5B58\u6EE1\u4E86\u65F6\uFF0C\u538B\u7F2920%\uFF08\u5373\u5220\u966420\u4EFD\u4F18\u5148\u7EA7\u4F4E\u7684\u7F13\u5B58\u9879\uFF09</span>
            <span class="token comment">//    CompactionPercentage = 0.2,</span>
            <span class="token comment">//    //3\u79D2\u949F\u67E5\u627E\u4E00\u6B21\u8FC7\u671F\u9879</span>
            <span class="token comment">//    ExpirationScanFrequency = TimeSpan.FromSeconds(3)</span>
            <span class="token comment">//});</span>
  <span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br></div></div><h2 id="\u6309\u89C4\u5219\u589E\u5220\u67E5" tabindex="-1">\u6309\u89C4\u5219\u589E\u5220\u67E5 <a class="header-anchor" href="#\u6309\u89C4\u5219\u589E\u5220\u67E5" aria-hidden="true">#</a></h2><p>\u5B58\u50A8\u5B57\u6BB5\u89C4\u5219\uFF1A<code>qm:\u5E94\u7528ID:\u7248\u672C:\u5B9E\u9645key</code> \u5B58\u50A8\u5B57\u6BB5\u89C4\u5219\uFF1A<code>qmk:\u5E94\u7528key:\u7248\u672C:\u5B9E\u9645key</code></p><div class="language-csharp line-numbers-mode"><pre><code>
 <span class="token comment">// \u6309\u89C4\u5219\u7F13\u5B58\u5185\u5BB9</span>
 QuickCache<span class="token punctuation">.</span>Memory<span class="token punctuation">.</span><span class="token function">SetCache</span><span class="token punctuation">(</span><span class="token string">&quot;test&quot;</span><span class="token punctuation">,</span> <span class="token string">&quot;4567&quot;</span><span class="token punctuation">,</span> TimeSpan<span class="token punctuation">.</span><span class="token function">FromHours</span><span class="token punctuation">(</span><span class="token number">1</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

 <span class="token comment">// \u6309\u89C4\u5219\u53D6\u51FA\u5185\u5BB9</span>
 <span class="token class-name"><span class="token keyword">string</span></span> _val <span class="token operator">=</span> QuickCache<span class="token punctuation">.</span>Memory<span class="token punctuation">.</span><span class="token generic-method"><span class="token function">GetCache</span><span class="token generic class-name"><span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">&gt;</span></span></span><span class="token punctuation">(</span><span class="token string">&quot;test&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

 <span class="token comment">// \u6309\u89C4\u5219\u5220\u9664</span>
 QuickCache<span class="token punctuation">.</span>Memory<span class="token punctuation">.</span><span class="token function">RemoveCache</span><span class="token punctuation">(</span><span class="token string">&quot;test&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token comment">// \u5B58\u50A8\u662F\u5426\u5B58\u5728</span>
 QuickCache<span class="token punctuation">.</span>Memory<span class="token punctuation">.</span><span class="token function">KeyExists</span><span class="token punctuation">(</span><span class="token string">&quot;test&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

 <span class="token class-name">List<span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">&gt;</span></span> ls <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token constructor-invocation class-name">List<span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">&gt;</span></span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
 ls<span class="token punctuation">.</span><span class="token function">Add</span><span class="token punctuation">(</span><span class="token string">&quot;test1&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
 ls<span class="token punctuation">.</span><span class="token function">Add</span><span class="token punctuation">(</span><span class="token string">&quot;test2&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

  <span class="token comment">// \u6279\u91CF\u83B7\u53D6</span>
 <span class="token class-name">IDictionary<span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">,</span> <span class="token keyword">object</span><span class="token punctuation">&gt;</span></span> lo <span class="token operator">=</span> QuickCache<span class="token punctuation">.</span>Memory<span class="token punctuation">.</span><span class="token function">GetCacheAll</span><span class="token punctuation">(</span>ls<span class="token punctuation">)</span><span class="token punctuation">;</span>

</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br></div></div><h2 id="quickcache-\u7F13\u5B58-1" tabindex="-1">QuickCache \u7F13\u5B58 <a class="header-anchor" href="#quickcache-\u7F13\u5B58-1" aria-hidden="true">#</a></h2><div class="language-csharp line-numbers-mode"><pre><code>
 <span class="token comment">// \u6309\u89C4\u5219\u7F13\u5B58\u5185\u5BB9</span>
  QuickCache<span class="token punctuation">.</span>Memory<span class="token punctuation">.</span><span class="token function">Set</span><span class="token punctuation">(</span><span class="token string">&quot;test&quot;</span><span class="token punctuation">,</span> <span class="token string">&quot;4567&quot;</span><span class="token punctuation">,</span> TimeSpan<span class="token punctuation">.</span><span class="token function">FromHours</span><span class="token punctuation">(</span><span class="token number">1</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

 <span class="token comment">// \u6309\u89C4\u5219\u53D6\u51FA\u5185\u5BB9</span>
  <span class="token class-name"><span class="token keyword">string</span></span> _val <span class="token operator">=</span> QuickCache<span class="token punctuation">.</span>Memory<span class="token punctuation">.</span><span class="token generic-method"><span class="token function">Get</span><span class="token generic class-name"><span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">&gt;</span></span></span><span class="token punctuation">(</span><span class="token string">&quot;test&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

 <span class="token comment">// \u6309\u89C4\u5219\u5220\u9664</span>
  QuickCache<span class="token punctuation">.</span>Memory<span class="token punctuation">.</span><span class="token function">Remove</span><span class="token punctuation">(</span><span class="token string">&quot;test&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

  <span class="token comment">// \u5B58\u50A8\u662F\u5426\u5B58\u5728</span>
  QuickCache<span class="token punctuation">.</span>Memory<span class="token punctuation">.</span><span class="token function">TryGetValue</span><span class="token punctuation">(</span><span class="token string">&quot;test&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

  <span class="token comment">// \u5B58\u50A8\u6570\u91CF</span>
  QuickCache<span class="token punctuation">.</span>Memory<span class="token punctuation">.</span><span class="token function">Count</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

  <span class="token class-name">List<span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">&gt;</span></span> ls<span class="token operator">=</span><span class="token keyword">new</span> <span class="token constructor-invocation class-name">List<span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">&gt;</span></span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token comment">// \u6279\u91CF\u83B7\u53D6</span>
  QuickCache<span class="token punctuation">.</span>Memory<span class="token punctuation">.</span><span class="token function">GetAll</span><span class="token punctuation">(</span>ls<span class="token punctuation">)</span>

</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br></div></div><h2 id="\u8FC7\u671F\u5220\u9664\u56DE\u8C03" tabindex="-1">\u8FC7\u671F\u5220\u9664\u56DE\u8C03 <a class="header-anchor" href="#\u8FC7\u671F\u5220\u9664\u56DE\u8C03" aria-hidden="true">#</a></h2><div class="language-csharp line-numbers-mode"><pre><code>
 <span class="token comment">// \u8BBE\u7F6E\u8FC7\u671F\u65F6\u95F4</span>


 <span class="token comment">//\u5355\u4E2A\u7F13\u5B58\u9879\u7684\u914D\u7F6E</span>
 <span class="token class-name">MemoryCacheEntryOptions</span> Opts <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token constructor-invocation class-name">MemoryCacheEntryOptions</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
 <span class="token punctuation">{</span>
    <span class="token comment">//\u7EDD\u5BF9\u8FC7\u671F\u65F6\u95F41</span>
    <span class="token comment">//AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddSeconds(2)),</span>
    <span class="token comment">//\u7EDD\u5BF9\u8FC7\u671F\u65F6\u95F42</span>
    <span class="token comment">//AbsoluteExpirationRelativeToNow=TimeSpan.FromSeconds(3),</span>
    <span class="token comment">//\u76F8\u5BF9\u8FC7\u671F\u65F6\u95F4</span>
    SlidingExpiration <span class="token operator">=</span> TimeSpan<span class="token punctuation">.</span><span class="token function">FromSeconds</span><span class="token punctuation">(</span><span class="token number">3</span><span class="token punctuation">)</span><span class="token punctuation">,</span>
    <span class="token comment">//\u4F18\u5148\u7EA7\uFF0C\u5F53\u7F13\u5B58\u538B\u7F29\u65F6\u4F1A\u4F18\u5148\u6E05\u9664\u4F18\u5148\u7EA7\u4F4E\u7684\u7F13\u5B58\u9879</span>
    Priority <span class="token operator">=</span> CacheItemPriority<span class="token punctuation">.</span>Low<span class="token punctuation">,</span><span class="token comment">//Low,Normal,High,NeverRemove</span>
    <span class="token comment">//\u7F13\u5B58\u5927\u5C0F\u53601\u4EFD</span>
    Size <span class="token operator">=</span> <span class="token number">1</span>
  <span class="token punctuation">}</span><span class="token punctuation">;</span>

 <span class="token comment">// \u7EDD\u5BF9\u8FC7\u671F</span>
 <span class="token comment">// Opts.SetAbsoluteExpiration(TimeSpan.FromSeconds(3));</span>
 <span class="token comment">// \u6ED1\u52A8\u8FC7\u671F</span>
 <span class="token comment">// Opts.SlidingExpiration = TimeSpan.FromSeconds(3);</span>

 <span class="token comment">// \u8FC7\u671F\u56DE\u6389</span>
 Opts<span class="token punctuation">.</span><span class="token function">RegisterPostEvictionCallback</span><span class="token punctuation">(</span><span class="token punctuation">(</span>keyInfo<span class="token punctuation">,</span> valueInfo<span class="token punctuation">,</span> reason<span class="token punctuation">,</span> state<span class="token punctuation">)</span> <span class="token operator">=&gt;</span><span class="token punctuation">{</span>
     Console<span class="token punctuation">.</span><span class="token function">WriteLine</span><span class="token punctuation">(</span><span class="token interpolation-string"><span class="token string">$&quot;\u56DE\u8C03\u51FD\u6570\u8F93\u51FA\u3010\u952E:</span><span class="token interpolation"><span class="token punctuation">{</span><span class="token expression language-csharp">keyInfo</span><span class="token punctuation">}</span></span><span class="token string">,\u503C:</span><span class="token interpolation"><span class="token punctuation">{</span><span class="token expression language-csharp">valueInfo</span><span class="token punctuation">}</span></span><span class="token string">,\u88AB\u6E05\u9664\u7684\u539F\u56E0:</span><span class="token interpolation"><span class="token punctuation">{</span><span class="token expression language-csharp">reason</span><span class="token punctuation">}</span></span><span class="token string">\u3011&quot;</span></span><span class="token punctuation">)</span><span class="token punctuation">;</span>
 <span class="token punctuation">}</span><span class="token punctuation">)</span>

 <span class="token comment">// \u6309\u89C4\u5219\u5B58\u50A8</span>
 QuickCache<span class="token punctuation">.</span>Memory<span class="token punctuation">.</span><span class="token function">AddCache</span><span class="token punctuation">(</span>&#39;key<span class="token char">&#39;,&#39;</span><span class="token keyword">value</span>&#39;<span class="token punctuation">,</span>Opts<span class="token punctuation">)</span>
 <span class="token comment">//\u6807\u51C6\u5B58\u50A8</span>
 QuickCache<span class="token punctuation">.</span>Memory<span class="token punctuation">.</span><span class="token function">Add</span><span class="token punctuation">(</span>&#39;key<span class="token char">&#39;,&#39;</span><span class="token keyword">value</span>&#39;<span class="token punctuation">,</span>Opts<span class="token punctuation">)</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br></div></div><h2 id="\u5185\u5B58\u7F13\u5B58-qm" tabindex="-1">\u5185\u5B58\u7F13\u5B58 qm <a class="header-anchor" href="#\u5185\u5B58\u7F13\u5B58-qm" aria-hidden="true">#</a></h2><div class="language-csharp line-numbers-mode"><pre><code>    <span class="token comment">/// &lt;summary&gt;</span>
    <span class="token comment">/// \u5185\u5B58\u7F13\u5B58\u65B9\u6CD5\uFF0C\u540D\u547D\u524D\u7F00 qm:</span>
    <span class="token comment">/// &lt;/summary&gt;</span>
    <span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">QuickMemoryProvider</span>
    <span class="token punctuation">{</span>
        <span class="token preprocessor property">#<span class="token directive keyword">region</span> \u5C5E\u6027</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5185\u5B58\u7F13\u5B58</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">private</span> <span class="token return-type class-name">MemoryCache</span> Cache <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5F53\u524D\u5E94\u7528 Id</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">private</span> <span class="token return-type class-name">Int32</span> AppID <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u81EA\u5B9A\u4E49\u5185\u5B58\u7F13\u5B58\u65B9\u6CD5</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;cacheOps&quot;&gt;\u81EA\u5B9A\u4E49\u914D\u7F6E&lt;/param&gt;</span>
        <span class="token keyword">public</span> <span class="token function">QuickMemoryProvider</span><span class="token punctuation">(</span><span class="token class-name">MemoryCacheOptions</span> cacheOps<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>

        <span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>

        <span class="token preprocessor property">#<span class="token directive keyword">region</span> \u539F\u751F\u65B9\u6CD5</span>
        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u83B7\u53D6\u5BF9\u8C61</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;key&quot;&gt;\u975E\u89C4\u5219\u7F13\u5B58Key&lt;/param&gt;</span>
        <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name">T</span> <span class="token generic-method"><span class="token function">Get</span><span class="token generic class-name"><span class="token punctuation">&lt;</span>T<span class="token punctuation">&gt;</span></span></span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> key<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>


        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u6DFB\u52A0\u6ED1\u52A8\u7F13\u5B58\uFF0C\u81EA\u52A8\u5EF6\u671F</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;key&quot;&gt;\u975E\u89C4\u5219\u7F13\u5B58Key&lt;/param&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;value&quot;&gt;\u7F13\u5B58Value&lt;/param&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;expiresSliding&quot;&gt; TimeSpan.FromHours(1) \u8FC7\u671F\u65F6\u957F\uFF08\u5982\u679C\u5728\u8FC7\u671F\u65F6\u95F4\u5185\u6709\u64CD\u4F5C\uFF0C\u5219\u4EE5\u5F53\u524D\u65F6\u95F4\u70B9\u5EF6\u957F\u8FC7\u671F\u65F6\u95F4&lt;/param&gt;</span>
        <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">object</span></span> <span class="token function">Set</span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> key<span class="token punctuation">,</span> <span class="token class-name"><span class="token keyword">object</span></span> <span class="token keyword">value</span><span class="token punctuation">,</span> <span class="token class-name">TimeSpan</span> expiresSliding<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u6DFB\u52A0\u7F13\u5B58\uFF0C\u81EA\u52A8\u5EF6\u671F</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;key&quot;&gt;\u7F13\u5B58Key&lt;/param&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;value&quot;&gt;\u7F13\u5B58Value&lt;/param&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;Opts&quot;&gt;\u81EA\u5B9A\u4E49\u914D\u7F6E\uFF08\u5230\u671F\u56DE\u8C03\u65B9\u6CD5\uFF1ARegisterPostEvictionCallback\uFF09&lt;/param&gt;</span>
        <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">object</span></span> <span class="token function">Add</span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> key<span class="token punctuation">,</span> <span class="token class-name"><span class="token keyword">object</span></span> <span class="token keyword">value</span><span class="token punctuation">,</span> <span class="token class-name">MemoryCacheEntryOptions</span> Opts<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">///\u901A\u8FC7Key\u5220\u9664\u7F13\u5B58\u6570\u636E</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;key&quot;&gt;\u975E\u89C4\u5219\u7F13\u5B58Key&lt;/param&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">void</span></span> <span class="token function">Remove</span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> key<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5F53\u524D\u5B58\u50A8\u603B\u6570</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">int</span></span> <span class="token function">Count</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u662F\u5426\u5B58\u5728</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">///  &lt;param name=&quot;key&quot;&gt;\u975E\u89C4\u5219\u7F13\u5B58Key&lt;/param&gt;</span>
        <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">bool</span></span> <span class="token function">TryGetValue</span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> key<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u83B7\u53D6\u7F13\u5B58\u96C6\u5408</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;keys&quot;&gt;\u7F13\u5B58Key\u96C6\u5408&lt;/param&gt;</span>
        <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name">IDictionary<span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">,</span> <span class="token keyword">object</span><span class="token punctuation">&gt;</span></span> <span class="token function">GetAll</span><span class="token punctuation">(</span><span class="token class-name">IEnumerable<span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">&gt;</span></span> keys<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>
        <span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>

        <span class="token preprocessor property">#<span class="token directive keyword">region</span> \u5B58\u50A8\u89C4\u8303</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5B58\u50A8\u5B57\u6BB5\u89C4\u5219</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token class-name"><span class="token keyword">string</span></span> keyPrefix <span class="token operator">=</span> <span class="token string">&quot;qm:{0}:{1}:{2}&quot;</span><span class="token punctuation">;</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u4EA7\u751F\u5B58\u50A8\u89C4\u5219\u540D\u79F0 keyPrefix</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;keyName&quot;&gt;\u6A21\u5757\u540D\u79F0:\u7F13\u5B58\u4E3B\u952E\u547D\u540D\uFF0C\u4E0D\u80FD\u91CD\u590D\uFF08ui:home\uFF09&lt;/param&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;version&quot;&gt;\u7248\u672C\u53F7,\u9ED8\u8BA40.1&lt;/param&gt;</span>
        <span class="token comment">/// &lt;returns&gt;qm:\u5E94\u7528ID:\u7248\u672C:key&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">string</span></span> <span class="token function">GetKeyName</span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> keyName<span class="token punctuation">,</span> <span class="token class-name"><span class="token keyword">string</span></span> version <span class="token operator">=</span> <span class="token string">&quot;0.1&quot;</span><span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u4EA7\u751F\u5B58\u50A8\u89C4\u5219\u540D\u79F0 keyPrefix</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;keyName&quot;&gt;\u6A21\u5757\u540D\u79F0:\u7F13\u5B58\u4E3B\u952E\u547D\u540D\uFF0C\u4E0D\u80FD\u91CD\u590D\uFF08ui:home\uFF09&lt;/param&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;appKey&quot;&gt;\u5E94\u7528\u914D\u7F6E\u6CE8\u518C QuickSettings.App.Key&lt;/param&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;version&quot;&gt;\u5B58\u50A8\u7248\u672C 0.1&lt;/param&gt;</span>
        <span class="token comment">/// &lt;returns&gt;keyPrefix&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">string</span></span> <span class="token function">GetKeyName</span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> keyName<span class="token punctuation">,</span> <span class="token class-name"><span class="token keyword">string</span></span> appKey<span class="token punctuation">,</span> <span class="token class-name"><span class="token keyword">string</span></span> version<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>


        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u83B7\u53D6\u6240\u6709\u7F13\u5B58\u952E</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name">List<span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">&gt;</span></span> <span class="token function">GetKeys</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u9A8C\u8BC1\u7F13\u5B58\u9879\u662F\u5426\u5B58\u5728</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;key&quot;&gt;\u7F13\u5B58Key&lt;/param&gt;</span>
        <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">bool</span></span> <span class="token function">KeyExists</span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> key<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>


        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u641C\u7D22 \u5339\u914D\u5230\u7684\u7F13\u5B58</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;pattern&quot;&gt;&lt;/param&gt;</span>
        <span class="token comment">/// &lt;returns&gt;SearchCacheRegex&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name">IList<span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">&gt;</span></span> <span class="token function">KeySearch</span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> pattern<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>
        <span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>



        <span class="token preprocessor property">#<span class="token directive keyword">region</span> \u6269\u5C55\u65B9\u6CD5</span>


        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u83B7\u53D6key\u503C\u7F13\u5B58</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;typeparam name=&quot;T&quot;&gt;&lt;/typeparam&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;key&quot;&gt;&lt;/param&gt;</span>
        <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name">T</span> <span class="token generic-method"><span class="token function">GetCache</span><span class="token generic class-name"><span class="token punctuation">&lt;</span>T<span class="token punctuation">&gt;</span></span></span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> key<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u83B7\u53D6\u975E\u6807\u7F13\u5B58\u96C6\u5408</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;keys&quot;&gt;\u7F13\u5B58Key\u96C6\u5408&lt;/param&gt;</span>
        <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name">IDictionary<span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">,</span> <span class="token keyword">object</span><span class="token punctuation">&gt;</span></span> <span class="token function">GetCacheAll</span><span class="token punctuation">(</span><span class="token class-name">IEnumerable<span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">&gt;</span></span> keys<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>



        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u6DFB\u52A0\u6ED1\u52A8\u7F13\u5B58\uFF0C\u81EA\u52A8\u5EF6\u671F</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;key&quot;&gt;\u7F13\u5B58Key&lt;/param&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;value&quot;&gt;\u7F13\u5B58Value&lt;/param&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;expiresSliding&quot;&gt; TimeSpan.FromHours(1) \u8FC7\u671F\u65F6\u957F\uFF08\u5982\u679C\u5728\u8FC7\u671F\u65F6\u95F4\u5185\u6709\u64CD\u4F5C\uFF0C\u5219\u4EE5\u5F53\u524D\u65F6\u95F4\u70B9\u5EF6\u957F\u8FC7\u671F\u65F6\u95F4&lt;/param&gt;</span>
        <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">object</span></span> <span class="token function">SetCache</span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> key<span class="token punctuation">,</span> <span class="token class-name"><span class="token keyword">object</span></span> <span class="token keyword">value</span><span class="token punctuation">,</span> <span class="token class-name">TimeSpan</span> expiresSliding<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u6DFB\u52A0\u7F13\u5B58\uFF0C\u81EA\u52A8\u5EF6\u671F</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;key&quot;&gt;\u7F13\u5B58Key&lt;/param&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;value&quot;&gt;\u7F13\u5B58Value&lt;/param&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;Opts&quot;&gt;\u81EA\u5B9A\u4E49\u914D\u7F6E\uFF08\u5230\u671F\u56DE\u8C03\u65B9\u6CD5\uFF1ARegisterPostEvictionCallback\uFF09&lt;/param&gt;</span>
        <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">object</span></span> <span class="token function">AddCache</span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> key<span class="token punctuation">,</span> <span class="token class-name"><span class="token keyword">object</span></span> <span class="token keyword">value</span><span class="token punctuation">,</span> <span class="token class-name">MemoryCacheEntryOptions</span> Opts<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">///\u901A\u8FC7Key\u5220\u9664\u7F13\u5B58\u6570\u636E</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;key&quot;&gt;Key&lt;/param&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">void</span></span> <span class="token function">RemoveCache</span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> key<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">///  \u6279\u91CF\u5220\u9664\u7F13\u5B58</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;keys&quot;&gt;\u5173\u952E\u5B57\u96C6\u5408\uFF08\u6309\u89C4\u5219\u5B58\u50A8\u952E\uFF09&lt;/param&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">void</span></span> <span class="token function">RemoveCache</span><span class="token punctuation">(</span><span class="token class-name">IEnumerable<span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">&gt;</span></span> keys<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>



        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5220\u9664\u6240\u6709\u5305\u542B\u5B57\u7B26\u4E32\u7684\u7F13\u5B58</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;contain&quot;&gt;\u5305\u542B\u7684\u5B57\u7B26\u4E32&lt;/param&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">void</span></span> <span class="token function">RemoveByContain</span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> contain<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>


        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u6279\u91CF\u5220\u9664\u7F13\u5B58</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;param name=&quot;keys&quot;&gt;\u5173\u952E\u5B57\u96C6\u5408\uFF08\u975E\u89C4\u5219\u5B58\u50A8\u952E\uFF09&lt;/param&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">void</span></span> <span class="token function">RemoveAll</span><span class="token punctuation">(</span><span class="token class-name">IEnumerable<span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">&gt;</span></span> keys<span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5220\u9664\u6240\u6709\u7684\u7F13\u5B58</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">void</span></span> <span class="token function">Clear</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">{</span><span class="token punctuation">}</span>

        <span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>
    <span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br><span class="line-number">52</span><br><span class="line-number">53</span><br><span class="line-number">54</span><br><span class="line-number">55</span><br><span class="line-number">56</span><br><span class="line-number">57</span><br><span class="line-number">58</span><br><span class="line-number">59</span><br><span class="line-number">60</span><br><span class="line-number">61</span><br><span class="line-number">62</span><br><span class="line-number">63</span><br><span class="line-number">64</span><br><span class="line-number">65</span><br><span class="line-number">66</span><br><span class="line-number">67</span><br><span class="line-number">68</span><br><span class="line-number">69</span><br><span class="line-number">70</span><br><span class="line-number">71</span><br><span class="line-number">72</span><br><span class="line-number">73</span><br><span class="line-number">74</span><br><span class="line-number">75</span><br><span class="line-number">76</span><br><span class="line-number">77</span><br><span class="line-number">78</span><br><span class="line-number">79</span><br><span class="line-number">80</span><br><span class="line-number">81</span><br><span class="line-number">82</span><br><span class="line-number">83</span><br><span class="line-number">84</span><br><span class="line-number">85</span><br><span class="line-number">86</span><br><span class="line-number">87</span><br><span class="line-number">88</span><br><span class="line-number">89</span><br><span class="line-number">90</span><br><span class="line-number">91</span><br><span class="line-number">92</span><br><span class="line-number">93</span><br><span class="line-number">94</span><br><span class="line-number">95</span><br><span class="line-number">96</span><br><span class="line-number">97</span><br><span class="line-number">98</span><br><span class="line-number">99</span><br><span class="line-number">100</span><br><span class="line-number">101</span><br><span class="line-number">102</span><br><span class="line-number">103</span><br><span class="line-number">104</span><br><span class="line-number">105</span><br><span class="line-number">106</span><br><span class="line-number">107</span><br><span class="line-number">108</span><br><span class="line-number">109</span><br><span class="line-number">110</span><br><span class="line-number">111</span><br><span class="line-number">112</span><br><span class="line-number">113</span><br><span class="line-number">114</span><br><span class="line-number">115</span><br><span class="line-number">116</span><br><span class="line-number">117</span><br><span class="line-number">118</span><br><span class="line-number">119</span><br><span class="line-number">120</span><br><span class="line-number">121</span><br><span class="line-number">122</span><br><span class="line-number">123</span><br><span class="line-number">124</span><br><span class="line-number">125</span><br><span class="line-number">126</span><br><span class="line-number">127</span><br><span class="line-number">128</span><br><span class="line-number">129</span><br><span class="line-number">130</span><br><span class="line-number">131</span><br><span class="line-number">132</span><br><span class="line-number">133</span><br><span class="line-number">134</span><br><span class="line-number">135</span><br><span class="line-number">136</span><br><span class="line-number">137</span><br><span class="line-number">138</span><br><span class="line-number">139</span><br><span class="line-number">140</span><br><span class="line-number">141</span><br><span class="line-number">142</span><br><span class="line-number">143</span><br><span class="line-number">144</span><br><span class="line-number">145</span><br><span class="line-number">146</span><br><span class="line-number">147</span><br><span class="line-number">148</span><br><span class="line-number">149</span><br><span class="line-number">150</span><br><span class="line-number">151</span><br><span class="line-number">152</span><br><span class="line-number">153</span><br><span class="line-number">154</span><br><span class="line-number">155</span><br><span class="line-number">156</span><br><span class="line-number">157</span><br><span class="line-number">158</span><br><span class="line-number">159</span><br><span class="line-number">160</span><br><span class="line-number">161</span><br><span class="line-number">162</span><br><span class="line-number">163</span><br><span class="line-number">164</span><br><span class="line-number">165</span><br><span class="line-number">166</span><br><span class="line-number">167</span><br><span class="line-number">168</span><br><span class="line-number">169</span><br><span class="line-number">170</span><br><span class="line-number">171</span><br><span class="line-number">172</span><br><span class="line-number">173</span><br><span class="line-number">174</span><br><span class="line-number">175</span><br><span class="line-number">176</span><br><span class="line-number">177</span><br><span class="line-number">178</span><br><span class="line-number">179</span><br><span class="line-number">180</span><br><span class="line-number">181</span><br><span class="line-number">182</span><br><span class="line-number">183</span><br><span class="line-number">184</span><br><span class="line-number">185</span><br><span class="line-number">186</span><br><span class="line-number">187</span><br><span class="line-number">188</span><br><span class="line-number">189</span><br><span class="line-number">190</span><br><span class="line-number">191</span><br><span class="line-number">192</span><br><span class="line-number">193</span><br><span class="line-number">194</span><br><span class="line-number">195</span><br><span class="line-number">196</span><br><span class="line-number">197</span><br><span class="line-number">198</span><br><span class="line-number">199</span><br><span class="line-number">200</span><br></div></div>`,15),c=[t];function l(o,r,u,m,i,k){return s(),a("div",null,c)}var g=n(e,[["render",l]]);export{y as __pageData,g as default};
