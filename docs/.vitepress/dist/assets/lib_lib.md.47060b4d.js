import{bm as l,aS as o,ap as c,a as e,bh as p,at as t,aq as n,au as s,a_ as r}from"./plugin-vue_export-helper.ec0df64b.js";const A='{"title":"quick.lib","description":"","frontmatter":{},"headers":[{"level":2,"title":"quick.lib \u4F7F\u7528","slug":"quick-lib-\u4F7F\u7528"},{"level":2,"title":"md5 \u52A0\u5BC6","slug":"md5-\u52A0\u5BC6"},{"level":2,"title":"base64 \u7F16\u7801","slug":"base64-\u7F16\u7801"},{"level":2,"title":"utf8 \u7F16\u7801","slug":"utf8-\u7F16\u7801"},{"level":2,"title":"ubbEncode html \u8F6C\u7801 ubb","slug":"ubbencode-html-\u8F6C\u7801-ubb"},{"level":2,"title":"ubbDecode ubb \u8F6C\u7801 html","slug":"ubbdecode-ubb-\u8F6C\u7801-html"},{"level":2,"title":"imgMin \u56FE\u7247\u538B\u7F29\u52A0\u6C34\u5370","slug":"imgmin-\u56FE\u7247\u538B\u7F29\u52A0\u6C34\u5370"}],"relativePath":"lib/lib.md","lastUpdated":1640620951664}',u={},i=t(`<h1 id="quick-lib" tabindex="-1">quick.lib <a class="header-anchor" href="#quick-lib" aria-hidden="true">#</a></h1><p>lib \u65B9\u6CD5</p><h2 id="quick-lib-\u4F7F\u7528" tabindex="-1">quick.lib \u4F7F\u7528 <a class="header-anchor" href="#quick-lib-\u4F7F\u7528" aria-hidden="true">#</a></h2><div class="language-js line-numbers-mode"><pre><code><span class="token comment">// \u65B9\u6CD5\u540D</span>
<span class="token keyword">export</span> <span class="token punctuation">{</span> md5<span class="token punctuation">,</span> ubbDecode<span class="token punctuation">,</span> ubbEncode<span class="token punctuation">,</span> imgMin <span class="token punctuation">}</span><span class="token punctuation">;</span>

<span class="token comment">// \u9879\u76EE\u5F15\u5165</span>
<span class="token keyword">import</span> <span class="token punctuation">{</span> lib <span class="token punctuation">}</span> <span class="token keyword">from</span> <span class="token string">&#39;quick.lib&#39;</span><span class="token punctuation">;</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br></div></div><h2 id="md5-\u52A0\u5BC6" tabindex="-1"><code>md5</code> \u52A0\u5BC6 <a class="header-anchor" href="#md5-\u52A0\u5BC6" aria-hidden="true">#</a></h2>`,5),b=n("div",{class:"language-js line-numbers-mode"},[n("pre",null,[n("code",null,[n("span",{class:"token comment"},`/**
 * md5 \u7B97\u6CD5\u6A21\u5757
 * @param {*} str \u4F20\u5165\u5185\u5BB9
 * \u8FD4\u56DE \u5C0F\u5199\u5BC6\u6587
 */`),s(`
`),n("span",{class:"token keyword"},"return"),s(" lib"),n("span",{class:"token punctuation"},"."),n("span",{class:"token function"},"md5"),n("span",{class:"token punctuation"},"("),n("span",{class:"token string"},"'acd12344'"),n("span",{class:"token punctuation"},")"),n("span",{class:"token punctuation"},";"),s(`
`)])]),n("div",{class:"line-numbers-wrapper"},[n("span",{class:"line-number"},"1"),n("br"),n("span",{class:"line-number"},"2"),n("br"),n("span",{class:"line-number"},"3"),n("br"),n("span",{class:"line-number"},"4"),n("br"),n("span",{class:"line-number"},"5"),n("br"),n("span",{class:"line-number"},"6"),n("br")])],-1),k=n("h2",{id:"base64-\u7F16\u7801",tabindex:"-1"},[s("base64 \u7F16\u7801 "),n("a",{class:"header-anchor",href:"#base64-\u7F16\u7801","aria-hidden":"true"},"#")],-1),m=n("p",null,[n("code",null,"0.4.3"),s(" \u91CD\u8981\u66F4\u65B0")],-1),d=n("ul",null,[n("li",null,[s("lib.base64Encode "),n("code",null,"\u7531\u4E8E\u4E0D\u517C\u5BB9 c# utf8 \u7F16\u7801"),s("\uFF0C\u5DF2\u7ECF\u5F03\u7528\uFF0C\u6539\u4E3A "),n("code",null,"base64"),s(" \u65B9\u6CD5")])],-1),_=n("div",{class:"language-js line-numbers-mode"},[n("pre",null,[n("code",null,[n("span",{class:"token comment"},"// import { base64 } from 'quick.lib';"),s(`

`),n("span",{class:"token comment"},`/**
 * base64 \u7F16\u7801
 * @param {string} str \u9700\u8981\u7F16\u7801\u7684\u5185\u5BB9
 */`),s(`
`),n("span",{class:"token comment"},"//\u4EE3\u66FF\u4E4B\u524D lib.base64Encod"),s(`
`),n("span",{class:"token keyword"},"let"),s(" _code "),n("span",{class:"token operator"},"="),s(" lib"),n("span",{class:"token punctuation"},"."),s("base64"),n("span",{class:"token punctuation"},"."),n("span",{class:"token function"},"encode"),n("span",{class:"token punctuation"},"("),n("span",{class:"token string"},"'\u6211\u662Fbase64 \u517C\u5BB9c# utf8 \u4E2D\u6587\u7F16\u7801'"),n("span",{class:"token punctuation"},")"),n("span",{class:"token punctuation"},";"),s(`

`),n("span",{class:"token keyword"},"return"),s(),n("span",{class:"token punctuation"},"{"),s(`
  `),n("span",{class:"token literal-property property"},"code"),n("span",{class:"token operator"},":"),s(" _code"),n("span",{class:"token punctuation"},","),s(`
  `),n("span",{class:"token literal-property property"},"text"),n("span",{class:"token operator"},":"),s(" lib"),n("span",{class:"token punctuation"},"."),s("base64"),n("span",{class:"token punctuation"},"."),n("span",{class:"token function"},"decode"),n("span",{class:"token punctuation"},"("),s("_code"),n("span",{class:"token punctuation"},")"),s(`
`),n("span",{class:"token punctuation"},"}"),n("span",{class:"token punctuation"},";"),s(`
`)])]),n("div",{class:"line-numbers-wrapper"},[n("span",{class:"line-number"},"1"),n("br"),n("span",{class:"line-number"},"2"),n("br"),n("span",{class:"line-number"},"3"),n("br"),n("span",{class:"line-number"},"4"),n("br"),n("span",{class:"line-number"},"5"),n("br"),n("span",{class:"line-number"},"6"),n("br"),n("span",{class:"line-number"},"7"),n("br"),n("span",{class:"line-number"},"8"),n("br"),n("span",{class:"line-number"},"9"),n("br"),n("span",{class:"line-number"},"10"),n("br"),n("span",{class:"line-number"},"11"),n("br"),n("span",{class:"line-number"},"12"),n("br"),n("span",{class:"line-number"},"13"),n("br")])],-1),h=n("h2",{id:"utf8-\u7F16\u7801",tabindex:"-1"},[s("utf8 \u7F16\u7801 "),n("a",{class:"header-anchor",href:"#utf8-\u7F16\u7801","aria-hidden":"true"},"#")],-1),f=n("p",null,[n("code",null,"0.4.2")],-1),y=n("div",{class:"language-js line-numbers-mode"},[n("pre",null,[n("code",null,[n("span",{class:"token comment"},"// import { base64 } from 'quick.lib';"),s(`

`),n("span",{class:"token comment"},`/**
 * base64 \u7F16\u7801
 * @param {string} str \u9700\u8981\u7F16\u7801\u7684\u5185\u5BB9
 */`),s(`
`),n("span",{class:"token comment"},"//\u4EE3\u66FF\u4E4B\u524D lib.base64Encod"),s(`
`),n("span",{class:"token keyword"},"let"),s(" _code "),n("span",{class:"token operator"},"="),s(" lib"),n("span",{class:"token punctuation"},"."),s("utf8"),n("span",{class:"token punctuation"},"."),n("span",{class:"token function"},"encode"),n("span",{class:"token punctuation"},"("),n("span",{class:"token string"},"'\u6211\u517C\u5BB9c# utf8 \u4E2D\u6587\u7F16\u7801'"),n("span",{class:"token punctuation"},")"),n("span",{class:"token punctuation"},";"),s(`

`),n("span",{class:"token keyword"},"return"),s(),n("span",{class:"token punctuation"},"{"),s(`
  `),n("span",{class:"token literal-property property"},"code"),n("span",{class:"token operator"},":"),s(" _code"),n("span",{class:"token punctuation"},","),s(`
  `),n("span",{class:"token literal-property property"},"text"),n("span",{class:"token operator"},":"),s(" lib"),n("span",{class:"token punctuation"},"."),s("utf8"),n("span",{class:"token punctuation"},"."),n("span",{class:"token function"},"decode"),n("span",{class:"token punctuation"},"("),s("_code"),n("span",{class:"token punctuation"},")"),s(`
`),n("span",{class:"token punctuation"},"}"),n("span",{class:"token punctuation"},";"),s(`
`)])]),n("div",{class:"line-numbers-wrapper"},[n("span",{class:"line-number"},"1"),n("br"),n("span",{class:"line-number"},"2"),n("br"),n("span",{class:"line-number"},"3"),n("br"),n("span",{class:"line-number"},"4"),n("br"),n("span",{class:"line-number"},"5"),n("br"),n("span",{class:"line-number"},"6"),n("br"),n("span",{class:"line-number"},"7"),n("br"),n("span",{class:"line-number"},"8"),n("br"),n("span",{class:"line-number"},"9"),n("br"),n("span",{class:"line-number"},"10"),n("br"),n("span",{class:"line-number"},"11"),n("br"),n("span",{class:"line-number"},"12"),n("br"),n("span",{class:"line-number"},"13"),n("br")])],-1),g=n("h2",{id:"ubbencode-html-\u8F6C\u7801-ubb",tabindex:"-1"},[n("code",null,"ubbEncode"),s(" html \u8F6C\u7801 ubb "),n("a",{class:"header-anchor",href:"#ubbencode-html-\u8F6C\u7801-ubb","aria-hidden":"true"},"#")],-1),v=n("div",{class:"language-js line-numbers-mode"},[n("pre",null,[n("code",null,[n("span",{class:"token comment"},`/**
 * html \u8F6Cubb
 * @param {String} str
 */`),s(`
`),n("span",{class:"token keyword"},"let"),s(" resp "),n("span",{class:"token operator"},"="),s(" lib"),n("span",{class:"token punctuation"},"."),n("span",{class:"token function"},"ubbEncode"),n("span",{class:"token punctuation"},"("),n("span",{class:"token string"},"'<div>abc</div>'"),n("span",{class:"token punctuation"},")"),n("span",{class:"token punctuation"},";"),s(`
`),n("span",{class:"token comment"},"//console.log('ubb', resp);"),s(`
`),n("span",{class:"token comment"},"// alert('\u5B8C\u6210\\r\\n' + resp);"),s(`
`),n("span",{class:"token keyword"},"return"),s(" resp"),n("span",{class:"token punctuation"},";"),s(`
`)])]),n("div",{class:"line-numbers-wrapper"},[n("span",{class:"line-number"},"1"),n("br"),n("span",{class:"line-number"},"2"),n("br"),n("span",{class:"line-number"},"3"),n("br"),n("span",{class:"line-number"},"4"),n("br"),n("span",{class:"line-number"},"5"),n("br"),n("span",{class:"line-number"},"6"),n("br"),n("span",{class:"line-number"},"7"),n("br"),n("span",{class:"line-number"},"8"),n("br")])],-1),w=n("h2",{id:"ubbdecode-ubb-\u8F6C\u7801-html",tabindex:"-1"},[n("code",null,"ubbDecode"),s(" ubb \u8F6C\u7801 html "),n("a",{class:"header-anchor",href:"#ubbdecode-ubb-\u8F6C\u7801-html","aria-hidden":"true"},"#")],-1),x=n("div",{class:"language-js line-numbers-mode"},[n("pre",null,[n("code",null,[n("span",{class:"token comment"},`/**
 * ubb\u8F6Chtml
 * @param {String} str
 */`),s(`
`),n("span",{class:"token keyword"},"let"),s(" resp "),n("span",{class:"token operator"},"="),s(" lib"),n("span",{class:"token punctuation"},"."),n("span",{class:"token function"},"ubbDecode"),n("span",{class:"token punctuation"},"("),n("span",{class:"token string"},"'&lt;div&gt;abc&lt;/div&gt;'"),n("span",{class:"token punctuation"},")"),n("span",{class:"token punctuation"},";"),s(`
`),n("span",{class:"token comment"},"//console.log('ubb', resp);"),s(`
`),n("span",{class:"token function"},"alert"),n("span",{class:"token punctuation"},"("),n("span",{class:"token string"},"'\u5B8C\u6210\\r\\n'"),s(),n("span",{class:"token operator"},"+"),s(" resp"),n("span",{class:"token punctuation"},")"),n("span",{class:"token punctuation"},";"),s(`
`),n("span",{class:"token keyword"},"return"),s(" resp"),n("span",{class:"token punctuation"},";"),s(`
`)])]),n("div",{class:"line-numbers-wrapper"},[n("span",{class:"line-number"},"1"),n("br"),n("span",{class:"line-number"},"2"),n("br"),n("span",{class:"line-number"},"3"),n("br"),n("span",{class:"line-number"},"4"),n("br"),n("span",{class:"line-number"},"5"),n("br"),n("span",{class:"line-number"},"6"),n("br"),n("span",{class:"line-number"},"7"),n("br"),n("span",{class:"line-number"},"8"),n("br")])],-1),q=t(`<h2 id="imgmin-\u56FE\u7247\u538B\u7F29\u52A0\u6C34\u5370" tabindex="-1"><code>imgMin</code> \u56FE\u7247\u538B\u7F29\u52A0\u6C34\u5370 <a class="header-anchor" href="#imgmin-\u56FE\u7247\u538B\u7F29\u52A0\u6C34\u5370" aria-hidden="true">#</a></h2><div class="language-js line-numbers-mode"><pre><code><span class="token comment">/**
 * \u56FE\u7247\u538B\u7F29\u7EC4\u4EF6 canvas
 * @param {*} options \u4F20\u5165\u53C2\u6570
 * {file:&#39;\u9700\u8981\u538B\u7F29\u7684\u56FE\u7247&#39;,callback:&#39;\u538B\u7F29\u540E\u56DE\u8C03(options)&#39;
 * ,type:&#39;\u7A7A\u9ED8\u8BA4\u6587\u4EF6\u7C7B\u578B&#39;,name:&#39;\u7A7A\u9ED8\u8BA4\u6587\u4EF6\u540D&#39;
 * ,maxsize:&#39;\u56FE\u7247\u9650\u5236\u5927\u5C0F&#39;
 * ,max:&#39;\u56FE\u7247\u5C3A\u5BF8\u7F29\u653E\u6BD4\u4F8B\u9ED8\u8BA41&#39;
 * ,scale:&#39;\u56FE\u7247\u6E05\u6670\u5EA6\u538B\u7F29\u6BD4\u4F8B\uFF0C\u9ED8\u8BA41&#39;
 * ,marktext:&#39;\u52A0\u6C34\u5370\u6587\u5B57&#39;
 * ,markstyle:&#39;\u6C34\u5370\u6837\u5F0F Canvas.fillStyle||rgba( 255 , 255 , 255 , 0.5 )&#39;
 * ,markfont:&#39;\u6C34\u5370\u6587\u5B57\u5927\u5C0F||bold 1rem Arial&#39;
 * ,markx:&#39;\u5DE6\u8FB9\u8DDD|| 10&#39;,
 * ,marky:&#39;\u9876\u8FB9\u8DDD|| 20&#39;
 * }
 * \u8FD4\u56DE{base64,size,width,height}
 */</span>

<span class="token keyword">let</span> _file <span class="token operator">=</span> <span class="token punctuation">{</span>
  <span class="token comment">// \u6CA1\u8D85\u8FC7max,\u53EF\u4EE5\u4E0A\u4F20</span>
  <span class="token literal-property property">isUpload</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
  <span class="token comment">// \u6587\u4EF6\u540D</span>
  <span class="token literal-property property">name</span><span class="token operator">:</span> file<span class="token punctuation">.</span>name<span class="token punctuation">,</span>
  <span class="token literal-property property">lastModified</span><span class="token operator">:</span> file<span class="token punctuation">.</span>lastModified<span class="token punctuation">,</span>
  <span class="token comment">// \u5C3A\u5BF8\u5927\u5C0F</span>
  <span class="token literal-property property">size</span><span class="token operator">:</span> file<span class="token punctuation">.</span>size<span class="token punctuation">,</span>
  <span class="token comment">// \u6587\u4EF6\u7C7B\u578B</span>
  <span class="token literal-property property">type</span><span class="token operator">:</span> file<span class="token punctuation">.</span>type<span class="token punctuation">,</span>
  <span class="token comment">// \u6587\u4EF6\u6570\u636E</span>
  <span class="token literal-property property">data</span><span class="token operator">:</span> <span class="token keyword">null</span><span class="token punctuation">,</span>
  <span class="token comment">// \u56FE\u7247\u8F6C\u6362base64</span>
  <span class="token literal-property property">src</span><span class="token operator">:</span> <span class="token keyword">null</span><span class="token punctuation">,</span>
  <span class="token comment">// \u6587\u4EF6\u63CF\u8FF0</span>
  <span class="token literal-property property">title</span><span class="token operator">:</span> <span class="token string">&#39;&#39;</span>
<span class="token punctuation">}</span><span class="token punctuation">;</span>

lib<span class="token punctuation">.</span><span class="token function">imgMin</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
  <span class="token comment">// \u6587\u4EF6\u8BFB\u53D6</span>
  <span class="token literal-property property">reader</span><span class="token operator">:</span> <span class="token keyword">new</span> <span class="token class-name">FileReader</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">,</span>
  <span class="token comment">// \u6587\u4EF6</span>
  file<span class="token punctuation">,</span>
  <span class="token comment">// \u6700\u5927\u957F\u5EA6</span>
  <span class="token literal-property property">maxsize</span><span class="token operator">:</span> props<span class="token punctuation">.</span>max <span class="token operator">*</span> <span class="token number">1024</span><span class="token punctuation">,</span>
  <span class="token comment">// \u56FE\u7247\u7684\u8D28\u91CF\u7B49\u7EA7</span>
  <span class="token literal-property property">ratio</span><span class="token operator">:</span> <span class="token number">0.6</span><span class="token punctuation">,</span>
  <span class="token comment">// \u56FE\u7247\u5C3A\u5BF8\u7F29\u653E\u6BD4\u4F8B</span>
  <span class="token literal-property property">scale</span><span class="token operator">:</span> <span class="token number">0.7</span><span class="token punctuation">,</span>
  <span class="token comment">// \u56DE\u8C03\u51FD\u6570</span>
  <span class="token function">callback</span><span class="token punctuation">(</span><span class="token parameter">data</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    _file<span class="token punctuation">.</span>title <span class="token operator">=</span> <span class="token string">&#39;\u538B\u7F29\uFF1A&#39;</span> <span class="token operator">+</span> <span class="token punctuation">(</span>data<span class="token punctuation">.</span>size <span class="token operator">/</span> <span class="token punctuation">(</span><span class="token number">1024</span> <span class="token operator">*</span> <span class="token number">1024</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">toFixed</span><span class="token punctuation">(</span><span class="token number">2</span><span class="token punctuation">)</span> <span class="token operator">+</span> <span class="token string">&#39;M&#39;</span><span class="token punctuation">;</span>

    <span class="token keyword">if</span> <span class="token punctuation">(</span>data<span class="token punctuation">.</span>size <span class="token operator">&gt;</span> props<span class="token punctuation">.</span>max <span class="token operator">*</span> <span class="token number">1024</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
      _file<span class="token punctuation">.</span>title <span class="token operator">+=</span>
        <span class="token string">&#39;\uFF0C\u8D85\u8FC7&#39;</span> <span class="token operator">+</span>
        <span class="token punctuation">(</span><span class="token punctuation">(</span>data<span class="token punctuation">.</span>size <span class="token operator">-</span> props<span class="token punctuation">.</span>max<span class="token punctuation">)</span> <span class="token operator">/</span> <span class="token punctuation">(</span><span class="token number">1024</span> <span class="token operator">*</span> <span class="token number">1024</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">toFixed</span><span class="token punctuation">(</span><span class="token number">2</span><span class="token punctuation">)</span> <span class="token operator">+</span>
        <span class="token string">&#39;M \u9650\u5236,\u65E0\u6CD5\u4E0A\u4F20&#39;</span><span class="token punctuation">;</span>
      _file<span class="token punctuation">.</span>isUpload <span class="token operator">=</span> <span class="token boolean">false</span><span class="token punctuation">;</span>
    <span class="token punctuation">}</span>

    <span class="token comment">// \u4E0A\u4F20\u6587\u4EF6\u5BF9\u8C61</span>
    _file<span class="token punctuation">.</span>src <span class="token operator">=</span> data<span class="token punctuation">.</span>base64<span class="token punctuation">;</span>
    _file<span class="token punctuation">.</span>size <span class="token operator">=</span> data<span class="token punctuation">.</span>size<span class="token punctuation">;</span>

    <span class="token keyword">if</span> <span class="token punctuation">(</span>_file<span class="token punctuation">.</span>isUpload<span class="token punctuation">)</span> <span class="token punctuation">{</span>
      <span class="token comment">// \u4E0A\u4F20\u89E6\u53D1\u4E8B\u4EF6</span>
    <span class="token punctuation">}</span>
  <span class="token punctuation">}</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br><span class="line-number">52</span><br><span class="line-number">53</span><br><span class="line-number">54</span><br><span class="line-number">55</span><br><span class="line-number">56</span><br><span class="line-number">57</span><br><span class="line-number">58</span><br><span class="line-number">59</span><br><span class="line-number">60</span><br><span class="line-number">61</span><br><span class="line-number">62</span><br><span class="line-number">63</span><br><span class="line-number">64</span><br><span class="line-number">65</span><br><span class="line-number">66</span><br><span class="line-number">67</span><br></div></div>`,2);function T(S,z,E,C,M,V){const a=r("CodeRun");return o(),c("div",null,[i,e(a,{dll:"lib",editable:""},{default:p(()=>[b]),_:1}),k,m,d,e(a,{dll:"lib",test:"",editable:""},{default:p(()=>[_]),_:1}),h,f,e(a,{dll:"lib",test:"",editable:""},{default:p(()=>[y]),_:1}),g,e(a,{dll:"lib",ubb:"",editable:""},{default:p(()=>[v]),_:1}),w,e(a,{dll:"lib",ubb:"",editable:""},{default:p(()=>[x]),_:1}),q])}var D=l(u,[["render",T]]);export{A as __pageData,D as default};
