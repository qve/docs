import{bm as n,aS as s,ap as a,at as p}from"./plugin-vue_export-helper.ec0df64b.js";const d='{"title":"Quick-Vue-Engineering","description":"","frontmatter":{},"headers":[{"level":2,"title":"QVE \u7EC4\u4EF6","slug":"qve-\u7EC4\u4EF6"},{"level":2,"title":"QVE Api","slug":"qve-api"},{"level":2,"title":"$plus \u7EC4\u4EF6\u5F15\u5165","slug":"plus-\u7EC4\u4EF6\u5F15\u5165"},{"level":2,"title":"views \u8DEF\u7531\u9875\u9762","slug":"views-\u8DEF\u7531\u9875\u9762"}],"relativePath":"qve/index.md","lastUpdated":1649205920474}',e={},t=p(`<h1 id="quick-vue-engineering" tabindex="-1">Quick-Vue-Engineering <a class="header-anchor" href="#quick-vue-engineering" aria-hidden="true">#</a></h1><blockquote><p>Quick Engineering UI for Vue 3.x</p></blockquote><p>\u4E00\u4E2A\u6781\u7B80\u7A33\u5065\u7684\u4E2D\u540E\u53F0\u9879\u76EE\u5F00\u53D1\u6846\u67B6</p><ul><li><a href="https://www.npmjs.com/package/qve" target="_blank" rel="noopener noreferrer">qve \u53D1\u5E03\u5305\u66F4\u65B0\u8BF4\u660E</a></li><li><a href="https://gitee.com/qve/qve-admin-template" target="_blank" rel="noopener noreferrer">\u5F00\u7BB1\u5373\u7528\u6A21\u677F qve-admin-template</a></li></ul><h2 id="qve-\u7EC4\u4EF6" tabindex="-1">QVE \u7EC4\u4EF6 <a class="header-anchor" href="#qve-\u7EC4\u4EF6" aria-hidden="true">#</a></h2><ul><li><a href="./panelListor.html">PanelListor</a></li><li><a href="./listor.html">listor</a></li><li><a href="./editor.html">Editor</a></li><li><a href="./control.html">Control</a></li><li><a href="./asyncModel.html">AsyncModel</a></li><li><a href="./codeEditor.html">CodeEditor</a></li></ul><h2 id="qve-api" tabindex="-1">QVE Api <a class="header-anchor" href="#qve-api" aria-hidden="true">#</a></h2><ul><li>clearCacheTpl</li><li>codeUnpackTpl</li><li>config</li><li>default</li><li>deleteCacheState</li><li>getCacheState</li><li>getCacheTpl</li><li>getSession</li><li>http</li><li>loadCacheTplByHttp</li><li>removeCacheState</li><li>removeCacheTpl</li><li>removeSession</li><li>setCacheState</li><li>setCacheTpl</li><li>setSession</li></ul><h2 id="plus-\u7EC4\u4EF6\u5F15\u5165" tabindex="-1">$plus \u7EC4\u4EF6\u5F15\u5165 <a class="header-anchor" href="#plus-\u7EC4\u4EF6\u5F15\u5165" aria-hidden="true">#</a></h2><ul><li>\u5168\u5C40\u9ED8\u8BA4\u53D8\u91CF\uFF1Awindow.$plus</li><li>\u6302\u8F7D\u65B9\u6CD5\uFF1Acomponents/index.js</li></ul><div class="language-js line-numbers-mode"><pre><code><span class="token comment">/** \u5F15\u5165\u57FA\u7840\u5E93 */</span>
<span class="token keyword">import</span> <span class="token operator">*</span> <span class="token keyword">as</span> quick <span class="token keyword">from</span> <span class="token string">&#39;quick.lib&#39;</span><span class="token punctuation">;</span>

<span class="token comment">/** \u5F15\u5165Ui\u7EC4\u4EF6\u5E93 */</span>
<span class="token keyword">import</span> <span class="token string">&#39;qveui/dist/fonts/iconfont.css&#39;</span><span class="token punctuation">;</span>
<span class="token keyword">import</span> <span class="token string">&#39;qveui/dist/styles/index.less&#39;</span><span class="token punctuation">;</span>
<span class="token keyword">import</span> <span class="token operator">*</span> <span class="token keyword">as</span> ui <span class="token keyword">from</span> <span class="token string">&#39;qveui&#39;</span><span class="token punctuation">;</span>

<span class="token comment">/** \u5F15\u5165\u9879\u76EE\u7EC4\u4EF6\u5E93 */</span>
<span class="token keyword">import</span> qve <span class="token keyword">from</span> <span class="token string">&#39;qve&#39;</span><span class="token punctuation">;</span>
<span class="token keyword">import</span> <span class="token operator">*</span> <span class="token keyword">as</span> $qve <span class="token keyword">from</span> <span class="token string">&#39;qve&#39;</span><span class="token punctuation">;</span>

<span class="token comment">/** vue\u539F\u751F\u65B9\u6CD5 */</span>
<span class="token keyword">import</span> <span class="token operator">*</span> <span class="token keyword">as</span> $vue <span class="token keyword">from</span> <span class="token string">&#39;./vue.api&#39;</span><span class="token punctuation">;</span>

<span class="token comment">// \u5168\u5C40\u72B6\u6001\u5F15\u5165</span>
<span class="token keyword">import</span> <span class="token operator">*</span> <span class="token keyword">as</span> $store <span class="token keyword">from</span> <span class="token string">&#39;../store&#39;</span><span class="token punctuation">;</span>

<span class="token comment">// \u5F15\u5165\u7F51\u7EDC\u8BF7\u6C42</span>
<span class="token keyword">import</span> <span class="token punctuation">{</span> request<span class="token punctuation">,</span> fail <span class="token punctuation">}</span> <span class="token keyword">from</span> <span class="token string">&#39;./utils/request.js&#39;</span><span class="token punctuation">;</span>

<span class="token comment">// \u8BF7\u6C42\u5730\u5740\u914D\u7F6E</span>
<span class="token keyword">import</span> <span class="token punctuation">{</span> host<span class="token punctuation">,</span> url <span class="token punctuation">}</span> <span class="token keyword">from</span> <span class="token string">&#39;../router/url.js&#39;</span><span class="token punctuation">;</span>

<span class="token comment">// \u672C\u5730\u9875\u9762\u96C6\u5408</span>
<span class="token keyword">import</span> <span class="token operator">*</span> <span class="token keyword">as</span> $Views <span class="token keyword">from</span> <span class="token string">&#39;../router/views&#39;</span><span class="token punctuation">;</span>

<span class="token comment">// \u5168\u5C40\u8DEF\u5F84\u65B9\u6CD5</span>
<span class="token keyword">import</span> <span class="token operator">*</span> <span class="token keyword">as</span> $router <span class="token keyword">from</span> <span class="token string">&#39;../router/method.js&#39;</span><span class="token punctuation">;</span>

<span class="token comment">/**
 * vue \u7EC4\u4EF6\u6302\u8F7D
 * @param {*} app
 * @param {*} opts
 */</span>
<span class="token keyword">const</span> <span class="token function-variable function">install</span> <span class="token operator">=</span> <span class="token keyword">function</span> <span class="token punctuation">(</span><span class="token parameter">app<span class="token punctuation">,</span> opts</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
  <span class="token comment">//  console.log(&#39;install&#39;);</span>
  <span class="token comment">// \u5224\u65AD\u662F\u5426\u5B89\u88C5</span>
  <span class="token keyword">if</span> <span class="token punctuation">(</span>install<span class="token punctuation">.</span>installed<span class="token punctuation">)</span> <span class="token keyword">return</span><span class="token punctuation">;</span>

  opts <span class="token operator">=</span> opts <span class="token operator">||</span> <span class="token punctuation">{</span>
    <span class="token comment">//\u8F93\u51FA\u65E5\u5FD7</span>
    <span class="token literal-property property">log</span><span class="token operator">:</span> <span class="token punctuation">{</span> <span class="token literal-property property">isPrint</span><span class="token operator">:</span> <span class="token boolean">true</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token comment">// \u662F\u5426\u7ED1\u5B9A\u5916\u6302 window</span>
    <span class="token literal-property property">plus</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token comment">// \u521D\u59CB\u5316 quick.lib \u539F\u751FJS\u51FD\u6570\u6269\u5C55</span>
    <span class="token literal-property property">init</span><span class="token operator">:</span> <span class="token boolean">true</span>
  <span class="token punctuation">}</span><span class="token punctuation">;</span>

  <span class="token comment">// \u5F53\u524D\u9879\u76EE\u5916\u90E8\u914D\u7F6E</span>
  <span class="token keyword">if</span> <span class="token punctuation">(</span>opts<span class="token punctuation">.</span>config<span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token comment">//console.log(&#39;install.host&#39;, opts.config.host);</span>
    url<span class="token punctuation">.</span><span class="token function">setHost</span><span class="token punctuation">(</span>opts<span class="token punctuation">.</span>config<span class="token punctuation">.</span>host<span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span>

  <span class="token comment">// \u6CE8\u518C\u7EC4\u4EF6</span>
  Object<span class="token punctuation">.</span><span class="token function">keys</span><span class="token punctuation">(</span>components<span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">forEach</span><span class="token punctuation">(</span><span class="token punctuation">(</span><span class="token parameter">key</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
    app<span class="token punctuation">.</span><span class="token function">component</span><span class="token punctuation">(</span>key<span class="token punctuation">,</span> components<span class="token punctuation">[</span>key<span class="token punctuation">]</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

  <span class="token comment">// \u4F7F\u7528install\u65B9\u6CD5\u5F15\u5165\u7EC4\u4EF6\u5E93</span>
  app<span class="token punctuation">.</span><span class="token function">use</span><span class="token punctuation">(</span>ui<span class="token punctuation">.</span>install<span class="token punctuation">,</span> opts<span class="token punctuation">)</span><span class="token punctuation">;</span>

  <span class="token comment">// 2\u79CD\u65B9\u6CD5\u90FD\u53EF\u4EE5\u5F15\u5165\u9879\u76EE\u5E93</span>
  app<span class="token punctuation">.</span><span class="token function">use</span><span class="token punctuation">(</span>qve<span class="token punctuation">,</span> <span class="token punctuation">{</span>
    <span class="token operator">...</span>opts<span class="token punctuation">,</span>
    <span class="token literal-property property">config</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token literal-property property">app</span><span class="token operator">:</span> _configApp<span class="token punctuation">,</span>
      <span class="token literal-property property">http</span><span class="token operator">:</span> <span class="token punctuation">{</span>
        <span class="token literal-property property">setting</span><span class="token operator">:</span> <span class="token punctuation">{</span>
          <span class="token comment">// \u8DE8\u57DF\u65F6\u662F\u5426\u53D1\u9001cookie</span>
          <span class="token literal-property property">withCredentials</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
          <span class="token comment">// \u670D\u52A1\u5668\u5730\u5740</span>
          <span class="token literal-property property">baseURL</span><span class="token operator">:</span> host<span class="token punctuation">.</span>base
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token comment">// \u8BF7\u6C42\u5E93</span>
        request<span class="token punctuation">,</span>
        <span class="token comment">// \u8BF7\u6C42\u5931\u8D25\u5904\u7406</span>
        fail
      <span class="token punctuation">}</span><span class="token punctuation">,</span>
      <span class="token comment">// \u5916\u6302\u811A\u672C\u5730\u5740</span>
      <span class="token literal-property property">script</span><span class="token operator">:</span> <span class="token punctuation">{</span>
        <span class="token comment">// \u5728\u7EBF\u4EE3\u7801\u7F16\u8F91\u5668\u8DEF\u5F84,initMonacoEditorScript \u81EA\u52A8\u52A0\u8F7D</span>
        <span class="token literal-property property">monacoEditorPath</span><span class="token operator">:</span> <span class="token string">&#39;/static/plus/monaco/min/vs&#39;</span><span class="token punctuation">,</span>
        <span class="token literal-property property">lessUrl</span><span class="token operator">:</span> <span class="token string">&#39;/static/plus/less/less.min.js&#39;</span>
      <span class="token punctuation">}</span>
    <span class="token punctuation">}</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

  <span class="token comment">// \u5168\u5C40\u6302\u8F7D</span>
  <span class="token keyword">let</span> $plus <span class="token operator">=</span> <span class="token punctuation">{</span>
    <span class="token literal-property property">vue</span><span class="token operator">:</span> $vue<span class="token punctuation">,</span>
    <span class="token comment">// \u81EA\u5B9A\u4E49\u6302\u8F7D</span>
    quick<span class="token punctuation">,</span>
    url<span class="token punctuation">,</span>
    <span class="token literal-property property">store</span><span class="token operator">:</span> $store<span class="token punctuation">,</span>
    <span class="token literal-property property">router</span><span class="token operator">:</span> $router<span class="token punctuation">,</span>
    ui<span class="token punctuation">,</span>
    <span class="token literal-property property">loading</span><span class="token operator">:</span> ui<span class="token punctuation">.</span>loading<span class="token punctuation">,</span>
    <span class="token literal-property property">confirm</span><span class="token operator">:</span> ui<span class="token punctuation">.</span>confirm<span class="token punctuation">,</span> <span class="token comment">//\u5BF9\u8BDD</span>
    <span class="token literal-property property">message</span><span class="token operator">:</span> ui<span class="token punctuation">.</span>message<span class="token punctuation">,</span> <span class="token comment">//\u6D88\u606F</span>
    <span class="token literal-property property">frame</span><span class="token operator">:</span> ui<span class="token punctuation">.</span>frame<span class="token punctuation">,</span> <span class="token comment">//\u5F39\u7A97</span>
    <span class="token literal-property property">qve</span><span class="token operator">:</span> $qve<span class="token punctuation">,</span>
    <span class="token literal-property property">http</span><span class="token operator">:</span> $qve<span class="token punctuation">.</span>http<span class="token punctuation">,</span>
    <span class="token literal-property property">config</span><span class="token operator">:</span> $qve<span class="token punctuation">.</span>config<span class="token punctuation">,</span>
    <span class="token literal-property property">views</span><span class="token operator">:</span> $Views <span class="token comment">//\u9875\u9762\u8C03\u7528</span>
  <span class="token punctuation">}</span><span class="token punctuation">;</span>

  <span class="token comment">// \u65E5\u5FD7\u8F93\u51FA</span>
  <span class="token keyword">let</span> _resp <span class="token operator">=</span> <span class="token template-string"><span class="token template-punctuation string">\`</span><span class="token string">version:</span><span class="token interpolation"><span class="token interpolation-punctuation punctuation">\${</span>$plus<span class="token punctuation">.</span>ui<span class="token punctuation">.</span>version<span class="token interpolation-punctuation punctuation">}</span></span><span class="token string">,lib:</span><span class="token template-punctuation string">\`</span></span> <span class="token operator">+</span> quick<span class="token punctuation">.</span>version<span class="token punctuation">;</span>

  <span class="token keyword">if</span> <span class="token punctuation">(</span><span class="token keyword">typeof</span> window <span class="token operator">!==</span> <span class="token string">&#39;undefined&#39;</span> <span class="token operator">&amp;&amp;</span> opts<span class="token punctuation">.</span>plus<span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token keyword">if</span> <span class="token punctuation">(</span>window<span class="token punctuation">.</span>$plus<span class="token punctuation">)</span> <span class="token punctuation">{</span>
      <span class="token comment">// \u6DFB\u52A0\u5176\u5B83\u81EA\u5B9A\u4E49\u5916\u6302</span>
      <span class="token keyword">for</span> <span class="token punctuation">(</span><span class="token keyword">let</span> key <span class="token keyword">in</span> window<span class="token punctuation">.</span>$plus<span class="token punctuation">)</span> <span class="token punctuation">{</span>
        $plus<span class="token punctuation">[</span>key<span class="token punctuation">]</span> <span class="token operator">=</span> window<span class="token punctuation">.</span>$plus<span class="token punctuation">[</span>key<span class="token punctuation">]</span><span class="token punctuation">;</span>
      <span class="token punctuation">}</span>
    <span class="token punctuation">}</span>

    <span class="token comment">// \u5168\u5C40\u539F\u751F\u6302\u8F7D</span>
    window<span class="token punctuation">.</span>$plus <span class="token operator">=</span> $plus<span class="token punctuation">;</span>
    _resp <span class="token operator">+=</span> <span class="token string">&#39;;window.$plus&#39;</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span> <span class="token keyword">else</span> <span class="token punctuation">{</span>
    <span class="token comment">// \u6302\u8F7D</span>
    <span class="token comment">/** \u5168\u5C40\u7ED1\u5B9A\u8C03\u7528 */</span>
    app<span class="token punctuation">.</span>config<span class="token punctuation">.</span>globalProperties<span class="token punctuation">.</span>$plus <span class="token operator">=</span> $plus<span class="token punctuation">;</span>
    _resp <span class="token operator">+=</span> <span class="token string">&#39;;ctx&#39;</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span>

  <span class="token comment">// \u8BFB\u53D6\u5F53\u524D\u5916\u6302\u7684\u7EC4\u4EF6</span>
  _resp <span class="token operator">+=</span> <span class="token string">&#39;[&#39;</span><span class="token punctuation">;</span>
  <span class="token comment">// \u663E\u793A\u5916\u6302\u7684key</span>
  <span class="token keyword">for</span> <span class="token punctuation">(</span><span class="token keyword">let</span> _key <span class="token keyword">in</span> $plus<span class="token punctuation">)</span> <span class="token punctuation">{</span>
    _resp <span class="token operator">+=</span> <span class="token string">&#39;,&#39;</span> <span class="token operator">+</span> _key<span class="token punctuation">;</span>
  <span class="token punctuation">}</span>
  _resp <span class="token operator">+=</span> <span class="token template-string"><span class="token template-punctuation string">\`</span><span class="token string">];</span><span class="token template-punctuation string">\`</span></span><span class="token punctuation">;</span>

  <span class="token comment">// \u8C03\u8BD5\u65E5\u5FD7</span>
  <span class="token keyword">if</span> <span class="token punctuation">(</span><span class="token keyword">typeof</span> opts<span class="token punctuation">.</span>log <span class="token operator">!==</span> <span class="token string">&#39;undefined&#39;</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token comment">//\u8C03\u8BD5\u65E5\u5FD7\u8F93\u51FA</span>
    console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token string">&#39;quick-vue-ui&#39;</span><span class="token punctuation">,</span> _resp<span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span>
<span class="token punctuation">}</span><span class="token punctuation">;</span>

<span class="token comment">/** vue\u539F\u751F\u65B9\u6CD5 */</span>
<span class="token keyword">export</span> <span class="token operator">*</span> <span class="token keyword">from</span> <span class="token string">&#39;./vue.api&#39;</span><span class="token punctuation">;</span>
<span class="token comment">// \u5BFC\u51FAUI\u7EC4\u4EF6\u5E93</span>
<span class="token keyword">export</span> <span class="token operator">*</span> <span class="token keyword">from</span> <span class="token string">&#39;qveui&#39;</span><span class="token punctuation">;</span>
<span class="token comment">/** \u5BFC\u51FA\u9879\u76EE\u7EC4\u4EF6\u5E93*/</span>
<span class="token keyword">export</span> <span class="token operator">*</span> <span class="token keyword">from</span> <span class="token string">&#39;qve&#39;</span><span class="token punctuation">;</span>

<span class="token comment">// \u5BFC\u51FA\u8BE5\u7EC4\u4EF6</span>
<span class="token keyword">export</span> <span class="token keyword">default</span> <span class="token punctuation">{</span>
  install
<span class="token punctuation">}</span><span class="token punctuation">;</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br><span class="line-number">52</span><br><span class="line-number">53</span><br><span class="line-number">54</span><br><span class="line-number">55</span><br><span class="line-number">56</span><br><span class="line-number">57</span><br><span class="line-number">58</span><br><span class="line-number">59</span><br><span class="line-number">60</span><br><span class="line-number">61</span><br><span class="line-number">62</span><br><span class="line-number">63</span><br><span class="line-number">64</span><br><span class="line-number">65</span><br><span class="line-number">66</span><br><span class="line-number">67</span><br><span class="line-number">68</span><br><span class="line-number">69</span><br><span class="line-number">70</span><br><span class="line-number">71</span><br><span class="line-number">72</span><br><span class="line-number">73</span><br><span class="line-number">74</span><br><span class="line-number">75</span><br><span class="line-number">76</span><br><span class="line-number">77</span><br><span class="line-number">78</span><br><span class="line-number">79</span><br><span class="line-number">80</span><br><span class="line-number">81</span><br><span class="line-number">82</span><br><span class="line-number">83</span><br><span class="line-number">84</span><br><span class="line-number">85</span><br><span class="line-number">86</span><br><span class="line-number">87</span><br><span class="line-number">88</span><br><span class="line-number">89</span><br><span class="line-number">90</span><br><span class="line-number">91</span><br><span class="line-number">92</span><br><span class="line-number">93</span><br><span class="line-number">94</span><br><span class="line-number">95</span><br><span class="line-number">96</span><br><span class="line-number">97</span><br><span class="line-number">98</span><br><span class="line-number">99</span><br><span class="line-number">100</span><br><span class="line-number">101</span><br><span class="line-number">102</span><br><span class="line-number">103</span><br><span class="line-number">104</span><br><span class="line-number">105</span><br><span class="line-number">106</span><br><span class="line-number">107</span><br><span class="line-number">108</span><br><span class="line-number">109</span><br><span class="line-number">110</span><br><span class="line-number">111</span><br><span class="line-number">112</span><br><span class="line-number">113</span><br><span class="line-number">114</span><br><span class="line-number">115</span><br><span class="line-number">116</span><br><span class="line-number">117</span><br><span class="line-number">118</span><br><span class="line-number">119</span><br><span class="line-number">120</span><br><span class="line-number">121</span><br><span class="line-number">122</span><br><span class="line-number">123</span><br><span class="line-number">124</span><br><span class="line-number">125</span><br><span class="line-number">126</span><br><span class="line-number">127</span><br><span class="line-number">128</span><br><span class="line-number">129</span><br><span class="line-number">130</span><br><span class="line-number">131</span><br><span class="line-number">132</span><br><span class="line-number">133</span><br><span class="line-number">134</span><br><span class="line-number">135</span><br><span class="line-number">136</span><br><span class="line-number">137</span><br><span class="line-number">138</span><br><span class="line-number">139</span><br><span class="line-number">140</span><br><span class="line-number">141</span><br><span class="line-number">142</span><br><span class="line-number">143</span><br><span class="line-number">144</span><br><span class="line-number">145</span><br><span class="line-number">146</span><br><span class="line-number">147</span><br><span class="line-number">148</span><br><span class="line-number">149</span><br><span class="line-number">150</span><br><span class="line-number">151</span><br><span class="line-number">152</span><br><span class="line-number">153</span><br><span class="line-number">154</span><br><span class="line-number">155</span><br></div></div><h2 id="views-\u8DEF\u7531\u9875\u9762" tabindex="-1">views \u8DEF\u7531\u9875\u9762 <a class="header-anchor" href="#views-\u8DEF\u7531\u9875\u9762" aria-hidden="true">#</a></h2><ul><li>\u9879\u76EE\u9700\u8981\u8DE8\u9875\u9762\u5F15\u5165\u8C03\u7528\u8DEF\u7531\u6CE8\u518C\u7684\u5176\u5B83\u9875\u9762</li></ul><div class="language-js line-numbers-mode"><pre><code><span class="token comment">// \u9664\u4E86\u4F7F\u7528 import DBTableFrame from &#39;\u9875\u9762\u5730\u5740&#39;</span>

<span class="token comment">// \u9879\u76EE\u53EF\u4EE5\u7528\u5916\u6302\u65B9\u5F0F\u5F15\u5165</span>

<span class="token keyword">const</span> <span class="token punctuation">{</span> DBTableFrame <span class="token punctuation">}</span> <span class="token operator">=</span> window<span class="token punctuation">.</span>$plus<span class="token punctuation">.</span>views<span class="token punctuation">;</span>

<span class="token keyword">const</span> <span class="token punctuation">{</span> frame <span class="token punctuation">}</span> <span class="token operator">=</span> window<span class="token punctuation">.</span>$plus<span class="token punctuation">;</span>

<span class="token comment">// \u65B0\u7A97\u53E3\u6253\u5F00</span>
frame<span class="token punctuation">.</span><span class="token function">open</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
  <span class="token literal-property property">title</span><span class="token operator">:</span> <span class="token string">&#39;\u6DFB\u52A0&#39;</span> <span class="token operator">+</span> title<span class="token punctuation">,</span>
  <span class="token literal-property property">model</span><span class="token operator">:</span> <span class="token punctuation">{</span>
    <span class="token literal-property property">props</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token comment">//workID: 11577</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token comment">// \u8FDB\u884C\u7EC4\u4EF6\u6807\u8BB0</span>
    <span class="token literal-property property">component</span><span class="token operator">:</span> <span class="token function">markRaw</span><span class="token punctuation">(</span>DBTableFrame<span class="token punctuation">)</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token comment">// onEvent: (_res) =&gt; {</span>
  <span class="token comment">//   console.log(&#39;win.on&#39;, _res);</span>
  <span class="token comment">// },</span>
  <span class="token function-variable function">onClose</span><span class="token operator">:</span> <span class="token punctuation">(</span><span class="token parameter">_resp</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
    console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>reloadNamed<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token keyword">if</span> <span class="token punctuation">(</span>reloadNamed<span class="token punctuation">)</span> <span class="token punctuation">{</span>
      <span class="token function">getInfoData</span><span class="token punctuation">(</span>reloadNamed<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token punctuation">}</span>
    <span class="token comment">// \u81EA\u5B9A\u4E49\u5173\u95ED\u65B9\u6CD5</span>
    _resp<span class="token punctuation">.</span><span class="token function">close</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token keyword">const</span> <span class="token function-variable function">open</span> <span class="token operator">=</span> <span class="token punctuation">(</span><span class="token parameter">res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token keyword">const</span> <span class="token punctuation">{</span> router <span class="token punctuation">}</span> <span class="token operator">=</span> window<span class="token punctuation">.</span>$plus<span class="token punctuation">;</span>
  router<span class="token punctuation">.</span><span class="token function">push</span><span class="token punctuation">(</span>
    <span class="token string">&#39;/frame/orm/dbTable?db=&#39;</span> <span class="token operator">+</span> res<span class="token punctuation">.</span>data<span class="token punctuation">.</span>DBCode <span class="token operator">+</span> <span class="token string">&#39;&amp;table=&#39;</span> <span class="token operator">+</span> res<span class="token punctuation">.</span>data<span class="token punctuation">.</span>TableCode
  <span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span><span class="token punctuation">;</span>

<span class="token keyword">const</span> <span class="token function-variable function">open</span> <span class="token operator">=</span> <span class="token punctuation">(</span><span class="token parameter">res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
  <span class="token keyword">const</span> <span class="token punctuation">{</span> frame <span class="token punctuation">}</span> <span class="token operator">=</span> window<span class="token punctuation">.</span>$plus<span class="token punctuation">;</span>
  <span class="token keyword">const</span> <span class="token punctuation">{</span> markRaw <span class="token punctuation">}</span> <span class="token operator">=</span> window<span class="token punctuation">.</span>$plus<span class="token punctuation">.</span>vue<span class="token punctuation">;</span>
  <span class="token keyword">const</span> <span class="token punctuation">{</span> DBTableFrame <span class="token punctuation">}</span> <span class="token operator">=</span> window<span class="token punctuation">.</span>$plus<span class="token punctuation">.</span>views<span class="token punctuation">;</span>
  frame<span class="token punctuation">.</span><span class="token function">open</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
    <span class="token literal-property property">title</span><span class="token operator">:</span> <span class="token string">&#39;\u7F16\u8F91&#39;</span><span class="token punctuation">,</span>
    <span class="token literal-property property">model</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token literal-property property">props</span><span class="token operator">:</span> <span class="token punctuation">{</span><span class="token punctuation">}</span><span class="token punctuation">,</span>
      <span class="token function-variable function">component</span><span class="token operator">:</span> <span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
        <span class="token keyword">return</span> <span class="token function">markRaw</span><span class="token punctuation">(</span>DBTableFrame<span class="token punctuation">)</span><span class="token punctuation">;</span>
      <span class="token punctuation">}</span>
    <span class="token punctuation">}</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span><span class="token punctuation">;</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br><span class="line-number">52</span><br></div></div>`,14),o=[t];function l(c,r,u,i,k,b){return s(),a("div",null,o)}var y=n(e,[["render",l]]);export{d as __pageData,y as default};
