import{bm as n,aS as s,ap as a,at as p}from"./plugin-vue_export-helper.ec0df64b.js";const g='{"title":"QuickEnum","description":"","frontmatter":{},"headers":[{"level":2,"title":"QuickEnum \u679A\u4E3E","slug":"quickenum-\u679A\u4E3E"},{"level":2,"title":"userAgent \u652F\u4ED8\u5B9D,\u5FAE\u4FE1,qq","slug":"useragent-\u652F\u4ED8\u5B9D-\u5FAE\u4FE1-qq"},{"level":2,"title":"Result \u679A\u4E3E\u8FD4\u56DE","slug":"result-\u679A\u4E3E\u8FD4\u56DE"},{"level":2,"title":"GetDescription \u679A\u4E3E\u63CF\u8FF0","slug":"getdescription-\u679A\u4E3E\u63CF\u8FF0"}],"relativePath":"quick/5.0/code/enum.md","lastUpdated":1622976166035}',e={},t=p(`<h1 id="quickenum" tabindex="-1">QuickEnum <a class="header-anchor" href="#quickenum" aria-hidden="true">#</a></h1><ul><li>enums \u679A\u4E3E\u662F\u503C\u7C7B\u578B\uFF0C\u6570\u636E\u76F4\u63A5\u5B58\u50A8\u5728\u6808\u4E2D\uFF0C\u800C\u4E0D\u662F\u4F7F\u7528\u5F15\u7528\u548C\u771F\u5B9E\u6570\u636E\u7684\u9694\u79BB\u65B9\u5F0F\u6765\u5B58\u50A8</li><li>\u9ED8\u8BA4\u60C5\u51B5\u4E0B\uFF0C\u679A\u4E3E\u4E2D\u7684\u7B2C\u4E00\u4E2A\u53D8\u91CF\u88AB\u8D4B\u503C\u4E3A 0\uFF0C\u5176\u4ED6\u7684\u53D8\u91CF\u7684\u503C\u6309\u5B9A\u4E49\u7684\u987A\u5E8F\u6765\u9012\u589E(0,12,3...)</li><li>enum \u679A\u4E3E\u7C7B\u578B\u7684\u53D8\u91CF\u7684\u540D\u79F0\u4E0D\u80FD\u76F8\u540C\uFF0C\u4F46\u662F\u503C\u53EF\u4EE5\u76F8\u540C</li></ul><h2 id="quickenum-\u679A\u4E3E" tabindex="-1">QuickEnum \u679A\u4E3E <a class="header-anchor" href="#quickenum-\u679A\u4E3E" aria-hidden="true">#</a></h2><ul><li><p>\u679A\u4E3E\u53EF\u4EE5\u4F7F\u4EE3\u7801\u66F4\u6613\u4E8E\u7EF4\u62A4\uFF0C\u6709\u52A9\u4E8E\u786E\u4FDD\u7ED9\u53D8\u91CF\u6307\u5B9A\u5408\u6CD5\u7684\u3001\u671F\u671B\u7684\u503C\u3002</p></li><li><p>\u679A\u4E3E\u4F7F\u4EE3\u7801\u66F4\u6E05\u6670\uFF0C\u5141\u8BB8\u7528\u63CF\u8FF0\u6027\u7684\u540D\u79F0\u8868\u793A\u6574\u6570\u503C\uFF0C\u800C\u4E0D\u662F\u7528\u542B\u4E49\u6A21\u7CCA\u7684\u6570\u6765\u8868\u793A\u3002</p></li><li><p>\u679A\u4E3E\u4F7F\u4EE3\u7801\u66F4\u6613\u4E8E\u952E\u5165\u3002\u5728\u7ED9\u679A\u4E3E\u7C7B\u578B\u7684\u5B9E\u4F8B\u8D4B\u503C\u65F6\uFF0C<a href="http://VS.NET" target="_blank" rel="noopener noreferrer">VS.NET</a> IDE \u4F1A\u901A\u8FC7 IntelliSense \u5F39\u51FA\u4E00\u4E2A\u5305\u542B\u53EF\u63A5\u53D7\u503C\u7684\u5217\u8868\u6846\uFF0C\u51CF\u5C11\u4E86\u6309\u952E\u6B21\u6570\uFF0C\u5E76\u80FD\u591F\u8BA9\u6211\u4EEC\u56DE\u5FC6\u8D77\u53EF\u80FD\u7684\u503C</p></li></ul><div class="language-csharp line-numbers-mode"><pre><code>        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u65E5\u5FD7\u5904\u7406</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token keyword">enum</span> <span class="token class-name">Log</span>
        <span class="token punctuation">{</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u65E5\u5FD7\u5199\u5165\u9519\u8BEF</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u65E5\u5FD7\u5199\u5165\u9519\u8BEF&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            errorWrite <span class="token operator">=</span> <span class="token number">900</span>
        <span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br></div></div><h2 id="useragent-\u652F\u4ED8\u5B9D-\u5FAE\u4FE1-qq" tabindex="-1">userAgent \u652F\u4ED8\u5B9D,\u5FAE\u4FE1,qq <a class="header-anchor" href="#useragent-\u652F\u4ED8\u5B9D-\u5FAE\u4FE1-qq" aria-hidden="true">#</a></h2><div class="language-csharp line-numbers-mode"><pre><code>  <span class="token class-name"><span class="token keyword">string</span></span> agent <span class="token operator">=</span> Request<span class="token punctuation">.</span>Headers<span class="token punctuation">[</span><span class="token string">&quot;User-Agent&quot;</span><span class="token punctuation">]</span><span class="token punctuation">;</span>
  <span class="token keyword">if</span> <span class="token punctuation">(</span>agent<span class="token punctuation">.</span><span class="token function">ToLower</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">Contains</span><span class="token punctuation">(</span><span class="token string">&quot;micromessenger&quot;</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">{</span>
    <span class="token comment">// \u662F\u5FAE\u4FE1</span>
  <span class="token punctuation">}</span>

  <span class="token class-name">QuickEnum<span class="token punctuation">.</span>UserAgentApp</span> uaa <span class="token operator">=</span>QuickEnum<span class="token punctuation">.</span><span class="token generic-method"><span class="token function">GetBy</span><span class="token generic class-name"><span class="token punctuation">&lt;</span>QuickEnum<span class="token punctuation">.</span>UserAgentApp<span class="token punctuation">&gt;</span></span></span><span class="token punctuation">(</span>agent<span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token comment">//  uaa= QuickBLL.CheckUserAgentApp(agent);</span>
  <span class="token keyword">switch</span> <span class="token punctuation">(</span>uaa<span class="token punctuation">)</span>
  <span class="token punctuation">{</span>
    <span class="token keyword">case</span> QuickEnum<span class="token punctuation">.</span>UserAgentApp<span class="token punctuation">.</span>notFound<span class="token punctuation">:</span>
       <span class="token comment">//\u4E0D\u662F \u5FAE\u4FE1\uFF0Cqq\uFF0C\u652F\u4ED8\u5B9D\uFF0C\u5224\u65AD\u5176\u5B83\u6D4F\u89C8\u5668</span>
    <span class="token keyword">break</span><span class="token punctuation">;</span>
    <span class="token keyword">default</span><span class="token punctuation">:</span>
    <span class="token comment">// \u9ED8\u8BA4</span>
  <span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br></div></div><h2 id="result-\u679A\u4E3E\u8FD4\u56DE" tabindex="-1">Result \u679A\u4E3E\u8FD4\u56DE <a class="header-anchor" href="#result-\u679A\u4E3E\u8FD4\u56DE" aria-hidden="true">#</a></h2><div class="language-csharp line-numbers-mode"><pre><code><span class="token keyword">try</span><span class="token punctuation">{</span>

<span class="token punctuation">}</span><span class="token keyword">catch</span> <span class="token punctuation">(</span><span class="token class-name">Exception</span> ex<span class="token punctuation">)</span>
<span class="token punctuation">{</span>
  <span class="token class-name"><span class="token keyword">string</span></span> _errMsg <span class="token operator">=</span> QuickEnum<span class="token punctuation">.</span><span class="token function">Result</span><span class="token punctuation">(</span>QuickEnum<span class="token punctuation">.</span>Log<span class="token punctuation">.</span>\u65E5\u5FD7\u5199\u5165\u9519\u8BEF<span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token comment">// &quot;code_900\uFF1A\u65E5\u5FD7\u5199\u5165\u9519\u8BEF&quot;</span>

  _errMsg <span class="token operator">+=</span> <span class="token string">&quot;;&quot;</span> <span class="token operator">+</span> ex<span class="token punctuation">.</span>Message<span class="token punctuation">;</span>
  <span class="token comment">//\u5C06\u9519\u8BEF\u629B\u5411\u4E0A\u4E00\u5C42</span>
  <span class="token keyword">throw</span> <span class="token keyword">new</span> <span class="token constructor-invocation class-name">Exception</span><span class="token punctuation">(</span>_errMsg<span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br></div></div><h2 id="getdescription-\u679A\u4E3E\u63CF\u8FF0" tabindex="-1">GetDescription \u679A\u4E3E\u63CF\u8FF0 <a class="header-anchor" href="#getdescription-\u679A\u4E3E\u63CF\u8FF0" aria-hidden="true">#</a></h2><p>\u83B7\u53D6\u5230\u5BF9\u5E94\u679A\u4E3E\u7684\u63CF\u8FF0-\u6CA1\u6709\u63CF\u8FF0\u4FE1\u606F\uFF0C\u8FD4\u56DE\u679A\u4E3E\u503C</p><div class="language-csharp line-numbers-mode"><pre><code>
<span class="token class-name"><span class="token keyword">string</span></span> _title<span class="token operator">=</span>QuickEnum<span class="token punctuation">.</span><span class="token function">GetDescription</span><span class="token punctuation">(</span><span class="token punctuation">(</span>DBStateFlagEnum<span class="token punctuation">)</span>SF_ID<span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token comment">//\u65E5\u5FD7\u5199\u5165\u9519\u8BEF</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br></div></div><div class="language-csharp line-numbers-mode"><pre><code>    <span class="token comment">/// &lt;summary&gt;</span>
    <span class="token comment">/// \u5E38\u7528\u679A\u4E3E\u5BF9\u8C61\u5B9A\u4E49</span>
    <span class="token comment">/// &lt;/summary&gt;</span>
    <span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">QuickEnumModel</span>
    <span class="token punctuation">{</span>

        <span class="token preprocessor property">#<span class="token directive keyword">region</span> \u7528\u6237</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u7F51\u7EDC\u4F1A\u5458\u72B6\u6001</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token keyword">enum</span> <span class="token class-name">User</span>
        <span class="token punctuation">{</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u672A\u6FC0\u6D3B</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u672A\u6FC0\u6D3B&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            activeNot <span class="token operator">=</span> <span class="token number">1</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u6D3B\u52A8</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u6D3B\u52A8&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            active <span class="token operator">=</span> <span class="token number">2</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u5173\u95ED</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u5173\u95ED&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            close <span class="token operator">=</span> <span class="token number">3</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u5DF2\u6CE8\u9500</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u5DF2\u6CE8\u9500&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            deregistration <span class="token operator">=</span> <span class="token number">4</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u6302\u5931</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u6302\u5931&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            lose <span class="token operator">=</span> <span class="token number">5</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u5DF2\u8FC7\u671F</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u5DF2\u8FC7\u671F&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            overdue <span class="token operator">=</span> <span class="token number">6</span><span class="token punctuation">,</span>
        <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u9A8C\u8BC1</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token keyword">enum</span> <span class="token class-name">Auth</span>
        <span class="token punctuation">{</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u5E94\u7528\u4E0D\u5B58\u5728</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u5E94\u7528\u4E0D\u5B58\u5728&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            NotFoundApp <span class="token operator">=</span> <span class="token number">300</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u5E94\u7528\u672A\u6388\u6743</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u5E94\u7528\u672A\u6388\u6743&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            NotAuthorizedApp <span class="token operator">=</span> <span class="token number">303</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u767B\u9646\u4FE1\u606F\u9A8C\u8BC1\u5931\u8D25</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u767B\u9646\u9A8C\u8BC1\u9519\u8BEF&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            LoginInfoFail <span class="token operator">=</span> <span class="token number">304</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u767B\u9646\u9A8C\u8BC1\u5931\u8D25</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u767B\u9646\u5931\u8D25&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            LoginFail <span class="token operator">=</span> <span class="token number">305</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u9A8C\u8BC1\u6B21\u6570\u8D85\u9650\uFF0C30\u5206\u949F</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u8BF730\u5206\u949F\u540E\u91CD\u8BD5&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            Overrun <span class="token operator">=</span> <span class="token number">306</span><span class="token punctuation">,</span>



            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// App\u4EE4\u724C\u914D\u7F6E\u4E0D\u5B58\u5728</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;App\u4EE4\u724C\u914D\u7F6E\u4E0D\u5B58\u5728&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            TokenConfigNot <span class="token operator">=</span> <span class="token number">307</span><span class="token punctuation">,</span>


            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u8BF7\u8F93\u5165\u6B63\u786E\u624B\u673A\u53F7</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u8BF7\u8F93\u5165\u6B63\u786E\u624B\u673A\u53F7&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            PhoneFail <span class="token operator">=</span> <span class="token number">320</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u8BF7\u8F93\u5165\u6B63\u786E\u77ED\u4FE1\u9A8C\u8BC1\u7801</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u8BF7\u8F93\u5165\u77ED\u4FE1\u9A8C\u8BC1\u7801&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            PhoneSMSEmpty <span class="token operator">=</span> <span class="token number">321</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u8BF7\u8F93\u5165\u6B63\u786E\u77ED\u4FE1\u9A8C\u8BC1\u7801</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u8BF7\u8F93\u5165\u6B63\u786E\u77ED\u4FE1\u9A8C\u8BC1\u7801&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            PhoneSMSFail <span class="token operator">=</span> <span class="token number">322</span><span class="token punctuation">,</span>


            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u8BF7\u8F93\u5165\u767B\u5F55\u5BC6\u7801</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u8BF7\u8F93\u5165\u767B\u5F55\u5BC6\u7801&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            PassWordSMSEmpty <span class="token operator">=</span> <span class="token number">323</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u624B\u673A\u53F7\u5DF2\u6CE8\u518C</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u624B\u673A\u53F7\u5DF2\u6CE8\u518C&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            PhoneIsJoin <span class="token operator">=</span> <span class="token number">324</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u9700\u7BA1\u7406\u5458\u6388\u6743</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u9700\u7BA1\u7406\u5458\u6388\u6743&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            AppLoginJoinTypeFail <span class="token operator">=</span> <span class="token number">325</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u6CA1\u6709\u4F20\u5165base 64 \u9A8C\u8BC1\u53C2\u6570</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u4EE4\u724C\u4E3A\u7A7A&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            TokenEmpty <span class="token operator">=</span> <span class="token number">330</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// basic \u5F00\u5934</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u4EE4\u724C\u683C\u5F0F\u9519\u8BEF&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            TokenFormatError <span class="token operator">=</span> <span class="token number">331</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u6CA1\u6709\u6309\u89C4\u8303\u4F20\u5165AppKey</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u4EE4\u724CAppKey\u4E3A\u7A7A&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            TokenAppKeyEmpty <span class="token operator">=</span> <span class="token number">332</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u6CA1\u6709val \u503C</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u4EE4\u724CValue\u4E3A\u7A7A&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            TokenValueEmpty <span class="token operator">=</span> <span class="token number">333</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u524D\u7AEF\u7B97\u6CD5\u683C\u5F0F\u4E0D\u5BF9</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u4EE4\u724C\u53C2\u6570\u683C\u5F0F\u4E0D\u5339\u914D&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            TokenNotMatch <span class="token operator">=</span> <span class="token number">334</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// redis \u6CA1\u6709\u7F13\u5B58</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u4EE4\u724C\u7528\u6237\u4FE1\u606F\u52A0\u8F7D\u5931\u8D25&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            TokenUserEmpty <span class="token operator">=</span> <span class="token number">335</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// redis \u6CA1\u6709\u7F13\u5B58</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u8BF7\u6C42\u9A8C\u8BC1\u8DEF\u5F84\u4E3A\u7A7A&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            PathEmpty <span class="token operator">=</span> <span class="token number">350</span><span class="token punctuation">,</span>
        <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u8BF7\u6C42\u72B6\u6001</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token keyword">enum</span> <span class="token class-name">HttpStatus</span>
        <span class="token punctuation">{</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u8BF7\u6C42\u9A8C\u8BC1\u5931\u8D25</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u8BF7\u6C42\u9A8C\u8BC1\u5931\u8D25&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            AuthenticationFailed <span class="token operator">=</span> <span class="token number">401</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u7981\u6B62\u8BBF\u95EE\u670D\u52A1\u5668\u62D2\u7EDD\u8BF7\u6C42</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u7981\u6B62\u8BBF\u95EE\u670D\u52A1\u5668\u62D2\u7EDD\u8BF7\u6C42&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            RequestReject <span class="token operator">=</span> <span class="token number">403</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u670D\u52A1\u5668\u627E\u4E0D\u5230\u8BF7\u6C42\u8FDE\u63A5</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u670D\u52A1\u5668\u627E\u4E0D\u5230\u8BF7\u6C42\u8FDE\u63A5&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            PathNotFound <span class="token operator">=</span> <span class="token number">404</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u670D\u52A1\u5668\u53D1\u751F\u9519\u8BEF</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u670D\u52A1\u5668\u53D1\u751F\u9519\u8BEF&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            ApplicationError <span class="token operator">=</span> <span class="token number">500</span>
        <span class="token punctuation">}</span>

        <span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>

        <span class="token preprocessor property">#<span class="token directive keyword">region</span> \u6570\u636E\u5E93</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u6570\u636E\u5E93\u63D0\u793A</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token keyword">enum</span> <span class="token class-name">DB</span>
        <span class="token punctuation">{</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u6570\u636E\u5E93\u8FDE\u63A5\u672A\u914D\u7F6E</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u6570\u636E\u5E93\u8FDE\u63A5\u672A\u914D\u7F6E&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            connectionNotConfigured <span class="token operator">=</span> <span class="token number">800</span>
        <span class="token punctuation">}</span>


        <span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>

        <span class="token preprocessor property">#<span class="token directive keyword">region</span> Log</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u65E5\u5FD7\u5904\u7406</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token keyword">enum</span> <span class="token class-name">Log</span>
        <span class="token punctuation">{</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u65E5\u5FD7\u5199\u5165\u9519\u8BEF</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u65E5\u5FD7\u5199\u5165\u9519\u8BEF&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            errorWrite <span class="token operator">=</span> <span class="token number">900</span>
        <span class="token punctuation">}</span>
        <span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>

        <span class="token preprocessor property">#<span class="token directive keyword">region</span> io</span>
        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// io \u6D88\u606F\u7C7B\u578B</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token keyword">enum</span> <span class="token class-name">MessageType</span>
        <span class="token punctuation">{</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u660E\u6587\u6587\u5B57\u6D88\u606F</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            Text <span class="token operator">=</span> <span class="token number">1</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// json\u6D88\u606F</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            Json <span class="token operator">=</span> <span class="token number">2</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u8FDE\u63A5\u6D88\u606F</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            Line <span class="token operator">=</span> <span class="token number">3</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">///  \u670D\u52A1\u6307\u4EE4</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            cmd <span class="token operator">=</span> <span class="token number">6</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u56DE\u6267\u6D88\u606F</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            Receipt <span class="token operator">=</span> <span class="token number">100</span>
        <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">///  \u6D88\u606F\u7B49\u7EA7</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token keyword">enum</span> <span class="token class-name">MessageLevel</span>
        <span class="token punctuation">{</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u7CFB\u7EDF\u6D88\u606F</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            System <span class="token operator">=</span> <span class="token number">1</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">///\u79C1\u804A\u6D88\u606F</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            Two <span class="token operator">=</span> <span class="token number">2</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u7FA4\u804A\u6D88\u606F</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            Group <span class="token operator">=</span> <span class="token number">3</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u670D\u52A1\u63A5\u53E3\u901A\u77E5</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            Sevice <span class="token operator">=</span> <span class="token number">5</span><span class="token punctuation">,</span>
        <span class="token punctuation">}</span>


        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">///  \u6D88\u606F\u72B6\u6001</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token keyword">enum</span> <span class="token class-name">MessageStatus</span>
        <span class="token punctuation">{</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u961F\u5217\u7B49\u5F85\u53D1\u9001</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            Queue <span class="token operator">=</span> <span class="token number">1</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u53D1\u9001\u6210\u529F</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            Success <span class="token operator">=</span> <span class="token number">2</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u64A4\u9500\u6D88\u606F</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            Revoke <span class="token operator">=</span> <span class="token number">3</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u5DF2\u5220\u9664</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            Delete <span class="token operator">=</span> <span class="token number">4</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u5DF2\u8BFB</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            Read <span class="token operator">=</span> <span class="token number">5</span>
        <span class="token punctuation">}</span>
        <span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>

        <span class="token preprocessor property">#<span class="token directive keyword">region</span> \u6D4F\u89C8\u5668</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u6D4F\u89C8\u5668\u7C7B\u578B</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token keyword">enum</span> <span class="token class-name">UserAgentType</span>
        <span class="token punctuation">{</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u672A\u8BC6\u522B</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            NotFound <span class="token operator">=</span> <span class="token number">0</span><span class="token punctuation">,</span>
            Android <span class="token operator">=</span> <span class="token number">2</span><span class="token punctuation">,</span>
            Windows <span class="token operator">=</span> <span class="token number">3</span><span class="token punctuation">,</span>
            iPhone <span class="token operator">=</span> <span class="token number">4</span><span class="token punctuation">,</span>
            iPod <span class="token operator">=</span> <span class="token number">5</span><span class="token punctuation">,</span>
            iPad <span class="token operator">=</span> <span class="token number">6</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// mac</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            Macintosh <span class="token operator">=</span> <span class="token number">7</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">///  \u7F51\u5173\u4F20\u5165\u5B89\u5353</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            AOS <span class="token operator">=</span> <span class="token number">8</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">///  \u7F51\u5173\u4F20\u5165 \u82F9\u679C</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            iOS <span class="token operator">=</span> <span class="token number">9</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">///  \u7F51\u5173\u4F20\u5165</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            Win <span class="token operator">=</span> <span class="token number">10</span><span class="token punctuation">,</span>
            WP <span class="token operator">=</span> <span class="token number">11</span><span class="token punctuation">,</span>
            XP <span class="token operator">=</span> <span class="token number">12</span>
        <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u6D4F\u89C8\u5668 \u8BF7\u6C42\u6765\u6E90\u81EA\u5B9A\u4E49\u5E94\u7528</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token keyword">enum</span> <span class="token class-name">UserAgentApp</span>
        <span class="token punctuation">{</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u672A\u8BC6\u522B</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            notFound <span class="token operator">=</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u5FAE\u4FE1\u8BF7\u6C42</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            micromessenger <span class="token operator">=</span> <span class="token number">1</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// QQ \u8BF7\u6C42</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            qq <span class="token operator">=</span> <span class="token number">2</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u652F\u4ED8\u5B9D</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            alipayclient <span class="token operator">=</span> <span class="token number">3</span><span class="token punctuation">,</span>

        <span class="token punctuation">}</span>
        <span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>
    <span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br><span class="line-number">52</span><br><span class="line-number">53</span><br><span class="line-number">54</span><br><span class="line-number">55</span><br><span class="line-number">56</span><br><span class="line-number">57</span><br><span class="line-number">58</span><br><span class="line-number">59</span><br><span class="line-number">60</span><br><span class="line-number">61</span><br><span class="line-number">62</span><br><span class="line-number">63</span><br><span class="line-number">64</span><br><span class="line-number">65</span><br><span class="line-number">66</span><br><span class="line-number">67</span><br><span class="line-number">68</span><br><span class="line-number">69</span><br><span class="line-number">70</span><br><span class="line-number">71</span><br><span class="line-number">72</span><br><span class="line-number">73</span><br><span class="line-number">74</span><br><span class="line-number">75</span><br><span class="line-number">76</span><br><span class="line-number">77</span><br><span class="line-number">78</span><br><span class="line-number">79</span><br><span class="line-number">80</span><br><span class="line-number">81</span><br><span class="line-number">82</span><br><span class="line-number">83</span><br><span class="line-number">84</span><br><span class="line-number">85</span><br><span class="line-number">86</span><br><span class="line-number">87</span><br><span class="line-number">88</span><br><span class="line-number">89</span><br><span class="line-number">90</span><br><span class="line-number">91</span><br><span class="line-number">92</span><br><span class="line-number">93</span><br><span class="line-number">94</span><br><span class="line-number">95</span><br><span class="line-number">96</span><br><span class="line-number">97</span><br><span class="line-number">98</span><br><span class="line-number">99</span><br><span class="line-number">100</span><br><span class="line-number">101</span><br><span class="line-number">102</span><br><span class="line-number">103</span><br><span class="line-number">104</span><br><span class="line-number">105</span><br><span class="line-number">106</span><br><span class="line-number">107</span><br><span class="line-number">108</span><br><span class="line-number">109</span><br><span class="line-number">110</span><br><span class="line-number">111</span><br><span class="line-number">112</span><br><span class="line-number">113</span><br><span class="line-number">114</span><br><span class="line-number">115</span><br><span class="line-number">116</span><br><span class="line-number">117</span><br><span class="line-number">118</span><br><span class="line-number">119</span><br><span class="line-number">120</span><br><span class="line-number">121</span><br><span class="line-number">122</span><br><span class="line-number">123</span><br><span class="line-number">124</span><br><span class="line-number">125</span><br><span class="line-number">126</span><br><span class="line-number">127</span><br><span class="line-number">128</span><br><span class="line-number">129</span><br><span class="line-number">130</span><br><span class="line-number">131</span><br><span class="line-number">132</span><br><span class="line-number">133</span><br><span class="line-number">134</span><br><span class="line-number">135</span><br><span class="line-number">136</span><br><span class="line-number">137</span><br><span class="line-number">138</span><br><span class="line-number">139</span><br><span class="line-number">140</span><br><span class="line-number">141</span><br><span class="line-number">142</span><br><span class="line-number">143</span><br><span class="line-number">144</span><br><span class="line-number">145</span><br><span class="line-number">146</span><br><span class="line-number">147</span><br><span class="line-number">148</span><br><span class="line-number">149</span><br><span class="line-number">150</span><br><span class="line-number">151</span><br><span class="line-number">152</span><br><span class="line-number">153</span><br><span class="line-number">154</span><br><span class="line-number">155</span><br><span class="line-number">156</span><br><span class="line-number">157</span><br><span class="line-number">158</span><br><span class="line-number">159</span><br><span class="line-number">160</span><br><span class="line-number">161</span><br><span class="line-number">162</span><br><span class="line-number">163</span><br><span class="line-number">164</span><br><span class="line-number">165</span><br><span class="line-number">166</span><br><span class="line-number">167</span><br><span class="line-number">168</span><br><span class="line-number">169</span><br><span class="line-number">170</span><br><span class="line-number">171</span><br><span class="line-number">172</span><br><span class="line-number">173</span><br><span class="line-number">174</span><br><span class="line-number">175</span><br><span class="line-number">176</span><br><span class="line-number">177</span><br><span class="line-number">178</span><br><span class="line-number">179</span><br><span class="line-number">180</span><br><span class="line-number">181</span><br><span class="line-number">182</span><br><span class="line-number">183</span><br><span class="line-number">184</span><br><span class="line-number">185</span><br><span class="line-number">186</span><br><span class="line-number">187</span><br><span class="line-number">188</span><br><span class="line-number">189</span><br><span class="line-number">190</span><br><span class="line-number">191</span><br><span class="line-number">192</span><br><span class="line-number">193</span><br><span class="line-number">194</span><br><span class="line-number">195</span><br><span class="line-number">196</span><br><span class="line-number">197</span><br><span class="line-number">198</span><br><span class="line-number">199</span><br><span class="line-number">200</span><br><span class="line-number">201</span><br><span class="line-number">202</span><br><span class="line-number">203</span><br><span class="line-number">204</span><br><span class="line-number">205</span><br><span class="line-number">206</span><br><span class="line-number">207</span><br><span class="line-number">208</span><br><span class="line-number">209</span><br><span class="line-number">210</span><br><span class="line-number">211</span><br><span class="line-number">212</span><br><span class="line-number">213</span><br><span class="line-number">214</span><br><span class="line-number">215</span><br><span class="line-number">216</span><br><span class="line-number">217</span><br><span class="line-number">218</span><br><span class="line-number">219</span><br><span class="line-number">220</span><br><span class="line-number">221</span><br><span class="line-number">222</span><br><span class="line-number">223</span><br><span class="line-number">224</span><br><span class="line-number">225</span><br><span class="line-number">226</span><br><span class="line-number">227</span><br><span class="line-number">228</span><br><span class="line-number">229</span><br><span class="line-number">230</span><br><span class="line-number">231</span><br><span class="line-number">232</span><br><span class="line-number">233</span><br><span class="line-number">234</span><br><span class="line-number">235</span><br><span class="line-number">236</span><br><span class="line-number">237</span><br><span class="line-number">238</span><br><span class="line-number">239</span><br><span class="line-number">240</span><br><span class="line-number">241</span><br><span class="line-number">242</span><br><span class="line-number">243</span><br><span class="line-number">244</span><br><span class="line-number">245</span><br><span class="line-number">246</span><br><span class="line-number">247</span><br><span class="line-number">248</span><br><span class="line-number">249</span><br><span class="line-number">250</span><br><span class="line-number">251</span><br><span class="line-number">252</span><br><span class="line-number">253</span><br><span class="line-number">254</span><br><span class="line-number">255</span><br><span class="line-number">256</span><br><span class="line-number">257</span><br><span class="line-number">258</span><br><span class="line-number">259</span><br><span class="line-number">260</span><br><span class="line-number">261</span><br><span class="line-number">262</span><br><span class="line-number">263</span><br><span class="line-number">264</span><br><span class="line-number">265</span><br><span class="line-number">266</span><br><span class="line-number">267</span><br><span class="line-number">268</span><br><span class="line-number">269</span><br><span class="line-number">270</span><br><span class="line-number">271</span><br><span class="line-number">272</span><br><span class="line-number">273</span><br><span class="line-number">274</span><br><span class="line-number">275</span><br><span class="line-number">276</span><br><span class="line-number">277</span><br><span class="line-number">278</span><br><span class="line-number">279</span><br><span class="line-number">280</span><br><span class="line-number">281</span><br><span class="line-number">282</span><br><span class="line-number">283</span><br><span class="line-number">284</span><br><span class="line-number">285</span><br><span class="line-number">286</span><br><span class="line-number">287</span><br><span class="line-number">288</span><br><span class="line-number">289</span><br><span class="line-number">290</span><br><span class="line-number">291</span><br><span class="line-number">292</span><br><span class="line-number">293</span><br><span class="line-number">294</span><br><span class="line-number">295</span><br><span class="line-number">296</span><br><span class="line-number">297</span><br><span class="line-number">298</span><br><span class="line-number">299</span><br><span class="line-number">300</span><br><span class="line-number">301</span><br><span class="line-number">302</span><br><span class="line-number">303</span><br><span class="line-number">304</span><br><span class="line-number">305</span><br><span class="line-number">306</span><br><span class="line-number">307</span><br><span class="line-number">308</span><br><span class="line-number">309</span><br><span class="line-number">310</span><br><span class="line-number">311</span><br><span class="line-number">312</span><br><span class="line-number">313</span><br><span class="line-number">314</span><br><span class="line-number">315</span><br><span class="line-number">316</span><br><span class="line-number">317</span><br><span class="line-number">318</span><br><span class="line-number">319</span><br><span class="line-number">320</span><br><span class="line-number">321</span><br><span class="line-number">322</span><br><span class="line-number">323</span><br><span class="line-number">324</span><br><span class="line-number">325</span><br><span class="line-number">326</span><br><span class="line-number">327</span><br><span class="line-number">328</span><br><span class="line-number">329</span><br><span class="line-number">330</span><br><span class="line-number">331</span><br><span class="line-number">332</span><br><span class="line-number">333</span><br><span class="line-number">334</span><br><span class="line-number">335</span><br><span class="line-number">336</span><br><span class="line-number">337</span><br><span class="line-number">338</span><br><span class="line-number">339</span><br><span class="line-number">340</span><br><span class="line-number">341</span><br><span class="line-number">342</span><br><span class="line-number">343</span><br><span class="line-number">344</span><br><span class="line-number">345</span><br><span class="line-number">346</span><br><span class="line-number">347</span><br><span class="line-number">348</span><br><span class="line-number">349</span><br><span class="line-number">350</span><br><span class="line-number">351</span><br><span class="line-number">352</span><br><span class="line-number">353</span><br><span class="line-number">354</span><br><span class="line-number">355</span><br><span class="line-number">356</span><br><span class="line-number">357</span><br><span class="line-number">358</span><br><span class="line-number">359</span><br><span class="line-number">360</span><br><span class="line-number">361</span><br><span class="line-number">362</span><br><span class="line-number">363</span><br><span class="line-number">364</span><br><span class="line-number">365</span><br><span class="line-number">366</span><br><span class="line-number">367</span><br><span class="line-number">368</span><br><span class="line-number">369</span><br><span class="line-number">370</span><br><span class="line-number">371</span><br><span class="line-number">372</span><br><span class="line-number">373</span><br><span class="line-number">374</span><br><span class="line-number">375</span><br><span class="line-number">376</span><br></div></div>`,13),c=[t];function l(o,r,u,m,i,b){return s(),a("div",null,c)}var y=n(e,[["render",l]]);export{g as __pageData,y as default};
