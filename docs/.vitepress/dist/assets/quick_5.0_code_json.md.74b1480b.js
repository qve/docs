import{bm as n,aS as s,ap as a,at as p}from"./plugin-vue_export-helper.ec0df64b.js";const d='{"title":"Json","description":"","frontmatter":{},"headers":[{"level":2,"title":"JsonDocument \u4E3A\u53EA\u8BFB","slug":"jsondocument-\u4E3A\u53EA\u8BFB"},{"level":2,"title":"JsonQuick \u539F\u751F\u8BED\u6CD5","slug":"jsonquick-\u539F\u751F\u8BED\u6CD5"},{"level":2,"title":"Json \u8F6C\u6362\u53BB\u91CD","slug":"json-\u8F6C\u6362\u53BB\u91CD"},{"level":2,"title":"JsonHelper","slug":"jsonhelper"},{"level":2,"title":"json \u53CD\u5E8F\u5217\u5316","slug":"json-\u53CD\u5E8F\u5217\u5316"},{"level":2,"title":"json \u6570\u7EC4","slug":"json-\u6570\u7EC4"},{"level":2,"title":"json \u52A8\u6001\u6CDB\u578B","slug":"json-\u52A8\u6001\u6CDB\u578B"},{"level":2,"title":"JsonHelper \u65B9\u6CD5","slug":"jsonhelper-\u65B9\u6CD5"}],"relativePath":"quick/5.0/code/json.md","lastUpdated":1639784918869}',t={},e=p(`<h1 id="json" tabindex="-1">Json <a class="header-anchor" href="#json" aria-hidden="true">#</a></h1><h2 id="jsondocument-\u4E3A\u53EA\u8BFB" tabindex="-1">JsonDocument \u4E3A\u53EA\u8BFB <a class="header-anchor" href="#jsondocument-\u4E3A\u53EA\u8BFB" aria-hidden="true">#</a></h2><p><code>System.Text.Json</code> <code>DOM</code> \u65E0\u6CD5\u6DFB\u52A0\u3001\u5220\u9664\u6216\u4FEE\u6539 JSON \u5143\u7D20</p><h2 id="jsonquick-\u539F\u751F\u8BED\u6CD5" tabindex="-1">JsonQuick \u539F\u751F\u8BED\u6CD5 <a class="header-anchor" href="#jsonquick-\u539F\u751F\u8BED\u6CD5" aria-hidden="true">#</a></h2><p>\u57FA\u4E8E\u63A5\u8FD1 js \u8BED\u6CD5\uFF0C\u53EF\u4EE3\u66FF\u65B9\u6CD5 JsonHelper</p><div class="language-csharp line-numbers-mode"><pre><code>
<span class="token class-name">JsonQuick</span> jq <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token constructor-invocation class-name">JsonQuick</span><span class="token punctuation">(</span><span class="token string">&quot;[{\\&quot;list\\&quot;:1,\\&quot;add\\&quot;:2},{\\&quot;itemAdd\\&quot;:20}]&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token comment">// \u53D6\u51FA add \u7684\u503C</span>
jq<span class="token punctuation">[</span><span class="token number">0</span><span class="token punctuation">]</span><span class="token punctuation">[</span><span class="token string">&quot;add&quot;</span><span class="token punctuation">]</span>

<span class="token comment">// \u53D6\u51FA itemAdd \u7684\u503C</span>
jq<span class="token punctuation">[</span><span class="token number">0</span><span class="token punctuation">]</span><span class="token punctuation">[</span><span class="token string">&quot;itemAdd&quot;</span><span class="token punctuation">]</span>

<span class="token class-name">StringBuilder</span> _control <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token constructor-invocation class-name">StringBuilder</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token comment">// \u53D6\u51FAjs\u6570\u7EC4 \u7B2C\u4E00\u4E2A\u5BF9\u8C61\uFF0C\u5FAA\u73AF\u53D6\u51FA\u503C</span>
<span class="token keyword">foreach</span> <span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">var</span></span> pp <span class="token keyword">in</span> jq<span class="token punctuation">[</span><span class="token number">0</span><span class="token punctuation">]</span><span class="token punctuation">.</span><span class="token function">EnumerateObject</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
<span class="token punctuation">{</span>
  <span class="token keyword">if</span> <span class="token punctuation">(</span>pp<span class="token punctuation">.</span>Name <span class="token operator">==</span> <span class="token string">&quot;list&quot;</span><span class="token punctuation">)</span>
  <span class="token punctuation">{</span>
   _control<span class="token punctuation">.</span><span class="token function">Append</span><span class="token punctuation">(</span><span class="token string">&quot;,&quot;</span> <span class="token operator">+</span> pp<span class="token punctuation">.</span>Value<span class="token punctuation">.</span><span class="token function">GetString</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span>
<span class="token punctuation">}</span>

</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br></div></div><h2 id="json-\u8F6C\u6362\u53BB\u91CD" tabindex="-1">Json \u8F6C\u6362\u53BB\u91CD <a class="header-anchor" href="#json-\u8F6C\u6362\u53BB\u91CD" aria-hidden="true">#</a></h2><div class="language-csharp line-numbers-mode"><pre><code>
<span class="token comment">// \u8F6C\u4E3A json \u5BF9\u8C61</span>
<span class="token class-name">JsonQuick</span> jqList <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token constructor-invocation class-name">JsonQuick</span><span class="token punctuation">(</span><span class="token string">&quot;[{\\&quot;493\\&quot;:\\&quot;1,2\\&quot;},{\\&quot;493\\&quot;:\\&quot;1,2,3,4,5,6\\&quot;,\\&quot;534\\&quot;:\\&quot;1,2,3,4,5,6,7,8\\&quot;}]&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token comment">// \u5B9A\u4E49\u65B0\u7684\u4E34\u65F6\u53D8\u91CF</span>
<span class="token class-name">Dictionary<span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">,</span> <span class="token keyword">string</span><span class="token punctuation">&gt;</span></span> ld <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token constructor-invocation class-name">Dictionary<span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">,</span> <span class="token keyword">string</span><span class="token punctuation">&gt;</span></span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token keyword">for</span> <span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">int</span></span> i <span class="token operator">=</span> <span class="token number">0</span><span class="token punctuation">;</span> i <span class="token operator">&lt;</span> jqList<span class="token punctuation">.</span><span class="token function">GetArrayLength</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span> i<span class="token operator">++</span><span class="token punctuation">)</span>
<span class="token punctuation">{</span>
  <span class="token keyword">foreach</span> <span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">var</span></span> pp <span class="token keyword">in</span> jqList<span class="token punctuation">[</span>i<span class="token punctuation">]</span><span class="token punctuation">.</span><span class="token function">EnumerateObject</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
  <span class="token punctuation">{</span>
    <span class="token comment">// Console.WriteLine(pp);</span>
    <span class="token comment">//\u5224\u65AD\u662F\u5426\u6709\u91CD\u590D\u952E\u540D</span>
     <span class="token keyword">if</span> <span class="token punctuation">(</span>ld<span class="token punctuation">.</span><span class="token function">TryGetValue</span><span class="token punctuation">(</span>pp<span class="token punctuation">.</span>Name<span class="token punctuation">,</span> <span class="token keyword">out</span> <span class="token class-name"><span class="token keyword">var</span></span> <span class="token keyword">value</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
     <span class="token punctuation">{</span>
        <span class="token comment">// Console.WriteLine(value);</span>
        <span class="token class-name"><span class="token keyword">string</span><span class="token punctuation">[</span><span class="token punctuation">]</span></span> _arr <span class="token operator">=</span> <span class="token keyword">value</span><span class="token punctuation">.</span><span class="token function">Split</span><span class="token punctuation">(</span><span class="token string">&quot;,&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
        <span class="token class-name"><span class="token keyword">string</span><span class="token punctuation">[</span><span class="token punctuation">]</span></span> _arr2 <span class="token operator">=</span> pp<span class="token punctuation">.</span>Value<span class="token punctuation">.</span><span class="token function">ToString</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">Split</span><span class="token punctuation">(</span><span class="token string">&quot;,&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
        <span class="token class-name"><span class="token keyword">string</span><span class="token punctuation">[</span><span class="token punctuation">]</span></span> x <span class="token operator">=</span> _arr<span class="token punctuation">.</span><span class="token function">Union</span><span class="token punctuation">(</span>_arr2<span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token generic-method"><span class="token function">ToArray</span><span class="token generic class-name"><span class="token punctuation">&lt;</span><span class="token keyword">string</span><span class="token punctuation">&gt;</span></span></span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span> <span class="token comment">//\u5254\u9664\u91CD\u590D\u9879</span>
        <span class="token comment">//Console.WriteLine(x);</span>
        ld<span class="token punctuation">[</span>pp<span class="token punctuation">.</span>Name<span class="token punctuation">]</span> <span class="token operator">=</span> String<span class="token punctuation">.</span><span class="token function">Join</span><span class="token punctuation">(</span><span class="token string">&quot;,&quot;</span><span class="token punctuation">,</span> x<span class="token punctuation">)</span><span class="token punctuation">;</span>

     <span class="token punctuation">}</span><span class="token keyword">else</span><span class="token punctuation">{</span>
        ld<span class="token punctuation">.</span><span class="token function">Add</span><span class="token punctuation">(</span>pp<span class="token punctuation">.</span>Name<span class="token punctuation">,</span> pp<span class="token punctuation">.</span>Value<span class="token punctuation">.</span><span class="token function">ToString</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
     <span class="token punctuation">}</span>

  <span class="token punctuation">}</span>
<span class="token punctuation">}</span>

<span class="token comment">// \u8F6C\u4E3Ajson</span>
Console<span class="token punctuation">.</span><span class="token function">WriteLine</span><span class="token punctuation">(</span>JsonHelper<span class="token punctuation">.</span><span class="token function">Serialize</span><span class="token punctuation">(</span>ld<span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br></div></div><h2 id="jsonhelper" tabindex="-1">JsonHelper <a class="header-anchor" href="#jsonhelper" aria-hidden="true">#</a></h2><p>\u5EFA\u8BAE\u4F7F\u7528\u8DDF\u63A5\u8FD1 js \u8BED\u6CD5\u7684 JsonQuick</p><h2 id="json-\u53CD\u5E8F\u5217\u5316" tabindex="-1">json \u53CD\u5E8F\u5217\u5316 <a class="header-anchor" href="#json-\u53CD\u5E8F\u5217\u5316" aria-hidden="true">#</a></h2><div class="language-csharp line-numbers-mode"><pre><code>
<span class="token class-name"><span class="token keyword">var</span></span> _jDoc <span class="token operator">=</span> JsonHelper<span class="token punctuation">.</span><span class="token function">Parse</span><span class="token punctuation">(</span><span class="token string">&quot;{\\&quot;list\\&quot;:1}&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token class-name"><span class="token keyword">var</span></span> _obj <span class="token operator">=</span> _jDoc<span class="token punctuation">.</span>RootElement<span class="token punctuation">.</span><span class="token function">GetProperty</span><span class="token punctuation">(</span><span class="token string">&quot;list&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token class-name"><span class="token keyword">string</span></span> _actionId <span class="token operator">=</span> _obj<span class="token punctuation">.</span><span class="token function">ToString</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br></div></div><h2 id="json-\u6570\u7EC4" tabindex="-1">json \u6570\u7EC4 <a class="header-anchor" href="#json-\u6570\u7EC4" aria-hidden="true">#</a></h2><div class="language-csharp line-numbers-mode"><pre><code>                <span class="token comment">// \u5206\u9875\u67E5\u8BE2\u53C2\u6570</span>
<span class="token keyword">foreach</span> <span class="token punctuation">(</span><span class="token class-name">JsonElement</span> po <span class="token keyword">in</span> JsonHelper<span class="token punctuation">.</span><span class="token function">ParseArray</span><span class="token punctuation">(</span>par<span class="token punctuation">)</span><span class="token punctuation">)</span>
<span class="token punctuation">{</span>

    <span class="token keyword">foreach</span> <span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">var</span></span> pp <span class="token keyword">in</span> po<span class="token punctuation">.</span><span class="token function">EnumerateObject</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
    <span class="token punctuation">{</span>

        <span class="token keyword">if</span> <span class="token punctuation">(</span>pp<span class="token punctuation">.</span>Name <span class="token operator">==</span> <span class="token string">&quot;q&quot;</span><span class="token punctuation">)</span>
        <span class="token punctuation">{</span>
          pp<span class="token punctuation">.</span>Value<span class="token punctuation">.</span><span class="token function">GetString</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
        <span class="token punctuation">}</span>

        <span class="token comment">// \u5224\u65AD\u5C5E\u6027\u53D6\u503C</span>
        <span class="token keyword">if</span> <span class="token punctuation">(</span>pp<span class="token punctuation">.</span><span class="token function">TryGetProperty</span><span class="token punctuation">(</span><span class="token string">&quot;list&quot;</span><span class="token punctuation">,</span> <span class="token keyword">out</span> <span class="token class-name"><span class="token keyword">var</span></span> <span class="token keyword">value</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
        <span class="token punctuation">{</span>
          _actionId <span class="token operator">=</span> <span class="token keyword">value</span><span class="token punctuation">.</span><span class="token function">ToString</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
          <span class="token keyword">break</span><span class="token punctuation">;</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br></div></div><h2 id="json-\u52A8\u6001\u6CDB\u578B" tabindex="-1">json \u52A8\u6001\u6CDB\u578B <a class="header-anchor" href="#json-\u52A8\u6001\u6CDB\u578B" aria-hidden="true">#</a></h2><div class="language-csharp line-numbers-mode"><pre><code>  <span class="token class-name">JsonElement</span> _obj <span class="token operator">=</span> pc<span class="token punctuation">.</span>obj<span class="token punctuation">;</span>
  <span class="token class-name"><span class="token keyword">int</span></span> a <span class="token operator">=</span> _obj<span class="token punctuation">.</span><span class="token function">GetProperty</span><span class="token punctuation">(</span><span class="token string">&quot;a&quot;</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">GetInt64</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br></div></div><h2 id="jsonhelper-\u65B9\u6CD5" tabindex="-1">JsonHelper \u65B9\u6CD5 <a class="header-anchor" href="#jsonhelper-\u65B9\u6CD5" aria-hidden="true">#</a></h2><div class="language-js line-numbers-mode"><pre><code>  <span class="token comment">/// &lt;summary&gt;</span>
  <span class="token comment">///  \u8F6C\u6362\u5C5E\u6027</span>
  <span class="token comment">/// &lt;/summary&gt;</span>
  <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
  <span class="token keyword">public</span> <span class="token keyword">static</span> JsonSerializerOptions <span class="token function">Options</span><span class="token punctuation">(</span><span class="token punctuation">)</span>

  <span class="token comment">/// &lt;summary&gt;</span>
  <span class="token comment">/// JsonSerializer \u5E8F\u5217\u5316\u4E3A\u5B57\u7B26\u4E32</span>
  <span class="token comment">/// &lt;/summary&gt;</span>
  <span class="token comment">/// &lt;param name=&quot;obj&quot;&gt;JsonSerializerOptions&lt;/param&gt;</span>
  <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
  <span class="token keyword">public</span> <span class="token keyword">static</span> string <span class="token function">Serialize</span><span class="token punctuation">(</span>object obj<span class="token punctuation">)</span>


  <span class="token comment">/// &lt;summary&gt;</span>
  <span class="token comment">/// \u5C06json\u53CD\u5E8F\u5217\u5316\u4E3A\u5B9E\u4F53\u5BF9\u8C61(\u5305\u542BDataTable\u548CList\u96C6\u5408\u5BF9\u8C61)</span>
  <span class="token comment">/// &lt;/summary&gt;</span>
  <span class="token comment">/// &lt;typeparam name=&quot;T&quot;&gt;&lt;/typeparam&gt;</span>
  <span class="token comment">/// &lt;param name=&quot;strJson&quot;&gt;&lt;/param&gt;</span>
  <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
  <span class="token keyword">public</span> <span class="token keyword">static</span> <span class="token constant">T</span> Deserialize<span class="token operator">&lt;</span><span class="token constant">T</span><span class="token operator">&gt;</span><span class="token punctuation">(</span>string strJson<span class="token punctuation">)</span>

  <span class="token comment">/// &lt;summary&gt;</span>
  <span class="token comment">///  \u4F7F\u7528 Parse \u65B9\u6CD5\u8FD4\u56DE\u5185\u5BB9</span>
  <span class="token comment">/// &lt;/summary&gt;</span>
  <span class="token comment">/// &lt;param name=&quot;strJson&quot;&gt;&lt;/param&gt;</span>
  <span class="token comment">/// &lt;returns&gt;RootElement&lt;/returns&gt;</span>
  <span class="token keyword">public</span> <span class="token keyword">static</span> dynamic <span class="token function">Deserialize</span><span class="token punctuation">(</span><span class="token parameter">string strJson</span><span class="token punctuation">)</span><span class="token punctuation">{</span>
    JsonSerializerOptions options <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token class-name">JsonSerializerOptions</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
    options<span class="token punctuation">.</span>Converters<span class="token punctuation">.</span><span class="token function">Add</span><span class="token punctuation">(</span><span class="token keyword">new</span> <span class="token class-name">JsonHelperConvert<span class="token punctuation">.</span>DynamicConverter</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token keyword">return</span> JsonSerializer<span class="token punctuation">.</span>Deserialize<span class="token operator">&lt;</span>dynamic<span class="token operator">&gt;</span><span class="token punctuation">(</span>strJson<span class="token punctuation">,</span> options<span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span>



  <span class="token comment">/// &lt;summary&gt;</span>
  <span class="token comment">///  \u53CD\u5E8F\u5217\u5316\u5230\u5BF9\u8C61</span>
  <span class="token comment">/// &lt;/summary&gt;</span>
  <span class="token comment">/// &lt;param name=&quot;strJson&quot;&gt;\u5B57\u7B26\u4E32&lt;/param&gt;</span>
  <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
  <span class="token keyword">public</span> <span class="token keyword">static</span> JsonDocument <span class="token function">Parse</span><span class="token punctuation">(</span><span class="token parameter">string strJson</span><span class="token punctuation">)</span>
  <span class="token punctuation">{</span>
     <span class="token keyword">return</span> JsonDocument<span class="token punctuation">.</span><span class="token function">Parse</span><span class="token punctuation">(</span>strJson<span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span>

  <span class="token comment">/// &lt;summary&gt;</span>
  <span class="token comment">/// \u8FD4\u56DE \u5BF9\u8C61</span>
  <span class="token comment">/// &lt;/summary&gt;</span>
  <span class="token comment">/// &lt;param name=&quot;strJson&quot;&gt;&lt;/param&gt;</span>
  <span class="token comment">/// &lt;returns&gt;EnumerateObject&lt;/returns&gt;</span>
  <span class="token keyword">public</span> <span class="token keyword">static</span> dynamic <span class="token function">ParseObject</span><span class="token punctuation">(</span><span class="token parameter">string strJson</span><span class="token punctuation">)</span>
  <span class="token punctuation">{</span>
     <span class="token keyword">return</span> JsonDocument<span class="token punctuation">.</span><span class="token function">Parse</span><span class="token punctuation">(</span>strJson<span class="token punctuation">)</span><span class="token punctuation">.</span>RootElement<span class="token punctuation">.</span><span class="token function">EnumerateObject</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span>

  <span class="token comment">/// &lt;summary&gt;</span>
  <span class="token comment">/// \u8FD4\u56DE \u6570\u7EC4</span>
  <span class="token comment">/// &lt;/summary&gt;</span>
  <span class="token comment">/// &lt;param name=&quot;strJson&quot;&gt;&lt;/param&gt;</span>
  <span class="token comment">/// &lt;returns&gt;EnumerateArray&lt;/returns&gt;</span>
  <span class="token keyword">public</span> <span class="token keyword">static</span> dynamic <span class="token function">ParseArray</span><span class="token punctuation">(</span><span class="token parameter">string strJson</span><span class="token punctuation">)</span>
  <span class="token punctuation">{</span>
     <span class="token keyword">return</span> JsonDocument<span class="token punctuation">.</span><span class="token function">Parse</span><span class="token punctuation">(</span>strJson<span class="token punctuation">)</span><span class="token punctuation">.</span>RootElement<span class="token punctuation">.</span><span class="token function">EnumerateArray</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span>


 <span class="token comment">/// &lt;summary&gt;</span>
 <span class="token comment">/// \u8F6C\u6362\u4E3A\u5BF9\u8C61,\u5BF9\u65F6\u95F4\u683C\u5F0F\u5316</span>
 <span class="token comment">/// &lt;/summary&gt;</span>
 <span class="token comment">/// &lt;typeparam name=&quot;T&quot;&gt;\u5BF9\u8C61&lt;/typeparam&gt;</span>
 <span class="token comment">/// &lt;param name=&quot;str&quot;&gt;\u65F6\u95F4\u683C\u5F0F&lt;/param&gt;</span>
 <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
 <span class="token keyword">public</span> <span class="token keyword">static</span> <span class="token constant">T</span> DeserializeJsonTime<span class="token operator">&lt;</span><span class="token constant">T</span><span class="token operator">&gt;</span><span class="token punctuation">(</span>string str<span class="token punctuation">)</span>
 <span class="token punctuation">{</span>
     <span class="token keyword">return</span> Deserialize<span class="token operator">&lt;</span><span class="token constant">T</span><span class="token operator">&gt;</span><span class="token punctuation">(</span><span class="token function">JsonTime</span><span class="token punctuation">(</span>str<span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
 <span class="token punctuation">}</span>

  <span class="token comment">/// &lt;summary&gt;</span>
 <span class="token comment">/// Json \u8F6C\u6362\u65F6\u95F4\u683C\u5F0F</span>
 <span class="token comment">/// &lt;/summary&gt;</span>
 <span class="token comment">/// &lt;param name=&quot;str&quot;&gt;\u9700\u8F6C\u6362\u7684\u5B57\u7B26\u4E32&lt;/param&gt;</span>
 <span class="token comment">/// &lt;returns&gt;\u8FD4\u56DE\u6807\u51C6\u65F6\u95F4\u683C\u5F0F\u4E32&lt;/returns&gt;</span>
 <span class="token keyword">public</span> <span class="token keyword">static</span> string <span class="token function">JsonTime</span><span class="token punctuation">(</span>string str<span class="token punctuation">)</span>

  <span class="token comment">/// &lt;summary&gt;</span>
 <span class="token comment">/// Json\u8FC7\u6EE4\u7279\u6B8A\u5B57\u7B26</span>
 <span class="token comment">/// &lt;/summary&gt;</span>
 <span class="token comment">/// &lt;param name=&quot;s&quot;&gt;\u521D\u59CB\u5B57\u7B26\u4E32&lt;/param&gt;</span>
 <span class="token comment">/// &lt;returns&gt;\u8FC7\u6EE4\u5904\u7406&lt;/returns&gt;</span>
 <span class="token keyword">public</span> <span class="token keyword">static</span> string <span class="token function">StringFormat</span><span class="token punctuation">(</span>string s<span class="token punctuation">)</span>


  <span class="token comment">/// &lt;summary&gt;</span>
 <span class="token comment">/// json \u53D6\u51FA\u5B57\u6BB5\u4E3B\u952E</span>
 <span class="token comment">/// &lt;/summary&gt;</span>
 <span class="token comment">/// &lt;param name=&quot;jsonElement&quot;&gt;EnumerateObject \u5BF9\u8C61&lt;/param&gt;</span>
 <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
 <span class="token keyword">public</span> string <span class="token function">JsonToKey</span><span class="token punctuation">(</span>JsonElement jsonElement<span class="token punctuation">)</span>


 <span class="token comment">/// &lt;summary&gt;</span>
 <span class="token comment">/// json \u5BF9\u8C61\u8F6C\u6362</span>
 <span class="token comment">/// &lt;/summary&gt;</span>
 <span class="token comment">/// &lt;param name=&quot;jsonElement&quot;&gt;jsonElement.EnumerateObject&lt;/param&gt;</span>
 <span class="token comment">/// &lt;returns&gt;string,jsonElement&lt;/returns&gt;</span>
 <span class="token keyword">public</span> IDictionary<span class="token operator">&lt;</span>string<span class="token punctuation">,</span> dynamic<span class="token operator">&gt;</span> <span class="token function">ToObject</span><span class="token punctuation">(</span>JsonElement jsonElement<span class="token punctuation">)</span>


  <span class="token comment">/// &lt;summary&gt;</span>
 <span class="token comment">/// \u53CD\u5E8F\u5217\u5316\u5230\u5BF9\u8C61</span>
 <span class="token comment">/// &lt;/summary&gt;</span>
 <span class="token comment">/// &lt;param name=&quot;json&quot;&gt;&lt;/param&gt;</span>
 <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
 <span class="token keyword">public</span> <span class="token keyword">static</span> dynamic <span class="token function">Reader</span><span class="token punctuation">(</span>string json<span class="token punctuation">)</span>

  <span class="token comment">/// &lt;summary&gt;</span>
 <span class="token comment">/// \u53CD\u5E8F\u5217\u5316\u5230\u5BF9\u8C61</span>
 <span class="token comment">/// &lt;/summary&gt;</span>
 <span class="token comment">/// &lt;param name=&quot;json&quot;&gt;\u5B57\u7B26\u4E32&lt;/param&gt;</span>
 <span class="token comment">/// &lt;param name=&quot;encoding&quot;&gt;\u7F16\u7801&lt;/param&gt;</span>
 <span class="token comment">/// &lt;returns&gt;JsonReaderWriterFactory&lt;/returns&gt;</span>
 <span class="token keyword">public</span> <span class="token keyword">static</span> dynamic <span class="token function">Reader</span><span class="token punctuation">(</span>string json<span class="token punctuation">,</span> Encoding encoding<span class="token punctuation">)</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br><span class="line-number">52</span><br><span class="line-number">53</span><br><span class="line-number">54</span><br><span class="line-number">55</span><br><span class="line-number">56</span><br><span class="line-number">57</span><br><span class="line-number">58</span><br><span class="line-number">59</span><br><span class="line-number">60</span><br><span class="line-number">61</span><br><span class="line-number">62</span><br><span class="line-number">63</span><br><span class="line-number">64</span><br><span class="line-number">65</span><br><span class="line-number">66</span><br><span class="line-number">67</span><br><span class="line-number">68</span><br><span class="line-number">69</span><br><span class="line-number">70</span><br><span class="line-number">71</span><br><span class="line-number">72</span><br><span class="line-number">73</span><br><span class="line-number">74</span><br><span class="line-number">75</span><br><span class="line-number">76</span><br><span class="line-number">77</span><br><span class="line-number">78</span><br><span class="line-number">79</span><br><span class="line-number">80</span><br><span class="line-number">81</span><br><span class="line-number">82</span><br><span class="line-number">83</span><br><span class="line-number">84</span><br><span class="line-number">85</span><br><span class="line-number">86</span><br><span class="line-number">87</span><br><span class="line-number">88</span><br><span class="line-number">89</span><br><span class="line-number">90</span><br><span class="line-number">91</span><br><span class="line-number">92</span><br><span class="line-number">93</span><br><span class="line-number">94</span><br><span class="line-number">95</span><br><span class="line-number">96</span><br><span class="line-number">97</span><br><span class="line-number">98</span><br><span class="line-number">99</span><br><span class="line-number">100</span><br><span class="line-number">101</span><br><span class="line-number">102</span><br><span class="line-number">103</span><br><span class="line-number">104</span><br><span class="line-number">105</span><br><span class="line-number">106</span><br><span class="line-number">107</span><br><span class="line-number">108</span><br><span class="line-number">109</span><br><span class="line-number">110</span><br><span class="line-number">111</span><br><span class="line-number">112</span><br><span class="line-number">113</span><br><span class="line-number">114</span><br><span class="line-number">115</span><br><span class="line-number">116</span><br><span class="line-number">117</span><br><span class="line-number">118</span><br><span class="line-number">119</span><br><span class="line-number">120</span><br><span class="line-number">121</span><br><span class="line-number">122</span><br></div></div>`,18),o=[e];function c(l,u,r,i,k,m){return s(),a("div",null,o)}var g=n(t,[["render",c]]);export{d as __pageData,g as default};
