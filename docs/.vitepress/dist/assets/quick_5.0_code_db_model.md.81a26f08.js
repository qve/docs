import{bm as n,aS as s,ap as a,at as p}from"./plugin-vue_export-helper.ec0df64b.js";const d='{"title":"\u6570\u636E\u5BF9\u8C61","description":"","frontmatter":{},"headers":[{"level":2,"title":"QF_Server \u83B7\u53D6\u6570\u636E\u5E93\u914D\u7F6E","slug":"qf-server-\u83B7\u53D6\u6570\u636E\u5E93\u914D\u7F6E"},{"level":2,"title":"QF_Model \u6570\u636E\u5BF9\u8C61","slug":"qf-model-\u6570\u636E\u5BF9\u8C61"},{"level":2,"title":"QuickORMBLL \u589E\u5220\u6539\u67E5","slug":"quickormbll-\u589E\u5220\u6539\u67E5"},{"level":3,"title":"\u6839\u636E\u7D22\u5F15\u53D6\u51FA\u5916\u952E\u5173\u7CFB","slug":"\u6839\u636E\u7D22\u5F15\u53D6\u51FA\u5916\u952E\u5173\u7CFB"},{"level":3,"title":"Join \u5916\u952E\u67E5\u8BE2","slug":"join-\u5916\u952E\u67E5\u8BE2"},{"level":2,"title":"Api \u6743\u9650","slug":"api-\u6743\u9650"},{"level":2,"title":"QuickDBEnum \u6570\u636E\u72B6\u6001","slug":"quickdbenum-\u6570\u636E\u72B6\u6001"}],"relativePath":"quick/5.0/code/db_model.md","lastUpdated":1638189508786}',e={},t=p(`<h1 id="\u6570\u636E\u5BF9\u8C61" tabindex="-1">\u6570\u636E\u5BF9\u8C61 <a class="header-anchor" href="#\u6570\u636E\u5BF9\u8C61" aria-hidden="true">#</a></h1><h2 id="qf-server-\u83B7\u53D6\u6570\u636E\u5E93\u914D\u7F6E" tabindex="-1">QF_Server \u83B7\u53D6\u6570\u636E\u5E93\u914D\u7F6E <a class="header-anchor" href="#qf-server-\u83B7\u53D6\u6570\u636E\u5E93\u914D\u7F6E" aria-hidden="true">#</a></h2><ul><li><code>QF_Server</code> \u5168\u5C40\u914D\u7F6E\u670D\u52A1\u5668\u52A0\u5BC6\u8FDE\u63A5\uFF0C\u65B9\u4FBF\u52A8\u6001\u5207\u6362\u670D\u52A1\u5668</li></ul><div class="language-csharp line-numbers-mode"><pre><code><span class="token comment">// \u6839\u636E\u6570\u636E\u5E93\u547D\u540D\u53D6\u51FA\u670D\u52A1\u5668\u914D\u7F6E\u8FDE\u63A5\u5B57\u7B26\u4E32</span>
<span class="token class-name">QuickORMBLL</span> qob <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token constructor-invocation class-name">QuickORMBLL</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token class-name"><span class="token keyword">string</span></span> connStr <span class="token operator">=</span> qob<span class="token punctuation">.</span><span class="token function">GetConnectByDBName</span><span class="token punctuation">(</span><span class="token string">&quot;QuickDB&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
connStr <span class="token operator">=</span>  qob<span class="token punctuation">.</span><span class="token function">GetFieldByDBName</span><span class="token punctuation">(</span><span class="token string">&quot;QuickDB&quot;</span><span class="token punctuation">,</span><span class="token string">&quot;DBConnect&quot;</span><span class="token punctuation">)</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br></div></div><h2 id="qf-model-\u6570\u636E\u5BF9\u8C61" tabindex="-1">QF_Model \u6570\u636E\u5BF9\u8C61 <a class="header-anchor" href="#qf-model-\u6570\u636E\u5BF9\u8C61" aria-hidden="true">#</a></h2><p>\u6570\u636E\u8868 <code>QF_Model</code> \u5B58\u50A8\u4E86\u5BF9\u8C61\u7684\u5B57\u6BB5\u4E0E\u5916\u952E\u5173\u7CFB\uFF0C\u4E0E\u6240\u5728\u6570\u636E\u5E93\u3002 <code>code</code> \u5B57\u6BB5\u7528\u4E8E\u7D22\u5F15\u67E5\u8BE2</p><h2 id="quickormbll-\u589E\u5220\u6539\u67E5" tabindex="-1">QuickORMBLL \u589E\u5220\u6539\u67E5 <a class="header-anchor" href="#quickormbll-\u589E\u5220\u6539\u67E5" aria-hidden="true">#</a></h2><table><thead><tr><th>\u4E8B\u4EF6\u540D</th><th>\u8BF4\u660E</th></tr></thead><tbody><tr><td>getDBName</td><td>\u6839\u636E\u7D22\u5F15\u53D6\u51FA\u6240\u5728\u7684\u6570\u636E\u5E93\u540D\uFF0C\u5E76\u7F13\u5B58\u5728 QuickCache.Memory1 \u5C0F\u65F6</td></tr><tr><td>getJoinCache</td><td>\u6839\u636E\u7D22\u5F15\u53D6\u51FA\u5916\u952E\u914D\u7F6E\uFF0C\u5E76\u7F13\u5B58\u5728 QuickCache.Memory1 \u5C0F\u65F6</td></tr><tr><td>GetJoinAttrBySql</td><td>\u6839\u636E\u7D22\u5F15\u53D6\u51FA\u5916\u952E\u914D\u7F6E\uFF0C\u5E76\u7F13\u5B58\u5728 QuickCache.Memory1 \u5C0F\u65F6</td></tr></tbody></table><h3 id="\u6839\u636E\u7D22\u5F15\u53D6\u51FA\u5916\u952E\u5173\u7CFB" tabindex="-1">\u6839\u636E\u7D22\u5F15\u53D6\u51FA\u5916\u952E\u5173\u7CFB <a class="header-anchor" href="#\u6839\u636E\u7D22\u5F15\u53D6\u51FA\u5916\u952E\u5173\u7CFB" aria-hidden="true">#</a></h3><ul><li>QuickORMBLL</li></ul><div class="language-csharp line-numbers-mode"><pre><code>
    <span class="token comment">/// &lt;summary&gt;</span>
    <span class="token comment">/// \u53D6\u51FA\u5916\u952E\u914D\u7F6E</span>
    <span class="token comment">/// &lt;/summary&gt;</span>
    <span class="token comment">/// &lt;param name=&quot;code&quot;&gt;\u5BF9\u8C61\u7D22\u5F15 QF_Model.code&lt;/param&gt;</span>
    <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
    <span class="token class-name"><span class="token keyword">string</span></span> _joinStr <span class="token operator">=</span> <span class="token function">getJoinCache</span><span class="token punctuation">(</span>code<span class="token punctuation">)</span><span class="token punctuation">;</span>

    <span class="token class-name"><span class="token keyword">string</span></span> _DBName<span class="token operator">=</span><span class="token string">&quot;&quot;</span><span class="token punctuation">;</span>

    <span class="token comment">/// &lt;summary&gt;</span>
    <span class="token comment">/// \u6839\u636E\u7D22\u5F15\u53D6\u51FA\u5916\u952E\u914D\u7F6E</span>
    <span class="token comment">/// &lt;/summary&gt;</span>
    <span class="token comment">/// &lt;param name=&quot;dbName&quot;&gt;&lt;/param&gt;</span>
    <span class="token comment">/// &lt;param name=&quot;code&quot;&gt;\u5BF9\u8C61\u7D22\u5F15 QF_Model.code&lt;/param&gt;</span>
    <span class="token comment">/// &lt;param name=&quot;field&quot;&gt;\u5916\u952E\u5B57\u6BB5&lt;/param&gt;</span>
    <span class="token comment">/// &lt;returns&gt;\u5916\u952E\u914D\u7F6E&lt;/returns&gt;</span>
    <span class="token class-name">QueryJoin</span> qj <span class="token operator">=</span> <span class="token function">getJoin</span><span class="token punctuation">(</span><span class="token keyword">out</span> _DBName<span class="token punctuation">,</span> Table<span class="token punctuation">,</span> pj<span class="token punctuation">.</span>par<span class="token punctuation">)</span><span class="token punctuation">;</span>

</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br></div></div><h3 id="join-\u5916\u952E\u67E5\u8BE2" tabindex="-1">Join \u5916\u952E\u67E5\u8BE2 <a class="header-anchor" href="#join-\u5916\u952E\u67E5\u8BE2" aria-hidden="true">#</a></h3><div class="language-csharp line-numbers-mode"><pre><code>
        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u67E5\u8BE2 Json \u6570\u7EC4\u5217\u8868,qp\u67E5\u8BE2\u6761\u4EF6</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;returns&gt;Json \u6570\u7EC4&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">dynamic</span></span> <span class="token function">ListJson</span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> Table<span class="token punctuation">,</span> <span class="token class-name">PagerInStr</span> pj<span class="token punctuation">)</span>
        <span class="token punctuation">{</span>
            qp<span class="token punctuation">.</span>DBName <span class="token operator">=</span> <span class="token function">getDBNameCache</span><span class="token punctuation">(</span>Table<span class="token punctuation">)</span><span class="token punctuation">;</span>
            qp<span class="token punctuation">.</span>TableName <span class="token operator">=</span> Table<span class="token punctuation">;</span>

            <span class="token comment">// \u67E5\u8BE2\u6761\u4EF6\u53C2\u6570</span>
            <span class="token keyword">if</span> <span class="token punctuation">(</span><span class="token operator">!</span><span class="token keyword">string</span><span class="token punctuation">.</span><span class="token function">IsNullOrWhiteSpace</span><span class="token punctuation">(</span>pj<span class="token punctuation">.</span><span class="token keyword">where</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
            <span class="token punctuation">{</span>
                qp<span class="token punctuation">.</span>TParams<span class="token punctuation">.</span><span class="token function">AddRange</span><span class="token punctuation">(</span>pj<span class="token punctuation">.</span><span class="token function">JsonParams</span><span class="token punctuation">(</span>pj<span class="token punctuation">.</span><span class="token keyword">where</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
            <span class="token punctuation">}</span>

            <span class="token comment">// \u67E5\u8BE2\u6570\u636E\uFF0C\u53D6\u51FA\u5E73\u53F0\u5916\u952E\u914D\u7F6E</span>
            pj<span class="token punctuation">.</span><span class="token keyword">join</span> <span class="token operator">=</span> <span class="token function">getJoinCache</span><span class="token punctuation">(</span>Table<span class="token punctuation">)</span><span class="token punctuation">;</span>
            <span class="token comment">// \u5916\u952E\u5B57\u6BB5\u67E5\u8BE2</span>
            <span class="token keyword">if</span> <span class="token punctuation">(</span><span class="token operator">!</span><span class="token keyword">string</span><span class="token punctuation">.</span><span class="token function">IsNullOrWhiteSpace</span><span class="token punctuation">(</span>pj<span class="token punctuation">.</span><span class="token keyword">join</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
            <span class="token punctuation">{</span> qp<span class="token punctuation">.</span>Join <span class="token operator">=</span> <span class="token function">bindJoin</span><span class="token punctuation">(</span>pj<span class="token punctuation">.</span><span class="token keyword">join</span><span class="token punctuation">)</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

            <span class="token comment">//\u81EA\u5B9A\u4E49\u67E5\u8BE2\u5B57\u6BB5</span>
            <span class="token keyword">if</span> <span class="token punctuation">(</span><span class="token operator">!</span><span class="token keyword">string</span><span class="token punctuation">.</span><span class="token function">IsNullOrWhiteSpace</span><span class="token punctuation">(</span>pj<span class="token punctuation">.</span>field<span class="token punctuation">)</span><span class="token punctuation">)</span>
            <span class="token punctuation">{</span> qp<span class="token punctuation">.</span>Field <span class="token operator">=</span> pj<span class="token punctuation">.</span>field<span class="token punctuation">;</span> <span class="token punctuation">}</span>

            qp<span class="token punctuation">.</span>PageSize <span class="token operator">=</span> pj<span class="token punctuation">.</span>size<span class="token punctuation">;</span>
            qp<span class="token punctuation">.</span>PageIndex <span class="token operator">=</span> pj<span class="token punctuation">.</span>index<span class="token punctuation">;</span>
            qp<span class="token punctuation">.</span>Orderfld <span class="token operator">=</span> pj<span class="token punctuation">.</span>sort<span class="token punctuation">;</span>
            qp<span class="token punctuation">.</span>OrderType <span class="token operator">=</span> pj<span class="token punctuation">.</span>desc<span class="token punctuation">;</span>

            <span class="token comment">//if (qp.Join != null)</span>
            <span class="token comment">//{</span>
            <span class="token comment">//    qp.Join.Add(new QueryJoin(&quot;MUser_ID&quot;, QuickData.SqlLink(&quot;[QF_User]&quot;, QuickData.DBBase, false), &quot;LastFirst&quot;));</span>
            <span class="token comment">//}</span>
            <span class="token keyword">return</span> QuickData<span class="token punctuation">.</span><span class="token function">List</span><span class="token punctuation">(</span>qp<span class="token punctuation">)</span><span class="token punctuation">;</span>
        <span class="token punctuation">}</span>

</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br></div></div><h2 id="api-\u6743\u9650" tabindex="-1">Api \u6743\u9650 <a class="header-anchor" href="#api-\u6743\u9650" aria-hidden="true">#</a></h2><ul><li>QF_Model \u6570\u636E\u5BF9\u8C61\u7BA1\u7406\u8868</li></ul><h2 id="quickdbenum-\u6570\u636E\u72B6\u6001" tabindex="-1">QuickDBEnum \u6570\u636E\u72B6\u6001 <a class="header-anchor" href="#quickdbenum-\u6570\u636E\u72B6\u6001" aria-hidden="true">#</a></h2><div class="language-csharp line-numbers-mode"><pre><code>
    <span class="token comment">/// &lt;summary&gt;</span>
    <span class="token comment">/// \u6570\u636E\u53C2\u6570</span>
    <span class="token comment">/// &lt;/summary&gt;</span>
    <span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">QuickDBEnum</span>
    <span class="token punctuation">{</span>
        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u6570\u636E\u72B6\u6001\u8BF4\u660E</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token keyword">enum</span> <span class="token class-name">StateFlag</span>
        <span class="token punctuation">{</span>
            <span class="token preprocessor property">#<span class="token directive keyword">region</span> \u5B9A\u4E49</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u5F02\u5E38\u72B6\u6001</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u672A\u914D\u7F6E&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            empty <span class="token operator">=</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u9ED8\u8BA4 \u6240\u6709\u4EBA\u53EF\u6B63\u5E38\u7F16\u8F91</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u6B63\u5E38&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            normal <span class="token operator">=</span> <span class="token number">1</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u5DF2\u5BA1\u6838\uFF0C\u4E0D\u53EF\u7F16\u8F91</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u5DF2\u5BA1&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            audited <span class="token operator">=</span> <span class="token number">2</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u5DF2\u64A4\u9500\u5BA1\u6838\uFF0C\u53EF\u672C\u4EBA\u7F16\u8F91</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u64A4\u5BA1&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            revoked <span class="token operator">=</span> <span class="token number">3</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u5220\u9664\uFF0C\u6240\u6709\u4EBA\u7981\u7528\uFF0C\u7B49\u5F85\u7269\u7406\u5220\u9664</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u5220\u9664&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            deleted <span class="token operator">=</span> <span class="token number">4</span><span class="token punctuation">,</span>

            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u5DF2\u6682\u505C\uFF0C\u6240\u6709\u4EBA\u505C\u6B62\u8BBF\u95EE\u4E0E\u7F16\u8F91</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            <span class="token punctuation">[</span><span class="token function">Description</span><span class="token punctuation">(</span><span class="token string">&quot;\u6682\u505C&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
            paused <span class="token operator">=</span> <span class="token number">5</span>

            <span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>

        <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u6570\u636E\u72B6\u6001 icon-\u540D\u79F0</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token keyword">enum</span> <span class="token class-name">StateFlagIcon</span> <span class="token punctuation">:</span> <span class="token type-list"><span class="token keyword">int</span></span>
        <span class="token punctuation">{</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u672A\u914D\u7F6E</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            gantan <span class="token operator">=</span> <span class="token number">0</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u6B63\u5E38</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            bianji <span class="token operator">=</span> <span class="token number">1</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u5DF2\u5BA1</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            suo <span class="token operator">=</span> <span class="token number">2</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u64A4\u5BA1</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            suokai <span class="token operator">=</span> <span class="token number">3</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u5220\u9664</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            del <span class="token operator">=</span> <span class="token number">4</span><span class="token punctuation">,</span>
            <span class="token comment">/// &lt;summary&gt;</span>
            <span class="token comment">/// \u6682\u505C</span>
            <span class="token comment">/// &lt;/summary&gt;</span>
            clear <span class="token operator">=</span> <span class="token number">5</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br><span class="line-number">52</span><br><span class="line-number">53</span><br><span class="line-number">54</span><br><span class="line-number">55</span><br><span class="line-number">56</span><br><span class="line-number">57</span><br><span class="line-number">58</span><br><span class="line-number">59</span><br><span class="line-number">60</span><br><span class="line-number">61</span><br><span class="line-number">62</span><br><span class="line-number">63</span><br><span class="line-number">64</span><br><span class="line-number">65</span><br><span class="line-number">66</span><br><span class="line-number">67</span><br><span class="line-number">68</span><br><span class="line-number">69</span><br><span class="line-number">70</span><br><span class="line-number">71</span><br><span class="line-number">72</span><br><span class="line-number">73</span><br><span class="line-number">74</span><br><span class="line-number">75</span><br><span class="line-number">76</span><br><span class="line-number">77</span><br><span class="line-number">78</span><br><span class="line-number">79</span><br><span class="line-number">80</span><br></div></div>`,17),c=[t];function o(l,r,u,i,m,k){return s(),a("div",null,c)}var g=n(e,[["render",o]]);export{d as __pageData,g as default};
