import{bm as n,aS as s,ap as a,at as e}from"./plugin-vue_export-helper.ec0df64b.js";const d='{"title":"Quick \u5FEB\u6846\u67B6","description":"","frontmatter":{},"headers":[{"level":2,"title":"\u7248\u672C","slug":"\u7248\u672C"},{"level":2,"title":"\u63D2\u4EF6","slug":"\u63D2\u4EF6"},{"level":2,"title":"net \u6CE8\u91CA\u8F6C Markdown","slug":"net-\u6CE8\u91CA\u8F6C-markdown"},{"level":2,"title":"summary \u6CE8\u91CA\u8BED\u6CD5\u89C4\u8303","slug":"summary-\u6CE8\u91CA\u8BED\u6CD5\u89C4\u8303"},{"level":3,"title":"\u6CE8\u91CA\u53C2\u6570","slug":"\u6CE8\u91CA\u53C2\u6570"},{"level":3,"title":"remarks \u6CE8\u91CA\u5907\u6CE8","slug":"remarks-\u6CE8\u91CA\u5907\u6CE8"},{"level":2,"title":"\u8BED\u6CD5\u89C4\u8303","slug":"\u8BED\u6CD5\u89C4\u8303"},{"level":3,"title":"Model \u5BF9\u8C61\u7C7B\u89C4\u8303","slug":"model-\u5BF9\u8C61\u7C7B\u89C4\u8303"}],"relativePath":"quick/index.md","lastUpdated":1640963883485}',t={},p=e(`<h1 id="quick-\u5FEB\u6846\u67B6" tabindex="-1">Quick \u5FEB\u6846\u67B6 <a class="header-anchor" href="#quick-\u5FEB\u6846\u67B6" aria-hidden="true">#</a></h1><h2 id="\u7248\u672C" tabindex="-1">\u7248\u672C <a class="header-anchor" href="#\u7248\u672C" aria-hidden="true">#</a></h2><p>dotnet --version</p><h2 id="\u63D2\u4EF6" tabindex="-1">\u63D2\u4EF6 <a class="header-anchor" href="#\u63D2\u4EF6" aria-hidden="true">#</a></h2><ul><li>Dapper</li><li>System.Data.SqlClient</li><li>Microsoft.Extensions.Configuration;</li></ul><h2 id="net-\u6CE8\u91CA\u8F6C-markdown" tabindex="-1">net \u6CE8\u91CA\u8F6C Markdown <a class="header-anchor" href="#net-\u6CE8\u91CA\u8F6C-markdown" aria-hidden="true">#</a></h2><ul><li><a href="./6.0/Quick.Docs/MarkDownHelper.html">Quick.Docs.MarkDownHelper</a></li></ul><ol><li>\u7C7B\u5E93\u5C5E\u6027&gt; \u751F\u6210&gt;\u52FE\u9009\u8F93\u51FA xml \u6587\u6863\u3002</li><li>\u5C06\u8F93\u51FA\u7684\u6CE8\u91CA xml \u6587\u4EF6\u5185\u5BB9\uFF0C\u62F7\u8D1D\u5230\uFF1A <code>http://docs.apwlan.com</code> \u8FDB\u884C MD \u6587\u6863\u8F6C\u6362</li></ol><h2 id="summary-\u6CE8\u91CA\u8BED\u6CD5\u89C4\u8303" tabindex="-1">summary \u6CE8\u91CA\u8BED\u6CD5\u89C4\u8303 <a class="header-anchor" href="#summary-\u6CE8\u91CA\u8BED\u6CD5\u89C4\u8303" aria-hidden="true">#</a></h2><ul><li><a href="https://docs.microsoft.com/zh-cn/dotnet/csharp/language-reference/xmldoc/" target="_blank" rel="noopener noreferrer">\u5FAE\u8F6F\u6CE8\u91CA \u8BED\u6CD5</a></li><li><a href="https://vitepress.vuejs.org/guide/frontmatter.html" target="_blank" rel="noopener noreferrer">vitepress \u8BED\u6CD5</a></li><li><a href="https://qve.gitee.io/docs/plugin/markdown.html" target="_blank" rel="noopener noreferrer">MarkDown \u8BED\u6CD5</a></li></ul><div class="warning custom-block"><p class="custom-block-title">WARNING</p><p>\u7F16\u8BD1\u540E\u6B63\u5E38\uFF0C\u8C03\u8BD5\u6A21\u5F0F\u4E0B\u8868\u683C\u4E2D\u6709\u5927\u62EC\u5F27<code>{}</code>\u4F1A\u5BFC\u81F4\u62A5\u9519\uFF0C\u524D\u540E\u52A0\u6807\u7B7E \` \u89E3\u51B3\u3002</p></div><div class="language-csharp line-numbers-mode"><pre><code>
<span class="token keyword">namespace</span> <span class="token namespace">Quick</span>
<span class="token punctuation">{</span>
    <span class="token comment">/// &lt;summary&gt;</span>
    <span class="token comment">/// \u8BF7\u4F7F\u7528\u65B0\u7684 DataSafe</span>
    <span class="token comment">/// &lt;/summary&gt;</span>
    <span class="token comment">/// &lt;remarks&gt;</span>
    <span class="token comment">/// &lt;version&gt;2019.02&lt;/version&gt;</span>
    <span class="token comment">/// &lt;obsolete&gt;\u5F03\u7528\u8BF4\u660E&lt;/obsolete&gt;</span>
    <span class="token comment">/// \u7EE7\u627F &lt;see cref=&quot;QuickUserModel&quot; /&gt;</span>
    <span class="token comment">/// &lt;/remarks&gt;</span>
    <span class="token punctuation">[</span>Obsolete<span class="token punctuation">]</span> <span class="token comment">//\u6807\u8BB0\u8BE5\u65B9\u6CD5\u5DF2\u5F03\u7528</span>
    <span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">QFD</span><span class="token punctuation">:</span> <span class="token type-list"><span class="token class-name">QuickUserModel</span></span>
    <span class="token punctuation">{</span>
        <span class="token comment">// Debug.LogError(&quot;\u4F60\u5E94\u8BE5\u8C03\u7528\u672C\u7C7B\u7684 OpenMessageBox \u65B9\u6CD5&quot;);</span>
    <span class="token punctuation">}</span>

    <span class="token comment">/// &lt;summary&gt;</span>
    <span class="token comment">/// \u6570\u636E\u6743\u9650\u5904\u7406\u65B9\u6CD5</span>
    <span class="token comment">/// &lt;/summary&gt;</span>
    <span class="token comment">/// &lt;remarks&gt;</span>
    <span class="token comment">/// &lt;version&gt;2021.01&lt;/version&gt;</span>
    <span class="token comment">/// &lt;see href=&quot;../code/safe.html&quot;&gt;\u4F7F\u7528\u793A\u4F8B &lt;/see&gt;</span>
    <span class="token comment">/// &lt;see cref=&quot;QuickSafeFilterNo&quot;&gt;QuickSafeFilterNo \u4E0D\u9A8C\u8BC1\u8BF7\u6C42\u53C2\u6570&lt;/see&gt;</span>
    <span class="token comment">/// &lt;/remarks&gt;</span>
    <span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">DataSafe</span>
    <span class="token punctuation">{</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u662F\u5426\u9A8C\u8BC1\u901A\u8FC7</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;remarks&gt;</span>
        <span class="token comment">///  &lt;seealso cref=&quot;bool&quot;/&gt;</span>
        <span class="token comment">/// &lt;/remarks&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">bool</span></span> isPass <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u8FD4\u56DE\u9A8C\u8BC1\u7684\u7C7B\u522B\u7F16\u7801</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;remarks&gt;</span>
        <span class="token comment">/// \u521B\u5EFA\u7C7B\u578B\u8FDE\u63A5\u7528 seealso</span>
        <span class="token comment">/// &lt;seealso cref=&quot;Int32&quot; /&gt;</span>
        <span class="token comment">/// &lt;/remarks&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name">Int32</span> code <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token preprocessor property">#<span class="token directive keyword">region</span> \u914D\u7F6E</span>
        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5F53\u524D\u8FDE\u63A5\u914D\u7F6E</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;remarks&gt;</span>
        <span class="token comment">///  &lt;seealso cref=&quot;RedisConfigModel&quot; /&gt;</span>
        <span class="token comment">/// &lt;/remarks&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name">RedisConfigModel</span> Config <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u8FDE\u63A5\u5BF9\u8C61</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;remarks&gt;</span>
        <span class="token comment">/// type \u4E0D\u521B\u5EFA\u8FDE\u63A5</span>
        <span class="token comment">/// &lt;type&gt;StackExchange.Redis.ConnectionMultiplexer&lt;/type&gt;</span>
        <span class="token comment">/// &lt;/remarks&gt;</span>
        <span class="token keyword">public</span> <span class="token class-name">ConnectionMultiplexer</span> Conn<span class="token punctuation">;</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u81EA\u5B9A\u4E49 \u4E3B\u952EKey\u524D\u7F00:</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;remarks&gt;</span>
        <span class="token comment">///  &lt;seealso cref=&quot;string&quot; /&gt;</span>
        <span class="token comment">/// &lt;/remarks&gt;</span>
        <span class="token keyword">public</span> <span class="token class-name"><span class="token keyword">string</span></span> CustomKey<span class="token punctuation">;</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u8FD4\u56DE\u6570\u636E\u8FDE\u63A5\u5BF9\u8C61</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;remarks&gt;</span>
        <span class="token comment">/// &lt;see cref=&quot;ConnectionMultiplexer.GetDatabase(int, object)&quot;/&gt;</span>
        <span class="token comment">/// &lt;type&gt;StackExchange.Redis.IDatabase&lt;/type&gt;</span>
        <span class="token comment">/// &lt;/remarks&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name">IDatabase</span> db
        <span class="token punctuation">{</span>
            <span class="token keyword">get</span> <span class="token punctuation">{</span> <span class="token keyword">return</span> Conn<span class="token punctuation">.</span><span class="token function">GetDatabase</span><span class="token punctuation">(</span>Config<span class="token punctuation">.</span>DBId<span class="token punctuation">)</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>
        <span class="token punctuation">}</span>


        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u4E0D\u7B49\u4E8E</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">///  &lt;remarks&gt;</span>
        <span class="token comment">/// \u6CE8\u91CA\u5305\u542B\u7279\u6B8A\u7B26\u53F7\u6309\u4EE5\u4E0B\u65B9\u5F0F\u4E66\u5199</span>
        <span class="token comment">/// &lt;![CDATA[ &lt;&gt; ]]&gt;</span>
        <span class="token comment">/// &lt;/remarks&gt;</span>
        <span class="token keyword">public</span> <span class="token keyword">const</span> <span class="token class-name"><span class="token keyword">string</span></span> NotEqualTo <span class="token operator">=</span> <span class="token string">&quot;&lt;&gt;&quot;</span><span class="token punctuation">;</span>

        <span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// This sample shows how to specify</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;remarks&gt;</span>
        <span class="token comment">/// the  attribute.</span>
        <span class="token comment">/// &lt;/remarks&gt;</span>
        <span class="token keyword">public</span> <span class="token function">DataSafe</span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> _CustomKey<span class="token punctuation">)</span>
        <span class="token punctuation">{</span>
           CustomKey<span class="token operator">=</span>_CustomKey
        <span class="token punctuation">}</span>
    <span class="token punctuation">}</span>



    <span class="token comment">/// &lt;summary&gt;</span>
    <span class="token comment">/// \u4EE3\u7801\u793A\u4F8B\u8BF4\u660E</span>
    <span class="token comment">/// &lt;/summary&gt;</span>
    <span class="token comment">/// &lt;example&gt;</span>
    <span class="token comment">/// This sample shows how to call the &lt;see cref=&quot;GetZero&quot;/&gt; method.</span>
    <span class="token comment">/// &lt;code&gt;</span>
    <span class="token comment">/// class TestClass</span>
    <span class="token comment">/// {</span>
    <span class="token comment">///     static int Main()</span>
    <span class="token comment">///     {</span>
    <span class="token comment">///         return GetZero();</span>
    <span class="token comment">///     }</span>
    <span class="token comment">/// }</span>
    <span class="token comment">/// &lt;/code&gt;</span>
    <span class="token comment">/// &lt;/example&gt;</span>
    <span class="token keyword">public</span> <span class="token keyword">static</span> <span class="token return-type class-name"><span class="token keyword">int</span></span> <span class="token function">GetZero</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
    <span class="token punctuation">{</span>
        <span class="token keyword">return</span> <span class="token number">0</span><span class="token punctuation">;</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br><span class="line-number">52</span><br><span class="line-number">53</span><br><span class="line-number">54</span><br><span class="line-number">55</span><br><span class="line-number">56</span><br><span class="line-number">57</span><br><span class="line-number">58</span><br><span class="line-number">59</span><br><span class="line-number">60</span><br><span class="line-number">61</span><br><span class="line-number">62</span><br><span class="line-number">63</span><br><span class="line-number">64</span><br><span class="line-number">65</span><br><span class="line-number">66</span><br><span class="line-number">67</span><br><span class="line-number">68</span><br><span class="line-number">69</span><br><span class="line-number">70</span><br><span class="line-number">71</span><br><span class="line-number">72</span><br><span class="line-number">73</span><br><span class="line-number">74</span><br><span class="line-number">75</span><br><span class="line-number">76</span><br><span class="line-number">77</span><br><span class="line-number">78</span><br><span class="line-number">79</span><br><span class="line-number">80</span><br><span class="line-number">81</span><br><span class="line-number">82</span><br><span class="line-number">83</span><br><span class="line-number">84</span><br><span class="line-number">85</span><br><span class="line-number">86</span><br><span class="line-number">87</span><br><span class="line-number">88</span><br><span class="line-number">89</span><br><span class="line-number">90</span><br><span class="line-number">91</span><br><span class="line-number">92</span><br><span class="line-number">93</span><br><span class="line-number">94</span><br><span class="line-number">95</span><br><span class="line-number">96</span><br><span class="line-number">97</span><br><span class="line-number">98</span><br><span class="line-number">99</span><br><span class="line-number">100</span><br><span class="line-number">101</span><br><span class="line-number">102</span><br><span class="line-number">103</span><br><span class="line-number">104</span><br><span class="line-number">105</span><br><span class="line-number">106</span><br><span class="line-number">107</span><br><span class="line-number">108</span><br><span class="line-number">109</span><br><span class="line-number">110</span><br><span class="line-number">111</span><br><span class="line-number">112</span><br><span class="line-number">113</span><br><span class="line-number">114</span><br><span class="line-number">115</span><br><span class="line-number">116</span><br><span class="line-number">117</span><br><span class="line-number">118</span><br><span class="line-number">119</span><br><span class="line-number">120</span><br><span class="line-number">121</span><br><span class="line-number">122</span><br><span class="line-number">123</span><br><span class="line-number">124</span><br><span class="line-number">125</span><br><span class="line-number">126</span><br><span class="line-number">127</span><br><span class="line-number">128</span><br><span class="line-number">129</span><br></div></div><h3 id="\u6CE8\u91CA\u53C2\u6570" tabindex="-1">\u6CE8\u91CA\u53C2\u6570 <a class="header-anchor" href="#\u6CE8\u91CA\u53C2\u6570" aria-hidden="true">#</a></h3><table><thead><tr><th>\u547D\u540D</th><th>\u8BF4\u660E</th><th>\u63CF\u8FF0</th></tr></thead><tbody><tr><td>summary</td><td>\u6CE8\u91CA</td><td>\u7B80\u8981\u8BF4\u660E</td></tr><tr><td>remarks</td><td>\u5907\u6CE8</td><td>\u8BF4\u660E\u7EC4\u4EF6\u7684\u4F5C\u7528</td></tr><tr><td>param</td><td>\u53C2\u6570</td><td></td></tr><tr><td>returns</td><td>\u8FD4\u56DE</td><td></td></tr><tr><td>obsolete</td><td>\u6807\u8BB0\u5F03\u7528</td><td>\u6B64\u65B9\u6CD5\uFF0C\u7528\u4E8E\u63D0\u793A\u66F4\u65B0</td></tr></tbody></table><h3 id="remarks-\u6CE8\u91CA\u5907\u6CE8" tabindex="-1">remarks \u6CE8\u91CA\u5907\u6CE8 <a class="header-anchor" href="#remarks-\u6CE8\u91CA\u5907\u6CE8" aria-hidden="true">#</a></h3><table><thead><tr><th>\u547D\u540D</th><th>\u8BF4\u660E</th><th>\u63CF\u8FF0</th></tr></thead><tbody><tr><td>version</td><td>\u7248\u672C\u6807\u8BB0</td><td>\u7528\u4E8E\u5E38\u7528\u51FD\u6570\u66F4\u65B0\u6807\u8BB0</td></tr><tr><td>obsolete</td><td>\u5F03\u7528\u8BF4\u660E</td><td>\u5EFA\u8BAE\u8DDF\u7248\u672C\u914D\u5408\uFF0C\u7528\u4E8E\u533A\u522B\u90A3\u4E2A\u7248\u672C\u88AB\u5F03\u7528</td></tr><tr><td>see</td><td>cref</td><td><a href="https://docs.microsoft.com/zh-cn/dotnet/csharp/programming-guide/xmldoc/cref-attribute" target="_blank" rel="noopener noreferrer">cref \u4EE3\u7801\u5F15\u7528</a></td></tr><tr><td>see</td><td>href</td><td>\u5E2E\u52A9\u8FDE\u63A5\u7B49</td></tr><tr><td>seealso</td><td>\u65B9\u6CD5\u8DDF see \u4E00\u81F4\uFF0C\u672C\u9879\u76EE\u4EC5\u7528\u4E8E\u5B9A\u4E49\u5C5E\u6027\u7C7B\u578B</td><td>\u6307\u5B9A\u5E0C\u671B\u5728\u201C\u8BF7\u53C2\u89C1\u201D\u4E00\u8282\u4E2D\u51FA\u73B0</td></tr><tr><td>type</td><td>\u5B9A\u4E49\u8FD4\u56DE\u7684\u6570\u636E\u7C7B\u578B\uFF0C\u6CE8\u91CA\u9ED8\u8BA4\u4E3A\u7A7A</td><td>\u5F15\u7528\u5168\u8DEF\u5F84</td></tr></tbody></table><h2 id="\u8BED\u6CD5\u89C4\u8303" tabindex="-1">\u8BED\u6CD5\u89C4\u8303 <a class="header-anchor" href="#\u8BED\u6CD5\u89C4\u8303" aria-hidden="true">#</a></h2><div class="warning custom-block"><p class="custom-block-title">WARNING</p><p>\u6587\u6863\u6807\u9898\u7531\u53C2\u6570\u540D\u4E0E\u6CE8\u91CA\u7B2C\u4E00\u884C\u7EC4\u6210\uFF0C\u5EFA\u8BAE\u529F\u80FD\u63CF\u8FF0\u6362\u884C</p></div><p>\u547D\u540D\u4F7F\u7528\u9A7C\u5CF0\u5F62\u5F0F</p><ul><li>\u53D8\u91CF\u5B9A\u4E49 <ul><li>\u7C7B\u53D8\u91CF\u5C0F\u5199\u5F00\u5934</li><li>\u65B9\u6CD5\u5185\u4E34\u65F6\u53D8\u91CF\uFF0C\u7528_\u5F00\u5934</li></ul></li><li>\u5927\u5199\u5F00\u5934\u540D\u79F0 <ul><li>\u7C7B\u540D</li><li>\u65B9\u6CD5\u540D</li></ul></li></ul><h3 id="model-\u5BF9\u8C61\u7C7B\u89C4\u8303" tabindex="-1">Model \u5BF9\u8C61\u7C7B\u89C4\u8303 <a class="header-anchor" href="#model-\u5BF9\u8C61\u7C7B\u89C4\u8303" aria-hidden="true">#</a></h3><p>\u5B9A\u4E49\u5B9E\u4F53\u5BF9\u8C61\u5EFA\u8BAE\u540D\u79F0\u7ED3\u5C3E\u52A0\u5165 Model</p><div class="language-csharp line-numbers-mode"><pre><code><span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">TestModel</span>
<span class="token punctuation">{</span>

   <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">string</span></span> name<span class="token punctuation">{</span><span class="token keyword">get</span><span class="token punctuation">;</span><span class="token keyword">set</span><span class="token punctuation">}</span>

   <span class="token keyword">public</span> <span class="token function">TestModel</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">:</span> <span class="token keyword">this</span><span class="token punctuation">(</span><span class="token string">&quot;\u9ED8\u8BA4\u540D&quot;</span><span class="token punctuation">)</span>
   <span class="token punctuation">{</span>

   <span class="token punctuation">}</span>

   <span class="token keyword">public</span> <span class="token function">TestModel</span><span class="token punctuation">(</span><span class="token class-name"><span class="token keyword">string</span></span> _name<span class="token punctuation">)</span><span class="token punctuation">{</span>
     name<span class="token operator">=</span>_name
   <span class="token punctuation">}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br></div></div>`,23),l=[p];function c(r,o,m,u,i,b){return s(),a("div",null,l)}var g=n(t,[["render",c]]);export{d as __pageData,g as default};
