import{bm as n,aS as s,ap as a,at as p}from"./plugin-vue_export-helper.ec0df64b.js";const d='{"title":"Quick","description":"","frontmatter":{},"headers":[{"level":2,"title":"nuget \u53D1\u884C\u5305","slug":"nuget-\u53D1\u884C\u5305"},{"level":2,"title":"Net 6 Program \u542F\u52A8\u914D\u7F6E","slug":"net-6-program-\u542F\u52A8\u914D\u7F6E"}],"relativePath":"quick/6.0/index.md","lastUpdated":1647739163503}',e={},t=p(`<h1 id="quick" tabindex="-1">Quick <a class="header-anchor" href="#quick" aria-hidden="true">#</a></h1><p>Net 6.0 \u4E0E Net 5 \u5BF9\u6BD4\u4F1A\u66F4\u7B80\u6D01\uFF0C\u4EE3\u7801\u91CF\u66F4\u5C11\u3002</p><ul><li><a href="https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/startup?view=aspnetcore-6.0" target="_blank" rel="noopener noreferrer">\u5B98\u65B9\u6587\u6863</a></li></ul><h2 id="nuget-\u53D1\u884C\u5305" tabindex="-1">nuget \u53D1\u884C\u5305 <a class="header-anchor" href="#nuget-\u53D1\u884C\u5305" aria-hidden="true">#</a></h2><ul><li><a href="https://www.nuget.org/packages/quick" target="_blank" rel="noopener noreferrer">Quick \u57FA\u7840\u7C7B\u5E93\u5305</a></li><li><a href="https://www.nuget.org/packages/quick.db" target="_blank" rel="noopener noreferrer">Quick.DB \u6570\u636E\u5E93\u4EA4\u4E92\u7C7B\u5E93</a></li></ul><h2 id="net-6-program-\u542F\u52A8\u914D\u7F6E" tabindex="-1">Net 6 Program \u542F\u52A8\u914D\u7F6E <a class="header-anchor" href="#net-6-program-\u542F\u52A8\u914D\u7F6E" aria-hidden="true">#</a></h2><ul><li>Program.cs</li></ul><div class="language-csharp line-numbers-mode"><pre><code><span class="token keyword">using</span> <span class="token namespace">Quick</span><span class="token punctuation">;</span>
<span class="token keyword">using</span> <span class="token namespace">Quick<span class="token punctuation">.</span>BLL</span><span class="token punctuation">;</span>
<span class="token keyword">using</span> <span class="token namespace">Quick<span class="token punctuation">.</span>Safe</span><span class="token punctuation">;</span>

<span class="token class-name"><span class="token keyword">var</span></span> builder <span class="token operator">=</span> WebApplication<span class="token punctuation">.</span><span class="token function">CreateBuilder</span><span class="token punctuation">(</span>args<span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token preprocessor property">#<span class="token directive keyword">region</span> \u4E2D\u95F4\u4EF6</span>

<span class="token comment">// \u6CE8\u518CQuickCache \u7F13\u5B58</span>
QuickCache<span class="token punctuation">.</span>Memory <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token constructor-invocation class-name">QuickMemoryProvider</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token comment">// \u81EA\u5B9A\u4E49\u914D\u7F6E\u53C2\u8003\uFF0Cusing Microsoft.Extensions.Caching.Memory;</span>
<span class="token comment">//QuickCache.Memory = new QuickMemoryProvider(new MemoryCacheOptions()</span>
<span class="token comment">//{</span>
<span class="token comment">//    //\u7F13\u5B58\u6700\u5927\u4E3A100\u4EFD</span>
<span class="token comment">//    //##\u6CE8\u610Fnetcore\u4E2D\u7684\u7F13\u5B58\u662F\u6CA1\u6709\u5355\u4F4D\u7684\uFF0C\u7F13\u5B58\u9879\u548C\u7F13\u5B58\u7684\u76F8\u5BF9\u5173\u7CFB</span>
<span class="token comment">//    SizeLimit = 100,</span>
<span class="token comment">//    //\u7F13\u5B58\u6EE1\u4E86\u65F6\uFF0C\u538B\u7F2920%\uFF08\u5373\u5220\u966420\u4EFD\u4F18\u5148\u7EA7\u4F4E\u7684\u7F13\u5B58\u9879\uFF09</span>
<span class="token comment">//    CompactionPercentage = 0.2,</span>
<span class="token comment">//    //3\u79D2\u949F\u67E5\u627E\u4E00\u6B21\u8FC7\u671F\u9879</span>
<span class="token comment">//    ExpirationScanFrequency = TimeSpan.FromSeconds(3)</span>
<span class="token comment">//});</span>

<span class="token comment">// \u6CE8\u5165\uFF08DI\uFF09\u5728\u540C\u4E00\u4E2A\u8BF7\u6C42\u53D1\u8D77\u5230\u7ED3\u675F\uFF0C\u5728\u670D\u52A1\u5BB9\u5668\u4E2D\u6CE8\u518C\u81EA\u5B9A\u4E49\u4E2D\u95F4\u4EF6\u7528\u6237\u7C7B</span>
builder<span class="token punctuation">.</span>Services<span class="token punctuation">.</span><span class="token function">AddScoped</span><span class="token punctuation">(</span><span class="token keyword">typeof</span><span class="token punctuation">(</span><span class="token type-expression class-name">QuickSafeFilterModel</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token comment">//\u6CE8\u518C\u8FC7\u6EE4\u5668\u670D\u52A1\uFF0C\u4F7F\u7528ServiceFilter \u65B9\u5F0F\u5FC5\u987B\u8981\u6CE8\u518C \u5426\u5219\u4F1A\u62A5\u6CA1\u6709\u6CE8\u518C\u8BE5\u670D\u52A1\u7684\u76F8\u5173\u5F02\u5E38</span>
<span class="token comment">//services.AddSingleton&lt;QuickAuthFilter&gt;();</span>
<span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>

<span class="token preprocessor property">#<span class="token directive keyword">region</span> \u8DE8\u57DF</span>

<span class="token comment">// \u6388\u6743\u8DE8\u57DF\u7684\u57DF\u540D\u914D\u7F6E</span>
<span class="token class-name"><span class="token keyword">string</span><span class="token punctuation">[</span><span class="token punctuation">]</span></span> _orgins <span class="token operator">=</span> QuickSettings<span class="token punctuation">.</span>Session<span class="token punctuation">.</span>WithOrigins<span class="token punctuation">;</span>
<span class="token comment">// \u8DE8\u57DF\u89C4\u5219\u7684\u540D\u79F0 APi \u7981\u6B62\u8DE8\u57DF  [DisableCors] \u5355\u72EC\u8DE8\u57DF [EnableCors(&quot;AllowCors&quot;)]</span>
builder<span class="token punctuation">.</span>Services<span class="token punctuation">.</span><span class="token function">AddCors</span><span class="token punctuation">(</span>options <span class="token operator">=&gt;</span> options<span class="token punctuation">.</span><span class="token function">AddPolicy</span><span class="token punctuation">(</span><span class="token string">&quot;AllowCors&quot;</span><span class="token punctuation">,</span> builder <span class="token operator">=&gt;</span>
<span class="token punctuation">{</span>
    <span class="token comment">//builder.AllowAnyOrigin() //\u5141\u8BB8\u4EFB\u610Forigin</span>
    <span class="token comment">//.AllowCredentials(); // \u53EF\u4EE5\u63A5\u6536cookie</span>

    builder<span class="token punctuation">.</span><span class="token function">AllowAnyHeader</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token comment">//\u5141\u8BB8\u4EFB\u610Fheader</span>
              <span class="token punctuation">.</span><span class="token function">AllowAnyMethod</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token comment">//\u5141\u8BB8\u4EFB\u610F\u8BF7\u6C42\u65B9\u5F0F</span>
                               <span class="token comment">//.AllowAnyOrigin() //\u5141\u8BB8\u4EFB\u610Forigin</span>
            <span class="token punctuation">.</span><span class="token function">WithOrigins</span><span class="token punctuation">(</span>_orgins<span class="token punctuation">)</span> <span class="token comment">//\u6307\u5B9A\u7279\u5B9A\u57DF\u540D\u624D\u80FD\u8BBF\u95EE</span>
            <span class="token punctuation">.</span><span class="token function">SetPreflightMaxAge</span><span class="token punctuation">(</span>TimeSpan<span class="token punctuation">.</span><span class="token function">FromSeconds</span><span class="token punctuation">(</span><span class="token number">60</span><span class="token punctuation">)</span><span class="token punctuation">)</span>    <span class="token comment">//\u5141\u8BB8\u9A8C\u8BC1OPTIONS\u8BF7\u6C42\u8FDB\u884C\u68C0\u6D4B\u65F6\u95F4</span>
            <span class="token punctuation">.</span><span class="token function">SetIsOriginAllowedToAllowWildcardSubdomains</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
           <span class="token punctuation">.</span><span class="token function">AllowCredentials</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span> <span class="token comment">// \u6307\u5B9A\u63A5\u6536cookie</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>

<span class="token preprocessor property">#<span class="token directive keyword">region</span> Session \u8DE8\u57DF\u4F1A\u8BDD\u4FDD\u6301</span>
builder<span class="token punctuation">.</span>Services<span class="token punctuation">.</span><span class="token function">AddSession</span><span class="token punctuation">(</span>option <span class="token operator">=&gt;</span>
<span class="token punctuation">{</span>
    option<span class="token punctuation">.</span>IOTimeout <span class="token operator">=</span> TimeSpan<span class="token punctuation">.</span><span class="token function">FromMinutes</span><span class="token punctuation">(</span>QuickSettings<span class="token punctuation">.</span>Session<span class="token punctuation">.</span>Cookie<span class="token punctuation">.</span>Timeout<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token comment">// \u4F1A\u8BDD\u8D85\u65F6\u5206\u949F\u914D\u7F6E</span>
    option<span class="token punctuation">.</span>IdleTimeout <span class="token operator">=</span> TimeSpan<span class="token punctuation">.</span><span class="token function">FromMinutes</span><span class="token punctuation">(</span>QuickSettings<span class="token punctuation">.</span>Session<span class="token punctuation">.</span>Cookie<span class="token punctuation">.</span>Timeout<span class="token punctuation">)</span><span class="token punctuation">;</span>
    option<span class="token punctuation">.</span>Cookie<span class="token punctuation">.</span>Name <span class="token operator">=</span> QuickSettings<span class="token punctuation">.</span>Session<span class="token punctuation">.</span>Cookie<span class="token punctuation">.</span>Name<span class="token punctuation">;</span>
    <span class="token comment">//js\u65E0\u6CD5\u83B7\u5F97cookie \uFF0C\u9632XSS\u653B\u51FB</span>
    option<span class="token punctuation">.</span>Cookie<span class="token punctuation">.</span>HttpOnly <span class="token operator">=</span> <span class="token boolean">true</span><span class="token punctuation">;</span>
    <span class="token comment">// \u4F7F\u7528</span>
    option<span class="token punctuation">.</span>Cookie<span class="token punctuation">.</span>IsEssential <span class="token operator">=</span> <span class="token boolean">true</span><span class="token punctuation">;</span>

    <span class="token comment">// \u4F7F\u7528\u5BBD\u677E\u6A21\u5F0F\uFF0C\u540C\u4E00\u7AD9\u70B9\u4FDD\u6301\u4F1A\u8BDD</span>
    <span class="token comment">//  option.Cookie.SameSite = SameSiteMode.Unspecified; //.Lax.None.Strict;</span>
    option<span class="token punctuation">.</span>Cookie<span class="token punctuation">.</span>SameSite <span class="token operator">=</span> SameSiteMode<span class="token punctuation">.</span>None<span class="token punctuation">;</span>
    <span class="token comment">//\u8BBE\u7F6E\u5B58\u50A8\u7528\u6237\u767B\u5F55\u4FE1\u606F\uFF08\u7528\u6237Token\u4FE1\u606F\uFF09\u7684Cookie\uFF0C\u53EA\u4F1A\u901A\u8FC7HTTPS\u534F\u8BAE\u4F20\u9012\uFF0C\u5982\u679C\u662FHTTP\u534F\u8BAE\uFF0CCookie\u4E0D\u4F1A\u88AB\u53D1\u9001\u3002\u6CE8\u610F\uFF0Coption.Cookie.SecurePolicy\u5C5E\u6027\u7684\u9ED8\u8BA4\u503C\u662FMicrosoft.AspNetCore.Http.CookieSecurePolicy.SameAsRequest</span>
    option<span class="token punctuation">.</span>Cookie<span class="token punctuation">.</span>SecurePolicy <span class="token operator">=</span> CookieSecurePolicy<span class="token punctuation">.</span>Always<span class="token punctuation">;</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>

<span class="token preprocessor property">#<span class="token directive keyword">region</span> \u5168\u5C40\u6743\u9650\u9A8C\u8BC1</span>
builder<span class="token punctuation">.</span>Services<span class="token punctuation">.</span><span class="token function">AddControllersWithViews</span><span class="token punctuation">(</span>options <span class="token operator">=&gt;</span>
<span class="token punctuation">{</span>
    <span class="token comment">// \u5168\u5C40\u6CE8\u518C\u6743\u9650\u9A8C\u8BC1\u8FC7\u6EE4\u5668</span>
    options<span class="token punctuation">.</span>Filters<span class="token punctuation">.</span><span class="token function">Add</span><span class="token punctuation">(</span><span class="token keyword">typeof</span><span class="token punctuation">(</span><span class="token type-expression class-name">QuickSafeFilter</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">AddJsonOptions</span><span class="token punctuation">(</span>configure <span class="token operator">=&gt;</span>
<span class="token punctuation">{</span>
    <span class="token comment">// \u5168\u5C40Json \u65F6\u95F4\u683C\u5F0F\u5316\uFF0C\u907F\u514D\u524D\u7AEFISO\u8F6C\u6362</span>
    configure<span class="token punctuation">.</span>JsonSerializerOptions<span class="token punctuation">.</span>Converters<span class="token punctuation">.</span><span class="token function">Add</span><span class="token punctuation">(</span><span class="token keyword">new</span> <span class="token constructor-invocation class-name">JsonHelperConvert<span class="token punctuation">.</span>DateTimeConverter</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>

<span class="token comment">// \u6DFB\u52A0\u670D\u52A1\u914D\u7F6E</span>
<span class="token comment">//builder.Services.AddControllersWithViews();</span>

<span class="token class-name"><span class="token keyword">var</span></span> app <span class="token operator">=</span> builder<span class="token punctuation">.</span><span class="token function">Build</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token comment">// Configure the HTTP request pipeline.</span>

<span class="token preprocessor property">#<span class="token directive keyword">region</span> \u81EA\u5B9A\u4E49\u914D\u7F6E</span>

<span class="token comment">// \u652F\u6301\u9ED8\u8BA4\u6587\u4EF6 index.html</span>
app<span class="token punctuation">.</span><span class="token function">UseDefaultFiles</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token comment">// \u9759\u6001\u6587\u4EF6</span>
app<span class="token punctuation">.</span><span class="token function">UseStaticFiles</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token comment">// \u8DE8\u57DF</span>
app<span class="token punctuation">.</span><span class="token function">UseCors</span><span class="token punctuation">(</span><span class="token string">&quot;AllowCors&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token comment">// Session</span>
app<span class="token punctuation">.</span><span class="token function">UseSession</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>

<span class="token preprocessor property">#<span class="token directive keyword">region</span> \u9ED8\u8BA4\u8DEF\u7531\u914D\u7F6E</span>
<span class="token comment">//app.MapGet(&quot;/hi&quot;, () =&gt; &quot;Hello!&quot;);</span>

app<span class="token punctuation">.</span><span class="token function">MapControllerRoute</span><span class="token punctuation">(</span><span class="token string">&quot;Page&quot;</span><span class="token punctuation">,</span> <span class="token string">&quot;/Page/{*path}&quot;</span><span class="token punctuation">,</span> <span class="token keyword">new</span>
<span class="token punctuation">{</span>
    controller <span class="token operator">=</span> <span class="token string">&quot;Page&quot;</span><span class="token punctuation">,</span>
    action <span class="token operator">=</span> <span class="token string">&quot;Index&quot;</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token comment">// \u81EA\u5B9A\u4E49 vue \u672C\u5730\u8DEF\u5F84</span>
app<span class="token punctuation">.</span><span class="token function">MapControllerRoute</span><span class="token punctuation">(</span><span class="token string">&quot;Vues&quot;</span><span class="token punctuation">,</span> <span class="token string">&quot;/Vue/{*path}&quot;</span><span class="token punctuation">,</span> <span class="token keyword">new</span>
<span class="token punctuation">{</span>
    controller <span class="token operator">=</span> <span class="token string">&quot;Vue&quot;</span><span class="token punctuation">,</span>
    action <span class="token operator">=</span> <span class="token string">&quot;Index&quot;</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token comment">// \u81EA\u5B9A\u4E49 \u6570\u636E\u7F16\u8F91\u63A5\u53E3</span>
app<span class="token punctuation">.</span><span class="token function">MapControllerRoute</span><span class="token punctuation">(</span><span class="token string">&quot;DB&quot;</span><span class="token punctuation">,</span> <span class="token string">&quot;/DB/{*path}&quot;</span><span class="token punctuation">,</span> <span class="token keyword">new</span>
<span class="token punctuation">{</span>
    controller <span class="token operator">=</span> <span class="token string">&quot;DB&quot;</span><span class="token punctuation">,</span>
    action <span class="token operator">=</span> <span class="token string">&quot;Index&quot;</span><span class="token punctuation">,</span> <span class="token comment">//\u6307\u5B9A\u63A5\u6536\u52A8\u4F5C</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token comment">// \u81EA\u5B9A\u4E49 \u6570\u636E\u7F16\u8F91\u63A5\u53E3</span>
app<span class="token punctuation">.</span><span class="token function">MapControllerRoute</span><span class="token punctuation">(</span><span class="token string">&quot;DI&quot;</span><span class="token punctuation">,</span> <span class="token string">&quot;/DI/{*path}&quot;</span><span class="token punctuation">,</span> <span class="token keyword">new</span>
<span class="token punctuation">{</span>
    controller <span class="token operator">=</span> <span class="token string">&quot;DI&quot;</span><span class="token punctuation">,</span>
    action <span class="token operator">=</span> <span class="token string">&quot;Index&quot;</span><span class="token punctuation">,</span> <span class="token comment">//\u6307\u5B9A\u63A5\u6536\u52A8\u4F5C</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>


<span class="token comment">// \u6807\u51C6MVC \u63A7\u5236\u5668</span>
<span class="token comment">//app.MapControllerRoute(</span>
<span class="token comment">//    name: &quot;default&quot;,</span>
<span class="token comment">//    pattern: &quot;{controller=Home}/{action=Index}/{id?}&quot;);</span>

app<span class="token punctuation">.</span><span class="token function">MapDefaultControllerRoute</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

<span class="token preprocessor property">#</span><span class="token return-type class-name">endregion</span>

app<span class="token punctuation">.</span><span class="token function">Run</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br><span class="line-number">52</span><br><span class="line-number">53</span><br><span class="line-number">54</span><br><span class="line-number">55</span><br><span class="line-number">56</span><br><span class="line-number">57</span><br><span class="line-number">58</span><br><span class="line-number">59</span><br><span class="line-number">60</span><br><span class="line-number">61</span><br><span class="line-number">62</span><br><span class="line-number">63</span><br><span class="line-number">64</span><br><span class="line-number">65</span><br><span class="line-number">66</span><br><span class="line-number">67</span><br><span class="line-number">68</span><br><span class="line-number">69</span><br><span class="line-number">70</span><br><span class="line-number">71</span><br><span class="line-number">72</span><br><span class="line-number">73</span><br><span class="line-number">74</span><br><span class="line-number">75</span><br><span class="line-number">76</span><br><span class="line-number">77</span><br><span class="line-number">78</span><br><span class="line-number">79</span><br><span class="line-number">80</span><br><span class="line-number">81</span><br><span class="line-number">82</span><br><span class="line-number">83</span><br><span class="line-number">84</span><br><span class="line-number">85</span><br><span class="line-number">86</span><br><span class="line-number">87</span><br><span class="line-number">88</span><br><span class="line-number">89</span><br><span class="line-number">90</span><br><span class="line-number">91</span><br><span class="line-number">92</span><br><span class="line-number">93</span><br><span class="line-number">94</span><br><span class="line-number">95</span><br><span class="line-number">96</span><br><span class="line-number">97</span><br><span class="line-number">98</span><br><span class="line-number">99</span><br><span class="line-number">100</span><br><span class="line-number">101</span><br><span class="line-number">102</span><br><span class="line-number">103</span><br><span class="line-number">104</span><br><span class="line-number">105</span><br><span class="line-number">106</span><br><span class="line-number">107</span><br><span class="line-number">108</span><br><span class="line-number">109</span><br><span class="line-number">110</span><br><span class="line-number">111</span><br><span class="line-number">112</span><br><span class="line-number">113</span><br><span class="line-number">114</span><br><span class="line-number">115</span><br><span class="line-number">116</span><br><span class="line-number">117</span><br><span class="line-number">118</span><br><span class="line-number">119</span><br><span class="line-number">120</span><br><span class="line-number">121</span><br><span class="line-number">122</span><br><span class="line-number">123</span><br><span class="line-number">124</span><br><span class="line-number">125</span><br><span class="line-number">126</span><br><span class="line-number">127</span><br><span class="line-number">128</span><br><span class="line-number">129</span><br><span class="line-number">130</span><br><span class="line-number">131</span><br><span class="line-number">132</span><br><span class="line-number">133</span><br><span class="line-number">134</span><br><span class="line-number">135</span><br><span class="line-number">136</span><br><span class="line-number">137</span><br><span class="line-number">138</span><br><span class="line-number">139</span><br><span class="line-number">140</span><br><span class="line-number">141</span><br><span class="line-number">142</span><br><span class="line-number">143</span><br><span class="line-number">144</span><br><span class="line-number">145</span><br><span class="line-number">146</span><br><span class="line-number">147</span><br><span class="line-number">148</span><br></div></div>`,8),o=[t];function c(l,u,r,i,k,b){return s(),a("div",null,o)}var g=n(e,[["render",c]]);export{d as __pageData,g as default};
