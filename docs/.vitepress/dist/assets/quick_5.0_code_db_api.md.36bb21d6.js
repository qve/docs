import{bm as n,aS as s,ap as a,at as p}from"./plugin-vue_export-helper.ec0df64b.js";const d='{"title":"API \u6570\u636E\u63A5\u53E3","description":"","frontmatter":{},"headers":[{"level":2,"title":"API \u63A7\u5236\u5668","slug":"api-\u63A7\u5236\u5668"},{"level":3,"title":"http get \u53C2\u6570","slug":"http-get-\u53C2\u6570"},{"level":3,"title":"http where \u53C2\u6570","slug":"http-where-\u53C2\u6570"},{"level":2,"title":"API \u524D\u7AEF\u53C2\u6570","slug":"api-\u524D\u7AEF\u53C2\u6570"},{"level":2,"title":"Http Get","slug":"http-get"},{"level":2,"title":"http post \u589E\u5220\u6539","slug":"http-post-\u589E\u5220\u6539"},{"level":2,"title":"DBController","slug":"dbcontroller"},{"level":2,"title":"PagerTplModel","slug":"pagertplmodel"},{"level":2,"title":"PagerTplOptsModel","slug":"pagertploptsmodel"},{"level":2,"title":"CRUD Api \u589E\u5220\u6539","slug":"crud-api-\u589E\u5220\u6539"},{"level":2,"title":"Join \u5916\u952E\u67E5\u8BE2\u914D\u7F6E","slug":"join-\u5916\u952E\u67E5\u8BE2\u914D\u7F6E"},{"level":2,"title":"DI \u6570\u636E\u5E94\u7528\u63A5\u53E3","slug":"di-\u6570\u636E\u5E94\u7528\u63A5\u53E3"},{"level":3,"title":"Startup \u63A5\u53E3\u914D\u7F6E","slug":"startup-\u63A5\u53E3\u914D\u7F6E"}],"relativePath":"quick/5.0/code/db_api.md","lastUpdated":1637823141612}',t={},e=p(`<h1 id="api-\u6570\u636E\u63A5\u53E3" tabindex="-1">API \u6570\u636E\u63A5\u53E3 <a class="header-anchor" href="#api-\u6570\u636E\u63A5\u53E3" aria-hidden="true">#</a></h1><ul><li><p><a href="https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/first-web-api?view=aspnetcore-5.0&amp;tabs=visual-studio" target="_blank" rel="noopener noreferrer">\u5B98\u65B9 \u5165\u95E8</a></p></li><li><p><a href="https://docs.microsoft.com/zh-cn/aspnet/core/web-api/advanced/formatting?view=aspnetcore-5.0" target="_blank" rel="noopener noreferrer">\u5B98\u65B9 \u54CD\u5E94\u6570\u636E\u7684\u683C\u5F0F</a></p></li><li><p><a href="./safe.html">QuickSafeFilter</a> \u63A5\u53E3\u6743\u9650\u9A8C\u8BC1\uFF0C\u53D6\u51FA\u8EAB\u4EFD\u4FE1\u606F</p></li><li><p><a href="./db_model.html">QF_Model \u6570\u636E\u5BF9\u8C61</a></p></li></ul><h2 id="api-\u63A7\u5236\u5668" tabindex="-1">API \u63A7\u5236\u5668 <a class="header-anchor" href="#api-\u63A7\u5236\u5668" aria-hidden="true">#</a></h2><h3 id="http-get-\u53C2\u6570" tabindex="-1">http get \u53C2\u6570 <a class="header-anchor" href="#http-get-\u53C2\u6570" aria-hidden="true">#</a></h3><div class="language-c#"><pre><code>public class TestController : ControllerBase
{
        /// &lt;summary&gt;
        /// \u5F53\u524D\u8BF7\u6C42\u7684\u4EE4\u724C
        /// &lt;/summary&gt;
        private QuickSafeFilterModel safe;

        /// &lt;summary&gt;
        /// \u6784\u9020\u6CE8\u5165
        /// &lt;/summary&gt;
        /// &lt;param name=&quot;_fiter&quot;&gt;&lt;/param&gt;
        public TestController(QuickSafeFilterModel _fiter)
        {
            safe = _fiter;
        }


        [HttpGet]
        public IActionResult Get([FromQuery] PagerInStr pj)
        {
          MessageModel&lt;dynamic&gt; mm = new MessageModel&lt;dynamic&gt;();

            try
            {
                switch (pj.tpl)
                {
                  case 11:
                  // \u81EA\u5B9A\u4E49tpl \u7C7B\u578B\u5EFA\u8BAE10\u4EE5\u4E0A\u5F00\u59CB
                  // \u53D6\u51FA\u5F53\u524D\u8BF7\u6C42\u7684\u4EE4\u724C\u7528\u6237ID
                    mm.data =&quot;\u8FD4\u56DE\u7684\u6570\u636E&quot;+ safe.TokenUser.UserID
                    mm.code=1; //\u8868\u793A\u53D6\u51FA\u6210\u529F
                   break;
                }
            }
            catch (Exception ex)
            {
                mm.msg = ex.Message;

            }

            return Ok(mm);
        }
}
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br></div></div><h3 id="http-where-\u53C2\u6570" tabindex="-1">http where \u53C2\u6570 <a class="header-anchor" href="#http-where-\u53C2\u6570" aria-hidden="true">#</a></h3><div class="language-c#"><pre><code>public class TestController : ControllerBase
{
        /// &lt;summary&gt;
        /// \u5F53\u524D\u8BF7\u6C42\u7684\u4EE4\u724C
        /// &lt;/summary&gt;
        private QuickSafeFilterModel safe;

        /// &lt;summary&gt;
        /// \u6784\u9020\u6CE8\u5165
        /// &lt;/summary&gt;
        /// &lt;param name=&quot;_fiter&quot;&gt;&lt;/param&gt;
        public TestController(QuickSafeFilterModel _fiter)
        {
            safe = _fiter;
        }


        [HttpGet]
        public IActionResult Get([FromQuery] Pager&lt;dynamic&gt; pj)
        {
          MessageModel&lt;dynamic&gt; mm = new MessageModel&lt;dynamic&gt;();

            try
            {
                switch (pj.tpl)
                {
                  case 11:
                    // \u81EA\u5B9A\u4E49tpl \u7C7B\u578B\u5EFA\u8BAE10\u4EE5\u4E0A\u5F00\u59CB
                    // \u53D6\u51FA\u5F53\u524D\u8BF7\u6C42\u7684\u81EA\u5B9A\u4E49where \u53C2\u6570
                    mm.data =&quot;\u8FD4\u56DE\u7684\u6570\u636E&quot;+ JsonHelper.Deserialize(pj.where);
                    mm.code=1;//\u8868\u793A\u53D6\u51FA\u6210\u529F
                   break;
                }
            }
            catch (Exception ex)
            {
                mm.msg = ex.Message;
            }

            return Ok(mm);
        }
}
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br></div></div><h2 id="api-\u524D\u7AEF\u53C2\u6570" tabindex="-1">API \u524D\u7AEF\u53C2\u6570 <a class="header-anchor" href="#api-\u524D\u7AEF\u53C2\u6570" aria-hidden="true">#</a></h2><p>Join \u5916\u952E</p><ul><li>\u524D\u7AEF\u4F20\u5165</li></ul><div class="language-json line-numbers-mode"><pre><code><span class="token punctuation">{</span>
  <span class="token property">&quot;sort&quot;</span><span class="token operator">:</span> <span class="token string">&quot;ID&quot;</span><span class="token punctuation">,</span>
  <span class="token property">&quot;desc&quot;</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">&quot;tpl&quot;</span><span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span>
  <span class="token property">&quot;index&quot;</span><span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
  <span class="token property">&quot;size&quot;</span><span class="token operator">:</span> <span class="token number">5</span><span class="token punctuation">,</span>
  <span class="token property">&quot;join&quot;</span><span class="token operator">:</span> <span class="token string">&quot;[{\\&quot;tField\\&quot;:\\&quot;MUser_ID\\&quot;,\\&quot;cTable\\&quot;:\\&quot;AP10000DB.[dbo].[QF_User]\\&quot;,\\&quot;cTitle\\&quot;:\\&quot;LastFirst\\&quot;}]&quot;</span><span class="token punctuation">,</span>
  <span class="token property">&quot;where&quot;</span><span class="token operator">:</span> <span class="token string">&quot;[{\\&quot;ID\\&quot;:\\&quot;5\\&quot;,\\&quot;q\\&quot;:\\&quot;&gt;\\&quot;},{\\&quot;ID\\&quot;:\\&quot;20\\&quot;,\\&quot;q\\&quot;:\\&quot;&lt;\\&quot;}]&quot;</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br></div></div><h2 id="http-get" tabindex="-1">Http Get <a class="header-anchor" href="#http-get" aria-hidden="true">#</a></h2><p>-\u67E5\u8BE2\u63A5\u53E3</p><div class="language-csharp line-numbers-mode"><pre><code><span class="token punctuation">[</span><span class="token attribute"><span class="token class-name">HttpGet</span></span><span class="token punctuation">]</span>
<span class="token keyword">public</span> <span class="token return-type class-name">JsonResult</span> <span class="token function">Index</span><span class="token punctuation">(</span><span class="token punctuation">[</span><span class="token attribute"><span class="token class-name">FromQuery</span></span><span class="token punctuation">]</span> <span class="token class-name">PagerInStr</span> pj<span class="token punctuation">)</span>
<span class="token punctuation">{</span>
  <span class="token class-name">PagerOutModel<span class="token punctuation">&lt;</span><span class="token keyword">dynamic</span><span class="token punctuation">&gt;</span></span> pm <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token constructor-invocation class-name">PagerOutModel<span class="token punctuation">&lt;</span><span class="token keyword">dynamic</span><span class="token punctuation">&gt;</span></span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

  <span class="token keyword">try</span>
  <span class="token punctuation">{</span>
    <span class="token comment">// \u5206\u9875\u67E5\u8BE2\u6570\u91CF</span>
    <span class="token keyword">if</span> <span class="token punctuation">(</span>pj<span class="token punctuation">.</span>size <span class="token operator">&gt;</span> <span class="token number">0</span><span class="token punctuation">)</span>
    <span class="token punctuation">{</span>
      <span class="token class-name">QueryParam</span> qp <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token constructor-invocation class-name">QueryParam</span><span class="token punctuation">(</span>QuickData<span class="token punctuation">.</span>DBQuick<span class="token punctuation">)</span><span class="token punctuation">;</span>

      <span class="token comment">// \u67E5\u8BE2\u6761\u4EF6\u53C2\u6570</span>
      <span class="token keyword">if</span> <span class="token punctuation">(</span><span class="token operator">!</span><span class="token keyword">string</span><span class="token punctuation">.</span><span class="token function">IsNullOrWhiteSpace</span><span class="token punctuation">(</span>pj<span class="token punctuation">.</span><span class="token keyword">where</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
      <span class="token punctuation">{</span>
        qp<span class="token punctuation">.</span>TParams<span class="token punctuation">.</span><span class="token function">AddRange</span><span class="token punctuation">(</span>pj<span class="token punctuation">.</span><span class="token function">JsonParams</span><span class="token punctuation">(</span>pj<span class="token punctuation">.</span><span class="token keyword">where</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
      <span class="token punctuation">}</span>

      <span class="token comment">// \u5916\u952E\u5B57\u6BB5\u67E5\u8BE2</span>
      <span class="token keyword">if</span> <span class="token punctuation">(</span><span class="token operator">!</span><span class="token keyword">string</span><span class="token punctuation">.</span><span class="token function">IsNullOrWhiteSpace</span><span class="token punctuation">(</span>pj<span class="token punctuation">.</span><span class="token keyword">join</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
      <span class="token punctuation">{</span> qp<span class="token punctuation">.</span>Join <span class="token operator">=</span> JsonHelper<span class="token punctuation">.</span><span class="token generic-method"><span class="token function">Deserialize</span><span class="token generic class-name"><span class="token punctuation">&lt;</span>List<span class="token punctuation">&lt;</span>QueryJoin<span class="token punctuation">&gt;</span><span class="token punctuation">&gt;</span></span></span><span class="token punctuation">(</span>pj<span class="token punctuation">.</span><span class="token keyword">join</span><span class="token punctuation">)</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

      <span class="token comment">//\u81EA\u5B9A\u4E49\u67E5\u8BE2\u5B57\u6BB5</span>
      <span class="token keyword">if</span> <span class="token punctuation">(</span><span class="token operator">!</span><span class="token keyword">string</span><span class="token punctuation">.</span><span class="token function">IsNullOrWhiteSpace</span><span class="token punctuation">(</span>pj<span class="token punctuation">.</span>field<span class="token punctuation">)</span><span class="token punctuation">)</span>
      <span class="token punctuation">{</span> qp<span class="token punctuation">.</span>Field <span class="token operator">=</span> pj<span class="token punctuation">.</span>field<span class="token punctuation">;</span> <span class="token punctuation">}</span>

      qp<span class="token punctuation">.</span>PageSize <span class="token operator">=</span> pj<span class="token punctuation">.</span>size<span class="token punctuation">;</span>
      qp<span class="token punctuation">.</span>PageIndex <span class="token operator">=</span> pj<span class="token punctuation">.</span>index<span class="token punctuation">;</span>
      qp<span class="token punctuation">.</span>Orderfld <span class="token operator">=</span> pj<span class="token punctuation">.</span>sort<span class="token punctuation">;</span>
      qp<span class="token punctuation">.</span>OrderType <span class="token operator">=</span> pj<span class="token punctuation">.</span>desc<span class="token punctuation">;</span>

      <span class="token comment">// \u67E5\u8BE2\u5217\u8868</span>
      pm<span class="token punctuation">.</span>obj <span class="token operator">=</span> QuickData<span class="token punctuation">.</span><span class="token function">List</span><span class="token punctuation">(</span>qp<span class="token punctuation">)</span><span class="token punctuation">;</span>
      pm<span class="token punctuation">.</span>index <span class="token operator">=</span> qp<span class="token punctuation">.</span>PageIndex<span class="token punctuation">;</span>
      pm<span class="token punctuation">.</span>total <span class="token operator">=</span> qp<span class="token punctuation">.</span>PageTotal<span class="token punctuation">;</span>
      pm<span class="token punctuation">.</span>count <span class="token operator">=</span> qp<span class="token punctuation">.</span>RecordCount<span class="token punctuation">;</span>

      pm<span class="token punctuation">.</span>code <span class="token operator">=</span> <span class="token number">1</span><span class="token punctuation">;</span>
    <span class="token punctuation">}</span>
  <span class="token punctuation">}</span>
  <span class="token keyword">catch</span> <span class="token punctuation">(</span><span class="token class-name">Exception</span> ex<span class="token punctuation">)</span>
  <span class="token punctuation">{</span>
    pm<span class="token punctuation">.</span>msg <span class="token operator">=</span> ex<span class="token punctuation">.</span>Message<span class="token punctuation">;</span>
    pm<span class="token punctuation">.</span>obj <span class="token operator">=</span> <span class="token string">&quot;[]&quot;</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span>

  <span class="token keyword">return</span> <span class="token function">Json</span><span class="token punctuation">(</span>pm<span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br></div></div><h2 id="http-post-\u589E\u5220\u6539" tabindex="-1">http post \u589E\u5220\u6539 <a class="header-anchor" href="#http-post-\u589E\u5220\u6539" aria-hidden="true">#</a></h2><ul><li><p>ajax \u8BF7\u6C42\u7C7B\u578B <code>Content-Type:application/json; charset=utf-8</code></p><div class="language-json line-numbers-mode"><pre><code><span class="token punctuation">{</span> <span class="token property">&quot;cmd&quot;</span><span class="token operator">:</span> <span class="token string">&quot;resc&quot;</span><span class="token punctuation">,</span> <span class="token property">&quot;par&quot;</span><span class="token operator">:</span> <span class="token string">&quot;494&quot;</span><span class="token punctuation">,</span> <span class="token property">&quot;obj&quot;</span><span class="token operator">:</span> <span class="token string">&quot;\u7F16\u8F91\u7684\u5BF9\u8C61&quot;</span> <span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br></div></div></li><li><p>Post</p></li></ul><p>\u67B6\u6784\u914D\u7F6E\u8868\uFF0C\u901A\u7528\u7684\u589E\u5220\u6539</p><div class="language-csharp line-numbers-mode"><pre><code>  <span class="token comment">///&lt;summary&gt;</span>
  <span class="token comment">///(\u7EC4\u7EC7)  /Api/Group/</span>
  <span class="token comment">///&lt;/summary&gt;</span>
  <span class="token punctuation">[</span><span class="token function">Route</span><span class="token punctuation">(</span><span class="token string">&quot;api/[controller]&quot;</span><span class="token punctuation">)</span><span class="token punctuation">]</span>
  <span class="token punctuation">[</span><span class="token attribute"><span class="token class-name">ApiController</span></span><span class="token punctuation">]</span>
  <span class="token comment">// [TypeFilter(typeof(QuickSafeFilter))]</span>
  <span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">GroupController</span> <span class="token punctuation">:</span> <span class="token type-list"><span class="token class-name">ControllerBase</span></span>
  <span class="token punctuation">{</span>

      <span class="token comment">/// &lt;summary&gt;</span>
      <span class="token comment">/// \u5F53\u524D\u8BF7\u6C42\u7684\u4EE4\u724C</span>
      <span class="token comment">/// &lt;/summary&gt;</span>
      <span class="token keyword">private</span> <span class="token class-name">QuickSafeFilterModel</span> safe<span class="token punctuation">;</span>

      <span class="token comment">/// &lt;summary&gt;</span>
      <span class="token comment">/// \u521D\u59CB\u5316</span>
      <span class="token comment">/// &lt;/summary&gt;</span>
      <span class="token comment">/// &lt;param name=&quot;_fiter&quot;&gt;DI\u6CE8\u5165\u8BF7\u6C42\u4EE4\u724C&lt;/param&gt;</span>
      <span class="token keyword">public</span> <span class="token function">GroupController</span><span class="token punctuation">(</span><span class="token class-name">QuickSafeFilterModel</span> _fiter<span class="token punctuation">)</span><span class="token punctuation">{</span>
        safe<span class="token operator">=</span>_fiter
      <span class="token punctuation">}</span>

      <span class="token punctuation">[</span><span class="token attribute"><span class="token class-name">HttpPost</span></span><span class="token punctuation">]</span>
      <span class="token keyword">public</span> <span class="token return-type class-name">IActionResult</span> <span class="token function">Post</span><span class="token punctuation">(</span><span class="token class-name">PageCmd<span class="token punctuation">&lt;</span>QuickCodeModel<span class="token punctuation">&gt;</span></span> pc<span class="token punctuation">)</span>
      <span class="token punctuation">{</span>
          <span class="token class-name">MessageModel<span class="token punctuation">&lt;</span><span class="token keyword">bool</span><span class="token punctuation">&gt;</span></span> mm <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token constructor-invocation class-name">MessageModel<span class="token punctuation">&lt;</span><span class="token keyword">bool</span><span class="token punctuation">&gt;</span></span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

          <span class="token keyword">try</span>
          <span class="token punctuation">{</span>
            <span class="token comment">// \u672C\u5730\u65B9\u6CD5</span>
             <span class="token class-name">QuickORMBLL</span> bll <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token constructor-invocation class-name">QuickORMBLL</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
             <span class="token comment">// \u9700\u8981\u7F16\u8F91\u8868\u7684\u7D22\u5F15\u7F16\u7801</span>
              <span class="token class-name"><span class="token keyword">string</span></span> code <span class="token operator">=</span><span class="token string">&quot;&quot;</span><span class="token punctuation">;</span>
              <span class="token keyword">switch</span> <span class="token punctuation">(</span>pc<span class="token punctuation">.</span>cmd<span class="token punctuation">)</span>
              <span class="token punctuation">{</span>
                  <span class="token keyword">case</span> <span class="token string">&quot;add&quot;</span><span class="token punctuation">:</span>
                      mm<span class="token punctuation">.</span>data <span class="token operator">=</span> bll<span class="token punctuation">.</span><span class="token keyword">add</span><span class="token punctuation">(</span>code<span class="token punctuation">,</span> pc<span class="token punctuation">.</span>obj<span class="token punctuation">,</span> safe<span class="token punctuation">.</span>TokenUser<span class="token punctuation">.</span>UserID<span class="token punctuation">)</span><span class="token punctuation">;</span>
                      <span class="token keyword">break</span><span class="token punctuation">;</span>
                  <span class="token keyword">case</span> <span class="token string">&quot;edit&quot;</span><span class="token punctuation">:</span>
                      mm<span class="token punctuation">.</span>obj <span class="token operator">=</span> bll<span class="token punctuation">.</span><span class="token function">edit</span><span class="token punctuation">(</span>code<span class="token punctuation">,</span> pc<span class="token punctuation">.</span>id<span class="token punctuation">,</span> pc<span class="token punctuation">.</span>obj<span class="token punctuation">,</span> safe<span class="token punctuation">.</span>TokenUser<span class="token punctuation">.</span>UserID<span class="token punctuation">)</span><span class="token punctuation">;</span>
                      <span class="token keyword">break</span><span class="token punctuation">;</span>
                  <span class="token keyword">case</span> <span class="token string">&quot;del&quot;</span><span class="token punctuation">:</span>
                      mm<span class="token punctuation">.</span>data <span class="token operator">=</span> bll<span class="token punctuation">.</span><span class="token function">delete</span><span class="token punctuation">(</span>code<span class="token punctuation">,</span> pc<span class="token punctuation">.</span>par<span class="token punctuation">.</span><span class="token function">ToString</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">,</span> safe<span class="token punctuation">.</span>TokenUser<span class="token punctuation">.</span>UserID<span class="token punctuation">)</span><span class="token punctuation">;</span>
                      <span class="token keyword">break</span><span class="token punctuation">;</span>
                  <span class="token keyword">case</span> <span class="token string">&quot;audit&quot;</span><span class="token punctuation">:</span> <span class="token comment">//\u6807\u51C6\u5BA1\u6838</span>
                      mm<span class="token punctuation">.</span>data <span class="token operator">=</span> bll<span class="token punctuation">.</span><span class="token function">audit</span><span class="token punctuation">(</span>code<span class="token punctuation">,</span> pc<span class="token punctuation">.</span>par<span class="token punctuation">.</span><span class="token function">ToString</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">,</span> safe<span class="token punctuation">.</span>TokenUser<span class="token punctuation">.</span>UserID<span class="token punctuation">)</span><span class="token punctuation">;</span>
                      <span class="token keyword">break</span><span class="token punctuation">;</span>
                  <span class="token keyword">case</span> <span class="token string">&quot;resc&quot;</span><span class="token punctuation">:</span> <span class="token comment">//\u8D85\u7EA7\u64A4\u9500\u5BA1\u6838\u6743\u9650</span>
                      mm<span class="token punctuation">.</span>data <span class="token operator">=</span> bll<span class="token punctuation">.</span><span class="token function">audit</span><span class="token punctuation">(</span>code<span class="token punctuation">,</span> pc<span class="token punctuation">.</span>par<span class="token punctuation">.</span><span class="token function">ToString</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">,</span> safe<span class="token punctuation">.</span>TokenUser<span class="token punctuation">.</span>UserID<span class="token punctuation">,</span> <span class="token number">1</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
                      <span class="token keyword">break</span><span class="token punctuation">;</span>
                  <span class="token keyword">case</span> <span class="token string">&quot;erase&quot;</span><span class="token punctuation">:</span> <span class="token comment">//\u7269\u7406\u5220\u9664</span>
                     mm<span class="token punctuation">.</span>obj <span class="token operator">=</span> bll<span class="token punctuation">.</span><span class="token function">erase</span><span class="token punctuation">(</span>pc<span class="token punctuation">.</span>par<span class="token punctuation">.</span><span class="token function">ToString</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">,</span> TokenUser<span class="token punctuation">.</span>UserID<span class="token punctuation">)</span><span class="token punctuation">;</span>
                       <span class="token keyword">break</span><span class="token punctuation">;</span>
              <span class="token punctuation">}</span>
              mm<span class="token punctuation">.</span>code <span class="token operator">=</span> <span class="token number">1</span><span class="token punctuation">;</span>
          <span class="token punctuation">}</span>
          <span class="token keyword">catch</span> <span class="token punctuation">(</span><span class="token class-name">Exception</span> ex<span class="token punctuation">)</span>
          <span class="token punctuation">{</span>
              mm<span class="token punctuation">.</span>msg <span class="token operator">=</span> ex<span class="token punctuation">.</span>Message<span class="token punctuation">;</span>
          <span class="token punctuation">}</span>
          <span class="token keyword">return</span> <span class="token function">Ok</span><span class="token punctuation">(</span>mm<span class="token punctuation">)</span><span class="token punctuation">;</span>
      <span class="token punctuation">}</span>
  <span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br><span class="line-number">52</span><br><span class="line-number">53</span><br><span class="line-number">54</span><br><span class="line-number">55</span><br><span class="line-number">56</span><br><span class="line-number">57</span><br><span class="line-number">58</span><br><span class="line-number">59</span><br><span class="line-number">60</span><br><span class="line-number">61</span><br><span class="line-number">62</span><br><span class="line-number">63</span><br></div></div><h2 id="dbcontroller" tabindex="-1">DBController <a class="header-anchor" href="#dbcontroller" aria-hidden="true">#</a></h2><p>\u901A\u7528\u6570\u636E\u63A5\u53E3</p><ul><li>Startup \u9700\u8981\u5B9A\u4E49\u52A8\u6001\u8DEF\u7531</li></ul><div class="language-csharp line-numbers-mode"><pre><code> <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">void</span></span> <span class="token function">Configure</span><span class="token punctuation">(</span><span class="token class-name">IApplicationBuilder</span> app<span class="token punctuation">,</span> <span class="token class-name">IWebHostEnvironment</span> env<span class="token punctuation">)</span>
 <span class="token punctuation">{</span>
    app<span class="token punctuation">.</span><span class="token function">UseEndpoints</span><span class="token punctuation">(</span>endpoints <span class="token operator">=&gt;</span>
    <span class="token punctuation">{</span>
                <span class="token comment">// \u81EA\u5B9A\u4E49 \u6570\u636E\u7F16\u8F91\u63A5\u53E3</span>
        endpoints<span class="token punctuation">.</span><span class="token function">MapControllerRoute</span><span class="token punctuation">(</span><span class="token string">&quot;DB&quot;</span><span class="token punctuation">,</span> <span class="token string">&quot;/DB/{*code}&quot;</span><span class="token punctuation">,</span> <span class="token keyword">new</span>
        <span class="token punctuation">{</span>
            controller <span class="token operator">=</span> <span class="token string">&quot;DB&quot;</span><span class="token punctuation">,</span>
            action <span class="token operator">=</span> <span class="token string">&quot;Index&quot;</span><span class="token punctuation">,</span> <span class="token comment">//\u6307\u5B9A\u63A5\u6536\u52A8\u4F5C</span>
        <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

    <span class="token punctuation">}</span>
 <span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br></div></div><h2 id="pagertplmodel" tabindex="-1">PagerTplModel <a class="header-anchor" href="#pagertplmodel" aria-hidden="true">#</a></h2><p>\u8FD4\u56DE\u7ED9\u524D\u7AEF\u7EC4\u4EF6 <code>Listor</code> \u914D\u7F6E\u53C2\u6570</p><div class="language-csharp line-numbers-mode"><pre><code> <span class="token comment">/// &lt;summary&gt;</span>
    <span class="token comment">/// \u6570\u636E\u6A21\u677F\u5BF9\u8C61</span>
    <span class="token comment">/// &lt;/summary&gt;</span>
    <span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">PagerTplModel</span>
    <span class="token punctuation">{</span>
        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u67E5\u8BE2\u5B57\u6BB5</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">string</span></span> sql <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u6570\u636E\u5B57\u6BB5</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">dynamic</span></span> field <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5916\u952E\u67E5\u8BE2</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token class-name"><span class="token keyword">dynamic</span></span> <span class="token keyword">join</span> <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// Ui \u5B57\u6BB5\u5C5E\u6027\u5B9A\u4E49\uFF1ATplOptsModel  DynamicObject</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">dynamic</span></span> attr <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u81EA\u5B9A\u4E49 Ui \u6570\u636E\u6743\u9650</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">dynamic</span></span> act <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">///  UI \u4E3B\u952Eprimary \u4E0E\u5BA1\u6838\u952E\u5B9A\u4E49 auth</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">dynamic</span></span> keys <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">///  \u81EA\u5B9A\u4E49 \u6392\u5E8F\u5B57\u6BB5</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">dynamic</span></span> sort <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u81EA\u5B9A\u4E49 \u8868\u5934\u663E\u793A\u5B57\u6BB5</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">dynamic</span></span> th <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u81EA\u5B9A\u4E49 \u53EF\u7F16\u8F91\u7684\u5B57\u6BB5</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">dynamic</span></span> edit <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5E8F\u5217\u5316JSON\u5BF9\u8C61\uFF0C\u5F97\u5230\u8FD4\u56DE\u7684JSON\u4EE3\u7801</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token comment">/// &lt;returns&gt;&lt;/returns&gt;</span>
        <span class="token keyword">public</span> <span class="token keyword">override</span> <span class="token return-type class-name"><span class="token keyword">string</span></span> <span class="token function">ToString</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
        <span class="token punctuation">{</span>
            <span class="token class-name">StringBuilder</span> sb <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token constructor-invocation class-name">StringBuilder</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
            sb<span class="token punctuation">.</span><span class="token function">Append</span><span class="token punctuation">(</span><span class="token string">&quot;field:&quot;</span> <span class="token operator">+</span> field<span class="token punctuation">)</span><span class="token punctuation">;</span>
            sb<span class="token punctuation">.</span><span class="token function">Append</span><span class="token punctuation">(</span><span class="token string">&quot;,attr:&quot;</span> <span class="token operator">+</span> attr<span class="token punctuation">)</span><span class="token punctuation">;</span>
            sb<span class="token punctuation">.</span><span class="token function">Append</span><span class="token punctuation">(</span><span class="token string">&quot;,act:&quot;</span> <span class="token operator">+</span> act<span class="token punctuation">)</span><span class="token punctuation">;</span>
            sb<span class="token punctuation">.</span><span class="token function">Append</span><span class="token punctuation">(</span><span class="token string">&quot;,sort:&quot;</span> <span class="token operator">+</span> sort<span class="token punctuation">)</span><span class="token punctuation">;</span>
            sb<span class="token punctuation">.</span><span class="token function">Append</span><span class="token punctuation">(</span><span class="token string">&quot;,th:&quot;</span> <span class="token operator">+</span> th<span class="token punctuation">)</span><span class="token punctuation">;</span>
            sb<span class="token punctuation">.</span><span class="token function">Append</span><span class="token punctuation">(</span><span class="token string">&quot;,edit:&quot;</span> <span class="token operator">+</span> edit<span class="token punctuation">)</span><span class="token punctuation">;</span>
            <span class="token keyword">return</span> <span class="token string">&quot;{&quot;</span> <span class="token operator">+</span> sb<span class="token punctuation">.</span><span class="token function">ToString</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">Substring</span><span class="token punctuation">(</span><span class="token number">1</span><span class="token punctuation">)</span> <span class="token operator">+</span> <span class="token string">&quot;}&quot;</span><span class="token punctuation">;</span>
        <span class="token punctuation">}</span>
    <span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br><span class="line-number">52</span><br><span class="line-number">53</span><br><span class="line-number">54</span><br><span class="line-number">55</span><br><span class="line-number">56</span><br><span class="line-number">57</span><br><span class="line-number">58</span><br><span class="line-number">59</span><br><span class="line-number">60</span><br><span class="line-number">61</span><br><span class="line-number">62</span><br><span class="line-number">63</span><br><span class="line-number">64</span><br><span class="line-number">65</span><br><span class="line-number">66</span><br></div></div><h2 id="pagertploptsmodel" tabindex="-1">PagerTplOptsModel <a class="header-anchor" href="#pagertploptsmodel" aria-hidden="true">#</a></h2><div class="language-csharp line-numbers-mode"><pre><code>    <span class="token comment">/// &lt;summary&gt;</span>
    <span class="token comment">/// \u5B57\u6BB5\u81EA\u5B9A\u4E49 UI\u5C5E\u6027</span>
    <span class="token comment">/// &lt;/summary&gt;</span>
    <span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">PagerTplOptsModel</span>
    <span class="token punctuation">{</span>

        <span class="token preprocessor property">#<span class="token directive keyword">region</span> \u7CFB\u7EDF\u4FDD\u7559\u5B9A\u4E49</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5B57\u6BB5\u540D\u79F0</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">string</span></span> f <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5B57\u6BB5\u7C7B\u578B</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">string</span></span> t <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u81EA\u5B9A\u4E49\u7F16\u8F91\u63A7\u4EF6\u7C7B\u578B\u540D\u79F0</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">string</span></span> c <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5FC5\u586B\u5B57\u6BB5</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">bool</span></span> n <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5B57\u6BB5\u53EA\u8BFB\uFF0C\u524D\u7AEF\u4E0D\u53EF\u7F16\u8F91,rad</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">int</span></span> r <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5B57\u6BB5\u8868\u5934\u4E0D\u663E\u793A,hid</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">int</span></span> h <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5B57\u6BB5\u5185\u5BB9\u9700\u5BA1\u6838,aut</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">int</span></span> a <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5916\u952E\u5B57\u6BB5\u63CF\u8FF0,def</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">int</span></span> d <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u591A\u9009\u9879</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">string</span></span> ls <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>


        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u9650\u5236\u6700\u5927\u957F\u5EA6</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name">Int32</span> max <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>
        <span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>

        <span class="token preprocessor property">#<span class="token directive keyword">region</span> \u6570\u636E\u8868\u5B57\u6BB5 \u53EF\u9009\u81EA\u5B9A\u4E49\u63CF\u8FF0</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// \u5B57\u6BB5 \u63D0\u793A</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">string</span></span> tip <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>

        <span class="token comment">/// &lt;summary&gt;</span>
        <span class="token comment">/// ui \u63A7\u4EF6\u6570\u636E\u6E90</span>
        <span class="token comment">/// &lt;/summary&gt;</span>
        <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">string</span></span> api <span class="token punctuation">{</span> <span class="token keyword">get</span><span class="token punctuation">;</span> <span class="token keyword">set</span><span class="token punctuation">;</span> <span class="token punctuation">}</span>
        <span class="token preprocessor property">#<span class="token directive keyword">endregion</span></span>
    <span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br><span class="line-number">52</span><br><span class="line-number">53</span><br><span class="line-number">54</span><br><span class="line-number">55</span><br><span class="line-number">56</span><br><span class="line-number">57</span><br><span class="line-number">58</span><br><span class="line-number">59</span><br><span class="line-number">60</span><br><span class="line-number">61</span><br><span class="line-number">62</span><br><span class="line-number">63</span><br><span class="line-number">64</span><br><span class="line-number">65</span><br><span class="line-number">66</span><br><span class="line-number">67</span><br><span class="line-number">68</span><br><span class="line-number">69</span><br><span class="line-number">70</span><br><span class="line-number">71</span><br><span class="line-number">72</span><br><span class="line-number">73</span><br></div></div><h2 id="crud-api-\u589E\u5220\u6539" tabindex="-1">CRUD Api \u589E\u5220\u6539 <a class="header-anchor" href="#crud-api-\u589E\u5220\u6539" aria-hidden="true">#</a></h2><div class="language-csharp line-numbers-mode"><pre><code>  <span class="token comment">//\u83B7\u53D6\u5F53\u524D\u8BF7\u6C42\u7684\u7C7B\u578B</span>
<span class="token class-name"><span class="token keyword">string</span></span> _Method <span class="token operator">=</span> context<span class="token punctuation">.</span>HttpContext<span class="token punctuation">.</span>Request<span class="token punctuation">.</span>Method<span class="token punctuation">;</span>
<span class="token keyword">switch</span> <span class="token punctuation">(</span>_Method<span class="token punctuation">)</span>
<span class="token punctuation">{</span>
    <span class="token keyword">case</span> <span class="token string">&quot;OPTIONS&quot;</span><span class="token punctuation">:</span>
       <span class="token comment">//\u8BF7\u6C42\u9884\u68C0\uFF0C\u9ED8\u8BA4\u901A\u8FC7</span>
       qsf<span class="token punctuation">.</span>isPass <span class="token operator">=</span> <span class="token boolean">true</span><span class="token punctuation">;</span>
      <span class="token keyword">throw</span> <span class="token keyword">new</span> <span class="token constructor-invocation class-name">Exception</span><span class="token punctuation">(</span><span class="token keyword">string</span><span class="token punctuation">.</span><span class="token function">Format</span><span class="token punctuation">(</span><span class="token string">&quot;\u9884\u68C0\u8BF7\u6C42:{0}&quot;</span><span class="token punctuation">,</span> _Method<span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token keyword">case</span> <span class="token string">&quot;GET&quot;</span><span class="token punctuation">:</span>
       qsf<span class="token punctuation">.</span>cmd <span class="token operator">=</span> <span class="token string">&quot;list&quot;</span><span class="token punctuation">;</span>
      <span class="token keyword">break</span><span class="token punctuation">;</span>
    <span class="token keyword">case</span> <span class="token string">&quot;DELETE&quot;</span><span class="token punctuation">:</span>
      <span class="token comment">// \u771F\u6B63\u5220\u9664</span>
      qsf<span class="token punctuation">.</span>cmd <span class="token operator">=</span> <span class="token string">&quot;erase&quot;</span><span class="token punctuation">;</span>
    <span class="token keyword">break</span><span class="token punctuation">;</span>
      <span class="token keyword">case</span> <span class="token string">&quot;PUT&quot;</span><span class="token punctuation">:</span> <span class="token comment">// \u6574\u4F53\u66F4\u65B0</span>
      qsf<span class="token punctuation">.</span>cmd <span class="token operator">=</span> <span class="token string">&quot;edit&quot;</span><span class="token punctuation">;</span>
    <span class="token keyword">break</span><span class="token punctuation">;</span>
      <span class="token keyword">case</span> <span class="token string">&quot;PATCH&quot;</span><span class="token punctuation">:</span> <span class="token comment">// \u5C40\u90E8\u5B57\u6BB5\u66F4\u65B0</span>
      qsf<span class="token punctuation">.</span>cmd <span class="token operator">=</span> <span class="token string">&quot;audit&quot;</span><span class="token punctuation">;</span>
    <span class="token keyword">break</span><span class="token punctuation">;</span>
    <span class="token keyword">default</span><span class="token punctuation">:</span>
      <span class="token comment">//\u83B7\u53D6\u8BF7\u6C42\u53C2\u6570 POST</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br></div></div><h2 id="join-\u5916\u952E\u67E5\u8BE2\u914D\u7F6E" tabindex="-1">Join \u5916\u952E\u67E5\u8BE2\u914D\u7F6E <a class="header-anchor" href="#join-\u5916\u952E\u67E5\u8BE2\u914D\u7F6E" aria-hidden="true">#</a></h2><div class="language-json line-numbers-mode"><pre><code><span class="token punctuation">{</span> <span class="token property">&quot;MUser_ID&quot;</span><span class="token operator">:</span> <span class="token punctuation">{</span> <span class="token property">&quot;server&quot;</span><span class="token operator">:</span> <span class="token number">2</span><span class="token punctuation">,</span> <span class="token property">&quot;table&quot;</span><span class="token operator">:</span> <span class="token string">&quot;QF_User&quot;</span><span class="token punctuation">,</span> <span class="token property">&quot;fields&quot;</span><span class="token operator">:</span> <span class="token string">&quot;LastFirst&quot;</span> <span class="token punctuation">}</span> <span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br></div></div><ul><li>\u5916\u952E\u67E5\u8BE2\u5B57\u6BB5 MUser_ID</li></ul><table><thead><tr><th>\u5C5E\u6027</th><th>\u8BF4\u660E</th><th>\u7C7B\u578B</th><th>\u9ED8\u8BA4\u503C</th></tr></thead><tbody><tr><td>sever</td><td>\u5982\u9700\u8DE8\u57DF\u8BF7\u914D\u7F6E\u670D\u52A1\u5668 ID</td><td>int</td><td>-</td></tr><tr><td>table</td><td>\u5916\u952E\u67E5\u8BE2\u8868</td><td>String</td><td>-</td></tr><tr><td>fields</td><td>\u5916\u952E\u8F93\u51FA\u7684\u5B57\u6BB5 \u591A\u4E2A\u5B57\u6BB5\u7528,\u53F7\u9694\u5F00</td><td>String</td><td>-</td></tr></tbody></table><h2 id="di-\u6570\u636E\u5E94\u7528\u63A5\u53E3" tabindex="-1">DI \u6570\u636E\u5E94\u7528\u63A5\u53E3 <a class="header-anchor" href="#di-\u6570\u636E\u5E94\u7528\u63A5\u53E3" aria-hidden="true">#</a></h2><h3 id="startup-\u63A5\u53E3\u914D\u7F6E" tabindex="-1">Startup \u63A5\u53E3\u914D\u7F6E <a class="header-anchor" href="#startup-\u63A5\u53E3\u914D\u7F6E" aria-hidden="true">#</a></h3><div class="language-csharp line-numbers-mode"><pre><code><span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">Startup</span><span class="token punctuation">{</span>
    <span class="token keyword">public</span> <span class="token return-type class-name"><span class="token keyword">void</span></span> <span class="token function">Configure</span><span class="token punctuation">(</span><span class="token class-name">IApplicationBuilder</span> app<span class="token punctuation">,</span> <span class="token class-name">IWebHostEnvironment</span> env<span class="token punctuation">)</span>
    <span class="token punctuation">{</span>
            app<span class="token punctuation">.</span><span class="token function">UseEndpoints</span><span class="token punctuation">(</span>endpoints <span class="token operator">=&gt;</span>
            <span class="token punctuation">{</span>

                <span class="token comment">// \u81EA\u5B9A\u4E49 \u6570\u636E\u7F16\u8F91\u63A5\u53E3</span>
                endpoints<span class="token punctuation">.</span><span class="token function">MapControllerRoute</span><span class="token punctuation">(</span><span class="token string">&quot;DB&quot;</span><span class="token punctuation">,</span> <span class="token string">&quot;/DB/{*path}&quot;</span><span class="token punctuation">,</span> <span class="token keyword">new</span>
                <span class="token punctuation">{</span>
                    controller <span class="token operator">=</span> <span class="token string">&quot;DB&quot;</span><span class="token punctuation">,</span>
                    action <span class="token operator">=</span> <span class="token string">&quot;Index&quot;</span><span class="token punctuation">,</span> <span class="token comment">//\u6307\u5B9A\u63A5\u6536\u52A8\u4F5C</span>
                <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

                <span class="token comment">// \u81EA\u5B9A\u4E49 \u6570\u636E\u7F16\u8F91\u63A5\u53E3</span>
                endpoints<span class="token punctuation">.</span><span class="token function">MapControllerRoute</span><span class="token punctuation">(</span><span class="token string">&quot;DI&quot;</span><span class="token punctuation">,</span> <span class="token string">&quot;/DI/{*path}&quot;</span><span class="token punctuation">,</span> <span class="token keyword">new</span>
                <span class="token punctuation">{</span>
                    controller <span class="token operator">=</span> <span class="token string">&quot;DI&quot;</span><span class="token punctuation">,</span>
                    action <span class="token operator">=</span> <span class="token string">&quot;Index&quot;</span><span class="token punctuation">,</span> <span class="token comment">//\u6307\u5B9A\u63A5\u6536\u52A8\u4F5C</span>
                <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

                <span class="token comment">// \u6807\u51C6MVC \u63A7\u5236\u5668</span>
                endpoints<span class="token punctuation">.</span><span class="token function">MapControllerRoute</span><span class="token punctuation">(</span>
                    <span class="token named-parameter punctuation">name</span><span class="token punctuation">:</span> <span class="token string">&quot;default&quot;</span><span class="token punctuation">,</span>
                    <span class="token named-parameter punctuation">pattern</span><span class="token punctuation">:</span> <span class="token string">&quot;{controller=Home}/{action=Index}/{id?}&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
            <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br></div></div>`,36),o=[e];function c(l,u,r,i,k,m){return s(),a("div",null,o)}var y=n(t,[["render",c]]);export{d as __pageData,y as default};
