import{bm as n,aS as e,ap as s,at as a}from"./plugin-vue_export-helper.ec0df64b.js";const d='{"title":"\u6307\u5F15\u6269\u5C55","description":"","frontmatter":{},"headers":[{"level":2,"title":"nginx \u914D\u7F6E","slug":"nginx-\u914D\u7F6E"},{"level":3,"title":"log_format \u683C\u5F0F\u53D8\u91CF","slug":"log-format-\u683C\u5F0F\u53D8\u91CF"},{"level":3,"title":"nginx header","slug":"nginx-header"},{"level":3,"title":"nginx upstream temporary","slug":"nginx-upstream-temporary"}],"relativePath":"guide/index.md","lastUpdated":1649325148373}',r={},l=a(`<h1 id="\u6307\u5F15\u6269\u5C55" tabindex="-1">\u6307\u5F15\u6269\u5C55 <a class="header-anchor" href="#\u6307\u5F15\u6269\u5C55" aria-hidden="true">#</a></h1><h2 id="nginx-\u914D\u7F6E" tabindex="-1">nginx \u914D\u7F6E <a class="header-anchor" href="#nginx-\u914D\u7F6E" aria-hidden="true">#</a></h2><div class="language-s line-numbers-mode"><pre><code>worker_processes  1; #CPU\u6838\u6570

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
error_log  logs/error_warn.log warn;
error_log  logs/error_info.log  info; #\u9519\u8BEF\u65E5\u5FD7log

events {
    worker_connections  102400;   #\u5141\u8BB8\u6700\u5927\u8FDE\u63A5\u6570
}

http {
    # \u8BF7\u6C42\u65E5\u5FD7\u8BB0\u5F55\uFF0C\u5173\u952E\u5B57\u6BB5\uFF1Arequest_time-\u8BF7\u6C42\u603B\u65F6\u95F4\uFF0Cupstream_response_time\u540E\u7AEF\u5904\u7406\u65F6 \u95F4
    log_format  main  &#39;$remote_addr  - $remote_user [$time_iso8601] &quot;$request&quot; &#39;
                 &#39;$status $body_bytes_sent &quot;$http_referer&quot; &#39;
                  &#39;&quot;$http_user_agent&quot; &quot;$http_x_forwarded_for&quot; &quot;$host&quot;  &quot;$cookie_ssl_edition&quot; &#39;
                 &#39;&quot;$upstream_addr&quot;   &quot;$upstream_status&quot;  &quot;$request_time&quot;  &#39;
                 &#39;&quot;$upstream_response_time&quot; &#39;;

    #access_log off; # \u5173\u95ED\u8BBF\u95EE\u65E5\u5FD7
    # \u8BF7\u6C42\u65E5\u5FD7,\u9700\u5EFA\u6587\u4EF6\u5939access/\u670D\u52A1\u540D\u5206\u7C7B\u65E5\u5FD7
    access_log  logs/access/\${server_name}_.log  main;

    # \u957F\u8FDE\u63A5\u8D85\u65F6 0 \u5173\u95ED\u957F\u8FDE\u63A5
    # keepalive_timeout  65;

    #\u5141\u8BB8\u5728header\u7684\u5B57\u6BB5\u4E2D\u5E26\u4E0B\u5212\u7EBF
    underscores_in_headers on;

    # \u7F13\u51B2\u5230\u5185\u5B58
    #client_max_body_size 5m;
    client_header_buffer_size 128k;
    client_body_buffer_size 1m;
    proxy_buffer_size 32k;
    proxy_buffers 64 32k;
    proxy_busy_buffers_size 1m;
    proxy_temp_file_write_size 512k;
    #gzip  on;
}
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br></div></div><h3 id="log-format-\u683C\u5F0F\u53D8\u91CF" tabindex="-1">log_format \u683C\u5F0F\u53D8\u91CF <a class="header-anchor" href="#log-format-\u683C\u5F0F\u53D8\u91CF" aria-hidden="true">#</a></h3><ul><li><a href="https://blog.csdn.net/strggle_bin/article/details/110561976" target="_blank" rel="noopener noreferrer">\u65E5\u5FD7\u914D\u7F6E\u53C2\u8003</a></li></ul><div class="language-"><pre><code>\u53C2\u6570                      \u8BF4\u660E                                         \u793A\u4F8B
$remote_addr             \u5BA2\u6237\u7AEF\u5730\u5740                                    211.28.65.253
$remote_user             \u5BA2\u6237\u7AEF\u7528\u6237\u540D\u79F0                                --
$time_local              \u8BBF\u95EE\u65F6\u95F4\u548C\u65F6\u533A                                18/Jul/2012:17:00:01 +0800
$request                 \u8BF7\u6C42\u7684URI\u548CHTTP\u534F\u8BAE                           &quot;GET /article-10000.html HTTP/1.1&quot;
$http_host               \u8BF7\u6C42\u5730\u5740\uFF0C\u5373\u6D4F\u89C8\u5668\u4E2D\u4F60\u8F93\u5165\u7684\u5730\u5740\uFF08IP\u6216\u57DF\u540D\uFF09     www.wang.com 192.168.100.100
$status                  HTTP\u8BF7\u6C42\u72B6\u6001                                  200
$upstream_status         upstream\u72B6\u6001                                  200
$body_bytes_sent         \u53D1\u9001\u7ED9\u5BA2\u6237\u7AEF\u6587\u4EF6\u5185\u5BB9\u5927\u5C0F                        1547
$http_referer            url\u8DF3\u8F6C\u6765\u6E90                                   https://www.test.com/
$http_user_agent         \u7528\u6237\u7EC8\u7AEF\u6D4F\u89C8\u5668\u7B49\u4FE1\u606F                           &quot;Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; SV1; GTB7.0; .NET4.0C;
$ssl_protocol            SSL\u534F\u8BAE\u7248\u672C                                   TLSv1
$ssl_cipher              \u4EA4\u6362\u6570\u636E\u4E2D\u7684\u7B97\u6CD5                               RC4-SHA
$upstream_addr           \u540E\u53F0upstream\u7684\u5730\u5740\uFF0C\u5373\u771F\u6B63\u63D0\u4F9B\u670D\u52A1\u7684\u4E3B\u673A\u5730\u5740     10.10.10.100:80
$request_time            \u6574\u4E2A\u8BF7\u6C42\u7684\u603B\u65F6\u95F4                               0.205
$upstream_response_time  \u8BF7\u6C42\u8FC7\u7A0B\u4E2D\uFF0Cupstream\u54CD\u5E94\u65F6\u95F4                    0.002
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br></div></div><h3 id="nginx-header" tabindex="-1">nginx header <a class="header-anchor" href="#nginx-header" aria-hidden="true">#</a></h3><ul><li><p>\u62A5\u9519 <code>client sent invalid header line: &quot;SOGOU_VERSION:</code></p><p>nginx \u5BF9 header name \u7684\u5B57\u7B26\u505A\u4E86\u9650\u5236\uFF0C\u9ED8\u8BA4 underscores_in_headers \u4E3A off\uFF0C\u8868\u793A\u5982\u679C header name \u4E2D\u5305\u542B\u4E0B\u5212\u7EBF\uFF0C\u5219\u5FFD\u7565\u6389\u3002</p></li><li><p>\u5904\u7406\u65B9\u6CD5 1\uFF1A\u914D\u7F6E\u4E2D http \u90E8\u5206 \u589E\u52A0 underscores<em>in_headers on; \u914D\u7F6E 2\uFF1A\u7528\u51CF\u53F7-\u66FF\u4EE3\u4E0B\u5212\u7EBF\u7B26\u53F7</em>\uFF0C\u907F\u514D\u8FD9\u79CD\u53D8\u6001\u95EE\u9898\u3002 YY \u4E00\u4E0B\uFF1A\u51CF\u53F7\u4EE3\u66FF\u4E0B\u5212\u7EBF\uFF0C\u5BF9\u4E8E plesk \u8BA4\u8BC1\u65B9\u5F0F\u505A\u4FEE\u6539\uFF0C\u81EA\u7136\u8FD9\u79CD\u5C0F\u767D\u660E\u663E\u662F\u5B8C\u6210\u4E0D\u4E86\u7684\u3002 \u8BED\u6CD5\uFF1Aunderscores_in_headers on|off \u9ED8\u8BA4\u503C\uFF1Aoff \u4F7F\u7528\u5B57\u6BB5\uFF1Ahttp, server \u662F\u5426\u5141\u8BB8\u5728 header \u7684\u5B57\u6BB5\u4E2D\u5E26\u4E0B\u5212\u7EBF\u3002</p></li></ul><h3 id="nginx-upstream-temporary" tabindex="-1">nginx upstream temporary <a class="header-anchor" href="#nginx-upstream-temporary" aria-hidden="true">#</a></h3><ul><li>\u62A5\u9519\uFF0C\u610F\u601D\u662F nginx \u9ED8\u8BA4\u7684 buffer \u592A\u5C0F\uFF0C\u6BCF\u4E2A\u8BF7\u6C42\u7684\u7F13\u5B58\u592A\u5C0F\uFF0C\u8BF7\u6C42\u5934 header \u592A\u5927\u65F6\u4F1A\u51FA\u73B0\u7F13\u5B58\u4E0D\u8DB3\uFF0C\u5185\u5B58\u653E\u4E0D\u4E0B\u4E0A\u4F20\u7684\u6587\u4EF6\uFF0C\u5C31\u5199\u5165\u5230\u4E86\u78C1\u76D8\u4E2D\uFF0C\u4F7F nginx \u7684 io \u592A\u591A\uFF0C\u9020\u6210\u8BBF\u95EE\u4E2D\u65AD\u3002 <code>an upstream response is buffered to a temporary file /proxy_temp/0/52/0002923520 while reading upstream</code></li><li>\u5904\u7406\u65B9\u6CD5</li></ul><div class="language-s line-numbers-mode"><pre><code>    # \u7F13\u51B2\u5230\u5185\u5B58
    #client_max_body_size 5m;
    client_header_buffer_size 128k;
    client_body_buffer_size 1m;
    proxy_buffer_size 32k;
    proxy_buffers 64 32k;
    proxy_busy_buffers_size 1m;
    proxy_temp_file_write_size 512k;
</code></pre><div class="line-numbers-wrapper"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br></div></div>`,11),p=[l];function i(t,o,u,b,m,c){return e(),s("div",null,p)}var h=n(r,[["render",i]]);export{d as __pageData,h as default};
