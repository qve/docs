/**
 * markdown 生成的代码 Net6
 */
let quick_6_md = [
  {
    text: 'Quick',
    link: 'Quick/index',
    children: [
      {
        text: 'BLL.JsonBind',
        title: '正则格式绑定对象值',
        link: 'BLL_JsonBind'
      },
      {
        text: 'BLL.JsonHelperConvert',
        title: 'json 类型转换自定义方法',
        link: 'BLL_JsonHelperConvert'
      },
      {
        text: 'BLL.JsonHelperConvert.DynamicConverter',
        title: 'json 泛型 动态转换',
        link: 'BLL_JsonHelperConvert_DynamicConverter'
      },
      {
        text: 'BLL.JsonHelperConvert.DateTimeConverter',
        title: 'Json 时间格式化，避免ISO转换js.toISOString()',
        link: 'BLL_JsonHelperConvert_DateTimeConverter'
      },
      {
        text: 'BLL.JsonHelperConvert.DateTimeNullableConverter',
        title: 'json 时间空值转换',
        link: 'BLL_JsonHelperConvert_DateTimeNullableConverter'
      },
      {
        text: 'BLL.JsonHelperConvert.LongConverter',
        title: 'json 数字转换',
        link: 'BLL_JsonHelperConvert_LongConverter'
      },
      {
        text: 'BLL.JsonHelperConvert.Int32Converter',
        title: '数字转换',
        link: 'BLL_JsonHelperConvert_Int32Converter'
      },
      {
        text: 'BLL.JsonHelperConvert.DecimalConverter',
        title: '金额转换',
        link: 'BLL_JsonHelperConvert_DecimalConverter'
      },
      {
        text: 'BLL.JsonHelperConvert.StringConverter',
        title: '文本与数字转换',
        link: 'BLL_JsonHelperConvert_StringConverter'
      },
      { text: 'BLL.QuickImg', title: '图片处理类', link: 'BLL_QuickImg' },
      { text: 'BLL.TextHelper', title: '文本读写', link: 'BLL_TextHelper' },
      {
        text: 'BLL.TextHelper.DirDateTime',
        title: '自定义日期类(实现倒排序)',
        link: 'BLL_TextHelper_DirDateTime'
      },
      { text: 'JsonHelper', title: 'json 转换序列化', link: 'JsonHelper' },
      {
        text: 'JsonHelper.JsonType',
        title: 'json 数据类型',
        link: 'JsonHelper_JsonType'
      },
      { text: 'JsonQuick', title: '前端 Json 语法对象', link: 'JsonQuick' },
      { text: 'QuickBLL', title: '基础方法', link: 'QuickBLL' },
      { text: 'QuickCache', title: '本机缓存管理', link: 'QuickCache' },
      { text: 'QuickEnum', title: '全局输出代码', link: 'QuickEnum' },
      { text: 'QuickLog', title: '文本日志读写', link: 'QuickLog' },
      {
        text: 'QuickMemoryProvider',
        title: '内存缓存方法             名命前缀 qm:',
        link: 'QuickMemoryProvider'
      },
      { text: 'QuickTokenBLL', title: '令牌方法', link: 'QuickTokenBLL' },
      { text: 'XMLHelper', title: '解析XML', link: 'XMLHelper' },
      { text: 'ZipHelper', title: '压缩库', link: 'ZipHelper' },
      {
        text: 'MessageBaseModel_T',
        title: '返回消息对象',
        link: 'MessageBaseModel_T'
      },
      {
        text: 'MessageModel_T',
        title: '前端模板消息传递实体',
        link: 'MessageModel_T'
      },
      {
        text: 'Model.QuickAppInfoModel',
        title: '架构应用配置 QF_AppInfo',
        link: 'Model_QuickAppInfoModel'
      },
      {
        text: 'Model.QuickLogModel',
        title: '批量日志写入',
        link: 'Model_QuickLogModel'
      },
      {
        text: 'Model.QuickTextLogModel',
        title: '标准文本日志',
        link: 'Model_QuickTextLogModel'
      },
      {
        text: 'Model.QuickTextLogWebModel',
        title: '文本日志-网页日志',
        link: 'Model_QuickTextLogWebModel'
      },
      {
        text: 'Model.QuickTokenAuthModel',
        title: '返回的认证票据',
        link: 'Model_QuickTokenAuthModel'
      },
      {
        text: 'Model.QuickUIApiModel',
        title: 'UI前端数据模板对象',
        link: 'Model_QuickUIApiModel'
      },
      {
        text: 'Model.QuickUserJoinBaseModel',
        title: '注册用户基本信息',
        link: 'Model_QuickUserJoinBaseModel'
      },
      {
        text: 'Model.QuickUserModel',
        title: '用户基础对象',
        link: 'Model_QuickUserModel'
      },
      {
        text: 'Model.QuickUserMoreModel',
        title: '扩展第三方',
        link: 'Model_QuickUserMoreModel'
      },
      {
        text: 'QuickEnumModel',
        title: '常用枚举对象定义',
        link: 'QuickEnumModel'
      },
      {
        text: 'QuickEnumModel.User',
        title: '网络会员状态',
        link: 'QuickEnumModel_User'
      },
      {
        text: 'QuickEnumModel.Auth',
        title: '验证',
        link: 'QuickEnumModel_Auth'
      },
      {
        text: 'QuickEnumModel.HttpStatus',
        title: '请求状态',
        link: 'QuickEnumModel_HttpStatus'
      },
      {
        text: 'QuickEnumModel.DB',
        title: '数据库提示',
        link: 'QuickEnumModel_DB'
      },
      {
        text: 'QuickEnumModel.Log',
        title: '日志处理',
        link: 'QuickEnumModel_Log'
      },
      {
        text: 'QuickEnumModel.MessageType',
        title: 'io 消息类型',
        link: 'QuickEnumModel_MessageType'
      },
      {
        text: 'QuickEnumModel.MessageLevel',
        title: '消息等级',
        link: 'QuickEnumModel_MessageLevel'
      },
      {
        text: 'QuickEnumModel.MessageStatus',
        title: '消息状态',
        link: 'QuickEnumModel_MessageStatus'
      },
      {
        text: 'QuickEnumModel.UserAgentType',
        title: '浏览器类型',
        link: 'QuickEnumModel_UserAgentType'
      },
      {
        text: 'QuickEnumModel.UserAgentApp',
        title: '浏览器 请求来源自定义应用',
        link: 'QuickEnumModel_UserAgentApp'
      },
      { text: 'QuickSettings', title: '配置节点', link: 'QuickSettings' },
      { text: 'QuickSettings.Log', title: '日志', link: 'QuickSettings_Log' },
      {
        text: 'QuickSettings.App',
        title: '应用配置',
        link: 'QuickSettings_App'
      },
      {
        text: 'QuickSettings.Session',
        title: '会话',
        link: 'QuickSettings_Session'
      },
      {
        text: 'QuickSettings.Session.Cookie',
        title: '浏览器会话',
        link: 'QuickSettings_Session_Cookie'
      },
      {
        text: 'QuickSettings.Session.Cache',
        title: '会话redis缓存配置',
        link: 'QuickSettings_Session_Cache'
      },
      { text: 'QuickSettings.DB', title: '数据库', link: 'QuickSettings_DB' },
      {
        text: 'QuickSettings.DB.Redis',
        title: '缓存数据库',
        link: 'QuickSettings_DB_Redis'
      },
      {
        text: 'QuickSettings.Service',
        title: '后台服务',
        link: 'QuickSettings_Service'
      },
      {
        text: 'QuickSettings.Socket',
        title: '通信服务',
        link: 'QuickSettings_Socket'
      },
      {
        text: 'QuickSettingsManager',
        title: 'appsettings.json 配置读取',
        link: 'QuickSettingsManager'
      },
      { text: 'QuickToken', title: '会话身份令牌', link: 'QuickToken' }
    ]
  },
  {
    text: 'Quick.DB',
    link: 'Quick.DB/index',
    children: [
      { text: 'DataSafe', title: '数据权限处理方法', link: 'DataSafe' },
      { text: 'QuickLogDB', title: '扩展日志方法', link: 'QuickLogDB' },
      {
        text: 'QuickORMAPIBLL',
        title: 'DI 请求权限处理接口',
        link: 'QuickORMAPIBLL'
      },
      {
        text: 'BLL.QuickORMAttrBLL',
        title: '属性规则方法',
        link: 'BLL_QuickORMAttrBLL'
      },
      {
        text: 'BLL.QuickORMSqlBLL',
        title: '平台数据对象SQL方法',
        link: 'BLL_QuickORMSqlBLL'
      },
      {
        text: 'BLL.QuickServerBLL',
        title: '数据服务器方法',
        link: 'BLL_QuickServerBLL'
      },
      { text: 'QuickORMBLL', title: 'ORM 对象方法', link: 'QuickORMBLL' },
      { text: 'ISqlClient', title: '数据库交互接口', link: 'ISqlClient' },
      { text: 'QueryJoin', title: '连接查询参数', link: 'QueryJoin' },
      { text: 'QueryParam', title: '查询参数类', link: 'QueryParam' },
      { text: 'QuickDAL', title: '数据库连接层', link: 'QuickDAL' },
      {
        text: 'QuickData',
        title: 'MSSQL 数据库 ORM 读写连接',
        link: 'QuickData'
      },
      { text: 'SqlClient_T', title: '数据库交互方法', link: 'SqlClient_T' },
      { text: 'SqlFactory', title: '数据工厂', link: 'SqlFactory' },
      { text: 'SqlImpl', title: 'sql 连接基础方法', link: 'SqlImpl' },
      {
        text: 'SqlParam',
        title: '存储过程参数             QueryParam',
        link: 'SqlParam'
      },
      { text: 'ParamType', title: '存储过程参数类型', link: 'ParamType' },
      { text: 'WhereItem', title: '数据表字段查询对象', link: 'WhereItem' },
      { text: 'DataType', title: '获取数据类型', link: 'DataType' },
      { text: 'QueryMode', title: '查询条件方式,小写', link: 'QueryMode' },
      { text: 'QuickDBEnum', title: '数据参数', link: 'QuickDBEnum' },
      {
        text: 'QuickDBEnum.StateFlag',
        title: '数据状态说明',
        link: 'QuickDBEnum_StateFlag'
      },
      {
        text: 'QuickDBEnum.StateFlagIcon',
        title: '数据状态 icon-名称',
        link: 'QuickDBEnum_StateFlagIcon'
      },
      {
        text: 'Model.DataSafeModel',
        title: '通用数据权限对象',
        link: 'Model_DataSafeModel'
      },
      {
        text: 'Model.DBConfigModel',
        title: '数据库连接配置',
        link: 'Model_DBConfigModel'
      },
      {
        text: 'Model.LogEventModel',
        title: 'Log_Event',
        link: 'Model_LogEventModel'
      },
      {
        text: 'Model.RedisConfigModel',
        title: 'Redis 配置',
        link: 'Model_RedisConfigModel'
      },
      {
        text: 'Model.TPLAPIModel',
        title: '应用接口对象',
        link: 'Model_TPLAPIModel'
      },
      {
        text: 'Model.TPLSQLSelectModel',
        title: '结构化查询对象',
        link: 'Model_TPLSQLSelectModel'
      },
      {
        text: 'PagerOutModel_T',
        title: 'API 分页查询，返回参数对象',
        link: 'PagerOutModel_T'
      },
      { text: 'PagerIn', title: '分页传入参数', link: 'PagerIn' },
      {
        text: 'PagerIn_T',
        title: '分页查询自定义where参数对象',
        link: 'PagerIn_T'
      },
      {
        text: 'PagerInString',
        title: '分页传入查询条件where参数为字符串',
        link: 'PagerInString'
      },
      {
        text: 'PagerInJson',
        title: '分页传入查询条件参数为Json',
        link: 'PagerInJson'
      },
      { text: 'PageCmd', title: '提交增删改动态数据', link: 'PageCmd' },
      { text: 'PageCmd_T', title: '提交增删改数据对象', link: 'PageCmd_T' },
      {
        text: 'PagerTplModel',
        title: '数据对象模板TPL',
        link: 'PagerTplModel'
      },
      {
        text: 'PagerTplOptsModel',
        title: 'UI字段属性              参阅',
        link: 'PagerTplOptsModel'
      },
      { text: 'TPLAttrModel', title: '数据表管理属性', link: 'TPLAttrModel' },
      {
        text: 'TPLConfigModel',
        title: 'QF_Model 数据库配置',
        link: 'TPLConfigModel'
      },
      {
        text: 'TPLTableModel',
        title: 'QF_Model 对象约束',
        link: 'TPLTableModel'
      },
      { text: 'DB.SqlMyImpl', title: 'mysql 实现', link: 'DB_SqlMyImpl' },
      { text: 'SqlServerImpl', title: 'SqlServer 实现', link: 'SqlServerImpl' },
      {
        text: 'MSSQL.SqlDataForTools',
        title: 'SQL 构造方法',
        link: 'MSSQL_SqlDataForTools'
      },
      {
        text: 'MSSQL.SqlDataTools',
        title: '数据库工具',
        link: 'MSSQL_SqlDataTools'
      },
      { text: 'MSSQL.SqlHelper', title: 'MSSQL 连接', link: 'MSSQL_SqlHelper' },
      {
        text: 'MSSQL.TableColume',
        title: '表列信息',
        link: 'MSSQL_TableColume'
      },
      {
        text: 'MSSQL.TableColumeToForgeinKey',
        title: '主外键关系表',
        link: 'MSSQL_TableColumeToForgeinKey'
      },
      {
        text: 'MSSQL.TableColumeToNet',
        title: '.net数据对应列',
        link: 'MSSQL_TableColumeToNet'
      },
      {
        text: 'MSSQL.TableEntity',
        title: '表实体类',
        link: 'MSSQL_TableEntity'
      },
      {
        text: 'MSSQL.TableEntityNet',
        title: 'Table映射.net',
        link: 'MSSQL_TableEntityNet'
      },
      {
        text: 'MSSQL.TableEntityNets',
        title: '表实体类',
        link: 'MSSQL_TableEntityNets'
      },
      {
        text: 'MSSQL.Table.TableFieldModel',
        title: '表字段明细',
        link: 'MSSQL_Table_TableFieldModel'
      },
      {
        text: 'SqlPostgreImpl',
        title: 'PostgreSql 实现',
        link: 'SqlPostgreImpl'
      },
      {
        text: 'QuickAppBLL',
        title: '应用架构方法，获取应用配置',
        link: 'QuickAppBLL'
      },
      { text: 'QuickDBBLL', title: '数据库公共工具类', link: 'QuickDBBLL' },
      { text: 'RedisBLL', title: '公共方法', link: 'RedisBLL' },
      { text: 'RedisHelper', title: '封装方法', link: 'RedisHelper' },
      { text: 'RedisKeys', title: 'Redis 统一存储Keys规则', link: 'RedisKeys' },
      { text: 'RedisKeys.Token', title: '令牌', link: 'RedisKeys_Token' },
      { text: 'RedisKeys.Cell', title: '格子库定义', link: 'RedisKeys_Cell' }
    ]
  }
];

/**
 * 页面导航
 */
const net_nav = [
  {
    text: 'Quick',
    link: '/quick/'
  },
  {
    text: 'Quick 6.0',
    link: '/quick/6.0/'
  },
  {
    text: 'Quick 5.0',
    link: '/quick/5.0/'
  },
  {
    text: 'c#',
    link: '/net/csharp/'
  }
  //   {
  //     text: 'Net5',
  //     link: '/flve/'
  //   },
  //   {
  //     text: 'Core',
  //     link: '/core/'
  //   },
  //   {
  //     text: 'Net 4.X',
  //     link: '/mvc/'
  //   }
];

/**
 * 侧边栏
 */
const net_sidebar = {
  '/quick/5.0/': [
    {
      text: '快框架说明',
      link: 'index'
    },
    {
      text: 'Code 示例',
      link: 'code/index',
      // path: 'quick/',
      //collapsable:false;
      //quick 二级目录
      children: [
        {
          text: '快速开始',
          link: 'index'
        },
        {
          text: 'QuickBLL',
          link: 'bll'
        },
        {
          text: 'QuickEnum',
          link: 'enum'
        },
        {
          text: 'QuickDB 数据库',
          link: 'db',
          collapsable: true
        },
        {
          text: 'Api 数据接口',
          link: 'db_api',
          collapsable: true
        },
        {
          text: 'Api 数据对象',
          link: 'db_model',
          collapsable: true
        },
        {
          text: 'QuickSafe 权限',
          link: 'safe'
        },
        {
          text: 'QuickSafe 认证',
          link: 'safe_auth'
        },
        {
          text: 'QuickFrame 框架',
          link: 'frame'
        },
        {
          text: 'QuickCache 缓存',
          link: 'cache'
        },
        {
          text: 'QuickSocket 消息库',
          link: 'socket'
        },
        {
          text: 'JsonHelper',
          link: 'json'
        },
        {
          text: 'XMLHelper',
          link: 'xml'
        }
      ]
    }
  ],
  '/quick/6.0/': [
    {
      text: '快框架说明',
      link: 'index'
    },
    {
      text: 'Code 示例',
      link: 'code/index',
      children: [
        {
          text: 'QuickBLL',
          link: 'bll'
        },
        {
          text: 'Json',
          link: 'json'
        },
        {
          text: 'QuickImg',
          link: 'img'
        },
        {
          text: 'ORM',
          link: 'orm'
        },
        {
          text: '查询',
          link: 'qp'
        },
        {
          text: '数据库',
          link: 'db'
        },
        {
          text: 'QuickSafe 权限',
          link: 'safe'
        }
      ]
    },
    ...quick_6_md
  ],
  '/quick/': [
    {
      text: '框架',
      link: 'index'
    }
  ],
  '/net/csharp/': [
    {
      text: 'c# 语法',
      link: '/index',
      children: [
        {
          text: '基础',
          link: 'base'
        },
        {
          text: 'DI函数注入',
          link: 'di'
        }
      ]
    }
  ]
};

module.exports = { net_nav, net_sidebar };
