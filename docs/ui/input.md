# Input 输入框

基本表单组件，用来输入信息。

## Input 输入示例

<CodeRun auto editable>

```vue
<template>
  <Form label-width="20%" label-mark="：">
    <Field label="文本">
      <Input v-model="the.code" placeholder="Enter LoginCode" autofocus />
    </Field>
    <Field label="密码">
      <Input
        v-model="the.password"
        type="password"
        placeholder="Enter password"
      />
    </Field>
    <Field label="数字">
      <Input v-model="the.GID" type="number" disabled="disabled" />
    </Field>
    <Field label="数字">
      <Input
        v-model="the.GID"
        named="GID"
        placeholder="请输入组织ID"
        type="number"
        icon="icon-sousuo"
        @onEvent="onInputEvent"
        @blur="onInputBlur"
        @focus="onInputFocus"
      />
    </Field>
    <Field label="文本">
      <Textarea
        v-model="the.text"
        placeholder="请输入多行文本框"
        spellcheck="false"
        rows="3"
        @onEvent="onInputEvent"
      />
    </Field>
  </Form>
</template>

<script>
export default {
  setup() {
    // 使用外挂方式引入
    const { reactive } = window.$plus.vue;

    const the = reactive({
      password: 'abc',
      code: '',
      GID: 123,
      text: '标题\r\n内容'
    });

    const onInputEvent = (resp) => {
      console.log(resp);
      switch (resp.cmd) {
        case 'clear':
          break;
        case 'password':
          break;
      }
    };

    /**
     * 失去焦点
     * @param {*} event 对象
     */
    const onInputBlur = (event) => {
      console.log(event.target.value);
      //let data = props.modelValue;
      //emit('onEvent', { cmd: 'blur', named: props.named, data, event });
    };

    /**
     * input失去焦点,并且更新值
     * @param {*} event 对象
     */
    const onChangeEvent = (event) => {
      console.log('change', event.target.value);
      // let data = props.modelValue;
      // emit('onEvent', { cmd: 'blur', named: props.named, data, event });
    };

    /**
     * 获取焦点
     * @param {*} event
     */
    const onInputFocus = (event) => {
      console.log(event.target.value);
      // let data = props.modelValue;
      // emit('onEvent', { cmd: 'focus', named: props.named, data, event });
    };

    return { the, onInputBlur, onInputFocus, onInputEvent };
  }
};
</script>
```

</CodeRun>

## v-model props 动态绑定

- 传递一个属性 value，然后接收一个 input 事件

```vue
<template>
  <div>
    <input :value="role.ID" @input="inputEvent" />
  </div>
</template>

<script>
export default {
  props: {
    //传入值
    role: {
      type: Object
    }
  },
  setup() {
    const inputEvent = (res) => {
      console.log('收到的值', res);
    };
    return { inputEvent };
  }
};
</script>
```

## Input props

| 属性          | 说明                                                                                       | 类型    | 默认值    |
| ------------- | ------------------------------------------------------------------------------------------ | ------- | --------- |
| named `0.4.2` | 组件命名                                                                                   | String  | input     |
| type          | 输入框类型 `text`, `textarea`,`password`, `url`, `email`, `date`, `number`,`tel`           | String  | text      |
| v-model       | 双向绑定数据                                                                               | String  | -         |
| clearable     | 是否显示清空按钮,触发事件名`clear`                                                         | Boolean | true      |
| cmd `0.6.4`   | 扩展图标,自定义触发事件名                                                                  | String  | InputPlus |
| icon          | 输入框扩展图标,点击触发事件名                                                              | String  | -         |
| disabled      | 设置输入框为禁用，可复制，不能接收焦点,设置后文字的颜色会变成灰色                          | string  | disabled  |
| readonly      | 设置输入框为只读,可复制使用 Tab 键切换到该字段,可选择,可以接收焦点，还可以选中或拷贝其文本 | string  | true      |

## Input slot 插槽

| 名称           | 说明           |
| -------------- | -------------- |
|                | 插入输入框头部 |
| append `0.4.8` | 输入框尾部     |

## Input events

| 事件名  | 说明                                                                      | 值     |
| ------- | ------------------------------------------------------------------------- | ------ |
| onEvent | 点击触发回调事件                                                          |        |
| cmd     | 指令类别 `clear` 清除内容,`password` 显示密码,`InputPlus`扩展图标默认命令 | string |
| named   | 自定义组件命名                                                            | string |
| data    | 交互数据                                                                  | string |
| event   | 触发组件对象$event，用于位置等                                            | string |

## input 标签事件

- @input 实时触发

在 Html 中 input 标签的相关事件写法：

- onfocus 获取焦点事件

- onblur 失去焦点事件（触发条件：先获取焦点，再失去焦点触发）

- onchange
  input 失去焦点并且它的 value 值发生变化时触发

- oninput
  input 框输入过程中 value 值改变时实时触发，及没输入一个字符都会触发

- onclick
  input 标签 type="button"时的点击事件

- onkeydown
  input 框输入时键盘按钮按下事件

- onkeyup
  input 框输入时键盘按钮抬起事件，触发 onkeyup 事件之前一定触发 onkeydown 事件

- onselect 选中触发
