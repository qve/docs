# config 组件配置

## ui.config 组件配置

- 0.4.1
  - `fontSize`

## config 动态响应

```vue
<template>
  <!-- Frames 全局浮动窗口组件 -->
  <Frames :mobile="frameMobile" :specs="frameSpecs" />
</template>

<script>
import { onMounted, ref } from 'vue';
export default {
  name: 'App',
  setup() {
    //const $plus = window.$plus;
    // const {ref} = window.$plus.vue;

    // 浮窗初始化 是否小屏幕尺寸，全屏打开
    const frameMobile = ref(false);
    // 浮窗基础参数配置
    const frameSpecs = ref({
      width: document.body.clientWidth,
      height: document.body.clientHeight
    });

    const onResize = () => {
      // 页面基础字体 缩放
      const rem = html.fontRem();

      // 初始化 基础字体大小
      fontSize(rem.fontSize);

      // 小屏幕尺寸，全屏打开
      frameMobile.value = rem.offsetWidth < 1024 ? true : false;

      // 窗体宽度
      frameSpecs.value = {
        width: rem.offsetWidth,
        height: document.body.clientHeight
      };

      // console.log(
      //   'App.frameSpecs:' + frameMobile.value,
      //   JSON.stringify(frameSpecs.value)
      // );
      // console.log('App.onResize:' + frameMobile.value, rem);
    };

    onMounted(() => {
      // 执行基础字体缩放
      onResize();
      // 窗口发生变化时
      window.onresize = onResize;
    });

    return { frameMobile, frameSpecs };
  }
};
</script>
```

## 配置全局坐标 z-index

> 全局统一动态计算 z-index 值

```js
/**
 * 组件库配置
 */
const config = {
  /** 日志配置 */
  log: {
    /** 是否打印输出 */
    isPrint: false
  },
  /** 组件配置 */
  ui: {
    /** 字体默认大小 */
    fontSize: 20,
    /** 全局组件深度 */
    zIndex: 9,
    // 是否手机端
    isMobile: true,
    /** 消息组件 */
    message: {
      /**全局默认自动关闭时间 15分之一秒 */
      timeOut: 20
    },
    /** 拖拽窗体 */
    drag: {
      min: {
        // 最小默认宽度
        width: 300,
        // 默认高度
        height: 32
      }
    }
  }
};

/**
 * 更新全局配置
 * @param {object} opts 需要更新的json参数最多3层
 */
config.bind = (opts) => {};

/**
 * 全局浮动层深度叠加
 * @param {number} _value 默认是+1
 * @returns  ui.zIndex,最高999
 */
const zIndexAdd = (_value) => {
  _value = _value || 1;
  config.ui.zIndex = config.ui.zIndex + _value;
  if (config.ui.zIndex > 9999) {
    config.ui.zIndex = 9;
  }
  return config.ui.zIndex;
};

/**
 * 查询配置是否小屏幕设备
 * @param {Boolean} _value 设置是否小屏幕
 * @returns
 */
const isMobile = (_value) => {
  if (_value != null) {
    config.ui.isMobile = _value;
  }
  return config.ui.isMobile;
};

/**
 * 更新基础字体大小
 * @param {Number} _value 设置字体大小
 * @returns
 */
const fontSize = (_value) => {
  if (_value != null) {
    config.ui.fontSize = _value;

    config.ui.drag.min = {
      // 最小默认宽度
      width: _value * 15,
      // 默认高度
      height: _value * 1.6
    };
  }
  return config.ui.fontSize;
};

export { config, zIndexAdd, isMobile, fontSize };
```

## 组件规范

### emit 组件交互

- 以下方式定义组件回调触发接口规范。

```vue
<template>
  <div>
    <Icon type="icon-cuo" @click="btnClear" />
  </div>
</template>

<script>
export default {
  props: {
    /** 组件自定义命名 */
    named: {
      type: [String, Number],
      default: 'myTest'
    }
  },
  setup(props, { emit, attrs }) {
    /** 点击清除事件 */
    const btnClear = (event) => {
      // 组件回调事件
      emit('onEvent', {
        cmd: 'clear',
        named: props.named,
        data: '交互数据',
        event
      });
    };

    return { btnClear };
  }
};
</script>
```

便于通用事件监听处理，建议按一下方式发送事件交互

| 事件名  | 说明                 | 值     |
| ------- | -------------------- | ------ |
| onEvent | 点击触发回调事件     | json   |
| cmd     | 指令类别             | string |
| named   | 组件命名             | string |
| data    | 交互数据             | string |
| event   | 触发组件，用于位置等 | string |

### emit props 更新

更新双向绑定值 `v-model`
`emit('update:modelValue', value)`

## ref 子组件交互

- 父组件触发调用子组件内的方法

```vue
<template>
  <div>
    <Button @click="btnEvent('cmd')">调用子组件方法</Button>
    <my-test ref="domPage" @onEvent="onEvent" />
  </div>
</template>

<script>
export default {
  props: {
    /** 组件自定义命名 */
    named: {
      type: [String, Number],
      default: 'myTest'
    }
  },
  setup(props, { emit, attrs }) {
    /** 定义动态变量名的对象 */
    const domPage = ref();

    /** 组件回调事件 */
    const onEvent = (resp) => {};

    const btnEvent = (resp) => {
      // 子组件内注册的方法
      domPage.value.btnClear();
    };

    return { domPage, onEvent, btnEvent };
  }
};
</script>
```
