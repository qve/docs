# UBB 组件

用于 ubb 与 html 标准转换

## Ubb html ASCII

由于 `CodeRun` 组件代码会转化 `ubb`,本示例都指定了 `format` 参数

<CodeRun auto test editable>

```vue
<template>
  <TextUbb
    format="html"
    v-model="the.ubb"
    rows="5"
    @onEvent="onEvent"
    style="width:60%;height:5rem;"
  />

  <TextUbb
    format="html"
    v-model="the.html"
    rows="5"
    style="width:60%;height:5rem;"
  />

  <TextUbb
    format="text"
    v-model="the.text"
    rows="5"
    style="width:60%;height:5rem;"
  />
</template>

<script>
export default {
  setup() {
    // 使用外挂方式引入
    const { reactive } = window.$plus.vue;

    const the = reactive({
      ubb: '&lt;b&gt;我是ubb代码&lt;/b&gt;',
      html: '<b>我是html 代码</b>',
      text: '我是文本\n换行\n换行'
    });

    const onEvent = (resp) => {
      console.log('json', resp);
    };

    return { the, onEvent };
  }
};
</script>
```

</CodeRun>

## TextUbb Props

| 属性           | 说明                       | 类型    | 默认值 |
| -------------- | -------------------------- | ------- | ------ |
| v-model        | 双向绑定数据               | String  | -      |
| disabled       | 是否只读                   | Boolean | false  |
| clearable      | 是否显示清空按钮           | Boolean | false  |
| format `0.8.1` | 初始内容格式 ubb,html,text | String  | ubb    |
| named `0.4.2`  | 组件触发事件名称           | String  | ubb    |

## TextUbb onEvent

- 点击转换时触发

| 参数名           | 说明                                        | 类型    |
| ---------------- | ------------------------------------------- | ------- |
| cmd              | format:格式化触发,error:错误触发,clear:清除 | String  |
| named            | 组件命名                                    | String  |
| data             | format 的内容                               | String  |
| format `0.8.1`   | 当前内容的格式                              | Boolean |
| byFormat `0.8.1` | 源内容格式                                  | Boolean |
| event            | 组件触发对象                                | Boolean |
| message          | error 错误提示                              | String  |
