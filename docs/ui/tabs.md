# Tabs 标签页

选项卡切换组件，常用于平级区域大块内容的的收纳和展现。

## Tabs 选项卡

自定义内容插槽名

- 支持标签按住拖动排序

<CodeRun auto editable>

```vue
<template>
  <div class="help-page">
    <Tabs
      :list="the.tabs.list"
      :active="the.tabs.active"
      closable
      scrolled
      @onEvent="onTabsEvent"
    >
      <template #bar>左插槽内容</template>
      <template #extra><Icon type="icon-jiahao" />扩展插槽</template>
      <div style="background: #fff;">公共内容</div>
      <template #qv-tab_dot>
        <TextJson v-model="the.body" style="height: 10rem; width:70%;" />
      </template>
      <template #qv-tabs-3>
        <TextUbb v-model="the.ubb" style="height: 10rem; width:70%;" rows="5" />
      </template>
      <template #qv-tabs-4>
        <div class="help-html">
          <Textarea
            v-model="the.md"
            placeholder="请输入多行文本框"
            spellcheck="false"
            cols="300"
            rows="20"
            @onEvent="onTabsEvent"
          />

          <TextHtml v-model="the.md" />
        </div>
      </template>
    </Tabs>
  </div>
</template>

<script>
// import { reactive, h } from '../../components/vue.api';
export default {
  setup() {
    // 外挂Api
    const { reactive, h } = window.$plus.vue;
    const the = reactive({
      body: { d: 3 },
      ubb: '4',
      md: '<b>html 5</b>',
      tabs: {
        // 当前激活的序号
        active: 0,
        list: [
          {
            title: '架构1',
            dot: true,
            body: '<p>内容 1 </p><b> html Body or Model</b>',
            model: {
              props: {
                value: 123
              },
              component: 'Input'
            }
          },
          {
            title: '架构2',
            body: '内容2',
            dot: '5m',
            model: {
              props: {
                title: '点此修改dot'
              },
              // 非全局自定义组件需标记markRaw
              //component: markRaw(TextPage)
              // 函数组件
              component: (props, { emit }) => {
                let _len = 12;
                const btnEvent = () => {
                  console.log('btnEvent');
                  // 传入参数, 可自定义触发选项卡索引号 byIndex
                  emit('onEvent', { cmd: 'edit', data: _len++ });
                };

                //  return h('div', `${props.title}`)
                return h('div', [
                  h(
                    'Button',
                    {
                      class: ['foo', 'bar'],
                      style: { margin: '10px' },
                      // attrs: attrs,
                      onClick: btnEvent
                    },
                    `${props.title}`
                  )
                ]);
              }
            },
            /** 监听组件回调 */
            onEvent: (resp) => {
              console.log('onEvent', resp);
              // 修改标识来自默认索引序号
              the.tabs.list[resp.byIndex].dot = resp.data;
            }
          },
          {
            // key 自定义插槽命名
            key: 'qv-tab_dot',
            title: '应用3',
            body: '内容3'
          },
          {
            title: '服务器4',
            body: '内容4'
          },
          {
            title: '组织列表5',
            body: '内容5'
          }
        ]
      }
    });

    // 本地组件引入
    // const EditPage = () => import('../views/EditPage.vue');
    // the.tabs.list.push({
    //   title: '外挂组件',
    //   dot: true,
    //   // 添加组件
    //   model: {
    //     props: {
    //       title: '组件传入的参数'
    //     },
    //     component: markRaw(EditPage)
    //   }
    // });

    const onTabsEvent = (resp) => {
      console.log('onTabsEvent', resp);
      switch (resp.cmd) {
        case 'click':
          // 标签点击切换触发事件
          break;
        case 'close':
          // 标签点击关闭触发
          the.tabs.list.splice(resp.index, 1);
          break;
        case 'tabDrag': // 拖动排序触发
          console.log('tabDrag', resp.index);
          the.tabs.list = resp.change(the.tabs.list);
          the.tabs.active = resp.index;
          break;
      }
    };

    return { the, onTabsEvent };
  }
};
</script>
```

</CodeRun>

## template 动态插槽名

- `template`可以用`v-slot`指令参数定义动态的插槽名

```vue
<template>
  <div class="ap-main-page">
    <Tabs :list="the.tabs.list" scrolled :active="the.tabs.active">
      <template
        v-for="(tab, index) in the.tabs.list"
        v-slot:[tab.key]
        :key="index"
      >
        <Listor :config="the.listor[tab.key]" />
        <Text class="qf-06 qcf-4">{{ tab.foot }}</Text>
      </template>
    </Tabs>
  </div>
</template>
<script>
export default {
  setup() {
    const { reactive } = window.$plus.vue;
    const the = reactive({
      tabs: {
        active: 0,
        list: [
          {
            // 动态插槽名
            key: 'history',
            title: '上网记录'
          },
          {
            key: 'filter',
            title: '免费放行记录'
          },
          {
            key: 'online',
            title: '在线终端'
          },
          {
            key: 'kill',
            title: '踢终端',
            // 底部绑定内容
            foot: '新增需要踢掉的终端,登录时会自动插入'
          }
        ]
      },
      // 动态列表配置参数
      listor: {
        history: {
          http: {
            url: '/DB/AP_History/',
            data: {
              tpl: 1
            }
          }
        },
        filter: {
          http: {
            url: '/DB/Nas_FilterDayLog/',
            data: {
              tpl: 1
            }
          }
        },
        online: {
          http: {
            url: '/DB/AP_Online/',
            data: {
              tpl: 1
            }
          }
        },
        kill: {
          http: {
            url: '/DB/AP_Kill/',
            data: {
              tpl: 1
            }
          }
        }
      }
    });
    return { the };
  }
};
</script>
```

## Tabs slot

| 名称             | 说明                                 | 置入位置     |
| ---------------- | ------------------------------------ | ------------ |
| bar `0.4.2`      | 插入头部标签条                       | 头部标签     |
| extra            | 头部标签条右边扩展                   | 头部右边扩展 |
| `qv-tabs-`+index | 默认命名用序号规则生成,可自定义`key` | 内容插槽     |
| 无               | 置于`qv-tabs-body` 顶部公共内容      | 内容插槽     |

## Tabs props

| 属性          | 说明                              | 类型    | 默认值         |
| ------------- | --------------------------------- | ------- | -------------- |
| named `0.4.2` | 组件触发事件名称                  | String  | tabs           |
| css `0.4.2`   | 样式皮肤，默认带框线              | String  | qv-tabs-border |
| auto          | 自动加载所有标签内容,默认点击触发 | Boolean | false          |
| active        | 选中页的序号,默认无激活           | Number  | -1             |
| closable      | 开启关闭按钮,默认无               | Boolean | false          |
| scrolled      | 关闭滑动按钮,默认有               | Boolean | false          |
| list          | 标签页的数据                      | Array   | []             |

### Tabs.list

| 参数名      | 说明                         | 类型           |
| ----------- | ---------------------------- | -------------- |
| key `0.7.0` | 拖动排序与插槽自定义 key     | String         |
| title       | 标签名称                     | String         |
| dot         | 标签徽标内容 `Badge`         | Boolean,String |
| body        | html 内容                    | String         |
| model       | 绑定组件 `{props,component}` | josn           |
| close       | 允许关闭标签,默认不允许      | Boolean        |
| onEvent     | 自定义监听组件交互事件       | function       |

## Tabs onEvent

| 事件名          | 说明                            | 值     |
| --------------- | ------------------------------- | ------ |
| onEvent         | 点击触发回调事件                | json   |
| cmd             | 指令 `click` ,`close`,`tabDrag` | string |
| named           | 组件命名                        | string |
| item            | 当前点击对象                    | Object |
| index           | 当前标签页的索引                | Number |
| byIndex `0.7.0` | 标签自定义触发事件来源的索引    | Number |

- cmd
  - `tabDrag` `0.7.0` 拖动排序触发更新事件 `resp.change`
