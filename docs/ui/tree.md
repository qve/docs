# Tree 树形组件

用来展示树形结构的数据，具有展开关闭等功能

## Tree 示例

<CodeRun auto  editable>

```vue
<template>
  <div>
    <Row>
      <Col span="4">
        <Tree checked :data="the.tree.list" @onEvent="onTreeEvent" />
      </Col>
      <Col span="20">
        <div>{{ the.tree.onItem.id }}{{ the.tree.onItem.title }}</div>
      </Col>
    </Row>
  </div>
</template>

<script>
/**
 * 测试树 数据
 */
const testTreeData = [
  {
    title: '组织',
    key: '1',
    node: { id: 1 },
    children: [
      {
        id: 2,
        title: '组织 1',
        key: '1-1',
        node: { id: 2 },
        children: [
          {
            title: '组织 1-1',
            key: '1-1-1',
            expand: true,
            node: { id: 3 },
            children: [
              {
                title: '组织 1-1-1',
                key: '1-1-1-1',
                node: { id: 4 }
              },
              {
                title: '组织 1-1-2',
                key: '1-1-1-2',
                node: { id: 5 }
              },
              {
                title: '组织 1-1-3',
                key: '1-1-1-3',
                node: { id: 6 }
              }
            ]
          },
          {
            title: '组织 1-2',
            key: '1-1-2',
            node: { id: 7 }
          }
        ]
      },
      {
        title: '组织 2',
        key: '1-2',
        node: { id: 8 },
        children: [
          {
            title: '组织 2-1',
            key: '1-2-1',
            node: { id: 9 }
          },
          {
            title: '组织 2-2',
            key: '1-2-2',
            node: { id: 10 }
          }
        ]
      }
    ]
  },
  {
    title: '角色组 2',
    key: '2',
    node: {
      id: 11
    },
    children: [
      {
        title: '角色组 2-1',
        key: '2-1',
        node: {
          id: 12
        }
      },
      {
        title: '角色组 2-2',
        key: '2-2',
        node: { id: 13 },
        children: []
      }
    ]
  }
];

export default {
  setup() {
    // 使用外挂方式引入
    const { reactive } = window.$plus.vue;
    const { bll, json } = window.$plus.quick;

    const the = reactive({
      tree: {
        // 当前点击节点
        onItem: {},
        // 勾选数组
        onChecks: [],
        // 节点数据列表
        list: [
          {
            title: '组织',

            node: {
              id: 1
            }
          },
          {
            title: '角色',
            node: { id: 2 },
            children: [
              {
                node: { id: 21 },
                title: '人民'
              },
              {
                node: { id: 22 },
                title: '群众'
              }
            ]
          }
        ]
      }
    });

    /**
     * 获取对象
     */
    const getTree = (resp) => {
      console.log('getTree:', resp.active);

      if (!resp.item) {
        return;
      }

      json.find({
        // 数据源
        data: testTreeData,
        // 搜索子项节点
        // subs: 'children',
        // 查询条件
        where: `data.key=="${resp.item.key}"`,
        //异步回调(对象,level:层次,序号)
        callback: (obj, level, index) => {
          console.log('find2:', level, index);

          if (obj && obj.children) {
            // console.log('find3:', JSON.stringify(obj))
            /**
             * 回调树节点绑定
             * @param {*} resp 子节点数组
             */
            resp.bind(obj.children);
            return 1;
          }
        }
      });
    };

    /** 树点击事件 */
    const onTreeEvent = (resp) => {
      console.log('onTreeEvent', resp.item);
      switch (resp.cmd) {
        case 'open':
          getTree(resp.bind, resp.item);
          break;
        case 'click':
          the.tree.onItem = resp.item;
          break;
        case 'check':
          // 勾选数组
          the.tree.onChecks = resp.checks;
          // 当前勾选的值
          console.log('onTreeEvent.check', resp.value);
          break;
      }
    };

    return { the, onTreeEvent };
  }
};
</script>
```

</CodeRun>

## Tree Props

| 属性    | 说明                   | 类型    | 默认值       |
| ------- | ---------------------- | ------- | ------------ |
| theme   | 默认黑暗主题           | String  | qv-tree-dark |
| active  | 自定义激活节点         | String  | -            |
| data    | 绑定的数据             | Array   | -            |
| checked | 是否启用勾选           | Boolean | false        |
| primary | 绑定勾选取值的主键字段 | String  | id           |

### Tree data 绑定的数据

| 属性     | 说明           | 类型   | 默认值 |
| -------- | -------------- | ------ | ------ |
| id       | 节点勾选绑定值 | String |        |
| title    | 节点描述       | String | -      |
| children | 子节点         | Array  | -      |

## Tree events

| 事件名  | 说明             | 返回值 |
| ------- | ---------------- | ------ |
| onEvent | 点击触发回调事件 | json   |

### Tree onEvent

组件触发返回数据

| 参数名          | 说明                                           | 类型   |
| --------------- | ---------------------------------------------- | ------ |
| cmd             | 命令打开`open`,`close`,点击`click`,勾选`check` | String |
| title           | 当前节点描述                                   | String |
| active          | 当前层级                                       | String |
| item            | 当前选中的项 `node`节点原数据                  | Object |
| primary `0.6.4` | 当前节点主字段，勾选字段                       | String |
| checks `0.6.4`  | 多选数组                                       | String |
