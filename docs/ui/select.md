# Select 选择器

统一封装组件接口，便于组件交互调用

## Select 下拉选择

<CodeRun auto editable>

```vue
<template>
  <div>
    <Select
      named="select_test"
      v-model="the.type"
      @onEvent="onSelectEvent"
      :list="the.ls"
    />

    <Select v-model="the.type" @onEvent="onSelectEvent">
      <option :value="null" label="未配置"></option>
      <option v-for="(item, index) in the.typeList" :key="index" :value="index">
        {{ item }}
      </option>
    </Select>

    <Select v-model="the.isTest" @onEvent="onSelectEvent">
      <option :value="true" label="true"></option>
      <option :value="false" label="false"></option>
    </Select>

    非双向绑定
    <Select :value="the.isTest" @onEvent="onSelectEvent">
      <option :value="true">true</option>
      <option :value="false">false</option>
    </Select>
  </div>
</template>

<script>
export default {
  setup(props, context) {
    // 使用外挂方式引入，具体查看demo
    const { message } = window.$plus;
    const { reactive } = window.$plus.vue;

    // 数据库选项定义参数格式转为 json
    let _ls = 'null:未配置,0:初始,1:正常,2:已审,3:撤审,4:删除';

    const the = reactive({
      isTest: true,
      type: 2,
      typeList: ['初始', '正常', '已审', '撤审', '删除']
    });

    /**
     * ls 正确解析交换参数
     */
    const bind = (_ls) => {
      let _res = {};
      // 正则替换
      _ls.replace(/([^?,]+):([^?,]+)/g, function (s, k, v) {
        // s全部
        // console.log('s:' + s, k, typeof k, v);
        // 交换key顺序 value，转换为数字
        _res[v] = parseInt(k);
        return v + '=' + k;
      });
      return _res;
    };

    the.ls = bind(_ls);

    const onSelectEvent = (res) => {
      console.log('onSelectEvent', res);

      switch (res.cmd) {
        case 'change':
          message.info('选择了:' + res.value);
          let obj = the.typeList[res.index]; //筛选出匹配数据
          // obj = the.typeList.find((i) => {
          //   return i === res.value;
          // });
          break;
      }
    };

    return { the, onSelectEvent };
  }
};
</script>
```

</CodeRun>

## Select props

- `0.7.0` `v-model` 支持绑定值为 `null`

| 属性          | 说明             | 类型   | 默认值 |
| ------------- | ---------------- | ------ | ------ |
| named `0.6.3` | 组件命名         | String | select |
| list `0.6.3`  | 绑定选项内容     | object |        |
| v-model       | vue 双向绑定数据 |        |        |
| :value        | vue 单向绑定数据 |        |        |

## Select events

| 事件名  | 说明             | 返回值 |
| ------- | ---------------- | ------ |
| onEvent | 点击触发回调事件 | json   |

## onEvent cmd

| 参数名        | 说明                   | 类型    |
| ------------- | ---------------------- | ------- |
| cmd           | 事件命令：变更`change` | string  |
| value         | 选中值的值             | Number  |
| type `0.6.3`  | 单向绑定选中值的类型   | string  |
| label `0.6.3` | 选中值的描述           | string  |
| event         | 选中对象               | Boolean |
