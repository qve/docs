# Checkbox 多选框

基本组件-多选框。主要用于一组可选项多项选择，或者单独用于标记切换某种状态。

## Checkbox 示例

<CodeRun auto editable>

```vue
<template>
  <Checkbox value="all" @onEvent="onCheckEvent" /> 全选

  <Checkbox
    v-for="(item, index) in the.typeList"
    :key="index"
    v-model="the.data.typeCk"
    :value="index"
    :index="index"
    :named="'check_' + index"
  >
    {{ item }}
  </Checkbox>
</template>

<script>
export default {
  setup() {
    // 使用外挂方式引入

    const { reactive } = window.$plus.vue;

    const the = reactive({
      data: {
        typeCk: [3]
      },
      typeList: ['初始', '正常', '已审', '撤审', '删除']
    });

    /** 全选 */
    const onCheckEvent = (resp) => {
      console.log('onCheckEvent:', resp);

      //resp.value 是组件自定义绑定值，根据实际应用定义
      if (resp.value === 'all') {
        // 全选
        let _list = [];
        if (resp.checked) {
          // 选中
          for (let i in the.typeList) {
            _list.push(i);
          }
        }
        the.data.typeCk = _list;
      }
    };

    return { the, onCheckEvent };
  }
};
</script>
```

</CodeRun>

## Checkbox props

| 属性          | 说明               | 类型    | 默认值 |
| ------------- | ------------------ | ------- | ------ |
| named `0.4.3` | 组件命名           | String  | check  |
| value         | 只在单独使用时有效 | Boolean | false  |
| v-model       | 双向绑定数据       | String  | -      |

## Checkbox events

| 事件名  | 说明             | 返回值 |
| ------- | ---------------- | ------ |
| onEvent | 点击触发回调事件 | json   |

## Checkbox cmd `check`

| 参数名        | 说明           | 类型           |
| ------------- | -------------- | -------------- |
| cmd `0.4.3`   | `check`        | json           |
| value         | 绑定的值       | String         |
| named         | 组件名         | String         |
| index `0.6.4` | 多组件序号     | String, Number |
| id            | 控件的随机名称 | String         |
| checked       | 是否选中       | Boolean        |
