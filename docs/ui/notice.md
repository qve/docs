# Notice 通知提醒

在界面右上角显示可关闭的全局通知，常用于以下场景：

- 通知内容带有描述信息
- [message 全局订阅消息](./message.md)

## Notice 通知提醒

点击执行[>]按钮测试

<CodeRun editable>

```vue
<template>
  <Notice
    title="组件非全局订阅消息"
    type="warning"
    style="top:2rem;right:2rem"
    v-if="the.noticeShow"
    @onEvent="noticeEvent"
  >
    <div style="padding:0.4rem;padding-top:0">
      <Icon type="icon-star" /> 我是自动关闭的
    </div>
  </Notice>
</template>
<script>
export default {
  setup() {
    const { reactive } = window.$plus.vue;
    const the = reactive({ noticeShow: true });
    const noticeEvent = (res) => {
      switch (res.cmd) {
        case 'close':
          the.noticeShow = false;
          break;
      }
    };

    return { the, noticeEvent };
  }
};
</script>
```

</CodeRun>

## Notice 通知消息

| 属性     | 说明                                                                 | 类型    | 默认值        |
| -------- | -------------------------------------------------------------------- | ------- | ------------- |
| title    | 通知提醒的标题                                                       | String  | -             |
| type     | 通知提醒的类型，可选`text`,`info`,`success`,`warning`,`error`,`mark` | String  | `text`        |
| closable | 是否可以关闭                                                         | Boolean | `true`        |
| named    | 组件命名，默认 `qv-notice_随机码`                                    | String  | getRandomCode |
| timeout  | 自动关闭时间 设置`0`不启用倒计时,初始配置`config.ui.message.timeOut` | Number  | 20            |
