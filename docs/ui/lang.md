# lang 多语言

项目支持简易多语言版本

## lang i18n 多语言

- 使用示例

<CodeRun auto editable>

```vue
<template>
  <div>
    <p>映射规则 today: {{ getLangBind() }} <Text v-html="getLangBind()" /></p>
    <lable>中文： </lable>
    <Text>{{ getLang('year') }}</Text
    ><br />
    <lable> 英文：</lable>
    <Text>{{ getLang('year', 'en') }}</Text>
  </div>
</template>

<script>
// import { lang } from 'qveui';
export default {
  setup() {
    const { lang } = window.$plus.ui;

    /**
     * 语法规范
     * 组件名.方法名.语言名
     */
    let langData = {
      zh: {
        datePanel: {
          year: '年',
          month: '月',
          today: '今日<b>{item}条记录</b>' //映射参数item
        }
      },
      en: {
        datePanel: {
          year: 'year',
          month: 'month',
          today: '<b>{item} records</b> today' //映射参数item
        }
      }
    };

    /**
     * 自定义读取语言方法名
     * @param {*} key 定义的关键词
     * @returns {string} 翻译文本
     */
    const getLang = (key, local) => {
      // 取出UI语言包
      // return lang.by('datePanel.' + key);

      // 取出自定义语言包,指定语种
      if (local) {
        return lang.by('datePanel.' + key, langData, local);
      } else {
        // 取出自定义语言包，默认 config.locale
        return lang.by('datePanel.' + key, langData);
      }
    };

    const getLangBind = () => {
      //  return lang.by('pageHelp.today', langPage);
      return lang.bind({ item: 6 }, lang.by('datePanel.today', langData));
    };

    return { langData, lang, getLang, getLangBind };
  }
};
</script>
```

</CodeRun>

## `lang.by` 语言读取

- [reduce 累加函数方法](../guide/h5/index.md#reduce-累加)

| 数组方法 | 说明                                           |
| -------- | ---------------------------------------------- |
| key      | 语言定义规则名                                 |
| data     | 自定义语言包,默认为库语言包                    |
| local    | 指定本地化语言名`zh`、`en`,配置`config.locale` |

## `lang.bind` 语言规则绑定

- json.bind 参数一致

| 数组方法 | 说明               |
| -------- | ------------------ |
| data     | 映射替换 json 内容 |
| format   | 需格式化规则       |

## `lang.getLocal` 当前浏览器语言

- 无参数
