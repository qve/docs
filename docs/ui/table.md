# Table 表格

用于展示大量结构化数据

- 支持表头排序、拖放、宽度调整、锁定表头 lock
- 单元格点击触发事件，求和自定义
- 表格导出 csv

## Table 示例

<CodeRun auto editable>

```vue
<template>
  <div>
    <Button @click="btnThXLS">导出csv</Button>
    <Table
      ref="tableDom"
      border
      :columns="the.columns"
      :data="the.dataTest"
      :start="the.paged.size * (the.paged.index - 1)"
      :foots="the.foots"
      :checkeds="the.checkeds"
      show-foot
      @onEvent="tableEvent"
    />
  </div>
</template>

<script>
export default {
  setup() {
    // 使用外挂方式引入
    const { frame, message } = window.$plus;
    const { reactive, ref, h } = window.$plus.vue;

    /** 表格对象,表格导出需要 */
    const tableDom = ref();

    /** 表格导出 */
    const btnThXLS = () => {
      // 触发导出 csv 事件
      tableDom.value.exportTo();
    };

    /**
     * 触发事件
     * @param {*} index 序号
     * @param {*} value 传入值
     */
    const btnIndex = (index, value) => {
      console.log('btnIndex:' + index, value);
      value = value || the.dataTest[index];
      frame.open({
        title: '测试函数组件' + index,
        model: {
          props: value,
          // 进行组件标记
          // 函数组件
          component: (props) => {
            return h('div', `${props.name}`);
          }
        }
      });
    };

    const the = reactive({
      columns: [
        {
          type: 'check',
          eventKey: '', //点击触发事件为空
          width: 40
        },
        {
          // 显示行号,锁定列zindex
          type: 'index',
          eventKey: 'index', //点击触发事件名
          title: '序',
          lock: 0, //锁定列序号
          width: 40
          // className: 'tb-row-cs',
          // 自定义样式
          //  style: 'width:1.5rem;text-align:left;',
        },
        {
          title: 'Name',
          lock: 1, //锁定列序号
          key: 'name'
        },
        {
          title: 'Age',
          key: 'age'
        },
        {
          title: 'Address',
          key: 'address',
          eventKey: 'address', // 事件触发,默认跟key一致，区分大小写
          width: 300
        },
        {
          title: '日期1',
          key: 'date'
        },
        {
          title: '日期2',
          key: 'date2'
        },
        {
          title: '日期3',
          key: 'date3'
        },
        {
          title: '日期4',
          key: 'date4'
        }
      ],
      // 默认选择
      checkeds: [0, 2],
      dataTest: [
        {
          name: 'John Brown',
          age: 1,
          address: '<b>New York</b> No. 1 Lake Park',
          date: '2016-10-03'
        },
        {
          name: 'Jim Green',
          age: 2,
          address: 'London No. 1 Lake Park',
          date: '2016-10-01'
        },
        {
          name: 'Joe Black',
          age: 3,
          address: 'Sydney No. 1 Lake Park',
          date: '2016-10-02'
        }
      ],
      // 表格底部
      foots: [
        {
          // 定义列名与 显示事件或者求和方法
          name: '合计',
          // 绑定方法(表格数据) 0.8.4 支持参数名
          age: (data) => {
            let value = 0;
            data.map((item) => {
              value += item.age;
            });
            //求和
            return value;
          }
        }
      ],
      data: [],
      // 查询分页标签
      paged: {
        // 查询地址
        api: '',
        // 查询模板类型 1:接口属性,2:只查询数据，不返回属性,其它自定义
        tpl: 2,
        // 查询条件
        where: [],
        // 查询扩展参数
        par: null,
        // 扩展外键查询
        join: null,
        // 当前页码
        index: 1,
        // 每页取数据条数
        size: 5,
        // 分页数
        sizes: [5, 10, 30, 50, 100],
        /** 总条数 */
        count: 0
        // 是否小型分页
        // small: true,
        // 最大页码按钮数
        // max: 4,
        // 页码按钮
        // layout: 'count, sizes, prev, pager, next, jumper',
      }
    });

    /**
     * 表格触发事件
     *
     */
    const tableEvent = (resp) => {
      // 触发事件
      console.log('tableEvent', resp);
      switch (resp.cmd) {
        case 'td':
          // if (resp.key === 'btn-td-index') {
          //   // 数据行点击
          //   btnIndex(resp.index);
          // } else {
          //   // 数据行点击 ，获取点击对象
          //   resp.event.currentTarget.innerHTML = 'ok';
          // }
          // 配置了头部点击触发事件
          if (resp.th.eventKey) {
            switch (resp.th.eventKey) {
              case 'index':
                // 数据行点击
                btnIndex(resp.row.index);
                break;
              default:
                // 数据行点击 ，获取点击对象
                resp.event.currentTarget.innerHTML = 'ok:' + resp.th.key;
            }
          }

          break;
        case 'checked':
          // 表格勾选
          the.checkeds = resp.checkeds;
          break;
        //thSort 表头排序
        case 'error_thSort':
          // 排序报错
          console.error('test.error_thSort', resp.error);
          message.error('排序错误，请检查配置:' + resp.data.key);
          break;
        case 'thDarg':
          // 拖动事件,更新表头顺序
          the.columns = resp.change(the.columns);
          break;
        default:
          console.log('test.tableEvent', JSON.stringify(resp));
          break;
      }
    };

    return { the, tableDom, btnThXLS, tableEvent };
  }
};
</script>
```

</CodeRun>

## Table props

| 属性     | 说明                                            | 类型   | 默认值          |
| -------- | ----------------------------------------------- | ------ | --------------- |
| data     | 显示的`JSON`结构化数据，数据内容支持 `html`标签 | Array  | []              |
| columns  | 表格列的配置                                    | Array  | []              |
| css      | 表格样式，可以设置表格宽度，例如`width:4rem`    | String | qv-table-border |
| rowMax   | 显示的行数，超过 10 条启用滑动条                | Number | 10              |
| foots    | 表格底部栏，显示数组，例如合计等                | Array  | []              |
| checkeds | 初始勾选中的数组                                | Array  | []              |

## columns 表头锁定列

| 属性         | 说明                                                       | 类型   | 默认值 |
| ------------ | ---------------------------------------------------------- | ------ | ------ |
| key          | 字段名                                                     | String |        |
| title        | 显示的表头标题                                             | String |        |
| type         | 值类型 `index:行号`, `none:不排序`, `int`, `date`,`String` | String |        |
| eventKey     | 点击触发事件名,默认跟 `key` 一致,类型`check`,指定绑定的值  | String |        |
| width        | 宽度                                                       | Number |        |
| className    | 列样式名                                                   | String |        |
| lock `0.6.3` | 锁定列的位置序号 0,1,2                                     | Number | 0      |

## exportTo csv 导出

- `0.7.0` 导出当前表格，需要 ref 调用, isDown 默认创建下载连接，false

- 调用参数 `exportTo(文件名，空为named, isDown = true)`

  | 事件名 | 返回内容说明                    |
  | ------ | ------------------------------- |
  | cmd    | exportToCSV                     |
  | name   | 导出文件名                      |
  | body   | isDown=false 导出生成的文件内容 |

## Table events

| 事件名  | 说明             | 返回值 |
| ------- | ---------------- | ------ |
| onEvent | 点击触发回调事件 | json   |

### Table `td`

- 点击表格数据触发

| 参数名 | 说明                  | 类型   |
| ------ | --------------------- | ------ |
| cmd    | 指令 `click` ,`close` | string |
| index  | 当前行的源数据序号    | Number |
| key    | 当前列的表头          | String |
| event  | 点击的回调事件        | Object |

### Table `thSort` 排序

- 点击表头排序触发

| 参数名 | 说明         | 类型   |
| ------ | ------------ | ------ |
| data   | 当前列的数据 | Object |

### Table `check` 勾选

- 源数据序号默认 key: `_tr_index`
  `0.6.8` 解决表格排序后，数据传出序号与源数据不一致。

- 点击勾选数据触发

| 参数名           | 说明                                 | 类型    |
| ---------------- | ------------------------------------ | ------- |
| named            | 勾选绑定的字段名                     | string  |
| id               | 触发对象 id                          | string  |
| index `0.6.8`    | 当前选中列的源数据序号               | Number  |
| value `0.6.8`    | 当前选中绑定的值，配置参数`eventKey` |         |
| checked          | 是否选中                             | Boolean |
| checkeds `0.4.5` | 所有选中的数据                       | Array   |

### Table cmd `checkAll`

- 点击全选触发

| 参数名           | 说明               | 类型    |
| ---------------- | ------------------ | ------- |
| named            | 触发组件的命名     | string  |
| id               | 触发对象 id        | string  |
| key              | 当前列的表头       | String  |
| index            | 当前选中列的索引   | Number  |
| checked          | 是否全部选中       | Boolean |
| checkeds `0.4.5` | 所有当前数组的序号 | Array   |

- `0.5.1` 取消的属性
  - list 属性取消
  - start 开始的页数

### Table foots 底部求和

- `0.8.4`
  `foots` 底部字段绑定方法回调新增(当前行数据，当前字段名,当前顺序) 比如求和方法(data,key,index)

```js
/**
 * 字段求和
 * @param {*} data 当前行数据
 * @param {*} key 当前字段名
 * @param {*} index 当前字段序号
 */
const sumField = (data, key, index) => {
  console.log(key, index);
  let value = 0;
  data.map((item) => {
    value += item[key];
  });
  //求和
  return value;
};

the.foots = [
  {
    name: '合计',
    // 求和方法(表格数据)
    age: sumField
  }
];
```
