# global

全局组件交互方法与接口

## global 全局接口

- [loading 加载](./loading.md)
- [message 消息](./message.md)
- [frame 弹窗](./frame.md)
- [confirm 对话框](./modal.md)

- subscribe
- SubscribeGlobalNamed
- emitSubscribeGlobal
- onSubscribeGlobal
- getRandomCode

## subscribe 全局订阅

全局组件订阅交互

## subscribe 订阅名

- [subscribe 方法](../lib/subscribe.md)

组件库中调用

<CodeRun dll="Subscribe" editable>

```js
/**
 * 全局组件事件订阅
 */
const { subscribe } = window.$plus.ui;

// 全局订阅keys
let _keys = subscribe.keys();

return _keys;
```

</CodeRun>

## base.js 组件库基础方法

## scrollTop 位置滑动

```js
/**
 * 对象滑动到
 * @param {*} el el.value 需要滑动的对象
 * @param {*} to 指定的高度px
 */
scrollTop(hourRef.value, 80);
// 创建一个 DOM 引用
// const hourRef = ref(null)
// hourRef.value.scrollTop=(parseInt(the.hh) - 1) * _height;
```

## getRandomCode 随机编码

```js
/**
 * 生成随机编码
 * @param {*} pre 命名前缀
 * @param {*} length 默认随机码长度8
 * @returns pre+'_'+code
 */
getRandomCode(pre, 8);
```

## tableExportToCSV 表格导出

```js
/**
 * 表格数据导出为CSV
 * @param {JSON} columns 需输出的数据表头
 * @param {JSON} data 需输出的数据列表
 * @param {string} fileName 导出文件名 默认 _qve.csv
 * @param {boolean} isDown 默认创建下载
 * @returns [fileName,body]
 */
tableExportToCSV(columns, data, fileName, (isDown = true));
```

## bindDateHead 日期周

```js
/**
 * 绑定日期周列表头部
 * @param {*} date 传入起始日期
 * @param {*} _weeks 传入星期表头new Array('日', '一', '二', '三', '四', '五', '六')
 * 返回{current:'当前日期',list:[{day:'日',week:'周几',date:'当前日期',css:'当前活跃 active'}]}
 */
```

## bindDataSort json 排序

```js
/**
 * 根据数据类型，创建json数据排序方法
 * @param {*} _key 排序的字段
 * @param {*} _type 排序的字段数据类型,支持index
 * @param {boolean} _desc 按降序排列
 * @returns
 */
```

## copyValue js 复制

- `0.8.0`

```js
import { message, copyValue } from 'qveui';
copyValue('https://lite.apwlan.com/#/work/do/oa/' + _item.ID); // 内容
message.info('已复制，可以分享粘贴了');
```

## bindForm 创建表单

- `0.8.0` 新增绑定原生表单值并默认提交

```js
import { bindForm } from 'qveui';

let config = {
  // 创建不自动提交,没有此参数自动提交
  make: true,
  // 表单自定义配置
  opts: {
    url: 'http://127.0.0.1:800/admin/login.php',
    method: 'post',
    target: '_blank',
    style: {
      // display: 'none'
    }
  },
  // 提交表单参数与内容
  data: {
    loginname: 'nasadministrator',
    password: ''
  },
  // 创建按钮不是必须
  btns: {
    提交: () => {
      // this.submit()
    }
  }
};

let _form = bindForm(config);

if (config.make) {
  // 只是创建表单，不提交表单
  document.body.appendChild(_form);
}
```

## loadScript js οnlοad

动态加载 js 脚本，JS 放在 head 和 body 中的区别

浏览器解析 html 是从上到下的。放在 head 中的 JS 代码会在页面加载完成之前就读取，而放在 body 中的 JS 代码，会在整个页面加载完成之后读取，放在 head 里则先被解析,但这时候 body 还没有解析，所以取 body 内容会返回空值。可以在页面加载完后执行 head 代码。
`windows.οnlοad=function(){//这里放入执行代码}`

```js
import { loadScript } from 'qveui';
// 创建请求对象
// document.body.appendChild(script);
let dom = document.body;
let reset = false; // 是否删除历史脚本重新加载
loadScript(
  {
    id: '配置此参数，只加载1次',
    src: '脚本地址',
    onload: () => {
      //加载完成初始化动作
    }
  }
  // ,reset // 自定义参数
  // ,dom // 自定义添加对象位置
);
```
