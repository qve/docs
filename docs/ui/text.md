# Text

文本标签样式

## Text 文本显示

<CodeRun auto editable>

```vue
<template>
  <div>
    <Text>hello</Text>
    <Text v-html="the.text" />
  </div>
</template>

<script>
export default {
  setup(props, context) {
    const { reactive } = window.$plus.vue;

    const the = reactive({
      text: '<s>测试html</s>'
    });

    return {
      the
    };
  }
};
</script>
```

</CodeRun>
