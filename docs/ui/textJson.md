# TextJson

- 支持编辑 json 与非表格式
- 点击眼睛图标转换格式

## TextJson 文本框

可以输入非标内容试试 `{a:2,c:'非标格式'}`

<CodeRun auto editable>

```vue
<template>
  <div>
    <TextJson
      v-model="the.body"
      placeholder="请输入json"
      rows="15"
      @onEvent="onEvent"
      style="width:60%;height:5rem;"
    />
  </div>
</template>

<script>
export default {
  setup(props, context) {
    // 使用外挂方式引入，具体查看demo
    const $plus = window.$plus;
    const { reactive } = $plus.vue;

    const the = reactive({
      body: { a: 1, b: 2, c: 'test' }
    });

    const onEvent = (resp) => {
      console.log('json', resp);
    };

    return { the, onEvent };
  }
};
</script>
```

</CodeRun>

## TextJson Props

| 属性           | 说明                   | 类型    | 默认值 |
| -------------- | ---------------------- | ------- | ------ |
| v-model        | 双向绑定数据           | String  | -      |
| placeholder    | 占位字符               | String  | -      |
| rows           | 文本框的行数           | Number  | -      |
| format `0.4.2` | 初始内容是否格式化显示 | Boolean | false  |
| named `0.4.2`  | 组件命名               | String  | json   |
| clearable      | 是否显示清空按钮       | Boolean | false  |

## TextJson onEvent

- 点击转换时触发

| 参数名        | 说明                                              | 类型    |
| ------------- | ------------------------------------------------- | ------- |
| cmd           | jsonUpdate:更新内容触发,error:错误触发,clear:清除 | Boolean |
| named `0.4.2` | 组件命名                                          | String  |
| type `0.4.2`  | 组件初始传入值类型,String,Object                  | String  |
| data          | 当前 `json` 内容                                  | String  |
| format        | 是否格式化显示 `json.makeFormat`                  | Boolean |
| event         | 组件触发对象                                      | Boolean |
| message       | error 错误提示                                    | String  |

## TextJson cmd

| 指令名        | 说明                       | 返回参数 |
| ------------- | -------------------------- | -------- |
| jsonUpdate    | 更新内容触发事件           |          |
| error         | 格式化错误触发             | message  |
| clear         | 内容清除                   |          |
| format`0.6.7` | 格式化点击触发事件         |          |
| change`0.6.7` | 传入内容发生变化时触发事件 |          |
