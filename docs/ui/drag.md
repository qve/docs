# Drag 拖拽窗体

标题可拖动窗体和缩放功能

- [frame 多窗口](./frame.md) 建议跨组件使用

## Drag 拖拽窗口

- 点击执行测试窗口

<CodeRun editable>

```vue
<template>
  <div>
    <Drag
      title=" Drag 拖拽窗口"
      :show="show"
      :move="move"
      @onEvent="onDragEvent"
    >
      <p>点击标题可拖拽Css: qv-drag</p>
      <p>边框上下可缩放</p>
      <p>标题栏边框可左右缩放</p>
    </Drag>
  </div>
</template>

<script>
export default {
  setup() {
    /** 弹窗按钮触发事件 */
    const onDragEvent = (resp) => {
      console.log('onDragEvent', resp);
      // 弹窗回调事件
    };

    // 窗口配置参数
    const move = {
      // 默认值
      width: 300,
      height: 200,
      top: 100,
      left: 100,
      // 多个窗体 间距
      space: 50,
      // 层级
      'z-index': 99
      // space: fontSize(),
    };

    /** 显示控制 */
    const show = {
      /** 显示头部栏 */
      head: true,
      /** 显示工具栏按钮 */
      btn: true,
      /** max 按钮 */
      max: true,
      /** 最小化按钮 */
      min: true,
      /** 是否允许拖动 */
      move: true,
      /** 关闭按钮 */
      close: true
    };

    return {
      onDragEvent,
      move,
      show
    };
  }
};
</script>
```

</CodeRun>

## Tag Props

| 属性          | 说明                 | 类型          | 默认值 |
| ------------- | -------------------- | ------------- | ------ |
| named `0.4.2` | 组件命名             | String,Number | drag   |
| title         | 头部描述             | String        |        |
| max           | 是否以最大化显示窗口 | Boolean       | false  |
| show          | 显示控制             | json          | {}     |
| move          | 指定移动位置参数     | json          | {}     |

- show

```json
{
  /** 显示头部栏 */
  "head": true,
  /** 显示工具栏按钮 */
  "btn": true,
  /** max 按钮 */
  "max": true,
  /** 最小化按钮 */
  "min": true,
  /** 是否允许拖动 */
  "move": true,
  /** 显示关闭按钮 */
  "close": true
}
```

- move

```json
{
  "width": 320,
  "height": 400,
  "top": 0,
  "left": 0,
  "z-inde": 1
}
```

## vue 拖放排序

- 按住拖动更新顺序

<CodeRun editable>

```vue
<template>
  <Card
    v-for="(item, index) in the.list"
    :key="index"
    draggable="true"
    @dragstart="dragStart(index)"
    @drop.prevent="dragStop()"
    @dragover.prevent="dragOver(index)"
  >
    {{ item.title }}
  </Card>
</template>

<script>
export default {
  setup() {
    const { reactive } = window.$plus.vue;

    const the = reactive({
      list: [
        { title: '事项1' },
        { title: '事项2' },
        { title: '事项3' },
        { title: '事项4' }
      ]
    });

    /**
     * 拖拽开始触发事件
     * @param {*} index 点击的序号
     */
    const dragStart = (index) => {
      the.dragIndexOld = index;
    };

    /**
     * 拖拽停止时触发
     * @returns
     */
    const dragStop = () => {
      // 位置没有发生改变
      if (the.dragIndexNew === the.dragIndexOld) {
        return;
      }

      // 位置发生了改变
      dragChange(the.list);
      // 上报更新
      // emit('onEvent',{
      //   cmd: 'drag',
      //   index: the.dragIndexNew,
      //   byIndex: the.dragIndexOld,
      //   change: dragChangeSort
      // });
    };

    /**
     * 拖拽经过其他元素时触发
     * @param {*} index
     */
    const dragOver = (index) => {
      // console.log('over', index);
      the.dragIndexNew = index;
    };

    /**
     * 更新序号
     * @param {*} list 数据列表
     * @returns
     */
    const dragChange = (list) => {
      // the.active = dragIndexNew;
      // 删除老的
      const changeItem = list.splice(the.dragIndexOld, 1)[0];
      // 在列表中目标位置增加新的
      list.splice(the.dragIndexNew, 0, changeItem);
      return list;
    };

    return {
      the,
      dragStart,
      dragStop,
      dragOver
    };
  }
};
</script>
```

</CodeRun>
