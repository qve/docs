# 全局样式

- 全局组件样式默认前缀为：`.qv-`

## UI 动画和过渡

<CodeRun auto editable>

```vue
<template>
  <div class="ui-css-page">
    <p>主题配置方案：</p>
    <div>
      <span class="primary">主色调 #0088ff</span>
      <span
        v-for="(item, key) in theme.color"
        :class="key"
        :key="key"
        :style="'background:' + item.color"
        >{{ item.text }} {{ item.color }}</span
      >
    </div>
    <p>文本颜色</p>
    <dl>
      <dt>　背景色：#f9f9f9;</dt>
      <dd
        v-for="(item, index) in theme.font"
        :key="index"
        :style="'background:' + item.color"
      >
        qcf-{{ index + 1 }} {{ item.text }} {{ item.color }}
      </dd>
    </dl>
    <dl>
      <dd
        v-for="(item, index) in theme.font"
        :key="index"
        :style="'color:' + item.color"
      >
        qcf-{{ index + 1 }} {{ item.text }} {{ item.color }}
      </dd>
    </dl>
  </div>
  <div class="css-test-page">
    <p>动画演示：</p>
    <Button type="info" @click="btnEvent('add')">添加动画</Button>
    <Button class="qml-025" @click="btnEvent">移除动画</Button>
    <transition-group name="list-fade" tag="p">
      <span v-for="item in the.items" :key="item" class="list-item">
        {{ item }}
      </span>
    </transition-group>

    <div
      class="shake"
      @mousemove="moveEvent"
      :style="{ backgroundColor: `hsl(${the.show.x}, 80%, 50%)` }"
    >
      鼠标过来试试{{ the.show.x }}
    </div>
  </div>
</template>

<script>
export default {
  setup() {
    // 使用外挂方式引入
    const { reactive } = window.$plus.vue;

    const the = reactive({
      items: [1, 2, 3, 4, 5, 6, 7, 8, 9],
      nextNum: 10,
      show: {
        x: 0,
        shake: false
      }
    });

    // 鼠标事件
    const moveEvent = (e) => {
      the.show.x = e.clientX;
    };

    const randomIndex = () => {
      return Math.floor(Math.random() * the.items.length);
    };

    const btnEvent = (cmd) => {
      if (cmd == 'add') {
        the.items.splice(randomIndex(), 0, the.nextNum++);
      } else {
        the.items.splice(randomIndex(), 1);
      }
    };

    // config.less 配置颜色
    const theme = {
      color: {
        success: { text: '成功  success', class: 'qc-2', color: '#00bb00' },
        info: { text: '信息 info', class: 'qc-3', color: '#00b9aa' },
        mark: { text: '标记 mark', class: 'qc-8', color: '#9933ff' },
        warning: { text: '警告 warning', class: 'qc-5', color: '#ff6600' },
        error: { text: '错误 error', class: 'qc-4', color: '#ee3000' },
        focus: { text: '焦点 focus', class: 'qc-6', color: '#7aafff' },
        hover: { text: '激活 hover', class: 'qc-7', color: '#7af2ff' }
      },
      font: [
        { text: '主标题 title-color', color: '#172333' },
        { text: '副标题 title-sub-color', color: '#23354d' },
        { text: '主文本3 text-color', color: '#355874' },
        { text: '副文本3 text-sub-color', color: '#c5cce6' },
        { text: '文本4 legend-color', color: '#9999bb' },
        { text: '文本4 legend-sub-color', color: '#dee0f6' }
      ]
    };

    return { the, theme, moveEvent, btnEvent };
  }
};
</script>

<style lang="less">
.ui-css-page .primary {
  height: 5rem;
  line-height: 5rem;
  display: block;
  margin: 0.25rem;
  background: #0088ff;
}

.ui-css-page span {
  font-size: 0.7rem;
  border-radius: 0.3rem;
  color: #fff;
  padding: 0.5rem;
  margin: 0.25rem 0 0 0.25rem;
  display: inline-block;
  text-align: center;
}

.ui-css-page dl {
  margin: 0.5rem;
  font-size: 0.7rem;
  background: #f9f9f9;
}

.ui-css-page dd {
  display: inline-block;
  color: #fff;
  padding: 1rem;
  margin: 0.1rem;
  border-radius: 0.3rem;
}

.css-test-page .list-item {
  display: inline-block;
  margin-right: 10px;
  color: rgb(12, 86, 240);
}

.css-test-page .shake {
  color: #fff;
  width: 60%;
  height: 5rem;
  padding: 6vmin;
  transition: 0.5s background-color ease;
  /*transition: 要过渡的属性 花费时间 运动曲线 何时开始;*/
  /*如果有多组属性，用逗号隔开*/
  /*transition: width 1s ease 0s, height 1s ease 0s, background-color 1s ease 0s;*/
  //	transition: all 1s; /*简写，所有属性都有过渡变化效果*/
}

// 动画

//进入过渡状态
.list-fade-enter-active,
//离开过渡状态
.list-fade-leave-active {
  // 所有动画延迟
  transition: all 1s ease;
  //.bll-transition('all',0.3s);
}

// 进入起始
.list-fade-enter-from,
// 离开结束
.list-fade-leave-to {
  opacity: 0;
  // 底部弹出
  transform: translateY(5rem);
}
</style>
```

</CodeRun>

## transition 组件动画

- `<transition>` 内置组件用来钩住组件进入和离开 `DOM`
- `<transition-group>` 多组件在处理多个元素位置更新时使用，通过 FLIP 技术来提高性能。
- `watchers` 用来处理应用中不同状态的过渡

### z-index 层级

qveui 默认初始配置

```less
#app {
  /*全局配置基础值
  * 其它组件继承
  * z-index: inherit;
  */
  z-index: 0;
}
```

## config.less 基础样式

- 组件常用的样式集合，命名规则结尾数字

- `0.8.1` 新增 `qmt-025`,`qpt-025`,`qwr`

```less
// Color
@primary-color: #0088ff;
@info-color: #00b9aa;
@success-color: #00bb00;
@warning-color: #ff7500;
@error-color: #ee3000;
@mark-color: #9933ff;

// 点击焦点样式
@focus-color: #7aafff;
// 激活颜色
@hover-color: #7af2ff;

// 小红点背景颜色
@dot-bg-color: #ee0000;
// 小红点背景颜色
@dot-color: #f9f9f9;

// 全局样式，命名规则结尾数字
.qm-025 {
  margin: 0.25rem;
}

.qmr-025 {
  margin-right: 0.25rem;
}

.qml-025 {
  margin-left: 0.25rem;
}

.qmt-025 {
  margin-top: 0.25rem;
}

.qmb-025 {
  margin-bottom: 0.25rem;
}

// padding
.qp-025 {
  padding: 0.25rem;
}

.qpr-025 {
  padding-right: 0.25rem;
}

.qpl-025 {
  padding-left: 0.25rem;
}

.qpt-025 {
  padding-top: 0.25rem;
}

.qpb-025 {
  padding-bottom: 0.25rem;
}

// 字体
.qf-06 {
  font-size: 0.6rem;
}

.qf-07 {
  font-size: 0.7rem;
}

.qf-08 {
  font-size: 0.8rem;
}

// 宽度
.qw-3 {
  width: 3rem;
}

.qwr-3 {
  width: 30%;
}

.qw-5 {
  width: 5rem;
}

.qwr-5 {
  width: 50%;
}

.qw-6 {
  width: 6rem;
}

.qwr-6 {
  width: 60%;
}

.qw-8 {
  width: 8rem;
}

.qwr-8 {
  width: 80%;
}

.qw-10 {
  width: 10rem;
}

.qwr-10 {
  width: 100%;
}

// 颜色
.qc-1 {
  color: @primary-color; //主颜色0078ff
}

.qc-2 {
  color: @success-color; //成功提示 #00bb00
}

.qc-3 {
  color: @info-color; //信息提示 #05caaa
}

.qc-4 {
  color: @error-color; //错误提示#ed4014
}

.qc-5 {
  color: @warning-color; //错误提示#ff7500
}

.qc-6 {
  color: @focus-color; //点击焦点样式#99d1ff
}

.qc-7 {
  color: @hover-color; //激活颜色#d1e6f8
}

.qc-8 {
  color: @mark-color; //标记颜色
}

// 文本颜色
.qcf-1 {
  color: @title-color;
}

.qcf-2 {
  color: @title-sub-color;
}

.qcf-3 {
  color: @text-color;
}

.qcf-4 {
  color: @text-sub-color;
}

.qcf-5 {
  color: @legend-color;
}

.qcf-6 {
  color: @legend-sub-color;
}
```

## qv 样式示例

```vue
<template>
  <div class="qm-025">
    <div class="qf-08">qf-08</div>
    <div class="qf-07">qf-07</div>
  </div>
</template>
```

## flex 布局

### 2 端对齐换行

```css
.box {
  display: flex;
  justify-content: space-between;
  word-wrap: break-word;
  word-break: normal;
}
```
