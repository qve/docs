# Textarea 多行文本框

cols 和设置宽度，通过 rows 设置高度

## Textarea 多行文本框

<CodeRun auto editable>

```vue
<template>
  <Textarea
    v-model="the.text"
    placeholder="请输入多行文本框"
    spellcheck="false"
    cols="300"
    rows="20"
    @onEvent="onRespEvent"
  />
</template>

<script>
export default {
  setup() {
    const $plus = window.$plus;
    const { reactive } = $plus.vue;

    const the = reactive({
      text: '多行文本框\r 测试 \n'
    });

    const onRespEvent = (resp) => {
      console.log('tree', resp);
    };

    return {
      the,
      onRespEvent
    };
  }
};
</script>
```

</CodeRun>

## Textarea props

| 属性      | 说明             | 类型    | 默认值   |
| --------- | ---------------- | ------- | -------- |
| named     | 组件命名         | String  | textarea |
| v-model   | 双向绑定数据     | String  | -        |
| clearable | 是否显示清空按钮 | Boolean | false    |
| onEvent   | 点击触发回调事件 |         |

## Textarea onEvent

| 事件名 | 说明                           | 值     |
| ------ | ------------------------------ | ------ |
| cmd    | 指令类别 `clear` 清除内容      | string |
| named  | 自定义组件命名                 | string |
| data   | 交互数据                       | string |
| event  | 触发组件对象$event，用于位置等 | string |
