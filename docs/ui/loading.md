# Loading 加载进度条

创建一个显示页面加载、异步请求、文件上传等的加载进度条。

- [Subscribe](../lib/subscribe.md)采用组件订阅事件方式，管理进度条

## 进度条订阅

订阅事件名：`onSubscribe_loading`

<CodeRun auto editable>

```vue
<template>
  <div class="test-load-page">
    <dl>
      <dt>触发组件内进度条</dt>
      <dd>
        <LoadingBar named="test" style="top:1rem" color="error" />
      </dd>
      <dd>
        <Button @click="btnLoad('start', 'test')"> 进度条开始 </Button>
        <Button @click="btnLoad('finish', 'test')"> 进度条结束 </Button>

        <Button @click="btnLoad('update', 'test')"> 指定进度 </Button>
      </dd>
    </dl>

    <dl>
      <dt>触发全局进度条</dt>

      <dd>
        <Button @click="btnLoad('start')"> 进度条开始 </Button>
        <Button @click="btnLoad('finish')"> 进度条结束 </Button>

        <Button @click="btnLoad('update')"> 指定进度 </Button>
      </dd>
    </dl>

    <dl>
      <dt>订阅事件</dt>
      <dd>
        <Button @click="btnSubscribe('keys')"> 所有订阅事件ID </Button>
      </dd>
      <dd>{{ the.keys }}</dd>
      <dd></dd>
    </dl>
  </div>
</template>

<script>
export default {
  setup() {
    const { reactive } = window.$plus.vue;
    const { loading } = window.$plus;
    const the = reactive({
      url: '/page/help/index',
      keys: []
    });

    const btnLoad = (val, named) => {
      // 组件命名
      named = named || 'loading';
      let resp;
      switch (val) {
        /** 进度开始 */
        case 'start':
          // console.log(qveui);
          resp = loading.start(null, named);
          // console.log(JSON.stringify(resp));
          break;

        case 'finish': // 加载完成
          resp = loading.finish(named);
          // console.log(JSON.stringify(resp));
          break;
        case 'update':
          // console.log(val);
          resp = loading.update(
            {
              start: false,
              show: true,
              percent: 30
            },
            named
          );
          //  console.log(JSON.stringify(resp));
          break;
      }
    };

    // 当前监听的事件ID
    const btnSubscribe = (val) => {
      switch (val) {
        case 'keys':
          // 全局组件keys
          the.keys = window.$plus.ui.subscribe.keys();
          // console.log()
          break;
      }
    };

    return { btnLoad, btnSubscribe, the };
  }
};
</script>
<style lang="less">
.test-load-page dl {
  padding: 0.25rem;
}
.test-load-page dt {
  padding: 0.25rem;
  font-size: 0.8rem;
}
.test-load-page dd {
  padding: 0.25rem;
}
</style>
```

</CodeRun>

## Loading 全局部署

`teleport` 把组件转移到目标元素中 (可选方式)

- index.html

```html
<body>
  <div id="appTopBody"></div>
  <div id="app"></div>
  <div id="endOfBody"></div>
  <script type="module" src="/src/main.js"></script>
</body>
```

- App.vue

```vue
<template>
  <!-- 注入到index.html > #appTopBody -->
  <teleport to="#appTopBody">
    <!-- LoadingBar 全局加载进度组件 -->
    <LoadingBar />
  </teleport>
</template>
```

## request 请求

- request.js

```js
import fly from 'flyio';
import { loading } from 'qveui';

/**
 * 添加请求拦截器
 */
fly.interceptors.request.use((config, promise) => {
  loading.start(); //开始加载进度条
  return config;
});

/**
 * 添加响应拦截器，响应拦截器会在then/catch处理之前执行
 */
fly.interceptors.response.use(
  (response, promise) => {
    loading.finish(); //结束进度条
    return promise.resolve(response.data);
  },
  (err, promise) => {
    loading.finish(); //结束进度条
    return promise.reject(err);
  }
);
const request = fly;
export { request };
```

## Loading 进度条方法

| 函数名 | 说明     | 参数          | 默认值                              |
| ------ | -------- | ------------- | ----------------------------------- |
| start  | 开始加载 | 订阅 id,optis | start: true,show: true,percent: 0   |
| finish | 完成加载 | 订阅 id,optis | start: false,show: false,percent: 0 |

## LoadingBar 属性

- 样式名字 progress.less

| 属性名  | 说明                                                                          | 类型    | 默认值         |
| ------- | ----------------------------------------------------------------------------- | ------- | -------------- |
| id      | 组件订阅事件名称,为空不订阅                                                   | String  | on-loading-bar |
| named   | 组件名称，用于组件实例区分                                                    | String  | -              |
| percent | 进度条宽度%                                                                   | Number  | 0              |
| color   | 进度条颜色,可选值为`primary`,`text`,`info`,`error`,`success`,`warning`,`mark` | String  | primary        |
| show    | 是否显示进度条                                                                | Boolean | false          |
