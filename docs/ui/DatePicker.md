# DatePicker

- [quick.bll 日期方法](../lib/bll.md)

时间日期选择器
选择或输入时间日期组件

- `0.7.0` 更新日期使用输出格式 `yyyy/MM/dd`,避免出现时差 8 小时问题

## DatePicker 时间日期

<CodeRun auto editable>

```vue
<template>
  <div>
    <DatePicker v-model="the.data.startDate" type="date" />
    <TimePicker v-model="the.data.startTime" />
    <DatePicker v-model="the.data.endTime" type="datetime" />
  </div>
</template>
<script>
export default {
  setup(props, context) {
    // 使用外挂方式引入，具体查看demo
    const { reactive } = window.$plus.vue;
    // 引入bll 时间方法
    const { bll } = window.$plus.quick;
    const _now = new Date();
    const the = reactive({
      data: {
        // startDate: '2020/09/02',
        startTime: '12:51:32',
        // 当前日期减少7天
        startDate: bll.date.add('d', -7, _now).format('yyyy/MM/dd'),
        endTime: _now.format('yyyy/MM/dd HH:mm:ss'),
        iso: new Date().toISOString()
      }
    });

    return { the };
  }
};
</script>
```

</CodeRun>

## DatePicker 日期时间

| 属性    | 说明                                   | 类型   | 默认值 |
| ------- | -------------------------------------- | ------ | ------ |
| v-model | 双向绑定数据                           | String | -      |
| type    | 显示类型,可选值为`date`,`datetime`     | String | date   |
| format  | 输出格式转换 `default `, `iso `, `utc` | Date   | -      |

## TimePicker 时间面板

| 属性    | 说明                                   | 类型   | 默认值    |
| ------- | -------------------------------------- | ------ | --------- |
| named   | 组件自定义命名，用于区别多组件触发事件 | String | timePanel |
| v-model | 双向绑定数据                           | String | -         |

## DatePanel 日历面板

日历，具体参考下面示例

| 属性    | 说明                                   | 类型   | 默认值    |
| ------- | -------------------------------------- | ------ | --------- |
| named   | 组件自定义命名，用于区别多组件触发事件 | String | datePanel |
| v-model | 双向绑定数据                           | String | -         |
| type    | 显示类型,可选值为`date`,`datetime`     | String | date      |
| format  | 输出格式转换 `default `, `iso `, `utc` | Date   | -         |
| marks   | 日期徽标提示                           | Object | null      |

## DateBar 日期栏

按横向列表显示日期与周

| 属性  | 说明                                   | 类型         | 默认值     |
| ----- | -------------------------------------- | ------------ | ---------- |
| named | 组件自定义命名，用于区别多组件触发事件 | String       | dateBar    |
| now   | 选中日期，默认为当天                   | String, Date | new Date() |
| marks | 日期徽标提示                           | Object       | null       |

<CodeRun auto editable>

```vue
<template>
  <div>
    <Card class="qm-025" style="width:50%">
      <template #extra>
        <Button size="small" shape="circle" icon="icon-suo" @click="btnEvent" />
      </template>
      <template #title>
        <DateBar
          :now="the.date.now"
          :marks="the.date.marks"
          @onEvent="onDateEvent"
        />
      </template>
      <!--卡片右上角-->

      内容
    </Card>

    <Poptip v-if="the.date.isShow" float>
      <DatePanel v-model="the.date.now" type="date" @onEvent="onDateEvent" />
    </Poptip>
  </div>
</template>

<script>
export default {
  setup() {
    // 使用外挂方式引入
    const { reactive } = window.$plus.vue;

    const the = reactive({
      date: {
        isShow: false,
        value: '',
        // 当前选中的日期
        now: '2021/11/16',
        // 日期标记
        marks: {
          '2021/11/16': '99',
          '2021/03/12': ''
        }
      }
    });

    const btnEvent = (cmd) => {
      console.log(cmd);

      switch (cmd) {
        case 'next':
          the.date.isShow = !the.date.isShow;
          break;
      }
    };

    /** 监听日期事件 */
    const onDateEvent = (resp) => {
      console.log(resp);

      switch (resp.cmd) {
        case 'click':
          the.date.now = resp.data.date;
          break;
        case 'next':
          the.date.isShow = !the.date.isShow;
          // the.date.now = resp.data.date;
          break;
      }
    };

    return { the, btnEvent, onDateEvent };
  }
};
</script>
```

</CodeRun>
