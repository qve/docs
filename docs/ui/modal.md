# Modal 对话框

展示信息或引导用户操作。

## Modal 对话框

<CodeRun auto editable>

```vue
<template>
  <div>
    <Modal
      class="test-mod-page-modal"
      title="提交服务器"
      body="内容被替换掉"
      :show="the.modal.show"
      @onEvent="ModalEvent"
    >
      <Textarea
        v-model="the.description"
        placeholder="请输入更新说明"
        spellcheck="false"
        cols="300"
        rows="10"
      />
    </Modal>

    <Button @click="the.modal.show = true"> 打开当前对话框 </Button>
    <Button type="info" @click="btnConfirm"> 打开全局订阅对话框 </Button>
  </div>
</template>

<script>
export default {
  setup(props, { refs }) {
    // 使用外挂方式引入全局事件与组件
    const { message, confirm } = window.$plus;
    const { Icon } = window.$plus.ui;
    const { reactive, h } = window.$plus.vue;
    // const getCurrentInstance = $plus.getCurrentInstance;

    const the = reactive({
      modal: {
        show: false
      },
      description: ''
    });

    /**
     * 对话框按钮触发事件
     */
    const ModalEvent = (resp) => {
      console.log('ModalEvent', resp);
      switch (resp.cmd) {
        case 'ok':
          //对话框确认
          if (the.description) {
            the.modal.show = !the.modal.show;
          } else {
            message.info('请输入内容');
            // 当前触发按钮的对象，关闭加载提示
            resp.item.loading = false;
          }

          break;
        case 'close':
        case 'cancel':
          // 对话框取消
          the.modal.show = !the.modal.show;
          message.info('点击取消');
          break;
      }
    };

    // 订阅方式打开全局对话框
    const btnConfirm = () => {
      let _resp = {
        type: 'error',
        title: '全局订阅事件对话框',
        body: '<p>通过订阅事件调用全局对话框</p><p>Content of dialog</p>',
        // 按钮事件
        onEvent: (resp) => {
          console.log('onEvent', resp);
          switch (resp.cmd) {
            case 'init':
              //对话框确认
              message.success('自定义事件init');
              break;
            case 'closed':
              // 窗口已关闭通知
              message.warning('窗口已关闭');
              break;
            case 'close':
            case 'cancel':
              // 对话框关闭按钮触发，0.6.2 版本调整关闭统一方法close()
              message.info('窗口关闭中');
              // 对话框关闭
              setTimeout(function () {
                // 关闭对话框
                resp.close();
                // _resp.close()
              }, 2000);
              break;
            default:
              message.info('点击：' + resp.cmd);
              break;
          }
        },
        // 自定义按钮
        btns: [
          {
            cmd: 'cancel',
            text: '不卖了'
          },
          {
            cmd: 'ok',
            type: 'primary',
            /** 是否显示加载图标 */
            loading: true,
            text: '继续购买'
          }
        ]
      };

      // 嵌入组件
      _resp.model = {
        props: {
          title: ' 传入的参数'
        },
        // 调用方式 1 全局已注册的组件名
        // component: 'Text',
        // 调用方式 2 非注册的组件需使用标记
        // component: markRaw(TextPage),
        // 调用方式 3 函数组件
        component: (props, { emit }) => {
          const btnClose = () => {
            // 关闭事件，回调当前对象的关闭方法
            emit('onEvent', { cmd: 'close', close: modalObject.close });
          };

          // 调用渲染函数 创建div，并引入当前注册组件
          return h('div', {}, [
            ` text comes first.${props.title}`,
            h(Icon, { type: 'icon-cuo' }, ''),
            h('button', { onClick: btnClose }, '调用窗口事件关闭')
          ]);
        }
      };

      // 返回当前对话框对象
      let modalObject = confirm(_resp);
      console.log(modalObject, 'confirm');
    };

    return { the, ModalEvent, btnConfirm };
  }
};
</script>

<style lang="less">
// 自定义对象高度
.test-mod-page-modal .qv-modal-body {
  max-height: 10rem;
  overflow: auto;
}
</style>
```

</CodeRun>

## confirm modelValue

- `qve 0.3.6` 支持全局对话框组件双向绑定值

```js
const PlanOnTime = ref(null);
PlanOnTime.value = props.data.PlanOnTime;

confirm({
  type: 'error',
  title: '延期时间',
  model: {
    props: {
      // 双向绑定
      modelValue: PlanOnTime
    },
    component: 'DatePicker'
  },
  // 事件
  onEvent: (res) => {
    // console.log(res, PlanOnTime)
    switch (res.cmd) {
      case 'ok':
        // 最新组件值
        console.log(res, PlanOnTime.value);
        break;
    }

    res.close && res.close();
  }
});
```

## Modal props

| 属性           | 说明                                              | 类型           | 默认值 |
| -------------- | ------------------------------------------------- | -------------- | ------ |
| named `0.4.2`  | 组件触发事件名称                                  | String         | modal  |
| title          | 对话框标题                                        | String         | -      |
| body           | 对话框内容，可写 html 标签                        | String         | -      |
| type           | 对话框类别`'info', 'error', 'success', 'warning'` | String         | -      |
| width          | 对话框宽度                                        | Number, String | 25rem  |
| show           | 显示或隐藏                                        | Boolean        | false  |
| masked `0.8.4` | 不显示遮罩                                        | Boolean        | false  |
| footerHide     | 隐藏底部按钮 `footer-hide`                        | Boolean        | false  |
| closed         | 显示或隐藏关闭按钮,屏蔽`close`命令                | Boolean        | false  |
| btns           | 自定义按钮默认 ok,cancel                          | Array          |        |

## Modal btns

- 自定义按钮参数

| 属性    | 说明             | 类型    | 默认值    |
| ------- | ---------------- | ------- | --------- |
| cmd     | 按钮点击触发命令 | String  | ok,cancel |
| text    | 按钮描述         | String  | 确认,取消 |
| type    | 按钮类型         | String  | primary   |
| loading | 是否显示加载图标 | Boolean |           |

## Modal events

| 事件名  | 说明                 | 返回值 |
| ------- | -------------------- | ------ |
| onEvent | 按钮点击触发回调事件 | json   |

## Modal onEvent

| 参数名 | 说明                                         | 类型   |
| ------ | -------------------------------------------- | ------ |
| named  | 组件命名，默认`modal`                        | String |
| cmd    | 按钮命令`btns.cmd:ok,close,closed,cancel`    | String |
| item   | 当前点击的自定义 btns 按钮`loading,text,cmd` | Object |
| event  | 触发按钮对象 `target`                        | Object |

- cmd
  - close 关闭窗口按钮命令，对话框属性配置 `closed=true` 可以拦截处理.
  - closed `0.6.3` 新增窗口已关闭回调通知命令

### Modal slot

组件插槽

| 名称   | 说明               |
| ------ | ------------------ |
|        | 内容插槽           |
| footer | 自定义底部按钮插槽 |

## Modal markRaw 全局调用

- modal 嵌入组件

| 属性      | 说明                                                  | 类型          | 默认值 |
| --------- | ----------------------------------------------------- | ------------- | ------ |
| props     | 组件参数                                              | Object        | null   |
| component | 组件,支持全局注册组件名,markRaw(import 组件),函数组件 | String,Object | null   |

::: tip
通过以下 3 种方法来使用
:::

```js
// 引入Icon 注册组件
// import { Icon } from '@/components'
// 引入页面
// import TextPage from './test-form.vue'

// 全局关闭当前对话框
$plus.confirm({ show: false });

// 返回当前对话框对象
let modal = $plus.confirm({
  title: '全局订阅事件对话框',
  body: '<p>是否关闭当前窗口？</p>',
  // 传入组件
  model: {
    // 组件参数
    props: {
      title: ' 传入的参数'
    },
    // 调用方式 1 全局已注册的组件名
    // component: 'Text',
    // 调用方式 2 非注册的组件需使用标记
    // component: markRaw(TextPage),
    // 调用方式 3 函数组件
    component: (props, { emit }) => {
      // 上报初始化事件
      emit('onEvent', { cmd: 'init', data: props.title });
      // 自定义按钮事件组件
      const btnClose = () => {
        // close 调用当前对象关闭方法
        emit('onEvent', { cmd: 'close', close: modal.close });
      };
      // 调用渲染函数 创建div，并引入当前注册组件
      return h('div', {}, [
        `Some text comes first.${props.title}`,
        h(Icon, { type: 'icon-cuo' }, ''),
        h('button', { onClick: btnClose }, '调用窗口事件关闭'),
        h(TextPage, {
          named: 'text-page'
        })
      ]);
    }
  },
  // 按钮事件
  onEvent: (resp) => {
    console.log('ModalEvent', resp);
    switch (resp.cmd) {
      case 'ok':
        //对话框确认
        setTimeout(function () {}, 5000);
        break;
      case 'close':
      case 'cancel':
        break;
    }
    //直接调用返回的内置关闭方法
    resp.close && resp.close();
  }
});
```
