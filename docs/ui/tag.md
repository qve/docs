# Tag 标签

可关闭的标识

## Tag 标签

<CodeRun auto editable>

```vue
<template>
  <div>
    <Tag
      named="del"
      v-for="(item, index) in the.tag.css"
      :key="index"
      :index="index"
      :css="item"
      closable
      @onEvent="onTagEvent"
    >
      {{ item }}
    </Tag>
  </div>
</template>

<script>
// import { reactive } from 'vue';
export default {
  setup() {
    // 使用外挂方式引入
    const { reactive } = window.$plus.vue;

    const the = reactive({
      tag: {
        key: '',
        css: [
          'default',
          'text',
          'info',
          'primary',
          'success',
          'error',
          'warning',
          'mark'
        ]
      }
    });

    /** 标签事件 */
    const onTagEvent = (resp) => {
      switch (resp.cmd) {
        case 'close':
          // 关闭
          the.tag.css.splice(resp.index, 1);
          break;
        case 'click':
          $plus.message.info(resp.event.target.innerHTML + ':' + resp.named);
          // console.log('onTagEvent:', resp.named, resp.event.target.innerHTML);
          // $plus.router.push('/Api/ClassTree/' + resp.named + '?title=' + resp.event.target.innerHTML)
          break;
        default:
          console.log('onTagEvent:', resp);
          break;
      }
    };
    return { the, onTagEvent };
  }
};
</script>

<style lang="less">
.qv-tag .qv-icon {
  font-size: 0.7rem;
}
</style>
```

</CodeRun>

## Tag Props

| 属性          | 说明                                                                           | 类型          | 默认值    |
| ------------- | ------------------------------------------------------------------------------ | ------------- | --------- |
| named `0.4.2` | 组件命名,配合 v-for，并支持关闭事件                                            | String,Number | tag       |
| index `0.6.3` | 循环序号                                                                       | String,Number | null      |
| css           | 标签的颜色`default`,`info`,`primary`,`success`,`error`,`warning`,`mark`,`text` | String        | `default` |
| closable      | 是否可以关闭                                                                   | Boolean       | `false`   |

## Tag Events

| 属性    | 说明     | 返回值 |
| ------- | -------- | ------ |
| onEvent | 点击事件 | Json   |

### onEvent cmd `click`

- 点击触发

| 参数名 | 说明         | 类型          |
| ------ | ------------ | ------------- |
| named  | 组件命名     | String,Number |
| event  | 点击事件回调 | Object        |

### onEvent cmd `close`

- 关闭触发

| 参数名 | 说明     | 类型          |
| ------ | -------- | ------------- |
| named  | 组件命名 | String,Number |
