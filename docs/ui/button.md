# Button 按钮

按钮组件，触发业务逻辑时使用。

<CodeRun auto editable>

```vue
<template>
  <div class="button-page">
    <p class="one">
      <Button @click="btnEvent">Default</Button>
      <Button type="primary" @click="btnEvent($event, 'primary')"
        >primary</Button
      >
      <Button @click="btnEvent" type="info">Info</Button>
      <Button @click="btnEvent" type="success">Success</Button>
      <Button @click="btnEvent" type="warning">Warning</Button>
      <Button @click="btnEvent" type="error">Error</Button>
      <Button @click="btnEvent" type="mark">Mark</Button>
    </p>
    <p class="one">
      small:小尺寸圆形按钮
      <Button size="small" shape="circle" icon="icon-jiantou" />
      <Button type="primary" size="small" shape="circle" icon="icon-shouye" />

      <Button type="info" size="small" shape="circle" icon="icon-jiantou" />

      <Button shape="circle" icon="icon-jiantou" />
      <Button type="primary" shape="circle" icon="icon-shouye" />

      <Button type="info" shape="circle" icon="icon-jiantou" />
    </p>
    <p class="one">
      多国语按钮：
      <Input size="small" />
      <Button size="small" type="primary">{{ getLang('primary') }}</Button>
      <Button size="small" type="primary">{{
        getLang('primary', 'en')
      }}</Button>
      <Button type="dashed">Dashed 虚线按钮</Button>
      <Button type="text">Text 文字按钮</Button>
    </p>
    <p>
      <Button loading type="primary" long icon="icon-sousuo">
        long 长按钮 loading
      </Button>
    </p>
  </div>
</template>
<script>
export default {
  setup() {
    const { message, lang } = window.$plus.ui;

    /**
     * 按钮事件
     * @param {*} el dom对象
     * @param {*} cmd 自定义传入参数
     */
    const btnEvent = (el, cmd) => {
      // console.log(cmd, el.target.tagName);
      // if (el) {
      //   // 默认方法，form 默认提交等，不是必须
      //   el.preventDefault();
      //   console.log(el.target.value);
      // }

      message.info('点击cmd' + cmd, { text: el.target.tagName });
    };

    /**
     * 多国语言规范
     * 组件名.方法名.语言名
     */
    let langPage = {
      zh: {
        pageHelp: {
          primary: '中文主按钮'
        }
      },
      en: {
        pageHelp: {
          primary: 'en primary button'
        }
      }
    };

    /**
     * 自定义读取语言方法名
     * @param {*} key 定义的关键词
     * @returns {string} 翻译文本
     */
    const getLang = (key, local) => {
      // 取出UI语言包
      // return lang.by('datePanel.' + key);

      // 取出自定义语言包,指定语种
      if (local) {
        return lang.by('pageHelp.' + key, langPage, local);
      } else {
        // 取出自定义语言包，默认 config.locale
        return lang.by('pageHelp.' + key, langPage);
      }
    };

    return { btnEvent, getLang };
  }
};
</script>
```

</CodeRun>

## ButtonGroup 示例

<CodeRun auto editable>

```vue
<template>
  <div class="button-page">
    <p>
      <ButtonGroup>
        <Icon type="icon-jiantou" />
        <Select>
          <option>选择锁定的列</option>
          <option value="index">序</option>
          <option value="name">姓名</option>
        </Select>
        <Input />
        <Button type="primary">primary</Button>
      </ButtonGroup>
    </p>
    <p>
      <ButtonGroup>
        <Button>Default</Button>
        <Button type="info">Info</Button>
        <Button type="success">Success</Button>
        <Button type="warning">Warning</Button>
        <Button type="error">Error</Button>
        <Button type="mark">Mark</Button>
      </ButtonGroup>
    </p>

    <p>
      large:大尺寸
      <ButtonGroup size="large">
        <Input />
        <Button type="primary">primary</Button>
      </ButtonGroup>
    </p>
    <p>
      <ButtonGroup size="large">
        <Button>Default</Button>
        <Button type="info">Info</Button>
        <Button type="success">Success</Button>
        <Button type="warning">Warning</Button>
        <Button type="error">Error</Button>
        <Button type="mark">Mark</Button>
      </ButtonGroup>
    </p>
    <p>
      small:小尺寸
      <ButtonGroup size="small">
        <Input />
        <Button type="primary">primary</Button>
      </ButtonGroup>
      <ButtonGroup size="small">
        <Button>Default</Button>
        <Button type="info">Info</Button>
        <Button type="success">Success</Button>
        <Button type="warning">Warning</Button>
        <Button type="error">Error</Button>
        <Button type="mark">Mark</Button>
      </ButtonGroup>
    </p>
    <p>
      small:圆形小尺寸
      <ButtonGroup size="small" shape="circle">
        <Button>Default</Button>
        <Button type="primary">primary</Button>
        <Button type="info">Info</Button>
        <Button type="success">Success</Button>
        <Button type="warning">Warning</Button>
        <Button type="error">Error</Button>
        <Button type="mark">Mark</Button>
      </ButtonGroup>
    </p>
    <p>
      large:圆形大尺寸
      <ButtonGroup size="large" shape="circle">
        <Button>Default</Button>
        <Button type="primary">primary</Button>
        <Button type="info">Info</Button>
        <Button type="success">Success</Button>
        <Button type="warning">Warning</Button>
        <Button type="error">Error</Button>
        <Button type="mark">Mark</Button>
      </ButtonGroup>
    </p>
    <p>
      circle:圆形
      <ButtonGroup shape="circle">
        <Button>Default</Button>
        <Button type="primary">primary</Button>
        <Button type="info">Info</Button>
        <Button type="success">Success</Button>
        <Button type="warning">Warning</Button>
        <Button type="error">Error</Button>
        <Button type="mark">Mark</Button>
      </ButtonGroup>
    </p>
  </div>
</template>

<style lang="less">
.button-page .one .qv-btn {
  margin: 0.25rem;
}
</style>
```

</CodeRun>

## Event 组件事件 arguments

```vue
<template>
  <Input type="text" value="123" @input="btnEvent($event, 'hello')" />
  <Button @click="btnEvent($event, '自定义参数')">自定义参数</Button>
  接收动态组件传入参数，再加入新的参数
  <div v-for="(item, index) in list" :key="index">
    <component
      :is="item.model.component"
      v-bind="item.model.props"
      @onEvent="(resp) => tabEvent(resp, index)"
    />
  </div>
</template>

<script>
export default {
  setup() {
    /**
     * 按钮事件
     * @param {*} el dom对象
     * @param {*} cmd 自定义传入参数
     */
    const btnEvent = (el, cmd) => {
      if (el) {
        // 默认方法，form 默认提交等，不是必须
        el.preventDefault();
        console.log(el.target.value);
      }

      console.log(cmd, el.target.tagName);
    };

    return { btnEvent };
  }
};
</script>
```

## Button props

| 属性     | 说明                                  | 类型    | 默认值  |
| -------- | ------------------------------------- | ------- | ------- |
| type     | 按钮类型，默认不设置                  | String  | default |
| size     | 按钮尺寸，`small`, `large`, `default` | String  | default |
| shape    | 圆形按钮，`circle`, `circle-outline`  | String  | null    |
| icon     | 按钮图标                              | String  |         |
| loading  | 是否显示加载                          | Boolean | false   |
| disabled | 是否禁用                              | Boolean | false   |
| long     | 是否按钮的长度为 100%                 | Boolean | false   |

- type
  - 可选值为 `default`、`primary`、`dashed`、`text`、`info`、`success`、`warning`、`error`

## Button events

| 事件名 | 说明             | 返回值 |
| ------ | ---------------- | ------ |
| click  | 点击触发回调事件 |        |
