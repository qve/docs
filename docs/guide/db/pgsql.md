# PostgreSQL

- [中文手册](http://www.postgres.cn/docs/14/index.html)
- [德说 github](https://github.com/digoal/blog)
- [德说文章](https://developer.aliyun.com/profile/expert/3x5dm5sgv4yq6)
- [用户标签](https://developer.aliyun.com/article/746189?spm=a2c6h.24874632.0.0.700e567ezS0dDC)
- [地理标签](https://developer.aliyun.com/article/745725?spm=a2c6h.24874632.0.0.70a67d2dzeTpzD)
- [PostgreSQL 多租户](https://developer.aliyun.com/article/64851)
- [时序数据](https://developer.aliyun.com/article/117309)
- [PostgreSQL 消息平台实践](https://developer.aliyun.com/article/698095?spm=a2c6h.24874632.0.0.7e9f45f2UtBpXj)
- [商品实时归类](https://developer.aliyun.com/article/647459?spm=a2c6h.24874632.0.0.7e9f45f2UtBpXj)
- [搜索引擎](https://developer.aliyun.com/article/672261?spm=a2c6h.24874632.0.0.7e9f45f2UtBpXj)
- [json 索引实践](https://developer.aliyun.com/article/647475?spm=a2c6h.24874632.0.0.7e9f45f2UtBpXj)
- [实时广告位推荐 2](https://developer.aliyun.com/article/590358?spm=a2c6h.24874632.0.0.41eaac21cXB4bs)
- [实时广告位推荐 1](https://developer.aliyun.com/article/590353?spm=a2c6h.24874632.0.0.41eaac21cXB4bs)
- [7 天消费金额](https://developer.aliyun.com/article/590350?spm=a2c6h.24874632.0.0.41eaac21cXB4bs)
- [家谱、族谱类图式关系存储与搜索](https://developer.aliyun.com/article/590335?spm=a2c6h.24874632.0.0.7d4141b1UJNfFj)
- [分组数据去重与打散](https://developer.aliyun.com/article/590329?spm=a2c6h.24874632.0.0.7d4141b1UJNfFj)
- [类微博 FEED 系统](https://developer.aliyun.com/article/582839?spm=a2c6h.24874632.0.0.33b83a48Rt8FUE)
- [社交类好友关系](https://developer.aliyun.com/article/582837?spm=a2c6h.24874632.0.0.33b83a48Rt8FUE)

- [AB 表切换最佳实践](https://developer.aliyun.com/article/647473?spm=a2c6h.24874632.0.0.7e9f45f2UtBpXj)
- [DBA 日常管理 SQL](https://developer.aliyun.com/article/700364?spm=a2c6h.24874632.0.0.70a67d2dzeTpzD)
- [PostgreSQL 互迁 MSSQL](https://developer.aliyun.com/article/698112?spm=a2c6h.24874632.0.0.414b2e99V9X2KE)
- [一主多从](https://developer.aliyun.com/article/582876?spm=a2c6h.24874632.0.0.7d4141b1UJNfFj)

## PostgreSQL 与 MSSQL 类型映射关系

- [SQL Server 官方类型手册](https://docs.microsoft.com/en-us/sql/t-sql/data-types/data-types-transact-sql?view=sql-server-2016)
- [PostgreSQL 官方类型手册](https://www.postgresql.org/docs/devel/static/datatype.html)

- 空间类型通过新建 PostGIS 插件，与 SQL Server geometry, geography 对应。

- SQL Server , PostgreSQL , Oracle , MySQL 详细的开发者使用对比手册 `https://www.w3resource.com/sql/sql-syntax.php`

PostgreSQL 与 SQL Server 的类型映射：

```sql
char                char / text
nchar               char / text
varchar             varchar / text
nvarchar            varchar / text
xml                 xml
int                 integer
bigint              bigint
bit                 boolean
uniqueidentifier    uuid
hierarchyid         bytea
geography           geography
tinyint             smallint
float               float
real                real
double              double precision
numeric             numeric
decimal             numeric
money               numeric
smallmoney          numeric
binary              bytea
varbinary           bytea
image               bytea
datetime            timestamptz
datetime2           timestamptz
```

## pg 清空表数据

清空数据，并且还原序列 `sequence` 自增 `id` 也会变为 0

```sql
TRUNCATE ap_mac RESTART IDENTITY;
```

## encryption pg_hba

当程序链接 `PostgreSQL` 时，可能会报错：`No pg_hba.conf entry`。

这条错误的原因是因为客户端远程访问`postgresql` 受限所致，默认情况下除本机外的机器是不能连接的。

默认情况下，`postgresql` 本机的连接是信任连接,只要有用户名就可以连接,不用密码. 要改变这种默认设置需要更改安装目录下的两个配置文件(`pg_hba.conf` 与 `postgresql.conf`）

### 认证文件 pg_hba.conf

文件目录：`postgresql`安装目录下的`..\PostGreSQL\14\data\pg_hba.conf`

- `pg_hba.conf`
  每条记录声明一种联接类型，一个客户端 IP 地址范围（如果和联接类型相关的话），一个数据库名，一个用户名字，以及对匹配这些参数的联接使用的认证方法。

加入一行 trust
无条件地允许联接。这个方法允许任何可以与 PostgreSQL 数据库服务器联接的用户以他们期望的任意 PostgreSQL 数据库用户身份进行联接，而不需要口令。
`host all all 0.0.0.0/0 trust`

- postgresql-x64-14 重启服务名

## create table

```sql
create table public.QF_Report(
ID serial not null, --ID
code 	varchar(36) -- 主键
,salary 		decimal(16,2)	-- 资产月日均
,age 			int
,gender 		varchar(1)		-- 性别标志：0男 1女
,create_time 	timestamp	default current_timestamp	-- 创建时间
,create_user	varchar(20)	default user	-- 创建用户
,update_time 	timestamp	default current_timestamp	-- 更新时间
,MUser_ID	int	default 1	-- 更新用户
,PRIMARY KEY(ID)
);
```

## INSERT

```sql
-- 插入
INSERT INTO public.qf_report(id, code, salary, age, gender)
VALUES (1,'test',1.5,20,0);

-- 查询
SELECT id, code, salary, age, gender, create_time, create_user, update_time, muser_id FROM qf_report;
```

## 表与列

```sql
--增加一列
ALTER TABLE table_name ADD column_name datatype NOT NULL DEFAULT '默认值' ;
--删除一列
ALTER TABLE table_name DROP  column_name;
--更改列的数据类型
ALTER TABLE table_name ALTER  column_name TYPE datatype;
--表的重命名
ALTER TABLE table_name RENAME TO new_name;
--更改列的名字
ALTER TABLE table_name RENAME column_name to new_column_name;
--字段的not null设置
ALTER TABLE table_name ALTER column_name {SET|DROP} NOT NULL;
--给列添加default
ALTER TABLE table_name ALTER column_name SET DEFAULT expression;
```

### 表与列默认值描述

```sql
select
	--c.relname as 表名,
    col.ordinal_position as column_index,
	  col.column_name as column_code,
    --col.is_identity	as 自增,
    col.column_default,
    coalesce(col.character_maximum_length,col.numeric_precision, -1) as column_length,
    data_type as column_dataType,
    --concat_ws('', t.typname) as 字段类型,
    --CASE col.is_nullable WHEN 'NO' THEN true ELSE false END AS column_isNullAble,
	 a.attnotnull as column_isNullAble,--非空,
	(case
		when (
		select count(pg_constraint.*)
		from pg_constraint
		inner join pg_class on
			pg_constraint.conrelid = pg_class.oid
		inner join pg_attribute on
			pg_attribute.attrelid = pg_class.oid
			and pg_attribute.attnum = any(pg_constraint.conkey)
		inner join pg_type on
			pg_type.oid = pg_attribute.atttypid
		where
			pg_class.relname = c.relname
			and pg_constraint.contype = 'p'
			and pg_attribute.attname = a.attname
        ) > 0 then true
		else false end
    ) as column_pk,--主键
	-- (case when a.attlen > 0 then a.attlen
	-- 	when t.typname='bit' then a.atttypmod
	-- 	else a.atttypmod - 4 end) as 长度,
	(select description from pg_description where objoid = a.attrelid and objsubid = a.attnum) as column_title --备注
from
	pg_class c,
	pg_attribute a,
--	pg_type t,
	information_schema.columns as col
where
	c.relname = 'ap_stat'
	and a.attnum>0
	and a.attrelid = c.oid
	--and a.atttypid = t.oid
	and col.table_name=c.relname and col.column_name=a.attname
order by
	c.relname desc,
	a.attnum asc
```

## 分区

### pgSQL 各分区和表的记录数

```sql
select
  relname as tableNmae,
  reltuples as rowCounts
from pg_class
where relkind = 'r'
  and relnamespace = (select oid from pg_namespace where nspname='public')
order by rowCounts desc;

-- 分区表与记录数
select
	c.relname, c.reltuples
from
	pg_class c
	join pg_inherits i on i.inhrelid = c. oid
	join pg_class d on d.oid = i.inhparent
where
	d.relname = 'ap_mac';
```
