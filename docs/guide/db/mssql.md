# MSSQL 数据库

- [微软数据库文档](https://docs.microsoft.com/zh-cn/sql/relational-databases/tables/tables)
- [Quick.DB 数据库访问组件](http://docs.apwlan.com/net/five/quick/db.html)

## 字段前端属性 Json

前端与后台字段属性适配

- 必填字段 n:true

| 字段        | 说明                                         | 类型          | 默认值     | 默认值                             |
| ----------- | -------------------------------------------- | ------------- | ---------- | ---------------------------------- |
| ID          | 序号`{'k':1,'v':0,'t':'long'}`               | bigint        | 主键自增长 | k:前端主键绑定,v:默认值,t:数据类型 |
| Title       | 描述`{'d':1}`                                | nvarchar(200) |            | d:外键结合查询输出                 |
| Type        | 类型`{'ls':'0:未配置,1:正常,2:启用,4:删除'}` | int           |            | h:前端不显示                       |
| Remark      | 备注`{'h':1,'c':'json'}`                     | varchar(MAX)  |            | h:前端不显示,c:前端控件类型        |
| Q_Time      | 编辑时间`{'r':1,'e':1}`                      | datetime      | getdate()  | r:只读,e:扩展列显示                |
| Q_UserID    | 编辑者`{'r':1,'e':1}`                        | bigint        | getdate()  | r:只读,e:扩展列显示                |
| Q_StateFlag | 编辑状态`{'a':1,'r':1,'e':1}`                | bigint        | getdate()  | r:只读,e:扩展列显示                |

## 常用 SQL

### mssql 四舍五入

```sql
--四舍五入 50
SELECT CAST(ROUND(50.2,0) AS INT)
--切舍 50
SELECT FLOOR(50.9)
--切上 51
SELECT CEILING(50.2)
```

### outer 链表查询

2 种写法查询主表，并取出子表数据

- 方式 1

```sql
select * from
(select * from OA_WorkItem where OA_Work_ID=12413)a
outer apply
(select top 1 UserName as byName,Body from QuickDB.dbo.OA_WorkUP where OA_WorkItem_ID = a.ID order by OnTime desc) b
```

- 方式 2

```sql
select a.*,b.* from OA_WorkItem a
outer apply
(select top 1 UserName as byName,Body from dbo.OA_WorkUP where OA_WorkItem_ID = a.ID order by OnTime desc) b
where a.OA_Work_ID=12413
```

### 复制表与结构

```sql
-- 备份整个表
select  * into QF_ClassTree_191023 from AP10000DB.[dbo].[QF_ClassTree]
```

### 复制数据

```sql
Insert into Table2(field1,field2,...) select value1,value2,... from Table1

--要求目标表Table2必须存在，由于目标表Table2已经存在，
--所以我们除了插入源表Table1的字段外，还可以插入常量。
```

### 列行转换字符串

```sql
-- ID 序号
select ','+CONVERT(varchar(max),ID) from [QF_ClassTree] where [TreeGuid] like'1.3.%' FOR XML PATH('')
```

### 批量替换

```sql
-- 批量替换 5G 信号为 2G信号
update [dbo].[AP_Info] set SSID=replace(SSID_5G,'53AP/','53AP/2G/') where AP_NAS_ID=844
```

### sql output 返回 ID inserted deleted

```sql
--INSERT,可以引用inserted表以查询新行的属性.
   insert into [表名] (a) OUTPUT Inserted.a values ('a')
--DELETE,可以引用deleted表以查询旧行的属性.
   delete [表名] OUTPUT deleted.a where links = 'a'
--UPDATE,使用deleted表查询被更新行在更改前的属性,用inserted表标识被更新行在更改后的值.
   update [表名] set a = 'b' OUTPUT Inserted.a where a = 'a'(返回修改后的值)
   update [表名] set a = 'b' OUTPUT deleted.a where a = 'a' (返回修改前的值)
```

### 清空表数据

```sql
truncate table [表名]
```

### 重置自增长 IDENTITY

- SQL SERVER 版本中，一般采用 GUID 或者 IDENTITY 来作为标示符，但是 IDENTITY 是一个表对象，只能保证在一张表里面的序列

```sql
-- 不清空已有数据，但将自增自段恢复从2020开始计数
dbcc checkident([AP_Kill],RESEED,2020)
```

- 从 SQL Server 2012 版本开始， 当 SQL Server 实例重启之后，表格的自动增长列的值会发生跳跃，而具体的跳跃值的大小是根据增长列的数据类型而定的。如果数据类型是 整型(int)，那么跳跃值为 1000；如果数据类型为 长整型(bigint)，那么跳跃值为 10000

- 解决方案

如果我们对微软提供的这个 “功能” 不感兴趣，我们可以通过两种途径来关闭它。

1. 使用序列 (Sequence)

2. 为 SQL Server 注册启动参数 -t272

### 自增长序列 sql Sequence

- Sequence 是 2012 版本以后出现的序列，是一个基于 SCHEMA 的对象，所以可以被多表调用

### 批量插入

```sql

insert into [表名]
select [AP_NAS_ID]
      ,[savetime]
      ,[natcount] from [服务器名].[数据库名].[dbo].[表名] where savetime>'2020-01-01'

```

- 多字段插入

```sql
insert into [表名] (字段名1,字段名2,字段名3) values (值1,值2,值3),(值1,值2,值3),(值1,值2,值3)
```

### sql 日期时间差

- [SQLServer 日期函数大全](https://www.cnblogs.com/zhangpengnike/p/6122588.html)

```sql

-- 查询未过期用户
select COUNT(1) from AP_User where StateID=6 and dateadd(day,LongTime,LanStateTime)>getdate()

-- 在向指定日期加上一段时间的基础上，返回新的 datetime 值，例如：向日期加上2天

select dateadd(day,1,GETDATE())
```

## sql 存储过程

### 循环游标

```sql
DECLARE @phone varchar(50),@joinTime datetime,@appId int,@rid int
set @appId=20

DECLARE My_Cursor CURSOR--定义游标
FOR (SELECT PhoneNo,JoinTime from QF_User where [JoinTime]>'2022-2-1' and  [JoinTime]<'2022-2-14') --查出需要的集合放到游标中
OPEN My_Cursor; --打开游标
FETCH NEXT FROM My_Cursor INTO @phone,@joinTime; --读取第一行数据(将MemberAccount表中的UserId放到@UserId变量中)
WHILE @@FETCH_STATUS = 0
    BEGIN

    -- 调用存储过程写入注册手机号
   -- EXEC Quick.[dbo].[QP_JoinPhone]
	--	@QF_AppInfo_ID =@appId,
	--	@QF_Group_ID =4,
	--	@Phone = @phone,
	--	@rJoinID=@rid output

	--1.查询手机号是否已经登录过
SELECT top 1
  @rid=isnull(ID,0)
FROM [QuickDB].[dbo].QF_Phone
WHERE Phone=@phone

-- 判断是否有记录
  IF @rid>0
  begin
  	set @rid=0
  end
  else
	  BEGIN

   INSERT INTO [QuickDB].[dbo].QF_Phone
    (QF_AppInfo_ID,QF_GroupID,Phone,Remark,JoinTime)
  VALUES(0,0, @Phone,'sys',@joinTime)
   select SCOPE_IDENTITY()
   end

        FETCH NEXT FROM My_Cursor INTO @phone,@joinTime; --读取下一行数据
    END
CLOSE My_Cursor; --关闭游标
DEALLOCATE My_Cursor; --释放游标
GO
```

## Sql OpenJson

sql 2016 将支持 OpenJson

- [openjson 官方文档](https://docs.microsoft.com/zh-cn/sql/relational-databases/json/convert-json-data-to-rows-and-columns-with-openjson-sql-server?view=sql-server-ver15)

```sql
declare @vcLoginIdJson nvarchar(max)
set @vcLoginIdJson = '{"admin":"","hyh":""}'
select [key] as vcLoginId FROM openjson(@vcLoginIdJson)
```

## Sql 改服务器名

- 产生的原因是因为安装完 SqlServer 之后，又修改了计算机的名称。
- 导致名称不一致，修改后需要重启 SQL 服务

```sql
-- 当前服务器名
select @@servername
-- 当前机器名，原则应同机器名一致
select serverproperty('servername')


--alter servername
sp_dropserver 'oldname'
go
sp_addserver 'newname','local'
go
```

## sql 运维

### SQL 进程占用过高

```sql

USE master
 GO
 --如果要指定数据库就把注释去掉
 SELECT * FROM sys.[sysprocesses] WHERE [spid]>50 --AND DB_NAME([dbid])='gposdb'
```

### NOLOCK spid kill 锁表进程

使用 NOLOCK 关键字可以避免阻塞造成无法查询出数据，但使用该关键字会有造成数据脏读的可能

```sql
declare @spid  int
Set @spid=105 --锁表进程
declare @sql varchar(1000)
set @sql='kill '+cast(@spid  as varchar)
exec(@sql)
```

### sql 指标是否正常，是否有阻塞

正常情况下搜索结果应该为空。

```sql
SELECT TOP 10
[session_id],
[request_id],
[start_time] AS '开始时间',
[status] AS '状态',
[command] AS '命令',
dest.[text] AS 'sql语句',
DB_NAME([database_id]) AS '数据库名',
[blocking_session_id] AS '正在阻塞其他会话的会话ID',
[wait_type] AS '等待资源类型',
[wait_time] AS '等待时间',
[wait_resource] AS '等待的资源',
[reads] AS '物理读次数',
[writes] AS '写次数',
[logical_reads] AS '逻辑读次数',
[row_count] AS '返回结果行数'
FROM sys.[dm_exec_requests] AS der
CROSS APPLY
sys.[dm_exec_sql_text](der.[sql_handle]) AS dest
WHERE [session_id]>50 AND DB_NAME(der.[database_id])='gposdb'
ORDER BY [cpu_time] DESC
```

### SQL 语句占用较大

```sql
--在SSMS里选择以文本格式显示结果
SELECT TOP 10
dest.[text] AS 'sql语句'
FROM sys.[dm_exec_requests] AS der
CROSS APPLY
sys.[dm_exec_sql_text](der.[sql_handle]) AS dest
WHERE [session_id]>50
ORDER BY [cpu_time] DESC

```

### sql CPU 占用最高

```sql
SELECT TOP 10
   total_worker_time/execution_count AS avg_cpu_cost, plan_handle,
   execution_count,
   (SELECT SUBSTRING(text, statement_start_offset/2 + 1,
      (CASE WHEN statement_end_offset = -1
         THEN LEN(CONVERT(nvarchar(max), text)) * 2
         ELSE statement_end_offset
      END - statement_start_offset)/2)
   FROM sys.dm_exec_sql_text(sql_handle)) AS query_text
FROM sys.dm_exec_query_stats
ORDER BY [avg_cpu_cost] DESC
```

### sql 查看索引缺失

找到索引缺失的表，根据查询结果中的关键次逐一建立索引

```sql
SELECT
    DatabaseName = DB_NAME(database_id)
    ,[Number Indexes Missing] = count(*)
FROM sys.dm_db_missing_index_details
GROUP BY DB_NAME(database_id)
ORDER BY 2 DESC;
SELECT  TOP 10
        [Total Cost]  = ROUND(avg_total_user_cost * avg_user_impact * (user_seeks + user_scans),0)
        , avg_user_impact
        , TableName = statement
        , [EqualityUsage] = equality_columns
        , [InequalityUsage] = inequality_columns
        , [Include Cloumns] = included_columns
FROM        sys.dm_db_missing_index_groups g
INNER JOIN    sys.dm_db_missing_index_group_stats s
       ON s.group_handle = g.index_group_handle
INNER JOIN    sys.dm_db_missing_index_details d
       ON d.index_handle = g.index_handle
ORDER BY [Total Cost] DESC;
```

### sql 存储过程耗时

```sql
 --查看存储过程执行的语句
  SELECT DB_NAME(ISNULL(EPS.database_id,'')) [数据库名称]
       --ISNULL(DBS.name, '') AS DatabaseName
       ,OBJECT_NAME(EPS.object_id, EPS.database_id) [存储过程名称] --AS ObjectName
       ,EPS.cached_time [添加到缓存的时间]--AS CachedTime
       ,EPS.last_elapsed_time  '最近执行所耗费时间（微秒）'--AS LastElapsedTime
       ,EPS.last_worker_time '上次执行存储过程所用的CPU时间(微秒)'
       ,EPS.execution_count [上次编译以来所执行的次数]--AS ExecutionCount
       ,EPS.total_worker_time / EPS.execution_count [平均每次执行所用的CPU时间总量(微秒)]--AS AvgWorkerTime
       ,EPS.total_elapsed_time / EPS.execution_count [平均每次执行所用的时间(微秒)]--AS AvgElapsedTime
       ,(EPS.total_logical_reads + EPS.total_logical_writes)
        / EPS.execution_count AS AvgLogicalIO
       ,b.text [存储过程内容]
 FROM sys.dm_exec_procedure_stats AS EPS
 CROSS APPLY sys.dm_exec_sql_text(EPS.sql_handle) b
 ORDER BY EPS.last_elapsed_time DESC;
```

### sql 语法顺序

```sql
SELECT
DISTINCT <select_list>
FROM <left_table>
<join_type> JOIN <right_table>
ON <join_condition>
WHERE <where_condition>
GROUP BY <group_by_list>
HAVING <having_condition>
ORDER BY <order_by_condition>
LIMIT <limit_number>
```

```sh
FROM
<表名> # 选取表，将多个表数据通过笛卡尔积变成一个表。
ON
<筛选条件> # 对笛卡尔积的虚表进行筛选
JOIN<join, left join, right join...>
<join表> # 指定join，用于添加数据到on之后的虚表中，例如left join会将左表的剩余数据添加到虚表中
WHERE
<where条件> # 对上述虚表进行筛选
GROUP BY
<分组条件> # 分组
<SUM()等聚合函数> # 用于having子句进行判断，在书写上这类聚合函数是写在having判断里面的
HAVING
<分组筛选> # 对分组后的结果进行聚合筛选
SELECT
<返回数据列表> # 返回的单列必须在group by子句中，聚合函数除外
DISTINCT
# 数据除重
ORDER BY
<排序条件> # 排序
LIMIT
<行数限制>
```

### 单用户模式

此时无法更改数据库 'QuickDB' 的状态或选项。此数据库处于单用户模式，当前某个用户已与其连接。

```sql
USE master;
GO
DECLARE @SQL VARCHAR(MAX);
SET @SQL=''
SELECT @SQL=@SQL+'; KILL '+RTRIM(SPID)
FROM master..sysprocesses
WHERE dbid=DB_ID('QuickDB');
EXEC(@SQL);
GO
ALTER DATABASE QuickDB SET MULTI_USER;
```

### mssql 3724

无法对数据库'XXX' 执行删除，因为它正用于复制”的解决方法
这个语句的解释是：从数据库中删除所有复制对象，但不更新分发服务器上的数据。此存储过程在发布服务器的发布数据库或订阅服务器的订阅数据库上执行。

```sql
sp_removedbreplication 'QuickDB'
```

### 无日志文件附加数据库

日志文件过大，迁移数据库到其它服务器。

1. 新建同名的数据库文件

2. 暂停 SQLSever 服务

3. 将原先的 mdf 文件，覆盖新建的数据库，删除新数据库的 ldf 文件

4. 重新启动 SQLSetver 服务 ，这时看到的数据库是这个样子的，打不开

如遇到报错，执行 3724 错误清理，然后在处理单用户

```sql
 --1.设置为紧急状态
 alter database QuickDB set emergency
 --2.设置为单用户模式
 alter database QuickDB set single_user
 --3.检查并重建日志文件
 dbcc checkdb('QuickDB',REPAIR_ALLOW_DATA_LOSS)
 --4.第3步操作如果有错误提示，运行第4步，没有错误则跳过
 dbcc checkdb('QuickDB',REPAIR_REBUILD)
--5.恢复成多用户模式
 alter database QuickDB set multi_user
```

### sql 日志清理

- 日志文件路径
  `C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\Log`
- 日志文件默认情况下，SQL Server 保存 7 个 ErrorLog 文件名为：ErrorLog ErrorLog.1 ErrorLog.2 ErrorLog.3 ErrorLog.4 ErrorLog.5 ErrorLog.6

- 清理方法
  只要我们执行一次 sp_cycle_errorlog 存储过程，就会删除 ErrorLog.6，其他日志编号自动加 1，并创建新的 ErrorLog，也就是说，只要执行 7 次 sp_cycle_errorlog，就能够把之前生成的错误日志全部删除。也可使用 SQLServer 代理，添加作业，每天执行一次 sp_cycle_errorlog

### mssql 连接数

查询当前服务器连接数，评估服务器性能

- 通过系统的“性能”来查看：
  开始->管理工具->性能（或者是运行里面输入 mmc）然后通过添加计数器添加 SQL 的常用统计 然后在下面列出的项目里面选择用户连接就可以时时查询到数据库的连接数了。不过此方法的话需要有访问那台计算机的权限，就是要通过 windows 账户登陆进去才可以添加此计数器。
- 通过系统表来查询：

```sql

-- 查总连接数
select COUNT(1) from sysprocesses where dbid in (select dbid from sysdatabases-- where name='master'
)

-- 查单一数据库
select * from sysprocesses where dbid in (select dbid from sysdatabases where name='Quick')
```
