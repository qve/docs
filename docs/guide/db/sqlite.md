# SQLite 数据库

> SQLite version 2.8.17

## sqlte

小型嵌入式数据库，

## sqlite 连接数据库

```sh
# 连接到临时数据库
sqlite /tmp/siptmp.db
```

## SQLITE_MASTER 查询表

每一个 SQLite 数据库都有一个叫 SQLITE_MASTER 的表， 里面存储着数据库的数据结构（表结构、视图结构、索引结构等），只可以对他使用查询语句

```sql
SELECT COUNT(*) FROM sqlite_master where type='table' and name='loginkey';
```

- 查询表列命令

```shell

## 查询表的列信息
sqlite> .schema login
## 返回以下内容
CREATE TABLE login (
ipaddr varchar(15) ,
userid varchar(60) ,
type varchar(8) ,
remaintime int ,
remaindata int ,
processed char(1) ,
logintime varchar(20) ,
maskword varchar(20) ,
mac varchar(100) ,
logintype int ,
username varchar(60) ,
PRIMARY KEY (ipaddr)
);

```

### Sqlite 增加字段列和更新列值

以下命名对于 sqlite v2 无效

```sql
alter table test add  column new text;
alter table login add column onekey text;

alter table login add column onekey varchar(60);-- NULL;

alter table test drop  column name; --删除表列 

alter table test modify address char(10) --修改表列类型  或者 alter table test change address address char(40) 

alter table test change column address address1 varchar(30)--修改表列名


--没有where 就全部更新
UPDATE Production SET category ="new" where id=1
```

### 一键登陆网关

本机登陆成功后，将本机返回的一键登陆码，缓存到 loginkey 表。用于本机跨浏览器，用户登陆识别

```sql
CREATE TABLE loginkey (
    ipaddr     VARCHAR( 15 )  PRIMARY KEY,
    mac        VARCHAR( 100 ),
    userid     VARCHAR( 60 ),
    useronekey VARCHAR( 100 ),
    usertoken  VARCHAR( 100 ),
    ontime     VARCHAR( 20 ),
    remark     VARCHAR( 200 )
);

select * from loginkey;

-- 查询表的列信息，判断是否更新成功
sqlite> .schema loginkey
```

## sqlite 删除表

```sql

DROP TABLE MyTable;

```

## 查询网关在线终端

```sql


SELECT * FROM login

-- 查询登录的一键码
SELECT loginkey.useronekey,login.logintime,login.userid,login.mac FROM login LEFT JOIN loginkey ON login.ipaddr=loginkey.ipaddr and login.ipaddr='10.10.20.139';


```
