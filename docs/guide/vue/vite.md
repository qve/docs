# vite

新一代快速打包工具

- [Vue3 + Vite 前端工程化-基础篇](https://zhuanlan.zhihu.com/p/428909652)

## vite.config 配置

- [打包配置](https://cn.vitejs.dev/guide/build.html)

Vue 打包后出现一些 map 文件,修改项目下

- vite.config.js

```js
import vue from '@vitejs/plugin-vue';
import { resolve } from 'path'; // 主要用于alias文件路径别名

module.exports = {
  // base: '/v3/', // 服务器部署的路径
  server: {
    port: 8080
  },
  build: {
    // outDir: './example', //输出路径
    // sourcemap: false,   //不输出 map
    // brotliSize: false, // 启用/禁用 brotli 压缩大小报告
    //压缩大型输出文件可能会很慢，因此禁用该功能可能会提高大型项目的构建
    // chunkSizeWarningLimit: 2000 // 消除打包大小超过2000kb警告
  },
  resolve: {
    alias: {
      '@': resolve('src'),
      // 带完整组件编译执行
      vue: 'vue/dist/vue.esm-bundler.js'
      //  qveui: resolve('packages') // 本地类库别名
    }
  },
  plugins: [vue()],
  // 生成保留 map 源文件
  productionSourceMap: false
};
```

## vite rollup 打包分析

分析打包后文件过大

- 可视化并分析构建包，查看哪些模块占用空间大小，以此来优化构建包的大小。
- vite 默认已经支持大部分的 Rollup 的 plugin。
- vite.config.js 配置完成后，执行打包命令，会在根目下生成报告文件`stats.html`

```js
// 安装分析插件
//yarn add -D rollup-plugin-visualizer

// 引入配置文件
import visualizer from 'rollup-plugin-visualizer';
module.exports = {
  plugins: [visualizer()]
};
```

## vite vendor 打包过大

由于打包有些依赖包体积过于庞大警告文件超过 500K，提示需要进行配置分割

- [分隔配置 manualChunks](https://rollupjs.org/guide/en/#outputmanualchunks)

```js
module.exports = {
  build: {
    // outDir: './example', //输出路径
    // sourcemap: false,   //不输出 map
    // brotliSize: false, // 启用/禁用 brotli 压缩大小报告
    // 压缩大型输出文件可能会很慢，因此禁用该功能可能会提高大型项目的构建
    // chunkSizeWarningLimit: 2000 // 消除打包大小超过2000kb警告
    rollupOptions: {
      // 拆包按模块打包，避免vendor过大
      output: {
        manualChunks(id) {
          if (id.includes('node_modules')) {
            //将qveui 独立打包，其它合并到命名为 modules
            if (
              id
                .toString()
                .split('node_modules/')[1]
                .split('/')[0]
                .includes('qveui')
            ) {
              return 'qveui';
            } else {
              // 其它组件合并为1个文件
              return 'modules';
            }
            // return id.toString().split('node_modules/')[1].split('/')[0].toString();
          }
        }
      }
    }
  }
};
```

## vue2.x

### 兼容 IE

IE 不支持 Promise 解决办法（可搜索 polyfill）
引入 `<script type="text/javascript" src = "https://cdn.polyfill.io/v2/polyfill.min.js?features=es6"></script>`

### esbuild

- 编译报错

  当针对旧版浏览器时，无法使用 esbuild 作为缩小器，因为 esbuild 缩小不是旧版安全的
  `Can't use esbuild as the minifier when targeting legacy browsers because esbuild minification is not legacy safe`
  需要关闭压缩，以兼容 IE

```js
module.exports = {
  // 本地路径

  build: {
    // 兼容IE 测试
    target: 'es2015'
    // 关闭压缩
    minify: false,
    // minify: 'esbuild',
  }
}
```
