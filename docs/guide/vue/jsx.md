# JSX

## JSX 语法

- [jsx-Babel 插件](https://github.com/vuejs/jsx-next)

```
npm i -D @vue/babel-plugin-jsx
```

.babelrc

```
{
  "plugins": ["@vue/babel-plugin-jsx"]
}
```

## Jsx vue

vue 组件

```js
@vitejs/plugin-vue-jsx


import vueJsx from '@vitejs/plugin-vue';

const config = {
  plugins: [vueJsx()],
};
module.exports = config;

```

## vue-template-compiler

```js
module.exports = {
  alias: {
    '@': path.resolve(__dirname, 'src'),
    // 带完整组件编译执行
    vue: 'vue/dist/vue.esm-bundler.js'
  }
};
```

```js
const compiler = require('vue-template-compiler');

const result = compiler.compile(`
  <div id="test">
    <div>
      <p>This is my vue render test</p>
    </div>
    <p>my name is {{myName}}</p>
  </div>`);

console.log(result);
```

## render h 渲染函数

- [h() 函数](https://v3.cn.vuejs.org/guide/render-function.html#h-%E5%8F%82%E6%95%B0)
  是一个用于创建 VNode 的实用程序。也许可以更准确地将其命名为 createVNode()，但由于频繁使用和简洁，它被称为 h() 。
  它接受三个参数

```js
export default {
  props: {
    /** 组件命名 */
    named: {
      type: String,
      default: 'mindmap'
    }
  },
  render(props) {
    return h('div', {}, 'render 渲染');
  },
  setup() {
    return () =>
      h(
        'div', // 标签名
        {}, // prop 或 attribute
        'setup 输出渲染' // 包含其子节点的数组
      );
  }
};
```
