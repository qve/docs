# vue 版本

## vue 3.2.x

性能提升
响应式的优化
更高效的 ref 实现，读取提升约 260%，写入提升约 50%
依赖收集速度提升约 40%
减少内存消耗约 17%
模板编译器优化
创建元素 VNodes 速度提升约 200%

- Web Components：自定义 web 组件
- New SFC Features：新的单文件组件特性
- Effect Scope API： effect 作用域，用来直接控制响应式副作用的释放时间(computed 和 watchers)。这是底层库的更新
- SSR：服务端渲染优化。@vue/server-renderer 包加了一个 ES 模块创建，与 Node.js 解耦，使在非 Node 环境用@vue/serve-render 做服务端渲染成为可能，比如(Workers、Service Workers)

### useContext 废弃

由于 expose、slots、emit、attrs 都不能通过 useContext 获取了，随之而来的是下面几个

```js
//原来的 useContext 是这样的，现在下面这个都不能用了
import { useContext } from 'vue';
const { expose, slots, emit, attrs } = useContext();

// 新的代替方式
import { useAttrs, useSlots } from 'vue';
const attrs = useAttrs();
const slots = useSlots();
```

### defineExpose 新增组件引入

不需要通过 import 引入，向下面这样直接使用，功能一样，对外暴露属性和方法

```js
defineExpose({
    name:"沐华"
    someMethod(){
        console.log("这是子组件的方法")
    }
})
```

### defineEmit 改名

```js
// 原来引入
// import { defineEmit } from "vue"

// 新的直接使用
defineEmits(['getName', 'myClick']);
```

### 导出下载文件 Blob

- [各种类型文件的 type 整理](https://blog.csdn.net/weixin_43299180/article/details/119818982?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522164146254616780269855978%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=164146254616780269855978&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v31_ecpm-3-119818982.first_rank_v2_pc_rank_v29&utm_term=vue+Blob+json+xls&spm=1018.2226.3001.4187)
