# ES6

## Symbol

- ES6 引入了一种新的原始数据类型 Symbol，表示独一无二的值。它是 JavaScript 语言的第七种数据类型，前六种是：Undefined、Null、布尔值（Boolean）、字符串（String）、数值（Number）、对象（Object）。

- Symbol 值通过 Symbol 函数生成。这就是说，对象的属性名现在可以有两种类型，一种是原来就有的字符串，另一种就是新增的 Symbol 类型。凡是属性名属于 Symbol 类型，就都是独一无二的，可以保证不会与其他属性名产生冲突。

- 注意，Symbol 函数前不能使用 new 命令，否则会报错。这是因为生成的 Symbol 是一个原始类型的值，不是对象。也就是说，由于 Symbol 值不是对象，所以不能添加属性。基本上，它是一种类似于字符串的数据类型。

Symbol 函数可以接受一个字符串作为参数，表示对 Symbol 实例的描述，主要是为了在控制台显示，或者转为字符串时，比较容易区分。

- 由于每一个 Symbol 值都是不相等的，这意味着 Symbol 值可以作为标识符，用于对象的属性名，就能保证不会出现同名的属性。这对于一个对象由多个模块构成的情况非常有用，能防止某一个键被不小心改写或覆盖。Symbol 值作为对象属性名时，不能用点运算符。在对象的内部，使用 Symbol 值定义属性时，Symbol 值必须放在方括号之中。

- Symbol 实例：消除魔术字符串

魔术字符串指的是，在代码之中多次出现、与代码形成强耦合的某一个具体的字符串或者数值。风格良好的代码，应该尽量消除魔术字符串，该由含义清晰的变量代替。

## import 加载

- 加载默认输出

```js
import quick from 'quick.lib';
```

- 按需加载

```js
import { bll } from 'quick.lib';
```

- 异步加载

```js
import('quick.lib')
  .then((_quick) => {
    // _quick.bll
  })
  .catch((error) => {
    console.error('import.mermaid.min', error);
  });
```

## 对象 getter setter class

- 属性 a 称为“数据属性”，它只有一个简单的值；
- 属性 b 这种用 getter 和 setter 方法定义的属性称为“存取器属性”。
- 存取器属性定义为一个或两个与属性同名的函数，这个函数定义没有使用 function 关键字，而是使用 get 或 set，也没有使用冒号将属性名和函数体分开，但函数体的结束和下一个方法之间有逗号隔开。
- 当一个属性被定义为存取器属性时，JavaScript 会忽略它的 value 和 writable 特性，取而代之的是 set 和 get（还有 configurable 和 enumerable）特性。
  对象本身的两个方法

```js
let value = 2;

let json = {
  a: value,
  b: {
    set(val) {
      value = val;
    },
    get() {
      return value;
    }
  }
};

json.b = 31;

// ES5 写法
function fun(n) {
  this._val = n;
}

fun.prototype = {
  get val() {
    console.log('get');
    return this._val;
  },
  set val(n) {
    console.log(n, 'set');
    this._val = n;
  }
};

fun = 5;

// ES6 写法
const obj = {
  log: ['a', 'b', 'c'],
  set latest(val) {
    this.log.push(val);
  },
  get latest() {
    if (this.log.length === 0) {
      return undefined;
    }
    return this.log[this.log.length - 1];
  }
};

obj.latest = 'f6';

return { json, fun, obj };
```

## 数组去重

Set 实例的方法分为两大类：操作方法（用于操作数据）和遍历方法（用于遍历成员）。下面先介绍四个操作方法。
add(value)：添加某个值，返回 Set 结构本身。
delete(value)：删除某个值，返回一个布尔值，表示删除是否成功。
has(value)：返回一个布尔值，表示该值是否为 Set 的成员。
clear()：清除所有成员，没有返回值。
Array.from 方法可以将 Set 结构转为数组。

Set 结构的实例有四个遍历方法，可以用于遍历成员。
keys()：返回键名的遍历器
values()：返回键值的遍历器
entries()：返回键值对的遍历器
forEach()：使用回调函数遍历每个成员
需要特别指出的是，Set 的遍历顺序就是插入顺序。
由于 Set 结构没有键名，只有键值（或者说键名和键值是同一个值），所以 keys 方法和 values 方法的行为完全一致。
Set 结构的实例默认可遍历，它的默认遍历器生成函数就是它的 values 方法。
这意味着，可以省略 values 方法，直接用 for...of 循环遍历 Set。
扩展运算符（...）内部使用 for...of 循环，所以也可以用于 Set 结构。

```js

let a=[1,2,3,4,5];

// Set 它类似于数组，但是成员的值都是唯一的，没有重复的值。

let set=new Set();

举例
//1 数组去重
let a=[1,2,3,4,5,6,7,1,2,3];
let b=new Set([...a]);
b=[...b];

// 2 求交集 并集  差集
let a=new Set[1,2,3];
let b=new Set[3,4,5];
//交集
let c=new Set([...a,...b]);
//并集
let d=new Set([...a].filter(x=>b.has[x]));
//交集
let d=new Set([...a].filter(x=>！b.has[x]));

let arr = [1, 1, 2, 9, 6, 9, 6, 3, 1, 4, 5];
// 交集去重并转为数组
return Array.from(new Set(...arr,...a));
```
