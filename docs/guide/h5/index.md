# 常用

js 常用原生函数

## w3school

- [w3school](https://www.w3school.com.cn/)

在 W3School，你可以找到你所需要的所有的网站建设教程。
从基础的 HTML 到 CSS，乃至进阶的 XML、SQL、JS、PHP 和 ASP.NET。

## 页面屏幕尺寸

网页可见区域宽：document.body.clientWidth
网页可见区域高：document.body.clientHeight
网页可见区域宽：document.body.offsetWidth (包括边线的宽)
网页可见区域高：document.body.offsetHeight (包括边线的宽)
网页正文全文宽：document.body.scrollWidth
网页正文全文高：document.body.scrollHeight
网页被卷去的高：document.body.scrollTop
网页被卷去的左：document.body.scrollLeft
网页正文部分上：window.screenTop
网页正文部分左：window.screenLeft
屏幕分辨率的高：window.screen.height
屏幕分辨率的宽：window.screen.width
屏幕可用工作区高度：window.screen.availHeight
屏幕可用工作区宽度：window.screen.availWidth

## JS 常用原生函数

### Array 数组

| 数组方法        | 说明                                                                     |
| --------------- | ------------------------------------------------------------------------ |
| toString()      | 将数组转换成字符串                                                       |
| toLocalString() | 将数组转换成本地约定的字符串                                             |
| join()          | 将数组元素连接起来以构建一个字符串                                       |
| map()           | 循环输出                                                                 |
| splice(1,1)     | 数组从第`2`位开始删除 1 个                                               |
| push            | 原数组追加                                                               |
| concat          | 合并数组,返回新构建的数组                                                |
| slice           | 返回新数组，(参数`1`开始位置,参数`2`截止的位置 空为到末尾)参数为负数倒数 |

<CodeRun editable>

```js
let a = [1, 2, 3, 4, 5, 6, 7, 8]; //定义数组
let b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]; //定义数组

a.push(9); //追加

//定义多维数组
let c = [
  [1, [2, 3], [4, 5]],
  [6, [7, [8, 9], 0]]
];

let res = {
  ac: a.concat(20, 21), //数组添加到新数组
  aaa: a.coucat([56, 59]),
  as: a.concat(3, 6),
  // s: a.toString(), //把数组转换为字符串,返回字符串“1,2,3,4,5,6,7,8,9,0”
  s: a + '', //转字符串
  //  cs: c.toString(), //把数组转换为字符串
  //  la: a.toLocalString(), //主要区别在于能够使用用户所在地区特定的分隔符
  str: a + ',' + b, //数组连接操作
  lj: a.join('=='), //指定分隔符,返回字符串“1==2==3==4==5”
  indexOf: a.indexOf(3)
};

res.type = typeof res.s; //返回类型
res.arr = res.s.split(','); //按指定符号分割，返回数组

//返回找出元素不是9元素
res.f = a.filter(function (item) {
  return item != 3;
});

// 查找元素位置
res.i = a.indexOf(5);
// 删除指定位置的长度,返回删除的对象
res.sr = a.splice(res.i, 1);
res.sa = a.map((item) => {
  //循环+1
  return item + 1;
});

return res;
```

</CodeRun>

## async await 异步并行

<CodeRun editable>

```js
const sum = async (int) => {
  return int++;
};

return {
  one: sum(55),
  two: sum(60)
};
```

</CodeRun>

## reduce 累加

支持谷歌、火狐、ie9 以上等主流浏览器

- reduce() 方法

  - 接收一个函数作为累加器，数组中的每个值（从左到右）开始缩减，最终计算为一个值。
  - 可以作为一个高阶函数，用于函数的 compose
  - 对于空数组是不会执行回调函数的

- 参数
  - prev：函数传进来的初始值或上一次回调的返回值
  - current：数组中当前处理的元素值
  - currentIndex：当前元素索引
  - arr：当前元素所属的数组本身
  - initialValue：传给函数的初始值

<CodeRun editable>

```js
const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// 传入的初始值count，不断回调叠加最终算出数组的和
const sum = arr.reduce(
  (prev, current) => {
    //对象加 当前值
    prev.start = prev.start + current;
    // 继续回调对象
    return prev;
  },
  // 传入对象
  { start: 0 }
);

// 累计循环取出对象
let res = '0.1'.split('.').reduce(
  (o, i) => {
    if (o.list) return o.list[i];
  },
  [
    { key: '0', list: [{ key: '0.1' }] },
    { key: '1', list: [{ key: '1.0' }] }
  ]
);

return {
  sum,
  res
};
```

</CodeRun>

## js 判断中英字符宽度

因为中文字符的宽度与英文字符宽度不一样
需取出中文和英文的字符数，中文长度加 2，英文长度加 1

```js
function strLen(str) {
  if (str == null) return 0;
  if (typeof str != 'string') {
    str += '';
  }
  return str.replace(/[^\x00-\xff]/g, '01').length;
}
```
