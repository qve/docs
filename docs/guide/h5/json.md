# JSON 原生函数

## stringify 转字符串

stringify 是支持三个参数

<CodeRun dll="json" editable>

```js
let obj = { d: 133, m: 'acd', time: new Date() };

//console.log(obj);

let resp = [
  // 标准转字符串
  JSON.stringify(obj),
  // 输出过滤处理
  JSON.stringify(obj, function (key, val) {
    // getType 自定义函数获取类型
    console.log(key, val, typeof val);

    return val;
  })
];

//每一个层级比上一个多一个制表符
console.log(JSON.stringify(obj, null, '\t'));

//每一个层级比上一个多10个空格
return JSON.stringify(resp, null, 10);
```

</CodeRun>

### json 转义

<CodeRun editable>

```js
let obj = { test: '测试转义' };

let str = JSON.stringify(obj);
console.log('转换为字符串', str);

console.log('转换为字符串，带转义符号', JSON.stringify(str));
return str;
```

</CodeRun>

### json 排序

- localeCompare 字符串的对比排序方法

<CodeRun editable>

```js

let obj = [{index:1 test: '测试1' },{index:3 test: '测试3' },{index:5 test: '测试5' }];

return {
  // 字符串排序
  str:obj.sort((a, b) => {
                let _sa = a['test'] || '',_sb = b['test'] || '';
                return _sa.localeCompare(_sb);
}),
int:the.data.sort((a, b)=> {
  return a.index - b.index;
})
}
```

</CodeRun>

## hasOwnProperty 判断属性

hasOwnProperty()方法用于检测一个对象是否含有特定的自身属性，返回一个布尔值

<CodeRun editable>

```js
function Fn() {
  this.age = 'wm';
}
Fn.prototype.sex = '女';
var obj = new Fn();
//  实例化对象会继承构造函数里的属性和方法

let user = {
  name: 'test'
};

return {
  name: user['name'], // 取值判断
  age: obj.hasOwnProperty('age'), // true
  a: Fn.prototype.hasOwnProperty('age'), //false
  sex: obj.hasOwnProperty('sex') // false
};
```

</CodeRun>
