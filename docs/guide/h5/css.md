# Css 样式

## Less

less 无法在浏览器中直接使用，浏览器不能识别

- [less 官网](https://less.bootcss.com/)

- 全局引入使用示例
  - 引入 less.js 插件
  - 引入 less 文件，必须在 link 标签中加入 type=“text/less”的声明，否则浏览器无法识别

```html
<html>
  <head>
    <!-- 必须声明type="text/less" -->
    <link rel="stylesheet" type="text/less" href="test.less" />
    <!-- 引入less.js -->
    <script src="less.js"></script>
    <script>
      // 方法可以监听less文件的活动，实时刷新，调试时使用
      less.watch();
    </script>
  </head>
  <body>
    <div>less</div>
  </body>
</html>
```

### Less 格式转换编译为 css

`qve`组件代码编辑器，模板编译使用此方法

```html
<html>
  <body>
    <script
      id="loadLessScript"
      type="text/javascript"
      src="/static/plus/less/less.min.js"
    ></script>

    <script>
      less
        .render('.test-tpl-page{h5{color: #333fff;}}')
        .then(function (output) {
          // output.css = string of css
          // output.map = undefined
          console.log('less', output);
        });
    </script>
  </body>
</html>
```

## css 属性

- initial（默认）
- inherit（继承）
- unset 可不设置，它是关键字 initial 和 inherit 的组合
- revert 定义的元素属性的默认值
- all 表示重设除 unicode-bidi 和 direction 之外的所有 CSS 属性的属性值，取值只能是 initial、inherit、unset 和 revert
  兼容性: IE 不支持，safari9-不支持，ios9.2-不支持，android4.4-不支持

### css 替换 important

项目中，需要覆盖原来的 css 样式，可以使用`!important`

```less
.sidebar {
  // 原来定义值
  width: 20rem;
}

.sidebar {
  // 以新值替换
  width: 16rem !important;
}
```

### position

static、relative、absolute 和 fixed sticky

### flex 兼容

flex 布局分为旧版本 dispaly: box;，过渡版本 dispaly: flex box;，以及现在的标准版本 display: flex;。
所以只是写新版本的语法形式，是肯定存在兼容性问题的。

- 支持版本

  - Android
    2.3 开始就支持旧版本 display:-webkit-box;
    4.4 开始支持标准版本 display: flex;
  - IOS
    6.1 开始支持旧版本 display:-webkit-box;
    7.1 开始支持标准版本 display: flex;
  - PC
    ie10 开始支持，但是 IE10 的是-ms 形式的。

- 盒子的兼容性写法

写法的顺序一定要按最新到最旧的顺序，否则不起作用

```less
// 部署兼容
.flex() {
  display: -webkit-flex; // 新版本语法: Chrome 21+
  display: flex; //  新版本语法: Opera 12.1, Firefox 22+
  display: -webkit-box; // 老版本语法: Safari, iOS, Android browser, older WebKit browsers.
  display: -moz-box; //  老版本语法: Firefox (buggy)
  display: -ms-flexbox; //  混合版本语法: IE 10
}

.box {
  .flex();
}
```

- 子元素的兼容性写法

```css
.flex1 {
    -webkit-flex: 1;        /* Chrome */
    -ms-flex: 1             /* IE 10 */
    flex: 1;                /* NEW, Spec - Opera 12.1, Firefox 20+ */
    -webkit-box-flex: 1     /* OLD - iOS 6-, Safari 3.1-6 */
    -moz-box-flex: 1;       /* OLD - Firefox 19- */
}
```
