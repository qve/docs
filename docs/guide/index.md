# 指引扩展

## nginx 配置

```s
worker_processes  1; #CPU核数

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
error_log  logs/error_warn.log warn;
error_log  logs/error_info.log  info; #错误日志log

events {
    worker_connections  102400;   #允许最大连接数
}

http {
    # 请求日志记录，关键字段：request_time-请求总时间，upstream_response_time后端处理时 间
    log_format  main  '$remote_addr  - $remote_user [$time_iso8601] "$request" '
                 '$status $body_bytes_sent "$http_referer" '
                  '"$http_user_agent" "$http_x_forwarded_for" "$host"  "$cookie_ssl_edition" '
                 '"$upstream_addr"   "$upstream_status"  "$request_time"  '
                 '"$upstream_response_time" ';

    #access_log off; # 关闭访问日志
    # 请求日志,需建文件夹access/服务名分类日志
    access_log  logs/access/${server_name}_.log  main;

    # 长连接超时 0 关闭长连接
    # keepalive_timeout  65;

    #允许在header的字段中带下划线
    underscores_in_headers on;

    # 缓冲到内存
    #client_max_body_size 5m;
    client_header_buffer_size 128k;
    client_body_buffer_size 1m;
    proxy_buffer_size 32k;
    proxy_buffers 64 32k;
    proxy_busy_buffers_size 1m;
    proxy_temp_file_write_size 512k;
    #gzip  on;
}
```

### log_format 格式变量

- [日志配置参考](https://blog.csdn.net/strggle_bin/article/details/110561976)

```
参数                      说明                                         示例
$remote_addr             客户端地址                                    211.28.65.253
$remote_user             客户端用户名称                                --
$time_local              访问时间和时区                                18/Jul/2012:17:00:01 +0800
$request                 请求的URI和HTTP协议                           "GET /article-10000.html HTTP/1.1"
$http_host               请求地址，即浏览器中你输入的地址（IP或域名）     www.wang.com 192.168.100.100
$status                  HTTP请求状态                                  200
$upstream_status         upstream状态                                  200
$body_bytes_sent         发送给客户端文件内容大小                        1547
$http_referer            url跳转来源                                   https://www.test.com/
$http_user_agent         用户终端浏览器等信息                           "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; SV1; GTB7.0; .NET4.0C;
$ssl_protocol            SSL协议版本                                   TLSv1
$ssl_cipher              交换数据中的算法                               RC4-SHA
$upstream_addr           后台upstream的地址，即真正提供服务的主机地址     10.10.10.100:80
$request_time            整个请求的总时间                               0.205
$upstream_response_time  请求过程中，upstream响应时间                    0.002
```

### nginx header

- 报错
  `client sent invalid header line: "SOGOU_VERSION:`

  nginx 对 header name 的字符做了限制，默认 underscores_in_headers 为 off，表示如果 header name 中包含下划线，则忽略掉。

- 处理方法
  1：配置中 http 部分 增加 underscores*in_headers on; 配置
  2：用减号-替代下划线符号*，避免这种变态问题。
  YY 一下：减号代替下划线，对于 plesk 认证方式做修改，自然这种小白明显是完成不了的。
  语法：underscores_in_headers on|off
  默认值：off
  使用字段：http, server
  是否允许在 header 的字段中带下划线。

### nginx upstream temporary

- 报错，意思是 nginx 默认的 buffer 太小，每个请求的缓存太小，请求头 header 太大时会出现缓存不足，内存放不下上传的文件，就写入到了磁盘中，使 nginx 的 io 太多，造成访问中断。
  `an upstream response is buffered to a temporary file /proxy_temp/0/52/0002923520 while reading upstream`
- 处理方法

```s
    # 缓冲到内存
    #client_max_body_size 5m;
    client_header_buffer_size 128k;
    client_body_buffer_size 1m;
    proxy_buffer_size 32k;
    proxy_buffers 64 32k;
    proxy_busy_buffers_size 1m;
    proxy_temp_file_write_size 512k;
```
