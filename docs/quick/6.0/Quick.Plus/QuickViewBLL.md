---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:27

---
# QuickViewBLL
- 引用：Quick.Plus.QuickViewBLL


## QuickViewBLL  `页面 View 压缩 筛选器`


- **构造函数**

  - QuickViewBLL([Stream](https://docs.microsoft.com/zh-cn/dotnet/api/system.io.stream)    body,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.func<system.string,system.string>)    filter)
       `页面压缩`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | body |返回内容
      | filter |过滤规则


 --- 
## Write

 方法:  QuickViewBLL.Write

### `模板输出`


> Write([Byte][byte]  buffer,[Int32][int32]  offset,[Int32][int32]  count)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | buffer|[Byte][byte] |内容  ||
    | offset|[Int32][int32] |  ||
    | count|[Int32][int32] |  ||



[byte]:https://docs.microsoft.com/zh-cn/dotnet/api/system.byte
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32


