---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:27
---

# Quick.Plus

template: 2021.12
author: quick.docs
date: 2021 年 12 月 31 日 15:27

## Quick.Plus 目录

### [Aliyun.AliyunSettings](./Aliyun_AliyunSettings.md)

      阿里云设置

### [Aliyun.AliyunSettings.SMS](./Aliyun_AliyunSettings_SMS.md)

      短信通知

### [Aliyun.AliyunSMSBLL](./Aliyun_AliyunSMSBLL.md)

      阿里云短信服务接口

### [BLL.HttpPlusBLL](./BLL_HttpPlusBLL.md)

      http接口请求

### [QuickViewBLL](./QuickViewBLL.md)

      页面 View 压缩 筛选器
