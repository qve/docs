---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:27

---
# Aliyun.AliyunSMSBLL
- 引用：Quick.Plus.Aliyun.AliyunSMSBLL


## Aliyun.AliyunSMSBLL  `阿里云短信服务接口`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | client|  | 阿里云客户端 |
- **构造函数**

  - Aliyun.AliyunSMSBLL()
       `根据配置创建`



 --- 
## Send

 方法:  Aliyun.AliyunSMSBLL.Send

### `发送模板短信`


> Send([String][string]  phones,[String][string]  TemplateParam,[String][string]  templateCode)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | phones|[String][string] |手机号；多个用逗号分隔  ||
    | TemplateParam|[String][string] |json短信内容  ||
    | templateCode|[String][string] |模板ID  ||

  - **返回**

   > AlibabaCloud.SDK.Dysmsapi20170525.Models.SendSmsResponse 

 --- 
## Send

 方法:  Aliyun.AliyunSMSBLL.Send

### `发送短信`


> Send([String][string]  phones,[String][string]  TemplateParam,[String][string]  templateCode,[String][string]  signName)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | phones|[String][string] |手机号；多个用逗号分隔  ||
    | TemplateParam|[String][string] |json短信内容  ||
    | templateCode|[String][string] |模板ID  ||
    | signName|[String][string] |签名【天天热点】  ||

  - **返回**

   > AlibabaCloud.SDK.Dysmsapi20170525.Models.SendSmsResponse 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string


