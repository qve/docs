---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:27

---
# Aliyun.AliyunSettings
- 引用：Quick.Plus.Aliyun.AliyunSettings


## Aliyun.AliyunSettings  `阿里云设置`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | SMS.sign|  | 模板签名 |
 | SMS.host|  | 短信服务地址 |
 | SMS.accessKeyId|  | 认证key |
 | SMS.accessKeySecret|  | 认证密钥 |



