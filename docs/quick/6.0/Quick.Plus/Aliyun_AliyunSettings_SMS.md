---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:27

---
# Aliyun.AliyunSettings.SMS
- 引用：Quick.Plus.Aliyun.AliyunSettings.SMS


## Aliyun.AliyunSettings.SMS  `短信通知`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | sign|  | 模板签名 |
 | host|  | 短信服务地址 |
 | accessKeyId|  | 认证key |
 | accessKeySecret|  | 认证密钥 |



