---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:27

---
# BLL.HttpPlusBLL
- 引用：Quick.Plus.BLL.HttpPlusBLL


## BLL.HttpPlusBLL  `http接口请求`



 --- 
## PostAsync

 方法:  BLL.HttpPlusBLL.PostAsync

### `Post 请求`


> PostAsync([String][string]  url,[String][string]  data)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | url|[String][string] |请求地址  ||
    | data|[String][string] |json 字符串  ||

  - **返回**

   > 

 --- 
## PostAsync

 方法:  BLL.HttpPlusBLL.PostAsync

### `Post 请求`


> PostAsync([String][string]  url,[HttpContent][httpcontent]  content)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | url|[String][string] |请求参数  ||
    | content|[HttpContent][httpcontent] |请求内容  ||

  - **返回**

   > HttpClient.ReadAsStringAsync 

 --- 
## GetAsync

 方法:  BLL.HttpPlusBLL.GetAsync

### `Get请求`


> GetAsync([String][string]  url)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | url|[String][string] |请求地址  ||

  - **返回**

   > Content.ReadAsStringAsync().Result 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[httpcontent]:https://docs.microsoft.com/zh-cn/dotnet/api/system.net.http.httpcontent


