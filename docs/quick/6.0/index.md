# Quick

Net 6.0 与 Net 5 对比会更简洁，代码量更少。

- [官方文档](https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/startup?view=aspnetcore-6.0)

## nuget 发行包

- [Quick 基础类库包](https://www.nuget.org/packages/quick)
- [Quick.DB 数据库交互类库](https://www.nuget.org/packages/quick.db)

## Net 6 Program 启动配置

- Program.cs

```csharp
using Quick;
using Quick.BLL;
using Quick.Safe;

var builder = WebApplication.CreateBuilder(args);

#region 中间件

// 注册QuickCache 缓存
QuickCache.Memory = new QuickMemoryProvider();

// 自定义配置参考，using Microsoft.Extensions.Caching.Memory;
//QuickCache.Memory = new QuickMemoryProvider(new MemoryCacheOptions()
//{
//    //缓存最大为100份
//    //##注意netcore中的缓存是没有单位的，缓存项和缓存的相对关系
//    SizeLimit = 100,
//    //缓存满了时，压缩20%（即删除20份优先级低的缓存项）
//    CompactionPercentage = 0.2,
//    //3秒钟查找一次过期项
//    ExpirationScanFrequency = TimeSpan.FromSeconds(3)
//});

// 注入（DI）在同一个请求发起到结束，在服务容器中注册自定义中间件用户类
builder.Services.AddScoped(typeof(QuickSafeFilterModel));

//注册过滤器服务，使用ServiceFilter 方式必须要注册 否则会报没有注册该服务的相关异常
//services.AddSingleton<QuickAuthFilter>();
#endregion

#region 跨域

// 授权跨域的域名配置
string[] _orgins = QuickSettings.Session.WithOrigins;
// 跨域规则的名称 APi 禁止跨域  [DisableCors] 单独跨域 [EnableCors("AllowCors")]
builder.Services.AddCors(options => options.AddPolicy("AllowCors", builder =>
{
    //builder.AllowAnyOrigin() //允许任意origin
    //.AllowCredentials(); // 可以接收cookie

    builder.AllowAnyHeader() //允许任意header
              .AllowAnyMethod()//允许任意请求方式
                               //.AllowAnyOrigin() //允许任意origin
            .WithOrigins(_orgins) //指定特定域名才能访问
            .SetPreflightMaxAge(TimeSpan.FromSeconds(60))    //允许验证OPTIONS请求进行检测时间
            .SetIsOriginAllowedToAllowWildcardSubdomains()
           .AllowCredentials(); // 指定接收cookie
}));
#endregion

#region Session 跨域会话保持
builder.Services.AddSession(option =>
{
    option.IOTimeout = TimeSpan.FromMinutes(QuickSettings.Session.Cookie.Timeout);
    // 会话超时分钟配置
    option.IdleTimeout = TimeSpan.FromMinutes(QuickSettings.Session.Cookie.Timeout);
    option.Cookie.Name = QuickSettings.Session.Cookie.Name;
    //js无法获得cookie ，防XSS攻击
    option.Cookie.HttpOnly = true;
    // 使用
    option.Cookie.IsEssential = true;

    // 使用宽松模式，同一站点保持会话
    //  option.Cookie.SameSite = SameSiteMode.Unspecified; //.Lax.None.Strict;
    option.Cookie.SameSite = SameSiteMode.None;
    //设置存储用户登录信息（用户Token信息）的Cookie，只会通过HTTPS协议传递，如果是HTTP协议，Cookie不会被发送。注意，option.Cookie.SecurePolicy属性的默认值是Microsoft.AspNetCore.Http.CookieSecurePolicy.SameAsRequest
    option.Cookie.SecurePolicy = CookieSecurePolicy.Always;
});

#endregion

#region 全局权限验证
builder.Services.AddControllersWithViews(options =>
{
    // 全局注册权限验证过滤器
    options.Filters.Add(typeof(QuickSafeFilter));

}).AddJsonOptions(configure =>
{
    // 全局Json 时间格式化，避免前端ISO转换
    configure.JsonSerializerOptions.Converters.Add(new JsonHelperConvert.DateTimeConverter());
});
#endregion

// 添加服务配置
//builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.

#region 自定义配置

// 支持默认文件 index.html
app.UseDefaultFiles();

// 静态文件
app.UseStaticFiles();

// 跨域
app.UseCors("AllowCors");

// Session
app.UseSession();
#endregion

#region 默认路由配置
//app.MapGet("/hi", () => "Hello!");

app.MapControllerRoute("Page", "/Page/{*path}", new
{
    controller = "Page",
    action = "Index"
});

// 自定义 vue 本地路径
app.MapControllerRoute("Vues", "/Vue/{*path}", new
{
    controller = "Vue",
    action = "Index"
});

// 自定义 数据编辑接口
app.MapControllerRoute("DB", "/DB/{*path}", new
{
    controller = "DB",
    action = "Index", //指定接收动作
});

// 自定义 数据编辑接口
app.MapControllerRoute("DI", "/DI/{*path}", new
{
    controller = "DI",
    action = "Index", //指定接收动作
});


// 标准MVC 控制器
//app.MapControllerRoute(
//    name: "default",
//    pattern: "{controller=Home}/{action=Index}/{id?}");

app.MapDefaultControllerRoute();

#endregion

app.Run();

```
