---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:30

---
# Model.QF_TreeMenuModel
- 引用：Quick.Frame.Model.QF_TreeMenuModel


## Model.QF_TreeMenuModel  `菜单对象`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | title|  | 菜单显示名称 |
 | icon|  | 菜单显示图标 |
 | path|  | 请求路径 |
 | menuOpen|  | 菜单打开模式 |
 | code|  | 页面编码 |



