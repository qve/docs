---
template: 2021.12
version: 2021/12/15 23:07:18
author: quick.docs
date: 2021年12月31日 15:30

---
# BLL.QF_ModelBLL
- 引用：Quick.Frame.BLL.QF_ModelBLL
- version: 2021/12/15 23:07:18


## BLL.QF_ModelBLL  `数据对象配置业务处理类`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | qp|  | 数据库指向参数对象 |

 --- 
## Query

 方法:  BLL.QF_ModelBLL.Query

### `查询对象，bll.qp`


> Query()


  - **返回**

   > 返回对象值 

 --- 
## GetById

 方法:  BLL.QF_ModelBLL.GetById

### `根据ID 查对象`


> GetById([Int32][int32]  id)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | id|[Int32][int32] |数据 ID  ||

  - **返回**

   > -1 返回默认对象值 

 --- 
## GetField

 方法:  BLL.QF_ModelBLL.GetField

### `返回 对象字段与参数`


> GetField()


 --- 
## GetTplModel

 方法:  BLL.QF_ModelBLL.GetTplModel

### `返回 字段Json描述`


> GetTplModel()


  - **返回**

   > 

 --- 
## GetTitleByID

 方法:  BLL.QF_ModelBLL.GetTitleByID

### `根据ID取出描述`


> GetTitleByID([String][string]  _field,[String][string]  _val)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _field|[String][string] |查询字段  ||
    | _val|[String][string] |查询值  ||

  - **返回**

   > 

 --- 
## Add

 方法:  BLL.QF_ModelBLL.Add

### `添加数据`


> Add([QF_ModelCompent][qf_modelcompent]  o,[Int64][int64]  MUser_ID,[Int32][int32]  action)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | o|[QF_ModelCompent][qf_modelcompent] |数据对象  ||
    | MUser_ID|[Int64][int64] |编辑者  ||
    | action|[Int32][int32] |数据插入方式1:返回影响行数,5:返回最新插入ID  ||

  - **返回**

   > 返回结果 

 --- 
## Edit

 方法:  BLL.QF_ModelBLL.Edit

### `修改`


> Edit([QF_ModelCompent][qf_modelcompent]  ID,[Int64][int64]  MUser_ID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | ID|[QF_ModelCompent][qf_modelcompent] |数据ID  ||
    | MUser_ID|[Int64][int64] |编辑者  ||

  - **返回**

   > 

 --- 
## Audit

 方法:  BLL.QF_ModelBLL.Audit

### `多条数据，标准审核`


> Audit([String][string]  _ids,[Int64][int64]  MUser_ID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _ids|[String][string] |多数据,ID  ||
    | MUser_ID|[Int64][int64] |编辑者  ||

  - **返回**

   > 返回影响的条数 

 --- 
## Audit

 方法:  BLL.QF_ModelBLL.Audit

### `多条数据，高级权限审核`


> Audit([String][string]  _ids,[Int32][int32]  SF_ID,[Int64][int64]  MUser_ID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _ids|[String][string] |多数据,ID  ||
    | SF_ID|[Int32][int32] |指定数据状态  ||
    | MUser_ID|[Int64][int64] |编辑者  ||

  - **返回**

   > 返回影响的行数 

 --- 
## Delete

 方法:  BLL.QF_ModelBLL.Delete

### `停用删除数据`


> Delete([String][string]  _ids,[Int64][int64]  MUser_ID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _ids|[String][string] |多数据,ID  ||
    | MUser_ID|[Int64][int64] |编辑者  ||

  - **返回**

   > 返回影响的行数 

 --- 
## Erase

 方法:  BLL.QF_ModelBLL.Erase

### `物理删除停用数据`


> Erase([String][string]  _ids,[Int64][int64]  MUser_ID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _ids|[String][string] |多数据,ID  ||
    | MUser_ID|[Int64][int64] |编辑者  ||

  - **返回**

   > 返回影响的行数 

 --- 
## RowID

 方法:  BLL.QF_ModelBLL.RowID

### `跳转数据行ID`


> RowID([Boolean][boolean]  next,[Object][object]  _id)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | next|[Boolean][boolean] |是下一条否上一条  ||
    | _id|[Object][object] |当前ID  ||

  - **返回**

   > 跳转ID 

 --- 
## List

 方法:  BLL.QF_ModelBLL.List

### `返回查询对象列表`


> List()


  - **返回**

   > 

 --- 
## ListJson

 方法:  BLL.QF_ModelBLL.ListJson

### `查询 Json 数组列表,qp查询条件`


> ListJson()


  - **返回**

   > Json 数组 



[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[qf_modelcompent]:../Quick.Frame/Model_QF_ModelCompent.md
[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64
[boolean]:https://docs.microsoft.com/zh-cn/dotnet/api/system.boolean
[object]:https://docs.microsoft.com/zh-cn/dotnet/api/system.object


