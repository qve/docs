---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:30

---
# BLL.QF_PageBLL
- 引用：Quick.Frame.BLL.QF_PageBLL


## BLL.QF_PageBLL  `架构页面方法`



 --- 
## GetPageByID

 方法:  BLL.QF_PageBLL.GetPageByID

### `读取页面信息`


> GetPageByID([String][string]  _ids)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _ids|[String][string] |多个页面ID  ||

  - **返回**

   > List[ID,Code,App_ID,Title,Path] 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string


