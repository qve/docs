---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:30

---
# Model.QF_ClassCompent
- 引用：Quick.Frame.Model.QF_ClassCompent


## Model.QF_ClassCompent  `QF_Class,对象(QF_Class)              T:2021/5/17 15:01:48`
Table( &quot;QF_Class &quot;)

- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | ID|  | ID |
 | App_ID|  | 应用ID |
 | ClassCode|  | 分组编码 |
 | NameCn|  | 名称\ &quot;d\ &quot;:1 |
 | NameEn|  | 拼音 |
 | TreeGuid|  | 架构根节点 |
 | NodeCss|  | 节点样式 |
 | DB|  | 外挂数据库 |
 | TableName|  | 外挂数据表名 |
 | FieldName|  | 显示字段名 |
 | DefaultID|  | 默认值 |
 | Api|  | Api |
 | Url|  | ashx数据源 |
 | Remark|  | 备注 |
 | SF_ID|  | 数据状态\ &quot;r\ &quot;:1 |
 | MUser_ID|  | 编辑者\ &quot;r\ &quot;:1 |
 | U_Time|  | 更新时间\ &quot;r\ &quot;:1 |
- **构造函数**

  - Model.QF_ClassCompent()
       `初始化设置默认值构造`



 --- 
## ToString

 方法:  Model.QF_ClassCompent.ToString

### `字段描述Json{field:字段,attr:属性}`


> ToString()






