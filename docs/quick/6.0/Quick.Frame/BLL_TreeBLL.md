---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:30

---
# BLL.TreeBLL
- 引用：Quick.Frame.BLL.TreeBLL


## BLL.TreeBLL  `架构树配置方法`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | qp|  | 数据查询条件 |
- **构造函数**

  - BLL.TreeBLL()
       `使用默认数据库QuickData.DBQuick`


  - BLL.TreeBLL([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    dblink)
       `初始操作数据库`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | dblink |指定webconfig 连接配置


 --- 
## GetMenu

 方法:  BLL.TreeBLL.GetMenu

### `根据角色组获取菜单，State已启用`


> GetMenu([String][string]  TGuid,[Int32][int32]  RoleID,[Int32][int32]  AppID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | TGuid|[String][string] |菜单树GUID.  ||
    | RoleID|[Int32][int32] |角色组ID  ||
    | AppID|[Int32][int32] |当前应用ID  ||

  - **返回**

   > Json 数组字符串 

 --- 
## MenuJson

 方法:  BLL.TreeBLL.MenuJson

### `创建菜单关系 Json`


> MenuJson([List][list]<[QF_TreeMenuModel][qf_treemenumodel]>  treeList,[String][string]  TGuid)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | treeList|[List][list]<[QF_TreeMenuModel][qf_treemenumodel]> |树结构  ||
    | TGuid|[String][string] |树关系  ||

  - **返回**

   > 

 --- 
## MenuJsonNode

 方法:  BLL.TreeBLL.MenuJsonNode

### `创建下级树型接点`


> MenuJsonNode([QF_TreeMenuModel][qf_treemenumodel]  treeList,[String][string]  TGuid,[Int32][int32]  Depth)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | treeList|[List][list]<[QF_TreeMenuModel][qf_treemenumodel]> |树结构  ||
    | TGuid|[String][string] |树关系  ||
    | Depth|[Int32][int32] |层级  ||

  - **返回**

   > 

 --- 
## GetTreeClass

 方法:  BLL.TreeBLL.GetTreeClass

### `取出分组查询表`


> GetTreeClass([Int32][int32]  ID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | ID|[Int32][int32] |分组ID  ||

  - **返回**

   > QF_Class 

 --- 
## GetTreeRoot

 方法:  BLL.TreeBLL.GetTreeRoot

### `顶层根`


> GetTreeRoot([Int32][int32]  RoleID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | RoleID|[Int32][int32] |角色组  ||

  - **返回**

   > 

 --- 
## GetTree

 方法:  BLL.TreeBLL.GetTree

### `获取节点下全部树结构, SF_ID非停用`


> GetTree([String][string]  TGuid,[Int32][int32]  Depth,[Int32][int32]  ClassCode,[Int32][int32]  RoleID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | TGuid|[String][string] |树节点  ||
    | Depth|[Int32][int32] |树层次  ||
    | ClassCode|[Int32][int32] |关联分组数据表QF_TreeClass.ID  ||
    | RoleID|[Int32][int32] |角色组，未实现权限方法  ||

  - **返回**

   > 返回 json 数组 

 --- 
## GetTree

 方法:  BLL.TreeBLL.GetTree

### `取出分类树节点接口`


> GetTree([String][string]  tGuid,[Int32][int32]  depth,[String][string]  tableName,[String][string]  fieldName)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | tGuid|[String][string] |树节点  ||
    | depth|[Int32][int32] |树深度  ||
    | tableName|[String][string] |外连表名  ||
    | fieldName|[String][string] |外连字段TreeName  ||

  - **返回**

   > 

 --- 
## GetTreeJson

 方法:  BLL.TreeBLL.GetTreeJson

### `取节点下全部树关系 Json`


> GetTreeJson([List][list]<[QF_ClassTreeCompent][qf_classtreecompent]>  treeList,[String][string]  TGuid)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | treeList|[List][list]<[QF_ClassTreeCompent][qf_classtreecompent]> |树结构  ||
    | TGuid|[String][string] |树关系  ||

  - **返回**

   > 

 --- 
## GetTreeNodeJson

 方法:  BLL.TreeBLL.GetTreeNodeJson

### `创建下级树型接点`


> GetTreeNodeJson([QF_ClassTreeCompent][qf_classtreecompent]  treeList,[String][string]  TGuid,[Int32][int32]  Depth)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | treeList|[List][list]<[QF_ClassTreeCompent][qf_classtreecompent]> |树结构  ||
    | TGuid|[String][string] |树关系  ||
    | Depth|[Int32][int32] |层级  ||

  - **返回**

   > 

 --- 
## DeleteTreeNode

 方法:  BLL.TreeBLL.DeleteTreeNode

### `标记为删除树节点`


> DeleteTreeNode([String][string]  TreeListID,[Int64][int64]  MUser_ID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | TreeListID|[String][string] |树节点ID数组字符串  ||
    | MUser_ID|[Int64][int64] |编辑者  ||

  - **返回**

   > 返回更新结果 

 --- 
## AddTreeNode

 方法:  BLL.TreeBLL.AddTreeNode

### `添加树节点`


> AddTreeNode([QF_ClassTreeApiModel][qf_classtreeapimodel]  o,[Int64][int64]  MUser_ID,[String][string]  tableCode)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | o|[QF_ClassTreeApiModel][qf_classtreeapimodel] |树对象  ||
    | MUser_ID|[Int64][int64] |编辑者  ||
    | tableCode|[String][string] |来源表  ||

  - **返回**

   > 

 --- 
## GetSort

 方法:  BLL.TreeBLL.GetSort

### `获取树父系节点子类最大排序号`


> GetSort([Int64][int64]  Current_ID,[Int32][int32]  Depth,[String][string]  TreeGuid)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | Current_ID|[Int64][int64] |子节点  ||
    | Depth|[Int32][int32] |  ||
    | TreeGuid|[String][string] |  ||

  - **返回**

   > 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[list]:https://docs.microsoft.com/zh-cn/dotnet/api/system.collections.generic.list
[qf_treemenumodel]:../Quick.Frame/Model_QF_TreeMenuModel.md
[qf_classtreecompent]:../Quick.Frame/Model_QF_ClassTreeCompent.md
[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64
[qf_classtreeapimodel]:../Quick.Frame/Model_QF_ClassTreeApiModel.md


