---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:30

---
# Model.QF_ClassTreeApiModel
- 引用：Quick.Frame.Model.QF_ClassTreeApiModel


## Model.QF_ClassTreeApiModel  `树添加节点对象`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | ID|  | 当前树节点ID |
 | App_ID|  | 应用ID |
 | ClassCode|  | 节点类别分组编码 |
 | Parent_ID|  | 新增树的父节点ID |
 | TreeGuid|  | 父节点关系 |
 | Depth|  | 父节点层次 |
 | Sort|  | 父节点排序 |
 | CurrentList|  | 新增多个子节点数组字符串              ID: &#39;子项描述Title &#39;, |



