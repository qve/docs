---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:30

---
# BLL.RoleRuleBLL
- 引用：Quick.Frame.BLL.RoleRuleBLL


## BLL.RoleRuleBLL  `角色与权限规则方法`



 --- 
## GetRoleRulesByID

 方法:  BLL.RoleRuleBLL.GetRoleRulesByID

### `查询角色与权限规则配置`


> GetRoleRulesByID([Int64][int64]  _rulesId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _rulesId|[Int64][int64] |角色组ID  ||

  - **返回**

   > 



[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64


