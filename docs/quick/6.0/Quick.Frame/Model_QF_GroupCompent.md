---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:30

---
# Model.QF_GroupCompent
- 引用：Quick.Frame.Model.QF_GroupCompent


## Model.QF_GroupCompent  `QF_Group,对象(组织)              T:2021/3/8 19:04:42`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | ID|  | ID |
 | QF_Location_City_ID|  | 所属城市 |
 | QF_Location_Area_ID|  | 所属区分 |
 | AreaCode|  | 地市编码 |
 | QF_Location_ID|  | 所属位置 |
 | SaleType|  | 销售模式\ &quot;ls\ &quot;:\ &quot;1:待定,2:个人付费,3:企业包月\ &quot; |
 | SaleLevel|  | 运营等级\ &quot;tip\ &quot;:\ &quot;单点营收划分\ &quot;,\ &quot;ls\ &quot;:\ &quot;1:未分,2:铁,3:铜,4:银,5:金,6:白金,7:钻石\ &quot; |
 | SaleMode|  | 运营模式\ &quot;ls\ &quot;:\ &quot;1:待定,2:直营,3:合营,5:代理\ &quot; |
 | Partner_Group_ID|  | 合作方ID |
 | OnState|  | 组织状态\ &quot;ls\ &quot;:\ &quot;停用:启用\ &quot; |
 | OnTime|  | 状态更新 |
 | OnNas|  | 是否运营\ &quot;ls\ &quot;:\ &quot;停止:正常\ &quot; |
 | APStart|  | 合约开始 |
 | APEnd|  | 合约结束 |
 | StopCard|  | 会员卡上线 |
 | Code|  | 组织编码 |
 | NameCn|  | 中文全称 |
 | NameEN|  | 英文全称 |
 | NameShort|  | 简称\ &quot;d\ &quot;:1 |
 | license|  | 营业执照号 |
 | licenseDate|  | 执照注册 |
 | NatureType|  | 组织性质\ &quot;ls\ &quot;:\ &quot;1:私企,2:国企,3:合资,4:其它,5:外资\ &quot; |
 | UserInt|  | 人数规模 |
 | BuyTitle|  | 主营\ &quot;tip\ &quot;:\ &quot;多种产品用,号隔开\ &quot; |
 | Phone|  | 主要电话 |
 | Fax|  | 主要传真 |
 | ZipCode|  | 邮政编码 |
 | Address|  | 组织地址 |
 | GPS|  | GPS定位 |
 | SayShort|  | 组织简介 |
 | SaySource|  | 来源说明 |
 | WebSite|  | 网站 |
 | WeiXin|  | 微信公众号 |
 | Email|  | 电子邮件 |
 | Remark|  | 备注 |
 | SF_ID|  | 审核\ &quot;r\ &quot;:1 |
 | MUser_ID|  | 编辑者\ &quot;r\ &quot;:1 |
 | U_Time|  | 编辑\ &quot;r\ &quot;:1 |
 | Flags|  | 标记 |
 | Persent|  | Persent |
 | dx|  | 电信 |
- **构造函数**

  - Model.QF_GroupCompent()
       `初始化设置默认值构造`



 --- 
## ToString

 方法:  Model.QF_GroupCompent.ToString

### `字段描述Json{field:字段,attr:属性}`


> ToString()






