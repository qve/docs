---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:30

---
# Model.QF_ClassTreeCompent
- 引用：Quick.Frame.Model.QF_ClassTreeCompent


## Model.QF_ClassTreeCompent  `QF_ClassTree,对象(QF_ClassTree)              T:2021/5/17 15:01:48`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | ID|  | ID |
 | App_ID|  | 应用ID |
 | Current_ID|  | 子ID |
 | Parent_ID|  | 父ID |
 | TreeGuid|  | 父节点关系 |
 | Depth|  | 层次 |
 | Sort|  | 序 |
 | HasChild|  | 有子节点\ &quot;ls\ &quot;:\ &quot;无:有\ &quot; |
 | ClassCode|  | 分组编码 |
 | NodeCss|  | 节点样式 |
 | Title|  | 描述\ &quot;d\ &quot;:1 |
 | Remark|  | 备注 |
 | SF_ID|  | 审核\ &quot;r\ &quot;:1 |
 | MUser_ID|  | 编辑者\ &quot;r\ &quot;:1 |
 | U_Time|  | 编辑时间\ &quot;r\ &quot;:1 |
 | CMT|  | QF_Class实体 |
- **构造函数**

  - Model.QF_ClassTreeCompent()
       `初始化设置默认值构造`



 --- 
## ToString

 方法:  Model.QF_ClassTreeCompent.ToString

### `字段描述Json{field:字段,attr:属性}`


> ToString()






