---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:30

---
# Model.TreeNodeModel
- 引用：Quick.Frame.Model.TreeNodeModel


## Model.TreeNodeModel  `树节点对象`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | id|  | 节点勾选绑定值 |
 | title|  | 节点描述 |
 | icon|  | 节点Icon |
 | children|  | 子节点 |
 | node|  | 当前节点 附加对象数据 |



