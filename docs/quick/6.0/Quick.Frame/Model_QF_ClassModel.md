---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:30

---
# Model.QF_ClassModel
- 引用：Quick.Frame.Model.QF_ClassModel


## Model.QF_ClassModel  `架构树分组`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | ID|  | 分组ID |
 | TableName|  | 表名 |
 | FieldName|  | 字段名 |
 | NodeCss|  | 节点样式 |
 | Api|  | 数据接口 |



