---
template: 2021.12
version: 2021/12/18 15:30:05
author: quick.docs
date: 2021年12月31日 15:30

---
# Model.QF_ModelCompent
- 引用：Quick.Frame.Model.QF_ModelCompent
- version: 2021/12/18 15:30:05


## Model.QF_ModelCompent  `QF_Model数据对象配置`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | AppKey|  | 所属应用 |
 | Description|  | 功能说明 |
 | DBServer|  | 所属服务器名 |
- **构造函数**

  - Model.QF_ModelCompent()
       `默认值构造`



 --- 
## ToString

 方法:  Model.QF_ModelCompent.ToString

### `字段描述Json`


> ToString()


  - **备注**

      {field:字段,attr:属性}





