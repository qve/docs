---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:30
---

# Quick.Frame

template: 2021.12
author: quick.docs
date: 2021 年 12 月 31 日 15:30

## Quick.Frame 目录

### [BLL.QF_ModelBLL](./BLL_QF_ModelBLL.md)

      数据对象配置业务处理类

### [BLL.QF_PageBLL](./BLL_QF_PageBLL.md)

      架构页面方法

### [BLL.RoleRuleBLL](./BLL_RoleRuleBLL.md)

      角色与权限规则方法

### [BLL.TreeBLL](./BLL_TreeBLL.md)

      架构树配置方法

### [Model.QF_ClassCompent](./Model_QF_ClassCompent.md)

      QF_Class,对象(QF_Class)
             T:2021/5/17 15:01:48

### [Model.QF_ClassModel](./Model_QF_ClassModel.md)

      架构树分组

### [Model.QF_ClassTreeApiModel](./Model_QF_ClassTreeApiModel.md)

      树添加节点对象

### [Model.QF_ClassTreeCompent](./Model_QF_ClassTreeCompent.md)

      QF_ClassTree,对象(QF_ClassTree)
             T:2021/5/17 15:01:48

### [Model.QF_GroupCompent](./Model_QF_GroupCompent.md)

      QF_Group,对象(组织)
             T:2021/3/8 19:04:42

### [Model.QF_ModelCompent](./Model_QF_ModelCompent.md)

      QF_Model数据对象配置

### [Model.QF_TableCompent](./Model_QF_TableCompent.md)

      QF_Table,对象(万能格)
             T:2021/1/25 14:28:14

### [Model.QF_TreeMenuModel](./Model_QF_TreeMenuModel.md)

      菜单对象

### [Model.TreeNodeModel](./Model_TreeNodeModel.md)

      树节点对象
