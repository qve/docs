---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:30

---
# Model.QF_TableCompent
- 引用：Quick.Frame.Model.QF_TableCompent


## Model.QF_TableCompent  `QF_Table,对象(万能格)              T:2021/1/25 14:28:14`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | ID|  | ID |
 | TableID|  | 表格 |
 | field|  | 字段 |
 | title|  | 描述 |
 | attr|  | 规则 |
 | value|  | 默认值 |
 | act|  | 权限 |
 | Remark|  | 备注 |
 | SF_ID|  | 数据状态 |
 | MUser_ID|  | 编辑者 |
 | U_Time|  | 编辑时间 |
- **构造函数**

  - Model.QF_TableCompent()
       `初始化设置默认值构造`



 --- 
## ToString

 方法:  Model.QF_TableCompent.ToString

### `字段描述Json{field:字段,attr:属性}`


> ToString()






