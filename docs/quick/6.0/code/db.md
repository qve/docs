# Quick.DB

数据库交互封装工具库

## ExecuteByProc 存储过程

```csharp
QueryParam qp=new QueryParam();
            qp.SParas = new SqlParam();
            qp.SParas.Add("@AP_Login_ID", AP_Login_ID);
            qp.SParas.Add("@AP_NAS_ID", AP_NAS_ID);
            qp.SParas.Add("@AP_Info_ID", AP_Info_ID);
            qp.SParas.Add("@AP_User_ID", AP_User_ID);
            qp.SParas.Add("@BrowserInfo", BrowserInfo);
            qp.SParas.Add("@rCode", 0, ParamType.Output); //返回状态 0:失败,1:更新了登录信息,2:更新了会员信息
            qp.SPName = "test";
            qp = QuickData.ExecuteByProc(qp);

            return qp.SParas.Get<Int16>("@rCode");
```

## QuickServerBLL 数据服务器

### 取出数据库连接 GetConnectByDBCode

`QF_Server`,`QuickSettings.DB.MSSQL` 命名必须保持一致

- `6.0.1` 取出顺序
  1. 从缓存取出连接`QuickCache` `db:sql:connt:{0}`
  2. 从配置取出 `QuickSetting:DB:MSSQL:`
  3. 从架构服务器配置中取出 `QuickServerBLL` `GetFieldByDBName`
     无配置报错：`Error:404," + dbCode + ":is null QuickData.Connection`
     若取到缓存到`QuickCache`

```csharp
// 根据数据库命名取出连接字符串
string connStr = QuickData.GetConnectByDBCode(model.dbCode);
```

### GetFieldByDBName 取出服务器连接配置

- `6.0.1` 新增

## 批量插入对象

```json
{
  "cmd": "add",
  // 插入的字段，注意顺序一致
  "par": "OA_Work_ID,Action,Title,Action_User_ID",
  // 多个对象
  "obj": [
    {
      "OA_Work_ID": 12,
      "Action": 8,
      "Title": "王1",
      "Action_User_ID": 390
    },
    {
      "OA_Work_ID": 12,
      "Action": 8,
      "Title": "王2",
      "Action_User_ID": 554
    }
  ]
}
```

## 数据库事务

- [MSSQL 事务介绍](https://www.cnblogs.com/xinysu/p/7860605.html)

同时插入两张表的业务逻辑，将 transaction 作为回调函数的参数，业务逻辑部分直接将 transaction 塞入到各自的业务代码中即可

```csharp

// 执行
return Open(qp.DBName, (conn, trans) =>
{
    int execnum = conn.Execute("insert into xxx ", transaction: trans);
    if (execnum == 0) return 0;
    int execnum2 = conn.Execute("update xxx set xxx", transaction: trans);
    if (execnum2 > 0) trans.Commit();
    return 1;
});

```

## Dapper 事务

```csharp
return Open(qp.DBName, (conn, trans) =>
{

       int res = 0;
       List<string> listsql = new List<string>();

       listsql.Add("update xxx set xxx");
       listsql.Add("insert into xxx");
       IDbTransaction transaction = conn.BeginTransaction();
       try
       {
         for (int j = 0; j < listsql.Count; j++)
         {
         res = conn.Execute(listsql[j], null, transaction);
         }
         transaction.Commit();
         return res;
       }
       catch (Exception exception)
       {
          transaction.Rollback();
          return res;
       }

});
```
