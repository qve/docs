# QuickSafe

> Api 请求的安全认证库，缓存 Redis

- [官方文档 ActionFilterAttribute](https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/filters)

- [Quick.DB](./db.md) 数据库组件，获取数据连接配置

## 全局权限认证

> 全局权限认证默认核验全部,token,user,role,cmd 权限

- [QuickSafeFilterModel](../Quick.Safe/QuickSafeFilterModel.md) 请求参数与验证用户身份信息
- [QuickSafeFilter](../Quick.Safe/QuickSafeFilter.md) 验证拦截方法
