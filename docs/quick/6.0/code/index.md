# Quick 组件库

## appsettings.json

项目配置,注意数据库加密方式

```json
{
  "QuickSettings": {
    "App": {
      // 应用Id
      "Id": "",
      // 当前应用
      "Key": "",
      // 应用密码
      "Secret": ""
    },
    "Log": {
      //日志存储的路径
      "Path": "\\Log\\",
      // 是否写入
      "IsWrite": true,
      // 是否缓存后写入
      "CacheMaxInt": 0,
      // 超过800kb行就备份
      "CacheMaxBack": 800000
    },
    "DB": {
      //加密方式1:明文,2:DES
      "PassType": 1,
      // 密钥
      "PassKey": "",
      "Redis": {
        // 小程序服务器
        "Lite": {
          "Host": "",
          // 缓存数据库
          "DBId": 12,
          // 消息规则
          "MsgKey": "wm:{0}:{1}"
        }
      },
      "MSSQL": {
        //基础架构数据库连接
        "Quick": ""
      }
    },
    "Session": {
      // 跨域
      "Cors": {
        // 授权域名
        "Origins": [
          "http://localhost:8080",
          "http://localhost:3000",
          "http://localhost:5000"
        ]
      },
      // 客户端 cookie 名称
      "Cookie": {
        "Name": "AP_UD",
        //超时(分钟数)
        "Timeout": 4320
      },
      "Cache": {
        //会话超时(分钟数，默认3天)
        "Expire": 4320,
        // 票据过期刷新有效期：30天
        "ExpireRefresh": 43200
      }
    },
    //企业微信
    "WeWork": {
      //是否加密
      "Pass": false,
      //企业ID
      "Corpid": "",
      //消息服务器配置
      "MsgBase": {
        "Token": "",
        "EncodingAESKey": ""
      },
      "APWork": {
        "AgentId": "",
        "Secret": ""
      },
      // 考勤应用
      "CheckWork": {
        "AgentId": "",
        "Secret": ""
      }
    }
  }
}
```

### QuickSettingsManager 配置

需要从 Nuget 安装并引用

- Microsoft.Extensions.Configuration
- Microsoft.Extensions.Configuration.Json
- Microsoft.Extensions.Configuration.Binder

## aliyun sms 短信

- SDK: AlibabaCloud.SDK.Dysmsapi20170525

## Minimal API MVC

- [文档演示](https://www.cnblogs.com/xiyuanMore/p/15422874.html)

在某些情况下，您可能只需要 MVC 框架的特定功能或具有使 MVC 不受欢迎的性能限制。随着更多 HTTP 功能作为 ASP.NET Core 中间件（例如身份验证、授权、路由等）出现，无需 MVC 即可构建轻量级 HTTP 应用程序变得更加容易，但通常需要一些功能，否则您必须自己构建，例如作为模型绑定和 HTTP 响应生成。

ASP.NET Core 6.0 旨在通过 Minimal API 弥合这一差距，以更少的仪式提供 ASP.NET MVC 的许多功能。如何将传统 MVC 概念转换为这种构建轻量级 HTTP API 和服务的新方法的分步指南。

- 启动配置示例 Minimal API：

该 `MapGet` 方法是 `Minimal API` 扩展的一部分。除此之外，它与 MVC 并没有太大区别（考虑到 HTTPS 重定向和授权中间件只是从 `Empty` 模板中省略而不是隐式启用）

```csharp
var builder = WebApplication.CreateBuilder(args);

// MVC 依赖注入缓存
builder.Services.AddScoped<ICache, MemoryCache>();


var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

// Minimal API 不是使用构造函数注入，而是在处理程序委托中将依赖作为参数传递
app.MapDelete("/cache/{id}", async (string id, ICache cache) =>
{
    await cache.Delete(id);
    return Results.Accepted();
});

//Minimal API 输出 hello world
app.MapGet("/", () => "Hello World!");

// Minimal API 中的模型绑定非常相似
app.MapPost("/payments", (PaymentRequest paymentRequest) =>
{

});

app.MapGet("/portfolios/{id}", (int id, int? page, int? pageSize) =>
{

});

// 标准MVC模式
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();

```

- 使用示例 Minimal API：

```csharp
public class RootController
{
    [HttpGet("/")]
    public string Hello() => "Hello World";
}
```

### 依赖注入

- MVC 方法

```csharp
public class CacheController : ControllerBase
{
    private readonly ICache _cache;

    public CacheController(ICache cache)
    {
        _cache = cache;
    }

    [HttpDelete("/cache/{id}")]
    public async Task<IActionResult> Delete(string id)
    {
        await _cache.Delete(id);
        return Accepted();
    }
}

```

## cross 跨域配置

- [vue 跨域配置](../../../guide/vue/index.md)
- [NET 跨域](https://docs.microsoft.com/zh-cn/aspnet/core/security/cors?view=aspnetcore-6.0)
