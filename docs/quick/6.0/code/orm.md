# ORM

数据库通用操作封装方法

以下为架构`ORM`配置参数表

- `QF_Model` 数据库对象表
- `QF_API` 应用接口

## 审核编辑字段默认命名

- 架构自带数据权限核验机制与编辑更新机制。
- 如需使用请在数据表中默认添加以下默认字段，或者自定义字段名配置标记

  | 字段名   | 类型     | 说明                   | 默认值    |
  | -------- | -------- | ---------------------- | --------- |
  | ID       | Int64    | `primary` 主键索引     | 自增长    |
  | SF_ID    | int      | `auth` 数据核验状态    | 1         |
  | MUser_ID | Int64    | `user` 编辑操作用户 ID | 1         |
  | U_Time   | datetime | `time` 编辑时间        | getdate() |

- AttrListor 可配置自定义列表参数

```json
{
  // 自定义默认字段名
  "keys": {
    "primary": "ID",
    "auth": "SF_ID",
    "user": "MUser_ID",
    "time": "U_Time"
  }
}
```

## Reqs 请求增删改查规则

配置动态 ORM 查询、编辑、新增的请求参数条件

### where 结构化查询

多条件查询配置，并且使用动态参数替换

- [JsonBind](./json.md#jsonbind) `param` 参数值替换
- `QueryMode` 查询条件符号
- `QuickDAL.GetQuerySql(qp)` 生成查询语句

- 查询条件参数 [WhereItem](../Quick.DB/WhereItem.md)

  | 参数名          | 说明             | 使用                                                         |
  | --------------- | ---------------- | ------------------------------------------------------------ |
  | key             | 字段名           |                                                              |
  | value           | 字段值           |                                                              |
  | [param](#param) | 动态映射规则参数 | 类型`TPLSQLSelectModel`,`JsonElement`,`String`               |
  | mode            | 查询条件         | 无配置默认为等于，对应 [QueryMode](../Quick.DB/QueryMode.md) |
  | left            | 放置左边条件     | 例如优先条件 `(`                                             |
  | right           | 放置左边条件     | 例如优先条件结束 `)`                                         |

### param

动态映射参数，约定变量命名，值按此规范填写`{变量}`,或者 `json` 对象

| 参数内容示例              | 解析类型    | 说明                                                                                      | 参数 |
| ------------------------- | ----------- | ----------------------------------------------------------------------------------------- | ---- |
| `{safe.TokenUser.UserID}` | JsonElement | 取出 safe 对象的令牌用 ID [QuickSafeFilterModel](../Quick.Safe/QuickSafeFilterModel.md)   |      |
| `{pi.ID}`                 | JsonElement | 传入查询参数对象 [PagerInString](../Quick.DB/PagerInString.md)                            |      |
| `{pc.ID}`                 | JsonElement | 执行传入参数对象 [PageCmd](../Quick.DB/PageCmd.md)                                        |      |
| `{t.ID}`                  | JsonElement | t 主表,字段 ID                                                                            |
| `{getdate()}`             | String      | mssql 当前时间                                                                            |      |
| `select ID form Test`     | String      | sql 自定义查询语句，配合`mode`条件`in`，慎用有注入风险，建议用`TPLSQLSelectModel`         |      |
| TPLSQLSelectModel         | Object      | 查询外连表对象 [动态映射表查询 TPLSQLSelectModel](../Quick.DB/Model_TPLSQLSelectModel.md) |      |

- 示例

```json
{
  // 单条查询
  "query": {
    "where": [
      {
        "left": "(",
        "key": "QF_User_ID",
        "param": "{safe.TokenUser.UserID}"
      },
      {
        //或者
        "left": "or",
        "key": "DutyUser_ID",
        "param": "{safe.TokenUser.UserID}"
      },
      {
        // 或者
        "left": "or",
        "key": "ID",
        "mode": "in",
        // 继续配置查询表
        "param": {
          "field": "OA_Work_ID",
          "table": "OA_WorkAction",
          "where": [
            {
              "key": "Action",
              "value": 8
            },
            {
              "key": "Action_User_ID",
              "param": "{safe.TokenUser.UserID}"
            }
          ]
        },
        "right": ")"
      }
    ]
  },
  // 多条数据查询
  "list": {
    "where": [
      {
        "key": "start",
        "value": 5,
        "mode": "<>"
      },
      {
        "key": "QF_User_ID",
        "param": "{safe.TokenUser.UserID}" // 从验证参数取出用户ID
        // "mode": "<>" //不填默认为等于
      }
    ]
  }
}
```

### outer 连表查询

- [sql 子表查询语句](../../../guide/db/mssql.md)

- Reqs 请求查询配置

```json
{
  // 多数据查询
  "list": {
    // 子表查询参数
    "outer": {
      // 查询外连表输出别名
      "b": {
        "size": 1,
        "field": "UserName,Body",
        "table": "OA_WorkUP",
        "where": [
          {
            "key": "OA_WorkItem_ID",
            "param": "{t.ID}"
          }
        ],
        "sort": "ID",
        "desc": 1
      }
    }
  }
}
```

### 增改绑定值

- QuickORMBLL.Edit

```json
{
  // 新增绑定字段
  "add": {
    "bind": { "QF_User_ID": "{safe.TokenUser.UserID}" }
  },
  // 编辑绑定字段
  "edit": {
    "bind": { "EditTime": "{getdate()}" } // sql参数默认当前时间
  }
}
```

### 数据事务

- 支持同一数据库下事务操作，执行事务动作
- trans 事务指令， 插入：`insert`,修改：`update`,删除：`delete`
- [QueryParam 查询事务](./qp.md)
- [db 数据库事务](./db.md)

```json
{
  "edit": {
    // 绑定指定值
    "bind": { "EditTime": "{getdate()}" },
    // 定义编辑执行的附加的多个数据库事务
    "trans": [
      {
        "table": "OA_WorkAction",
        "insert": {
          "QF_AppInfo_ID": "{safe.appID}",
          "OA_Work_ID": "{pc.id}",
          "Title": "事务内容变更",
          "Action": 6,
          "Action_User_ID": "{safe.TokenUser.UserID}"
        }
      }
    ]
  }
}
```

## Join 外键查询配置

- BindAttrJoin 转换为外键查询条件
- QuickDAL.SqlLink 跨服务器数据库表名正则`sql`

```json
{
  "MUser_ID": {
    "table": "QF_User",
    "fields": "LastFirst"
    // "primary": "ID", //不配置，默认为ID
    // "server": "Quick" // 默认不配置， 当前表是否与外键表不同服务器
  },
  "QF_Book_ID": { "table": "QF_Book", "fields": "NameCn" },
  "QF_Group_ID": { "table": "QF_Group", "fields": "NameShort" }
}
```

## Attr 前端参数

## AttrListor 前端查询

### select 查询配置

注意此处配置仅用于前端绑定，后台接口不做查询验证。
如需严格后端验证，请使用`Reqs.list`

- lock 锁定前端查询参数
- where 初始默认参数

```csharp
{
  "select": {
    "lock": [
      "QF_Group_ID"
    ],
    "where": [
      {
        "QF_Group_ID": 1,
        "q": "<"
      }
    ]
  }
}
```

## AttrResp 字段正则隐藏脱敏输出

如需要对输出数据，进行脱敏处理，可以定义字段显示的正则方式。

- 数据对象`QF_Model`和应用接口 `QF_API`都支持
- `AttrResp` 定义字段需要显示的正则，查询结果进行替换处理。

```json
{
  "Phone": null, //为空 默认影响手机号
  "NickName": {
    // 保留姓隐藏名
    "reg": "(?<=.).", // 正则参数
    "tag": "*" // 替换的标签参数
  }
}
```

- [BindAttrResp](../Quick.DB/BLL_QuickORMSqlBLL.md#bindattrresp)
  调用 `QuickBLL.ToHide` 参数进行输出处理

```csharp

/// <summary>
/// 查询 Json 数组列表,qp查询条件
/// </summary>
/// <param name="code">数据对象索引</param>
/// <returns>Json 数组</returns>
/// <remarks>
/// 按输出规则配置替换内容
/// </remarks>
public dynamic List(string code)
{

    dynamic _list = QuickData.List(qp);
    // 取出数据表属性
    TPLAttrModel tam = GetAttrJsonCache(code);
    if (string.IsNullOrWhiteSpace(tam.AttrResp))
    {
      return _list;
    }
    else
    {
      return BindAttrResp(_list, tam.AttrResp);
    }
}
```

## DapperRow Object 数据正则输出

使用`IDictionary<string, object>` 转换`object`数据，查看跟踪可以看到是键对值的方式

```csharp

// 替换正则
string _attrResp="{\"Phone\":null,\"NickName\":{\"reg\":\"(?<=.).\",\"tag\":\"*\"}}"
// 转换为json对象
dynamic obj= JsonHelper.ParseElement(_attrResp);

// 循环取出数组列表
foreach (var row in QuickData.List(qp))
{
    //  转换object数据
   var item = (IDictionary<string, object>)row;

   // 循环取出需要替换的字段正则
   foreach (var field in jq.EnumerateObject())
   {
     //  Console.WriteLine(field.Name);
        string _value = null;
        if (item[field.Name] != null)
        {
            if (field.Value.ToString() != "")
            {
                // 自定义正则参数
                _value = QuickBLL.ToHide(item[field.Name].ToString(), @jq[field.Name]["reg"].ToString(), jq[field.Name]["tag"].ToString());
            }
            else{
                 // 正则脱敏隐藏手机号
                 _value = QuickBLL.ToHide(item[field.Name].ToString());
            }
        }

        //  Console.WriteLine(_value);
        item[field.Name] = _value;
   }
}

```
