# QuickBLL

## DES 加密与解密

- [TripleDES](https://docs.microsoft.com/zh-cn/dotnet/api/system.security.cryptography.tripledes.create?view=net-6.0)

## Encrypt

- 微信 Net 4.8 版本 Url 加密传入方法与 DB 数据加密传入方法有差异
- DESCryptoServiceProvider 在 Net 6 已弃用，当前可以使用。

## ToHide 正则隐藏内容

- 参数

  | 名称 | 类型   | 说明                                    | 默认值              |
  | ---- | ------ | --------------------------------------- | ------------------- |
  | str  | string | 源内容                                  |                     |
  | reg  | string | 正则，默认手机号分组                    | `@"(.{3}).*(.{4})"` |
  | tag  | string | 替换标签，默认保留 1,2 组,其它`*`号代替 | `$1****$2`          |

```csharp

// 默认为手机号隐藏正则
QuickBLL.ToHide("18911111188")

// 隐藏姓名,姓隐藏，返回 *王
QuickBLL.ToHide("李王大",@"^(.).+(.)$","*$1$2")

// 返回 李**
QuickBLL.ToHide("李王大", @"(?<=.).", "*")
```

- [js 正则文档](https://qve.gitee.io/docs/docs/lib/bll.html)

```js
return {
  name3: '李王大的'.replace(/(?<=.)./g, '*'), //保留姓
  name: '李王大'.replace(/^(.).+$/, '*$1$2') //隐藏姓
};
```

## 取值正则

- js 正则通用语法

```csharp

string _str="{safe.TokenUser.UserName}:事务内容变更";
// 正则取出 {safe.TokenUser.UserName}
string _value=QuickBLL.Match(_keys, @"\{(.*?)}").Value;
Console.WriteLine(_value)

```
