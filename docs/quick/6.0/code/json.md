# Json

`JSON (JavaScript Object Notation)` 是一种轻量级的数据交换格式。它是基于 `ECMAScript` 的一个子集。采用完全独立于语言的文本格式，但是也使用了类似于 C 语言家族的习惯（包括 C、C++、C#、Java、JavaScript、Perl、Python 等）。这些特性使它成为理想的数据交换语言。 易于人阅读和编写，同时也易于机器解析和生成(一般用于网络传输)。

## JsonElement ValueKind obj 传参

```csharp
JsonElement obj = jsonDocument.RootElement;

// 取出JSON 对象的属性值
int AP_Group_ID = obj.GetProperty("levelTo").GetInt16();
long CRMID= obj.GetProperty("crmID").GetInt64();

if (obj.TryGetProperty("Grade", out var grade))
{
  Console.WriteLine(grade.GetDouble());
}
```

## JsonQuick js 语法

- 操作 Json 字符串，一般都是先定义一个和 Json 格式相匹配的类，然后将 json 字符串反序列化成对象，这样便于编程使用，由于`.NET`是强类型语言，Json 是多变的，非常灵活的，导致服务器端定义的 json 序列化类越来越多，操作不便。

- JsonQuick 接近 `js` 语法读写 `Json`, 优点是读取时直接调用高效的原生方法 `System.Text.Json`,编辑时缓存到`KV`字典`IDictionary<string, dynamic> ,ExpandoObject`，支持`Core Net 5 Net 6`

- DynamicJson 是专门为.NET 程序员开发的 Json 操作库，其源码非常简单，仅仅只有 400 行代码，一个对应的`class`类,目前只支持`.NET 4.0`以上的`.NET Framework`。

```csharp
// 字符串转为 Json 对象
JsonQuick jq = new JsonQuick("{\"test\":{\"id\":1},\"ed\":2,\"arr\":[{\"index\":1,\"add\":2},{\"itemAdd\":20}]}");

// 取出数组第一项
Console.WriteLine(jq["arr"][0]);
int ed=jq["ed"]; //可以和javascript一样通过索引器获取

// 编辑字典
jq["ed"] = 28;
jq["test"]["id"] = 52;

// 取出ed
Console.WriteLine(jq["ed"]);

// 构造新的json 方式一
jq["test"]["us"] = new JsonQuick("[{\"t\":1},{\"t\":2},{\"t\":3}]").element;
// 构造创建对象方式二
jq["ts"] = JsonHelper.ParseElement("[{\"t\":1},{\"t\":2},{\"t\":3}]");

// 更新值方式 三 (字典名,对象)
jq.Update("up",3);

// 删除对象
jq.Remove("ed");

// 将编辑字典绑定更新当前对象
JsonElement o= jq.Bind();

// 当前源字符串
jq.source;
// 直接序列化输出json字符串
var jsonstring = jq.ToString();

// 已有Json对象赋值构造方式一
JsonQuick jq2 = new JsonQuick(o);

// Json对象转为KV字典，
// 发送编辑时，默认触发此方法初始化编辑字典
jq2.BindDict()

// 将字典序列化字符串，Bind调用，不会更新source
jq2.Serialize();


StringBuilder _control = new StringBuilder();

// 取出js数组, 第一个对象, 循环取出值
foreach (var pp in jq["arr"][0].EnumerateObject())
{
   if (pp.TryGetProperty("index", out var value))
   {
       Console.WriteLine(Value);
   }else{
       Console.WriteLine(pp.Name,pp.Value.GetString());
   }
}

```

## JsonHelper

json 序列化方法

```csharp

// json转换为对象
dynamic obj=JsonHelper.Deserialize<PagerTplModel>("{\"field\":\"test\"}");

// 序列化为字符串
JsonHelper.Serialize(obj);
// 原生方法
JsonSerializer.Serialize(dict);

// 转为数组
string arr="[{\"id\":1},{\"id\":2,\"q\":\">\"},{\"id\":30,\"q\":\"<\"}]";
// 查询参数
List<WhereItem> lo = new List<WhereItem>();
foreach (JsonElement je in JsonHelper.ParseArray(_where))
{

    WhereItem pw = new WhereItem();
    pw.Mode = "=";
    // 循环取出json 元素
    foreach (var pp in je.EnumerateObject())
    {
        pw.Field = pp.Name;
        pw.Value = pp.Value;
    }

    // 判断是否有元素，并取出值
    if (je.TryGetProperty("q", out var mode))
    {
        pw.Mode = mode.ToString();
    }

    lo.Add(pw);
}
```

## JsonBind 格式化正则绑定

支持`json`对象进行动态变量字符串正则绑定替换。

- 支持 {name} 这种替换方式。
- 支持替换 vue 的模板替换：{{name}}
- [string.format 格式化](https://docs.microsoft.com/zh-cn/dotnet/api/system.string.format)

```csharp

// object[] args = Array.Empty<string>();
// string.Format(_sql, args);

// 绑定sql语句
string _sql = "select * from QF_Rules where ID={id}";
// 需要替换id 值为 123
string _pars="{\"id\":123}";

// 方式1 初始构造替换源对象json
JsonBind jb = new JsonBind(_pars);
string _sql=jb.Bind(_sql);
Console.WriteLine(_sql);

// 方式2 自定义构造替换字典
JsonBind jb = new JsonBind();

JsonQuick jqPars = new JsonQuick(_pars);
foreach (var item in jqPars.EnumerateObject())
{
  Console.WriteLine(item.Name, item.Value);
  // 字典缓存需要正则替换的值
  jb.SetValue(item.Name, item.Value);
}

// 绑定需替换的字符串
string _sql=jb.Bind(_sql);
Console.WriteLine(_sql);
```

## JsonValueKind 新增值与类型

- [指定 JSON 值的数据类型](https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonvaluekind?view=net-6.0)

```csharp

Dictionary<string, object> kvs = new();
kvs["Test"]=new KeyValuePair<JsonValueKind,string>(JsonValueKind.String,string.Format("{0}", "我是sting值"));

public enum JsonValueKind : byte
{
    Undefined = 0,
    Object = 1,
    Array = 2,
    String = 3,
    Number = 4,
    True = 5,
    False = 6,
    Null = 7
}
```

## json 创建

```csharp
string json = string.Empty;
using (MemoryStream ms = new MemoryStream())
{
    using (Utf8JsonWriter writer = new Utf8JsonWriter(ms))
    {
        writer.WriteStartObject();
        writer.WriteString("Name", "Ron");
        writer.WriteNumber("Money", 4.5);
        writer.WriteNumber("Age", 30);
        writer.WriteEndObject();
        writer.Flush();
    }
    json = Encoding.UTF8.GetString(ms.ToArray());
}
```
