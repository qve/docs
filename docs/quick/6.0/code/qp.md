# QueryParam

- 查询条件参数[TPLSQLSelectModel](../Quick.DB/Model_TPLSQLSelectModel.md)

## QueryParam 查询表

- 建议使用结构化查询便于适配各类型数据库

```csharp
QueryParam qp = new(QuickData.DBQuickPlus);
qp.TableName = "OA_Test";
qp.Wheres = new List<WhereItem>();
// 查询ID等于123
qp.Wheres.Add(new("ID",123));
// 查询 in(166,178,179,180)
qp.Wheres.Add(new(){
      field = "QF_Book_ID",
      param= "166,178,179,180", //传入参数
      mode ="in"
});

// 以上为结构化查询
return QuickData.Query<OA_TestModel>(qp);
```

- 使用自定义 Sql 语句，不建议有注入风险

```csharp
QueryParam qp = new(QuickData.DBQuickPlus);
qp.sql = string.Format("select * from OA_Test where  ID={0}", id);
return QuickData.Query<OA_TestModel>(qp);
```

## WhereItem 查询条件

- TPLSQLSelectModel 表查询参数

```csharp
// 自查询条件
List<WhereItem> _inWhere = new List<WhereItem>();
_inWhere.Add(new()
{
    field = "ActUser_ID",
    value = safeFilter.TokenUser.UserID,
});

// 主查询
QueryParam qp = new(QuickData.DBQuickPlus);
qp.TableName = "OA_Test";
qp.Wheres = new List<WhereItem>();

qp.Wheres.Add(new("ID", "in", new TPLSQLSelectModel(){
    table = "OA_WorkUP",
    field = "OA_Work_ID",
    where= _inWhere,
    group= "OA_Work_ID"
 }));

// 等同于自定义sql 语句，以下书写注意注入风险
// qp.Wheres.Add(new ("ID", string.Format("select OA_Work_ID from OA_WorkUP where ActUser_ID={0} group by OA_Work_ID", safeFilter.TokenUser.UserID), "in"));
```

## Action 更新数据

- 结构化更新数据

```csharp
QueryParam qp = new QueryParam("数据库连接命名");
qp.TableName ="TableCode";// 编辑的数据表
qp.Action = 2; //执行编辑动作

// 编辑条件
qp.Wheres = new List<WhereItem>();
// 批量编辑的ID
qp.Wheres.Add(new WhereItem(){
                    field = "ID",
                    param = _ids,
                    mode = "in"
                });
// 编辑条件
qp.Wheres.Add(new(tm.auth, 2, "<>")); //非审核
qp.Wheres.Add(new(tm.editUser, MUser_ID)); //仅修改自己的数据

// 需更新的动态键值内容
Dictionary<string, object> dc = new();
dc.Add(tm.auth, 4);
dc.Add(tm.editTime, DateTime.Now);

// 也可以使用 JsonQuick 构造Json对象，或传入对象
qp.ORM = dc;
// 执行返回结果
Int64 rint = QuickData.Action(qp);
```

## Transaction 事务

使用 `Dapper` 事务

```csharp

QueryParam qp = new QueryParam(tm.DBCode);
// 操作主表
qp.TableName = tm.TableCode;
qp.Action = 2;
qp.ORM = o;
// 加入多个对象参数
qp.Trans =new Dictionary<string, QueryParam>();

long rint = QuickData.Action(qp);

```
