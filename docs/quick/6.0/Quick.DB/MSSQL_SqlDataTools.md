---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# MSSQL.SqlDataTools
- 引用：Quick.DB.MSSQL.SqlDataTools


## MSSQL.SqlDataTools  `数据库工具`
2021.12.3

- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | ConnString|  | 数据库连接字符串 |
- **构造函数**

  - MSSQL.SqlDataTools([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    strConn)
       `连接数据库字符串`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | strConn |数据库连接字符串


## GetSqlConnection  `获取数据连接`

 方法:  MSSQL.SqlDataTools.GetSqlConnection


> GetSqlConnection()


## GetDataTableViewList  `读取数据库表与视图`

 方法:  MSSQL.SqlDataTools.GetDataTableViewList


> GetDataTableViewList([String][string]  _tablename,[Boolean][boolean]  _isView,[Boolean][boolean]  _isColume)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _tablename|[String][string] |指定表名称  ||
    | _isView|[Boolean][boolean] |是否读取视图  ||
    | _isColume|[Boolean][boolean] |是否读取列与外键  ||

  * 返回

   > 返回表名与描述 

## GetDataTableList  `读取数据库表与说明`

 方法:  MSSQL.SqlDataTools.GetDataTableList


> GetDataTableList([String][string]  _tablename)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _tablename|[String][string] |多个表  ||

## GetDataViewList  `读取视图`

 方法:  MSSQL.SqlDataTools.GetDataViewList


> GetDataViewList()


## GetColume  `读取数据表列信息与TPL模板配置参数`

 方法:  MSSQL.SqlDataTools.GetColume


> GetColume([Boolean@][boolean@]  _isIdentity,[String][string]  _TableCode,[List][list]&lt;[TableColumeToForgeinKey][tablecolumetoforgeinkey]&gt;  tctfkList)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _isIdentity|[Boolean@][boolean@] |是否包含自增长列  ||
    | _TableCode|[String][string] |需生成的表名  ||
    | tctfkList|[List][list]&lt;[TableColumeToForgeinKey][tablecolumetoforgeinkey]&gt; |外键列表  ||

## GetColumeItem  `字段处理`

 方法:  MSSQL.SqlDataTools.GetColumeItem


> GetColumeItem([DataRow][datarow]  varColume)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | varColume|[DataRow][datarow] |  ||

## GetColumeForgeinKey  `取得所有的外键信息`

 方法:  MSSQL.SqlDataTools.GetColumeForgeinKey


> GetColumeForgeinKey()




[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[boolean]:https://docs.microsoft.com/zh-cn/dotnet/api/system.boolean
[boolean@]:https://docs.microsoft.com/zh-cn/dotnet/api/system.boolean@
[list]:https://docs.microsoft.com/zh-cn/dotnet/api/system.collections.generic.list
[tablecolumetoforgeinkey]:../Quick.DB/MSSQL_TableColumeToForgeinKey.md
[datarow]:https://docs.microsoft.com/zh-cn/dotnet/api/system.data.datarow


