---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# QueryMode
- 引用：Quick.DB.QueryMode


## QueryMode  `查询条件方式,小写`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | EqualTo|  | 等于 <br /> = |
 | NotEqualTo|  | 不等于 <br /> &lt;&gt; |
 | StartWith|  | 大于等于 <br /> &gt;= |
 | EndWith|  | 小于等于 <br /> &lt;= |
 | SmallerThan|  | 小于 <br /> &lt; |
 | GreaterThan|  | 大于 <br /> &gt; |
 | Like|  | 相似 <br /> like |
 | LikeNot|  | 不相似 <br /> likenot |
 | LikeB|  | 前面相似 <br /> likeb |
 | LikeE|  | 后面相似 <br /> likee |
 | Between|  | 时间段查询,默认查询T+1天 <br /> between |
 | In|  | 多集合查询 <br /> in |
 | InNot|  | 集合排除查询 <br /> 字段有非空限制 |



