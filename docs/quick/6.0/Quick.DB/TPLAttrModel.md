---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# TPLAttrModel
- 引用：Quick.DB.TPLAttrModel


## TPLAttrModel  `数据表管理属性`
QF_Model,QF_API

- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | AttrModel|  | TPL 主模板对象数据属性\ &quot;h\ &quot;:1,\ &quot;c\ &quot;:\ &quot;json\ &quot; |
 | AttrJoin|  | 外键关系\ &quot;h\ &quot;:1,\ &quot;c\ &quot;:\ &quot;json\ &quot; |
 | AttrListor|  | 列表UI Listor 配置json |
 | AttrEditor|  | 编辑UI\ &quot;h\ &quot;:1,\ &quot;c\ &quot;:\ &quot;json\ &quot; |
 | AttrResp|  | 输出显示规则 |



