---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# MSSQL.Table.TableFieldModel
- 引用：Quick.DB.MSSQL.Table.TableFieldModel


## MSSQL.Table.TableFieldModel  `表字段明细`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | code|  | 列代码 |
 | title|  | 说明描述 |
 | type|  | 数据类型 |
 | defaultValue|  | 默认值 |
 | past|  | 编辑的原始内容 |



