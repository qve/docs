---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# MSSQL.TableEntityNets
- 引用：Quick.DB.MSSQL.TableEntityNets


## MSSQL.TableEntityNets  `表实体类`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | NameSpace|  | 命名空间 |
 | DateTime|  | 时间 |
 | TableNames|  | 表名集合 |



