---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# QuickLogDB
- 引用：Quick.DB.QuickLogDB


## QuickLogDB  `扩展日志方法`



## ToEvent  `写入日志对象`

 方法:  QuickLogDB.ToEvent


> ToEvent([LogEventModel][logeventmodel]  o)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | o|[LogEventModel][logeventmodel] |日志对象  ||

## ToEvent  `写入日志标题`

 方法:  QuickLogDB.ToEvent


> ToEvent([String][string]  title,[Int64][int64]  userID,[Int32][int32]  appID,[Int32][int32]  eventType)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | title|[String][string] |日志简述 100字  ||
    | userID|[Int64][int64] |编辑者  safe.TokenUser.UserID   ||
    | appID|[Int32][int32] |来源应用 safe.appID   ||
    | eventType|[Int32][int32] |QF_Book 日志类型 默认187 用户级,188 数据级,189 架构级  ||

## ToEvent  `写入日志事件详情`

 方法:  QuickLogDB.ToEvent


> ToEvent([String][string]  title,[Int64][int64]  userID,[Int32][int32]  appID,[Int32][int32]  eventType,[Int32][int32]  cmd,[String][string]  eventBody)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | title|[String][string] |日志简述 100字  ||
    | userID|[Int64][int64] |编辑者  safe.TokenUser.UserID   ||
    | appID|[Int32][int32] |来源应用 safe.appID   ||
    | eventType|[Int32][int32] |QF_Book 日志类型 默认187 用户级,188 数据级,189 架构级  ||
    | cmd|[Int32][int32] |QF_Control 操作指令 0:无,1:查,2:add,3:edit,4:del,5:added 更多  ||
    | eventBody|[String][string] |日志详情  ||

## ToEvent  `写入日志事件`

 方法:  QuickLogDB.ToEvent


> ToEvent([String][string]  title,[Int64][int64]  userID,[Int32][int32]  appID,[Int32][int32]  eventType,[Int32][int32]  cmd,[String][string]  eventBody,[String][string]  eventSQL)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | title|[String][string] |日志简述 100字  ||
    | userID|[Int64][int64] |编辑者  safe.TokenUser.UserID   ||
    | appID|[Int32][int32] |来源应用 safe.appID   ||
    | eventType|[Int32][int32] |QF_Book 日志类型 默认187 用户级,188 数据级,189 架构级  ||
    | cmd|[Int32][int32] |QF_Control 操作指令 0:无,1:查,2:add,3:edit,4:del,5:added 更多  ||
    | eventBody|[String][string] |日志详情  ||
    | eventSQL|[String][string] |操作SQL  ||

  * 备注 

      DBLog.Log_Event



[logeventmodel]:../Quick.DB/Model_LogEventModel.md
[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32


