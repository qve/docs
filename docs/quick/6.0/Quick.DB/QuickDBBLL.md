---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# QuickDBBLL
- 引用：Quick.DB.QuickDBBLL


## QuickDBBLL  `数据库公共工具类`



## GetPassType  `加密方式1:明文,2:DES`

 方法:  QuickDBBLL.GetPassType


> GetPassType()


  * 备注 

      QuickSettings.DB.PassType

## GetPassKey  `取出当前应用配置的加密密钥`

 方法:  QuickDBBLL.GetPassKey


> GetPassKey()


  * 备注 

      QuickSettings.DB.PassKey

## SetEncrypt  `加密字符串来自当前应用配置密钥`

 方法:  QuickDBBLL.SetEncrypt


> SetEncrypt([String][string]  value)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | value|[String][string] |原文  ||

  * 返回

   > Base64String 

## GetDecrypt  `解密来自配置密钥`

 方法:  QuickDBBLL.GetDecrypt


> GetDecrypt([String][string]  value)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | value|[String][string] |密文  ||

  * 备注 

      开启数据库连接加密 QuickSettings.DB.PassKey

## GetDecrypt  `解密来自配置，自定义密钥组合`

 方法:  QuickDBBLL.GetDecrypt


> GetDecrypt([String][string]  value,[String][string]  _passKey)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | value|[String][string] |密文 base64   ||
    | _passKey|[String][string] |自定义配置密钥  ||

## BindWhere  `前端查询参数where 转为QP查询条件`

 方法:  QuickDBBLL.BindWhere


> BindWhere([String][string]  _where)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _where|[String][string] |json 字符串  ||

  * 返回

   > List.WhereItem 

## BindWhere  `前端查询参数where 转为QP查询条件`

 方法:  QuickDBBLL.BindWhere


> BindWhere([JsonElement][jsonelement]  _where)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _where|[JsonElement][jsonelement] |json 条件对象  ||

  * 返回

   > List.WhereItem 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[jsonelement]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonelement


