---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# DataType
- 引用：Quick.DB.DataType


## DataType  `获取数据类型`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Str|  | 字符 |
 | Dat|  | 日期 |
 | Int|  | 整型 |
 | Long|  | 长整型 |
 | Double|  | 双精度小数 |
 | CharAndNum|  | 只限字符和数字 |
 | Email|  | 只限邮件地址 |
 | CharAndNumAndChinese|  | 只限字符和数字和中文 |
 | Decimal|  | 货币对应money |
 | Boolean|  | 布尔值是/否 |



