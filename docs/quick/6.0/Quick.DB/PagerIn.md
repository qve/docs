---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# PagerIn
- 引用：Quick.DB.PagerIn


## PagerIn  `分页传入参数`
2021.11.11

- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | id| [Int64](https://docs.microsoft.com/zh-cn/dotnet/api/system.int64) | 指定数据ID |
 | index| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 取数据分页页码 |
 | size| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 取数据分页 条数 <br /> 默认0 条不查询 |
 | field| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 取数据字段 |
 | sort| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 取数据排序字段 |
 | desc| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 1:降序，0:升序 |
 | tpl| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 取出分类数据 <br /> 1:取出模板,2:只取数据,6:外键查询 |
 | par| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 传入参数字符串 <br /> 或者 Json字符串 编码 |
- **构造函数**

  - PagerIn()
       `分页传入参数`





