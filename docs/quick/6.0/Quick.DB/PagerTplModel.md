---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# PagerTplModel
- 引用：Quick.DB.PagerTplModel


## PagerTplModel  `数据对象模板TPL`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | field| dynamic | 数据表输出字段 |
 | attr| dynamic | Ui 字段属性定义 <br /> 属性参阅[PagerTplOptsModel](../Quick.DB/PagerTplOptsModel.md) |
 | act| dynamic | UI控件权限 <br /> QF_Control 用逗号分割命令[CMD]的编码 |
 | keys| dynamic | UI 主键primary 与审核键定义 auth <br /> 参阅[TPLTableModel](../Quick.DB/TPLTableModel.md) |
 | sort| dynamic | 查询排序字段 |
 | listor| dynamic | 响应列表界面参数 |
 | editor| dynamic | 响应编辑界面参数 |



