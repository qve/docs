---
template: 2021.12
version: 2021.07
author: quick.docs
date: 2022年03月20日 09:23

---
# RedisKeys
- 引用：Quick.DB.RedisKeys
- version: 2021.07


## RedisKeys  `Redis 统一存储Keys规则`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | UserHash|  | QuickUser 的命名规则             170701 |

## GetUserKey  `返回Session 会话的子键定义`

 方法:  RedisKeys.GetUserKey


> GetUserKey([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |子键名称  ||

## GetUserRules  `用户角色组规则`

 方法:  RedisKeys.GetUserRules


> GetUserRules([Int32][int32]  _RoleID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _RoleID|[Int32][int32] |角色组  ||

  * 返回

   > ur:角色组 

## Token.UserTokenAccess  `令牌集合 会话缓存子项 u:L:UserID`

 方法:  RedisKeys.Token.UserTokenAccess


> Token.UserTokenAccess([Int32][int32]  AppID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | AppID|[Int32][int32] |应用ID  ||

  * 返回

   > AppID_us 

## Token.UserTokenRefresh  `刷新票据缓存子项`

 方法:  RedisKeys.Token.UserTokenRefresh


> Token.UserTokenRefresh([Int32][int32]  AppID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | AppID|[Int32][int32] |应用ID  ||

  * 返回

   > AppID_ur 

## Token.RefreshToken  `刷新令牌 绑定用户的登陆ID,默认超时30天`

 方法:  RedisKeys.Token.RefreshToken


> Token.RefreshToken([Int32][int32]  AppID,[String][string]  refreshToken)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | AppID|[Int32][int32] |应用ID  ||
    | refreshToken|[String][string] |刷新令牌  ||

  * 返回

   > u:r:{AppID}:{refreshToken} 

## Token.LoginUserKey  `用户已登陆ID 绑定的SessionID`

 方法:  RedisKeys.Token.LoginUserKey


> Token.LoginUserKey([Int64][int64]  LoginID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | LoginID|[Int64][int64] |登陆ID  ||

  * 返回

   > u:L:{LoginID} 

## Token.APUserLoginKey  `wifi登陆账户缓存`

 方法:  RedisKeys.Token.APUserLoginKey


> Token.APUserLoginKey([Int64][int64]  UserID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | UserID|[Int64][int64] |用户ID  ||

  * 返回

   > u:w:UserID 

## Cell.key  `标准格子对象`

 方法:  RedisKeys.Cell.key


> Cell.key([Int64][int64]  CellID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | CellID|[Int64][int64] |格子id  ||

  * 返回

   > sc:{CellID} 

## Cell.keyAll  `取出格子集合ID`

 方法:  RedisKeys.Cell.keyAll


> Cell.keyAll([Int32][int32]  GroupID,[Int32][int32]  Channel,[Int32][int32]  SLGroupID,[Int32][int32]  NasID,[Int32][int32]  APID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | GroupID|[Int32][int32] |  ||
    | Channel|[Int32][int32] |  ||
    | SLGroupID|[Int32][int32] |  ||
    | NasID|[Int32][int32] |  ||
    | APID|[Int32][int32] |  ||

  * 返回

   > scg:{0}:{1}:{2}:{3}:{4} 

## Cell.GroupEx  `组织禁用的格子id`

 方法:  RedisKeys.Cell.GroupEx


> Cell.GroupEx([Int32][int32]  GroupID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | GroupID|[Int32][int32] |组织id  ||

  * 返回

   > ex:g:GroupID 

  * 备注 

      老版本是:  exgid:{0}

## Cell.News  `新闻内容格子key`

 方法:  RedisKeys.Cell.News


> Cell.News([Int64][int64]  CellID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | CellID|[Int64][int64] |格子ID  ||

  * 返回

   > scn:CellID 

## Cell.NewsGroup  `发布新闻组织频道关系`

 方法:  RedisKeys.Cell.NewsGroup


> Cell.NewsGroup([Int32][int32]  GroupID,[Int32][int32]  Channel)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | GroupID|[Int32][int32] |发布组织ID，默认1：所有人看  ||
    | Channel|[Int32][int32] |发布频道ID，默认频道5：公共平台  ||

  * 返回

   > scn:g:1:5 

## UrlKey  `请求缓存的ID`

 方法:  RedisKeys.UrlKey


> UrlKey([String][string]  _key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _key|[String][string] |url 地址  ||

  * 返回

   > qp:_key 

## LoginInit  `已经重试登陆次数`

 方法:  RedisKeys.LoginInit


> LoginInit([String][string]  _key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _key|[String][string] |关键字  ||

  * 返回

   > login:l:{LoginCode} 

## PassWordSmsInt  `修改密码发短信计次`

 方法:  RedisKeys.PassWordSmsInt


> PassWordSmsInt([String][string]  _key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _key|[String][string] |  ||

  * 返回

   > login:s:登陆账户 

  * 备注 

      建议限制半小时最多2次修改

## code  `验证码`

 方法:  RedisKeys.code


> code([String][string]  _key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _key|[String][string] |关键字  ||



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64


