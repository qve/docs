---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# QuickDBEnum.StateFlagIcon
- 引用：Quick.DB.QuickDBEnum.StateFlagIcon


## QuickDBEnum.StateFlagIcon  `数据状态 icon-名称`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | gantan|  | 未配置 |
 | bianji|  | 正常 |
 | suo|  | 已审 |
 | suokai|  | 撤审 |
 | del|  | 删除 |
 | clear|  | 暂停 |



