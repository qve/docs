---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# SqlClient&lt;T&gt;
- 引用：Quick.DB.SqlClient&lt;T&gt;


## SqlClient&lt;T&gt;  `数据库交互方法`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | _sqlImpl|  | 数据库实现实例 |
- **构造函数**

  - SqlClient&lt;T&gt;([SqlImpl](../Quick.DB/SqlImpl.md)    sqlImpl)
       `构造注入数据库实例`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | sqlImpl |


## Dispose  `释放数据库连接实例`

 方法:  SqlClient&lt;T&gt;.Dispose


> Dispose()






