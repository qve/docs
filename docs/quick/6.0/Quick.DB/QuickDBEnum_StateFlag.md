---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# QuickDBEnum.StateFlag
- 引用：Quick.DB.QuickDBEnum.StateFlag


## QuickDBEnum.StateFlag  `数据状态说明`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | empty|  | 异常状态 |
 | normal|  | 默认 所有人可正常编辑 |
 | audited|  | 已审核，不可编辑 |
 | revoked|  | 已撤销审核，可本人编辑 |
 | deleted|  | 删除，所有人禁用，等待物理删除 |
 | paused|  | 已暂停，所有人停止访问与编辑 |



