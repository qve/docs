---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# QuickDBEnum
- 引用：Quick.DB.QuickDBEnum


## QuickDBEnum  `数据参数`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | StateFlag.empty|  | 异常状态 |
 | StateFlag.normal|  | 默认 所有人可正常编辑 |
 | StateFlag.audited|  | 已审核，不可编辑 |
 | StateFlag.revoked|  | 已撤销审核，可本人编辑 |
 | StateFlag.deleted|  | 删除，所有人禁用，等待物理删除 |
 | StateFlag.paused|  | 已暂停，所有人停止访问与编辑 |
 | StateFlagIcon.gantan|  | 未配置 |
 | StateFlagIcon.bianji|  | 正常 |
 | StateFlagIcon.suo|  | 已审 |
 | StateFlagIcon.suokai|  | 撤审 |
 | StateFlagIcon.del|  | 删除 |
 | StateFlagIcon.clear|  | 暂停 |



