---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# RedisBLL
- 引用：Quick.DB.RedisBLL


## RedisBLL  `公共方法`



## DeserializeList  `转换数组 RedisValue[]`

 方法:  RedisBLL.DeserializeList


> DeserializeList&lt;T&gt;(RedisValue  value)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | value|RedisValue |数组  ||

## DeserializeValue  `转换 RedisValue`

 方法:  RedisBLL.DeserializeValue


> DeserializeValue&lt;T&gt;(RedisValue  value)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | value|RedisValue |内容  ||

## DeserializeValue  `转换类型 泛型`

 方法:  RedisBLL.DeserializeValue


> DeserializeValue&lt;T&gt;([Byte][byte]  value)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | value|[Byte][byte] |  ||

## DeserializeByte  `转换Byte[]为对象`

 方法:  RedisBLL.DeserializeByte


> DeserializeByte&lt;T&gt;([Byte][byte]  value)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | value|[Byte][byte] |  ||

  * 返回

   > JsonHelper.DeserializeJsonTime 



[byte]:https://docs.microsoft.com/zh-cn/dotnet/api/system.byte


