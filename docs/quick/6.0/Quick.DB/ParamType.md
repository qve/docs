---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# ParamType
- 引用：Quick.DB.ParamType


## ParamType  `存储过程参数类型`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Input|  | 输入参数类型 |
 | Output|  | 输出参数类型 |
 | InputOutput|  | 输入输出参数类型 |



