---
template: 2021.12
version: 2021.01
author: quick.docs
date: 2022年03月20日 09:23

---
# RedisHelper
- 引用：Quick.DB.RedisHelper
- version: 2021.01


## RedisHelper  `封装方法`
基于：StackExchange.Redis

- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Conn| StackExchange.Redis.ConnectionMultiplexer | 连接对象 |
 | CustomKey| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 自定义 主键Key前缀: |
- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Config| [RedisConfigModel](../Quick.DB/Model_RedisConfigModel.md) | 当前连接配置 |
 | db| StackExchange.Redis.IDatabase | 返回数据连接对象 |
- **构造函数**

  - RedisHelper()
       `架构配置`


  - RedisHelper([Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32)    dbId)
       `架构配置，AppConns             QuickSettings.App.Key`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | dbId |指定存储库位置

  - RedisHelper([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _appKey)
       `架构配置，指定应用连接与db`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _appKey |架构应用Key

  - RedisHelper([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _appKey,[Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32)    dbIndex)
       `架构配置，指定应用连接与db`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _appKey |架构应用Key,默认-1 由配置决定
      | dbIndex |自定义db

  - RedisHelper([Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32)    dbId,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    host)
       `redis 自定义连接`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | dbId |自定义数据序号
      | host |服务器连接


## GetManager  `初始化配置`

 方法:  RedisHelper.GetManager


> GetManager()


## MuxerConfigurationChanged  `配置更改时`

 方法:  RedisHelper.MuxerConfigurationChanged


> MuxerConfigurationChanged([Object][object]  sender,EndPointEventArgs  e)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | sender|[Object][object] |  ||
    | e|EndPointEventArgs |  ||

## MuxerErrorMessage  `发生错误时`

 方法:  RedisHelper.MuxerErrorMessage


> MuxerErrorMessage([Object][object]  sender,RedisErrorEventArgs  e)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | sender|[Object][object] |  ||
    | e|RedisErrorEventArgs |  ||

## MuxerConnectionRestored  `重新建立连接之前的错误`

 方法:  RedisHelper.MuxerConnectionRestored


> MuxerConnectionRestored([Object][object]  sender,ConnectionFailedEventArgs  e)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | sender|[Object][object] |  ||
    | e|ConnectionFailedEventArgs |  ||

## MuxerConnectionFailed  `连接失败 ， 如果重新连接成功你将不会收到这个通知`

 方法:  RedisHelper.MuxerConnectionFailed


> MuxerConnectionFailed([Object][object]  sender,ConnectionFailedEventArgs  e)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | sender|[Object][object] |  ||
    | e|ConnectionFailedEventArgs |  ||

## MuxerHashSlotMoved  `更改集群`

 方法:  RedisHelper.MuxerHashSlotMoved


> MuxerHashSlotMoved([Object][object]  sender,HashSlotMovedEventArgs  e)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | sender|[Object][object] |  ||
    | e|HashSlotMovedEventArgs |  ||

## MuxerInternalError  `redis类库错误`

 方法:  RedisHelper.MuxerInternalError


> MuxerInternalError([Object][object]  sender,InternalErrorEventArgs  e)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | sender|[Object][object] |  ||
    | e|InternalErrorEventArgs |  ||

## AddSysCustomKey  `组合默认配置的主键`

 方法:  RedisHelper.AddSysCustomKey


> AddSysCustomKey([String][string]  Key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | Key|[String][string] |  ||

## Do  `泛型执行 方法`

 方法:  RedisHelper.Do


> Do&lt;T&gt;([Func][func]&lt;IDatabase,T&gt;  func)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | func|[Func][func]&lt;IDatabase,T&gt; |  ||

## ConvertJson  `对象转换为Json 字符串`

 方法:  RedisHelper.ConvertJson


> ConvertJson&lt;T&gt;(T  value)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | value|T |  ||

## ConvertObj  `字符串 转换为 Json 对象`

 方法:  RedisHelper.ConvertObj


> ConvertObj&lt;T&gt;(RedisValue  value)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | value|RedisValue |Json 字符串  ||

## ConvetList  `转换为数组列表`

 方法:  RedisHelper.ConvetList


> ConvetList&lt;T&gt;(RedisValue  values)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | values|RedisValue |  ||

## ConvertRedisKeys  `转换 String 数组 类型`

 方法:  RedisHelper.ConvertRedisKeys


> ConvertRedisKeys([String][string]  redisKeys)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | redisKeys|[List][list]&lt;[String][string]&gt; |string 列表  ||

## Set  `保存单条记录`

 方法:  RedisHelper.Set


> Set([String][string]  key,[String][string]  val,[Int64][int64]  KeyTimeOut)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |缓存建  ||
    | val|[String][string] |缓存值  ||
    | KeyTimeOut|[Int64][int64] |过期时间，单位秒,-1：不过期，0：默认过期时间  ||

## Set  `保存单条记录并设置超时`

 方法:  RedisHelper.Set


> Set([String][string]  key,[String][string]  val,[Nullable][nullable]&lt;[TimeSpan][timespan]&gt;  expiry)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |缓存主键  ||
    | val|[String][string] |对象值  ||
    | expiry|[Nullable][nullable]&lt;[TimeSpan][timespan]&gt; |TimeSpan.FromSeconds(超时秒数)  ||

## Set  `保存多条记录`

 方法:  RedisHelper.Set


> Set([List][list]&lt;[KeyValuePair][keyvaluepair]&lt;RedisKe&gt;,RedisValue&gt;  keyValues)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | keyValues|[List][list]&lt;[KeyValuePair][keyvaluepair]&lt;RedisKe&gt;,RedisValue&gt; |批量键值对  ||

## Set  `写入Json对象`

 方法:  RedisHelper.Set


> Set&lt;T&gt;([String][string]  key,T  obj,[TimeSpan][timespan]  expiry)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |缓存主键  ||
    | obj|T |对象值  ||
    | expiry|[Nullable][nullable]&lt;[TimeSpan][timespan]&gt; |TimeSpan.FromSeconds(超时秒数)  ||

## Set  `写入Json对象,并设置超时`

 方法:  RedisHelper.Set


> Set&lt;T&gt;([String][string]  key,T  obj,[DateTime][datetime]  dtTimeOut)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |缓存主键  ||
    | obj|T |对象值  ||
    | dtTimeOut|[DateTime][datetime] |到期时间  ||

## StringAppend  `字符串追加值`

 方法:  RedisHelper.StringAppend


> StringAppend([String][string]  key,[String][string]  value)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||
    | value|[String][string] |追加的值  ||

## Get  `获取值`

 方法:  RedisHelper.Get


> Get([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||

## MGet  `获取多个值`

 方法:  RedisHelper.MGet


> MGet&lt;T&gt;([String][string]  keys)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | keys|[List][list]&lt;[String][string]&gt; |Key集合  ||

## Get  `获取Json对象`

 方法:  RedisHelper.Get


> Get&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||

## incr  `为数字自增长`

 方法:  RedisHelper.incr


> incr([String][string]  key,[Double][double]  val)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |  ||
    | val|[Double][double] |可以为负  ||

  * 返回

   > 增长后的值t 

  * 备注 

      HINCRBY val+1              db.StringIncremen

## Decr  `为数字减少`

 方法:  RedisHelper.Decr


> Decr([String][string]  key,[Double][double]  val)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |  ||
    | val|[Double][double] |可以为负 -1  ||

  * 返回

   > 减少后的值db.StringDecrement 

## SetAsync  `异步写入`

 方法:  RedisHelper.SetAsync


> SetAsync([String][string]  key,[String][string]  value,[TimeSpan][timespan]  expiry)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||
    | value|[String][string] |保存的值  ||
    | expiry|[Nullable][nullable]&lt;[TimeSpan][timespan]&gt; |过期时间  ||

## HashGetAll  `获取所有Hash值`

 方法:  RedisHelper.HashGetAll


> HashGetAll([String][string]  hashId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | hashId|[String][string] |键名  ||

  * 返回

   > 返回泛型 

## HashAll  `获取所有Hash值`

 方法:  RedisHelper.HashAll


> HashAll([String][string]  hashId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | hashId|[String][string] |键名  ||

## HashAllAsync  `异步获取所有`

 方法:  RedisHelper.HashAllAsync


> HashAllAsync([String][string]  hashId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | hashId|[String][string] |  ||

## HashGet  `取出单个Hash值`

 方法:  RedisHelper.HashGet


> HashGet([String][string]  hashId,[String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | hashId|[String][string] |  ||
    | key|[String][string] |  ||

  * 返回

   > 默认String 

## HashGet  `取出Hash值并转为对象`

 方法:  RedisHelper.HashGet


> HashGet&lt;T&gt;([String][string]  hashId,[String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | hashId|[String][string] |  ||
    | key|[String][string] |  ||

## HashGetJsonObj  `取出 hash转为 json`

 方法:  RedisHelper.HashGetJsonObj


> HashGetJsonObj&lt;T&gt;([String][string]  hashId,[String][string]  dataKey)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | hashId|[String][string] |主键  ||
    | dataKey|[String][string] |数据关键字  ||

## HashSet  `散列数据类型批量新增`

 方法:  RedisHelper.HashSet


> HashSet([String][string]  key,[List][list]&lt;HashEntry&gt;  hashEntrys,CommandFlags  flags)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||
    | hashEntrys|[List][list]&lt;HashEntry&gt; |数据集合  ||
    | flags|CommandFlags |标识  ||

## HashSet  `插入Hash,序列化为JSON`

 方法:  RedisHelper.HashSet


> HashSet&lt;T&gt;([String][string]  key,[String][string]  field,T  val,[Int32][int32]  timer)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||
    | field|[String][string] |属性名  ||
    | val|T |写入对象  ||
    | timer|[Int32][int32] |超时分钟数  ||

## HashSet  `插入 String`

 方法:  RedisHelper.HashSet


> HashSet([String][string]  key,[String][string]  field,[String][string]  val,[Int32][int32]  timer)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||
    | field|[String][string] |属性名  ||
    | val|[String][string] |内容  ||
    | timer|[Int32][int32] |超时分钟数  ||

## HashSet  `散列数据类型`

 方法:  RedisHelper.HashSet


> HashSet&lt;T&gt;([String][string]  key,[String][string]  field,T  val,When  when,CommandFlags  flags)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||
    | field|[String][string] |属性名  ||
    | val|T |内容  ||
    | when|When |条件  ||
    | flags|CommandFlags |标识  ||

## HashMSet  `批量插入`

 方法:  RedisHelper.HashMSet


> HashMSet([String][string]  key,HashEntry  hashFields)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |缓存主键  ||
    | hashFields|HashEntry |KeyValuePair  ||

## HashMSetTimeOut  `批量插入并设置过期时间`

 方法:  RedisHelper.HashMSetTimeOut


> HashMSetTimeOut([String][string]  key,HashEntry  hashFields,[Int32][int32]  timer)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |缓存主键  ||
    | hashFields|HashEntry |存储内容  ||
    | timer|[Int32][int32] |存储时间,分钟数，0为不设置超时  ||

  * 返回

   > 没有设置时间，默认为false 

## ZAdd  `添加有序集合关系 SortedSetAdd`

 方法:  RedisHelper.ZAdd


> ZAdd([String][string]  setId,[Int64][int64]  value,[Int64][int64]  score,[Int32][int32]  timer)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | setId|[String][string] |关键字  ||
    | value|[Int64][int64] |内容  ||
    | score|[Int64][int64] |排序  ||
    | timer|[Int32][int32] |超时分钟数  ||

  * 备注 

      排序取 ZRange(, 0, 1);

## ZAdd  `添加有序集合关系`

 方法:  RedisHelper.ZAdd


> ZAdd([String][string]  setId,[String][string]  value,[Double][double]  score,[Int32][int32]  timer)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | setId|[String][string] |关键字  ||
    | value|[String][string] |内容  ||
    | score|[Double][double] |排序  ||
    | timer|[Int32][int32] |超时分钟数  ||

  * 备注 

      排序取 ZRange(key, 0, 1);

## ZAddRS  `批量添加有序集合`

 方法:  RedisHelper.ZAddRS


> ZAddRS([String][string]  setId,SortedSetEntry  values)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | setId|[String][string] |  ||
    | values|SortedSetEntry |  ||

  * 备注 

      排序取 ZRange(key, 0, 1);

## ZRem  `移除有序集合关系内的子项`

 方法:  RedisHelper.ZRem


> ZRem([String][string]  setId,[String][string]  item)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | setId|[String][string] |键名  ||
    | item|[String][string] |子项名  ||

## ZIncrBy  `有序集合 排序score减等于`

 方法:  RedisHelper.ZIncrBy


> ZIncrBy([String][string]  setId,[Double][double]  sort,[String][string]  _key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | setId|[String][string] |主键  ||
    | sort|[Double][double] |排序值0:减少,1:增加  ||
    | _key|[String][string] |子项名称  ||

  * 备注 

      sort&gt;0?SortedSetIncrement:SortedSetDecrement

## ZRange  `取出有序集合`

 方法:  RedisHelper.ZRange


> ZRange&lt;T&gt;([String][string]  key,[Int32][int32]  min,[Int32][int32]  max)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |存储键名  ||
    | min|[Int32][int32] |0  ||
    | max|[Int32][int32] |-1  ||

  * 备注 

      ZAdd 添加排序

## ZRange  `取出有序集合指定排序`

 方法:  RedisHelper.ZRange


> ZRange&lt;T&gt;([String][string]  key,[Int32][int32]  min,[Int32][int32]  max,Order  _order)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |存储的关键名  ||
    | min|[Int32][int32] |从0  ||
    | max|[Int32][int32] |到 -1 标识全部  ||
    | _order|Order |排序  ||

  * 备注 

      ZAdd 添加排序,zrange

## ZRangeScores  `取出有序集合并取出对象`

 方法:  RedisHelper.ZRangeScores


> ZRangeScores([String][string]  key,[Int64][int64]  strat,[Int32][int32]  stop)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |存储的关键名  ||
    | strat|[Int64][int64] |开始排序  ||
    | stop|[Int32][int32] |结束排序  ||

## ZRangeAll  `取出有序全部集合`

 方法:  RedisHelper.ZRangeAll


> ZRangeAll([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |存储的Key  ||

  * 返回

   > 集合与排序因子 

## ZRangeLength  `有序集合存储的条数`

 方法:  RedisHelper.ZRangeLength


> ZRangeLength([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |存储的Key  ||

## ZRangeLength  `取出排序因子内容条数`

 方法:  RedisHelper.ZRangeLength


> ZRangeLength([String][string]  key,[Int32][int32]  min,[Int32][int32]  max)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |存储的Key  ||
    | min|[Int32][int32] |排序因子开始  ||
    | max|[Int32][int32] |排序因子结束  ||

## ZRangeScore  `返回内容排序因子`

 方法:  RedisHelper.ZRangeScore


> ZRangeScore([String][string]  key,[String][string]  val)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||
    | val|[String][string] |内容  ||

## SAddGetAll  `获取无序集合内所有元素`

 方法:  RedisHelper.SAddGetAll


> SAddGetAll&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||

## SetMembers  `获取无序集合内所有元素`

 方法:  RedisHelper.SetMembers


> SetMembers&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||

## SAdd  `添加无序集合关系`

 方法:  RedisHelper.SAdd


> SAdd([String][string]  setId,[String][string]  item)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | setId|[String][string] |主键  ||
    | item|[String][string] |子项  ||

## SAddRS  `批量添加无序集合子项关系`

 方法:  RedisHelper.SAddRS


> SAddRS([String][string]  setId,RedisValue  values)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | setId|[String][string] |主键  ||
    | values|RedisValue |子项数组  ||

## SRem  `移除无序集合关系`

 方法:  RedisHelper.SRem


> SRem([String][string]  setId,[String][string]  item)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | setId|[String][string] |主键  ||
    | item|[String][string] |单个子项  ||

## smembers  `返回set的所有元素`

 方法:  RedisHelper.smembers


> smembers&lt;T&gt;([String][string]  setId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | setId|[String][string] |主键  ||

## srandmember  `随机返回set的一个元素`

 方法:  RedisHelper.srandmember


> srandmember&lt;T&gt;([String][string]  setId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | setId|[String][string] |主键  ||

## Sort  `获取无序集合`

 方法:  RedisHelper.Sort


> Sort&lt;T&gt;([String][string]  key,[String][string]  _GetPattern,[Boolean][boolean]  desc)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |  ||
    | _GetPattern|[String][string] |  ||
    | desc|[Boolean][boolean] |  ||

## SortTop  `获取无序集合第一个值`

 方法:  RedisHelper.SortTop


> SortTop&lt;T&gt;([String][string]  key,[String][string]  _GetPattern,[Boolean][boolean]  desc)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |  ||
    | _GetPattern|[String][string] |  ||
    | desc|[Boolean][boolean] |  ||

## SortJson  `获取链表全部排序Json对象`

 方法:  RedisHelper.SortJson


> SortJson&lt;T&gt;([String][string]  key,[String][string]  _GetPattern,[Boolean][boolean]  desc)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |关键字 cg:{0}:{1}  ||
    | _GetPattern|[String][string] |关联对象的Key c:{0}:*  ||
    | desc|[Boolean][boolean] |排序 true表示最新  ||

## KeyExists  `判断key是否存储`

 方法:  RedisHelper.KeyExists


> KeyExists([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |redis key  ||

## KeyRename  `重新命名key`

 方法:  RedisHelper.KeyRename


> KeyRename([String][string]  key,[String][string]  newKey)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |现在Key  ||
    | newKey|[String][string] |新的Key  ||

## KeyDelete 

 方法:  RedisHelper.KeyDelete


> KeyDelete([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键名称  ||

## KeyDelete  `批量删除多个键`

 方法:  RedisHelper.KeyDelete


> KeyDelete([String][string]  keys)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | keys|[List][list]&lt;[String][string]&gt; |键数组  ||

## ExpireAt  `设置Key的 过期时间`

 方法:  RedisHelper.ExpireAt


> ExpireAt([String][string]  key,[DateTime][datetime]  expireAt)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||
    | expireAt|[DateTime][datetime] |到期时间  ||

## KeyExpire 

 方法:  RedisHelper.KeyExpire


> KeyExpire([String][string]  key,[Int64][int64]  Timeout)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |关键字  ||
    | Timeout|[Int64][int64] |分钟数  ||

## KeyExpire  `设置Key的 过期时间差`

 方法:  RedisHelper.KeyExpire


> KeyExpire([String][string]  key,[TimeSpan][timespan]  expiry)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||
    | expiry|[Nullable][nullable]&lt;[TimeSpan][timespan]&gt; |TimeSpan.FromSeconds(超时秒数)  ||

## KeyExpire  `设置过期`

 方法:  RedisHelper.KeyExpire


> KeyExpire([String][string]  key,[DateTime][datetime]  time)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||
    | time|[DateTime][datetime] |过期时间 DateTime.Now.AddSeconds(20)  ||

## KeyExpire  `批处理多个Key 过期`

 方法:  RedisHelper.KeyExpire


> KeyExpire([String][string]  keys,[DateTime][datetime]  time)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | keys|[List][list]&lt;[String][string]&gt; |  ||
    | time|[DateTime][datetime] |  ||

## ScriptLua  `lua脚本`

 方法:  RedisHelper.ScriptLua


> ScriptLua([String][string]  _script)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _script|[String][string] |lua脚本内容  ||

  * 返回

   > 执行返回，不代表成功 

  * 备注 

      慎重使用

## Subscribe  `订阅，消息需要反序列化`

 方法:  RedisHelper.Subscribe


> Subscribe(RedisChannel  channel,[Action][action]&lt;RedisChannel,RedisValue&gt;  handle)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | channel|RedisChannel |  ||
    | handle|[Action][action]&lt;RedisChannel,RedisValue&gt; |  ||

## Publish  `发布，消息需要序列化`

 方法:  RedisHelper.Publish


> Publish(RedisChannel  channel,RedisValue  message)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | channel|RedisChannel |  ||
    | message|RedisValue |  ||

## Publish  `发布（使用序列化）`

 方法:  RedisHelper.Publish


> Publish&lt;T&gt;(RedisChannel  channel,T  message)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | channel|RedisChannel |  ||
    | message|T |  ||

## SubscribeAsync  `订阅`

 方法:  RedisHelper.SubscribeAsync


> SubscribeAsync(RedisChannel  channel,[Action][action]&lt;RedisChannel,RedisValue&gt;  handle)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | channel|RedisChannel |订阅频道  ||
    | handle|[Action][action]&lt;RedisChannel,RedisValue&gt; |回调方法（异步消息需要用Deserialize 反序列化）  ||

## PublishAsync  `发布`

 方法:  RedisHelper.PublishAsync


> PublishAsync(RedisChannel  channel,RedisValue  message)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | channel|RedisChannel |  ||
    | message|RedisValue |  ||

## PublishAsync  `发布（使用序列化）`

 方法:  RedisHelper.PublishAsync


> PublishAsync&lt;T&gt;(RedisChannel  channel,T  message)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | channel|RedisChannel |  ||
    | message|T |Serialize序列化内容  ||

## Serialize  `序列化 json,Deserialize`

 方法:  RedisHelper.Serialize


> Serialize([Object][object]  obj)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | obj|[Object][object] |  ||

  * 备注 

      BinaryFormatter 存在安全漏洞 换用json

## Deserialize  `反序列化 json.Serialize`

 方法:  RedisHelper.Deserialize


> Deserialize&lt;T&gt;([Byte][byte]  data)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | data|[Byte][byte] |  ||

  * 备注 

      BinaryFormatter 存在安全漏洞 换用json

## CreateBatch  `批量执行`

 方法:  RedisHelper.CreateBatch


> CreateBatch()




[object]:https://docs.microsoft.com/zh-cn/dotnet/api/system.object
[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[func]:https://docs.microsoft.com/zh-cn/dotnet/api/system.func
[list]:https://docs.microsoft.com/zh-cn/dotnet/api/system.collections.generic.list
[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64
[nullable]:https://docs.microsoft.com/zh-cn/dotnet/api/system.nullable
[timespan]:https://docs.microsoft.com/zh-cn/dotnet/api/system.timespan
[keyvaluepair]:https://docs.microsoft.com/zh-cn/dotnet/api/system.collections.generic.keyvaluepair
[datetime]:https://docs.microsoft.com/zh-cn/dotnet/api/system.datetime
[double]:https://docs.microsoft.com/zh-cn/dotnet/api/system.double
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[boolean]:https://docs.microsoft.com/zh-cn/dotnet/api/system.boolean
[action]:https://docs.microsoft.com/zh-cn/dotnet/api/system.action
[byte]:https://docs.microsoft.com/zh-cn/dotnet/api/system.byte


