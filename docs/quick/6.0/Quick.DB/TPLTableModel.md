---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# TPLTableModel
- 引用：Quick.DB.TPLTableModel


## TPLTableModel  `QF_Model 对象约束`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | TableCode| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 所属数据表名 |
 | DBCode| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 所属配置库名 <br /> \ &quot;tip\ &quot;:\ &quot;quick,base\ &quot; |
 | DBServer| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 所属服务器名 |
 | primary| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 自增长主键字段名 <br /> 参阅,             默认 ID             定义 tpl.keys.primary[PagerTplModel](../Quick.DB/PagerTplModel.md) |
 | auth| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 权限验证字段名 <br /> 默认 SF_ID             自定义 tpl.keys.auth |
 | editUser| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 默认编辑者字段名 <br /> 默认 MUser_ID             自定义tpl.keys.user |
 | editTime| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 默认编辑时间字段名 <br /> 默认 U_Time             自定义 tpl.keys.time |
- **构造函数**

  - TPLTableModel()
       `数据表默认字段命名`





