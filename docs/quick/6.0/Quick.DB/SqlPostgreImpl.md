---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# SqlPostgreImpl
- 引用：Quick.DB.SqlPostgreImpl


## SqlPostgreImpl  `PostgreSql 实现`


- **构造函数**

  - SqlPostgreImpl([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    connectionString)
       `构造 初始成员`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | connectionString |


## GetDbConnection  `获取数据库连接对象`

 方法:  SqlPostgreImpl.GetDbConnection


> GetDbConnection()


  * 返回

   > IDbConnection 





