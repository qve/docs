---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# Model.DataSafeModel
- 引用：Quick.DB.Model.DataSafeModel


## Model.DataSafeModel  `通用数据权限对象`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | ID|  | 数据ID |
 | StateFlag|  | 数据状态 |
 | MUser_ID|  | 数据编辑者 |



