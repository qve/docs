---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# QuickDAL
- 引用：Quick.DB.QuickDAL


## QuickDAL  `数据库连接层`



## Open  `打开数据库`

 方法:  QuickDAL.Open


> Open&lt;T&gt;([String][string]  _dbCode,[Func][func]&lt;[IDbConnection][idbconnection],T&gt;  func)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _dbCode|[String][string] |指定数据库命名  ||
    | func|[Func][func]&lt;[IDbConnection][idbconnection],T&gt; |回调连接  ||

  * 调试信息


## Open  `打开数据库,支持事务tarns`

 方法:  QuickDAL.Open


> Open&lt;T&gt;([String][string]  _dbCode,[Func][func]&lt;[IDbConnection][idbconnection],[IDbTransaction][idbtransaction],T&gt;  func)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _dbCode|[String][string] |指定数据库命名  ||
    | func|[Func][func]&lt;[IDbConnection][idbconnection],[IDbTransaction][idbtransaction],T&gt; |回调连接,事务  ||

  * 调试信息


## GetSelectSql  `生成SQL分页查询语句`

 方法:  QuickDAL.GetSelectSql


> GetSelectSql([QueryParam][queryparam]  qp)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | qp|[QueryParam][queryparam] |查询条件参数  ||

  * 返回

   > SQL查询语句 

## GetActionSql  `拼接数据库执行语句`

 方法:  QuickDAL.GetActionSql


> GetActionSql([QueryParam][queryparam]  qp)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | qp|[QueryParam][queryparam] |执行参数  ||

  * 返回

   > sql 

## GetUpdateSqlTxt  `更新SQL更新语句`

 方法:  QuickDAL.GetUpdateSqlTxt


> GetUpdateSqlTxt([String][string]  TableName,[Object][object]  o,[List][list]&lt;[WhereItem][whereitem]&gt;  tParams,[String][string]  _output)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | TableName|[String][string] |表名  ||
    | o|[Object][object] |更新的内容对象  ||
    | tParams|[List][list]&lt;[WhereItem][whereitem]&gt; |条件  ||
    | _output|[String][string] |输出  ||

  * 返回

   > SQL更新语句 

## GetUpdateSqlTxtByJson  `SQL更新语句来自JSON`

 方法:  QuickDAL.GetUpdateSqlTxtByJson


> GetUpdateSqlTxtByJson([String][string]  TableName,[JsonElement][jsonelement]  o,[WhereItem][whereitem]  tParams,[String][string]  _output)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | TableName|[String][string] |表名  ||
    | o|[JsonElement][jsonelement] |更新的内容JsonElement  ||
    | tParams|[List][list]&lt;[WhereItem][whereitem]&gt; |条件  ||
    | _output|[String][string] |输出  ||

  * 返回

   > SQL更新语句 

## GetUpdateSqlTxtByObject  `SQL更新语句来自JSON编辑对象`

 方法:  QuickDAL.GetUpdateSqlTxtByObject


> GetUpdateSqlTxtByObject([String][string]  TableName,[Object][object]  o,[WhereItem][whereitem]  tParams,[String][string]  _output)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | TableName|[String][string] |表名  ||
    | o|[IDictionary][idictionary]&lt;[String][string],[Object][object]&gt; |对象内容  ||
    | tParams|[List][list]&lt;[WhereItem][whereitem]&gt; |查询条件  ||
    | _output|[String][string] |输出  ||

## GetUpdateSqlTxtByModel  `SQL更新语句来自对象`

 方法:  QuickDAL.GetUpdateSqlTxtByModel


> GetUpdateSqlTxtByModel([String][string]  TableName,[Object][object]  o,[WhereItem][whereitem]  tParams,[String][string]  _output)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | TableName|[String][string] |表名  ||
    | o|[Object][object] |更新的内容对象  ||
    | tParams|[List][list]&lt;[WhereItem][whereitem]&gt; |条件  ||
    | _output|[String][string] |输出字段 qp.output  ||

  * 返回

   > SQL更新语句 

## GetInsertSql  `生成新增语句 JsonElement`

 方法:  QuickDAL.GetInsertSql


> GetInsertSql([String][string]  TableName,[Object][object]  o,[String][string]  _output)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | TableName|[String][string] |操作表名  ||
    | o|[Object][object] |数据对象  ||
    | _output|[String][string] |输出字段 qp.outpu,默认不返回  ||

  * 备注 

      output inserted.id

## GetInsertSqlByJsonList  `批量插入Json`

 方法:  QuickDAL.GetInsertSqlByJsonList


> GetInsertSqlByJsonList([String][string]  TableName,[Object][object]  jsonArray)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | TableName|[String][string] |操作表名  ||
    | jsonArray|[Object][object] |批量插入的数据对象  ||

  * 调试信息


## GetInsertSqlByJson  `生成新增语句来自Json`

 方法:  QuickDAL.GetInsertSqlByJson


> GetInsertSqlByJson([String][string]  TableName,[JsonElement][jsonelement]  o,[String][string]  _output)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | TableName|[String][string] |数据表名  ||
    | o|[JsonElement][jsonelement] |JsonElement 对象  ||
    | _output|[String][string] |是否返回  ||

## GetInsertSqlByJson  `生成新增语句来自Json`

 方法:  QuickDAL.GetInsertSqlByJson


> GetInsertSqlByJson([String][string]  TableName,[JsonDocument][jsondocument]  doc,[String][string]  _output)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | TableName|[String][string] |数据表名  ||
    | doc|[JsonDocument][jsondocument] |JsonDocument 文档  ||
    | _output|[String][string] |是否返回id   ||

## GetInsertSqlByObject  `生成新增语句来自对象`

 方法:  QuickDAL.GetInsertSqlByObject


> GetInsertSqlByObject([String][string]  TableName,[Object][object]  o,[String][string]  _output)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | TableName|[String][string] |  ||
    | o|[IDictionary][idictionary]&lt;[String][string],[Object][object]&gt; |  ||
    | _output|[String][string] |是否返回id   ||

## GetInsertSqlByModel  `生成新增语句来自Model对象`

 方法:  QuickDAL.GetInsertSqlByModel


> GetInsertSqlByModel([String][string]  TableName,[Object][object]  o,[String][string]  _output)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | TableName|[String][string] |数据表名  ||
    | o|[Object][object] |对象  ||
    | _output|[String][string] |是否返回id   ||

## ToSQLInsertValue  `json对象转为SQL字段值`

 方法:  QuickDAL.ToSQLInsertValue


> ToSQLInsertValue([JsonElement][jsonelement]  jsonElement)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | jsonElement|[JsonElement][jsonelement] |EnumerateObject  ||

## GetOuterSql  `生成连接语句 outer apply`

 方法:  QuickDAL.GetOuterSql


> GetOuterSql([TPLSQLSelectModel][tplsqlselectmodel]  o)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | o|[TPLSQLSelectModel][tplsqlselectmodel] |参数对象  ||

## GetSortText  `构造 查询排序`

 方法:  QuickDAL.GetSortText


> GetSortText([String][string]  _prefix,[String][string]  _sort,[Int32][int32]  _desc)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _prefix|[String][string] |字段前缀  ||
    | _sort|[String][string] |排序字段  ||
    | _desc|[Int32][int32] |1:降序,0:升序  ||

## GetGroupByText  `构造 查询分组`

 方法:  QuickDAL.GetGroupByText


> GetGroupByText([String][string]  _prefix,[String][string]  _group)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _prefix|[String][string] |字段前缀  ||
    | _group|[String][string] |分组多字段用逗号分割  ||

## GetRowPageSql  `分页查询 OFFSET`

 方法:  QuickDAL.GetRowPageSql


> GetRowPageSql([String][string]  _sql,[Int32][int32]  _size,[Int32][int32]  _index)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _sql|[String][string] |sql 查询语句  ||
    | _size|[Int32][int32] |每页页数[需大于零]  ||
    | _index|[Int32][int32] |页数[从壹开始算]  ||

## GetWhereText  `构造参数条件拼接`

 方法:  QuickDAL.GetWhereText


> GetWhereText([WhereItem][whereitem]  tParams,[String][string]  tPre)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | tParams|[List][list]&lt;[WhereItem][whereitem]&gt; |组合查询条件  ||
    | tPre|[String][string] |字段前缀  ||

  * 返回

   > Where条件组合 

## GetFieldText  `查询字段加前缀`

 方法:  QuickDAL.GetFieldText


> GetFieldText([String][string]  _field,[String][string]  tPre)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _field|[String][string] |原字段名  ||
    | tPre|[String][string] |前缀 t.  ||

## GetWhereFlag  `防注入标识规范校验`

 方法:  QuickDAL.GetWhereFlag


> GetWhereFlag([String][string]  _flag)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _flag|[String][string] |  ||

  * 调试信息

  不符合标准规范 


  * 备注 

      允许标识 () or and and(

## GetWhereMode  `构造字段查询与条件`

 方法:  QuickDAL.GetWhereMode


> GetWhereMode([String][string]  _field,[Object][object]  obj,[Object][object]  param,[String][string]  _mode)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _field|[String][string] |字段类型  ||
    | obj|[Object][object] |字段值对象  ||
    | param|[Object][object] |字段参数  ||
    | _mode|[String][string] |查询条件 QueryMode   ||

## GetDeleteSqlByObject  `删除`

 方法:  QuickDAL.GetDeleteSqlByObject


> GetDeleteSqlByObject([String][string]  TableName,[WhereItem][whereitem]  tParams)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | TableName|[String][string] |表名  ||
    | tParams|[List][list]&lt;[WhereItem][whereitem]&gt; |条件参数  ||

## inSQL  `格式化SQL值防注入`

 方法:  QuickDAL.inSQL


> inSQL([String][string]  formatStr)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | formatStr|[String][string] |需要格式化的字符串  ||

  * 返回

   > 字符串 

## SqlLink  `取出跨服务器数据库正则表名sql`

 方法:  QuickDAL.SqlLink


> SqlLink([String][string]  _table,[String][string]  _dbCode,[Boolean][boolean]  isServerExt)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _table|[String][string] |查询表  ||
    | _dbCode|[String][string] |数据库命名 connectionStrings.name   ||
    | isServerExt|[Boolean][boolean] |默认跨数据库   ||

  * 返回

   > [server].db.dbo.table 

## GetObjecType  `返回对象属性类型`

 方法:  QuickDAL.GetObjecType


> GetObjecType([Object][object]  o,[String][string]  propertyName)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | o|[Object][object] |实体对象  ||
    | propertyName|[String][string] |属性名称  ||

  * 返回

   > PropertyType 

## ToSQLValue  `转为SQL值来自实体对象`

 方法:  QuickDAL.ToSQLValue


> ToSQLValue([Object][object]  o)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | o|[Object][object] |对象  ||

  * 返回

   > 处理时间类型 

## ToSQLValue  `转sql值来自json值`

 方法:  QuickDAL.ToSQLValue


> ToSQLValue([JsonElement][jsonelement]  value)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | value|[JsonElement][jsonelement] |对象  ||

  * 返回

   > jsonElement.ValueKind 

## ToSQLByList  `json 数组转对象`

 方法:  QuickDAL.ToSQLByList


> ToSQLByList([JsonElement][jsonelement]  jsonElement)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | jsonElement|[JsonElement][jsonelement] |  ||

  * 返回

   > EnumerateArray 

## ToSQLUpdateValue  `json 转sql 编辑语句组合`

 方法:  QuickDAL.ToSQLUpdateValue


> ToSQLUpdateValue([JsonElement][jsonelement]  jsonElement,[String][string]  mark)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | jsonElement|[JsonElement][jsonelement] |JsonElement.EnumerateObject 对象  ||
    | mark|[String][string] |组合条件  ||

  * 返回

   > sql 更新语句组合 

## ToSQLUpdateValue  `可编辑JSON 对象转换`

 方法:  QuickDAL.ToSQLUpdateValue


> ToSQLUpdateValue([Object][object]  o,[String][string]  mark)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | o|[IDictionary][idictionary]&lt;[String][string],[Object][object]&gt; |IDictionary 动态json 对象转换  ||
    | mark|[String][string] |组合条件  ||

  * 返回

   > sql 更新语句组合 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[func]:https://docs.microsoft.com/zh-cn/dotnet/api/system.func
[idbconnection]:https://docs.microsoft.com/zh-cn/dotnet/api/system.data.idbconnection
[idbtransaction]:https://docs.microsoft.com/zh-cn/dotnet/api/system.data.idbtransaction
[queryparam]:../Quick.DB/QueryParam.md
[object]:https://docs.microsoft.com/zh-cn/dotnet/api/system.object
[list]:https://docs.microsoft.com/zh-cn/dotnet/api/system.collections.generic.list
[whereitem]:../Quick.DB/WhereItem.md
[jsonelement]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonelement
[idictionary]:https://docs.microsoft.com/zh-cn/dotnet/api/system.collections.generic.idictionary
[jsondocument]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsondocument
[tplsqlselectmodel]:../Quick.DB/Model_TPLSQLSelectModel.md
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[boolean]:https://docs.microsoft.com/zh-cn/dotnet/api/system.boolean


