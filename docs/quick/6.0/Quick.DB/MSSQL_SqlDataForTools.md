---
template: 2021.12
version: 21.11.28
author: quick.docs
date: 2022年03月20日 09:23

---
# MSSQL.SqlDataForTools
- 引用：Quick.DB.MSSQL.SqlDataForTools
- version: 21.11.28


## MSSQL.SqlDataForTools  `SQL 构造方法`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | ConnString|  | 数据库连接字符串 |
- **构造函数**

  - MSSQL.SqlDataForTools([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    strConn)
       `连接数据库字符串`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | strConn |连接字符串,非加密


## GetSqlConnection  `获取数据连接`

 方法:  MSSQL.SqlDataForTools.GetSqlConnection


> GetSqlConnection()


## GetDataBaseTable  `取得表实体集`

 方法:  MSSQL.SqlDataForTools.GetDataBaseTable


> GetDataBaseTable([String][string]  _table,[Boolean][boolean]  _isView)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _table|[String][string] |表名,为空，取全部  ||
    | _isView|[Boolean][boolean] |是否包含视图  ||

## GetForgeinKeyTable  `取得外键信息`

 方法:  MSSQL.SqlDataForTools.GetForgeinKeyTable


> GetForgeinKeyTable()




[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[boolean]:https://docs.microsoft.com/zh-cn/dotnet/api/system.boolean


