---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# PagerInString
- 引用：Quick.DB.PagerInString


## PagerInString  `分页传入查询条件where参数为字符串`
继承：[PagerIn](../Quick.DB/PagerIn.md)

- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | where| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 传入查询字段条件 <br /> json数组字符串 [{id:1,q: &quot;&gt; &quot;}] |
 | join| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 自定义查询条件字符串 <br /> json数组 List[QueryJoin](../Quick.DB/QueryJoin.md) |
 | joinPre| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 外键字段前缀 <br /> 默认:为空,多表查询默认为:t             ,             sql组合 t.字段名称.外键字段名称 |



