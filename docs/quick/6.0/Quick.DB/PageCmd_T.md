---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# PageCmd&lt;T&gt;
- 引用：Quick.DB.PageCmd&lt;T&gt;


## PageCmd&lt;T&gt;  `提交增删改数据对象`
[FromBody] 注意：对象属性类型不符，将无法取到参数

- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | cmd| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 操作指令 QF_Control <br /> list,add,edit,del:状态删除,erase:物理删除,audit:审核,resc:超级撤销审核权限 |
 | id| [Int64](https://docs.microsoft.com/zh-cn/dotnet/api/system.int64) | 数据ID |
 | par| !:dynamic | 提交参数 |
 | where| !:dynamic | 条件参数 |
 | obj| !:T | 提交对象数据 |



