---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# QuickAppBLL
- 引用：Quick.DB.QuickAppBLL


## QuickAppBLL  `应用架构方法，获取应用配置`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | config|  | 配置对应数据表 [QF_AppInfo] |
- **构造函数**

  - QuickAppBLL([Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32)    _appId)
       `读取架构应用配置`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _appId |

  - QuickAppBLL([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _appkey)
       `读取架构应用配置`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _appkey |来源请求应用key


## AppInfoCacheKey  `缓存的AppInfoKey`

 方法:  QuickAppBLL.AppInfoCacheKey


> AppInfoCacheKey([String][string]  _appkey)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _appkey|[String][string] |来源请求应用key  ||

  * 返回

   > QuickAppInfo 

## AppInfoCacheKey  `缓存的AppInfo`

 方法:  QuickAppBLL.AppInfoCacheKey


> AppInfoCacheKey([Int32][int32]  _appID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _appID|[Int32][int32] |  ||

## RemoveCacheAppInfoByKey  `移除缓存的应用信息`

 方法:  QuickAppBLL.RemoveCacheAppInfoByKey


> RemoveCacheAppInfoByKey([String][string]  _appkey)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _appkey|[String][string] |来源请求应用key  ||

## GetInfoByKey  `取出应用信息`

 方法:  QuickAppBLL.GetInfoByKey


> GetInfoByKey([String][string]  _appkey)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _appkey|[String][string] |应用配置key  ||

  * 备注 

      首次查数据库，后面进行应用缓存处理

## GetInfoByID  `取出应用信息`

 方法:  QuickAppBLL.GetInfoByID


> GetInfoByID([Int32][int32]  _appID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _appID|[Int32][int32] |来自应用ID  ||

## GetInfoBySql  `从数据库取应用信息 QF_AppInfo`

 方法:  QuickAppBLL.GetInfoBySql


> GetInfoBySql([String][string]  appkey)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | appkey|[String][string] |应用编码  ||

  * 返回

   > 返回服务器解密 

## GetInfoBySql  `取出应用信息`

 方法:  QuickAppBLL.GetInfoBySql


> GetInfoBySql([Int32][int32]  _AppID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _AppID|[Int32][int32] |应用编号  ||

## GetInfoBySql  `取出应用信息`

 方法:  QuickAppBLL.GetInfoBySql


> GetInfoBySql([QueryParam][queryparam]  qp)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | qp|[QueryParam][queryparam] |  ||



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[queryparam]:../Quick.DB/QueryParam.md


