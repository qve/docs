---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# MSSQL.TableColumeToForgeinKey
- 引用：Quick.DB.MSSQL.TableColumeToForgeinKey


## MSSQL.TableColumeToForgeinKey  `主外键关系表`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Main_ShowColName|  | 外键显示的描述 |



