---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# BLL.QuickORMSqlBLL
- 引用：Quick.DB.BLL.QuickORMSqlBLL


## BLL.QuickORMSqlBLL  `平台数据对象SQL方法`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | cacheKeyPre|  | 缓存的ORM前缀 <br /> cacheKeyPre + modelTable[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) |
 | modelTable|  | 源数据对象QF_Model，QF_API |
- **构造函数**

  - BLL.QuickORMSqlBLL()
       `默认数据库名 Quick`


  - BLL.QuickORMSqlBLL([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _modelTable)
       `自定义操作数据表`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _modelTable |QF_Model,QF_API


## GetModelBySql  `数据表对象管理`

 方法:  BLL.QuickORMSqlBLL.GetModelBySql


> GetModelBySql([String][string]  code)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |QF_Model.Code对象索引  ||

  * 返回

   > ID,TableCode,DBCode,DBServer,AttrModel,AttrJoin,AttrListor,AttrEditor,StateFlag,Record,Verify 

## GetAPIDBByCode  `取出API接口定义对象`

 方法:  BLL.QuickORMSqlBLL.GetAPIDBByCode


> GetAPIDBByCode([String][string]  code)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |QF_API.Code  ||

## GetAttrJsonCache  `根据索引取出对象数据模板TPL`

 方法:  BLL.QuickORMSqlBLL.GetAttrJsonCache


> GetAttrJsonCache([String][string]  code)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |对象索引 QF_Model.code  ||

## RemoveCacheByCode  `删除数据对象配置缓存`

 方法:  BLL.QuickORMSqlBLL.RemoveCacheByCode


> RemoveCacheByCode([String][string]  code)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |对象索引 QF_Model.code  ||

  * 返回

   > attr:属性,join:外键,named:数据库名 

## GetDBTableCache  `取出对象数据表与数据库配置`

 方法:  BLL.QuickORMSqlBLL.GetDBTableCache


> GetDBTableCache([String][string]  code)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |对象索引 QF_Model.code  ||

## GetJoinCache  `取出外键配置`

 方法:  BLL.QuickORMSqlBLL.GetJoinCache


> GetJoinCache([String][string]  code)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |对象索引 QF_Model.code  ||

## BindAttrResp  `数据列表输出绑定正则`

 方法:  BLL.QuickORMSqlBLL.BindAttrResp


> BindAttrResp([Object][object]  _list,[String][string]  _attrResp)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _list|[Object][object] |源内容  ||
    | _attrResp|[String][string] |输出正则定义  ||

## Audit  `多条数据，高级审核权限`

 方法:  BLL.QuickORMSqlBLL.Audit


> Audit([String][string]  code,[String][string]  _ids,[Int64][int64]  MUser_ID,[Int32][int32]  appID,[Int32][int32]  SF_ID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |对象索引 QF_Model.code  ||
    | _ids|[String][string] |多数据,ID  ||
    | MUser_ID|[Int64][int64] |编辑者  ||
    | appID|[Int32][int32] |来源AppID  ||
    | SF_ID|[Int32][int32] |指定数据状态,1:撤销审核  ||

  * 返回

   > 返回影响的行数 

## Audit  `多条数据，标准审核`

 方法:  BLL.QuickORMSqlBLL.Audit


> Audit([String][string]  code,[String][string]  _ids,[Int64][int64]  MUser_ID,[Int32][int32]  appID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |对象索引 QF_Model.code  ||
    | _ids|[String][string] |多数据,ID  ||
    | MUser_ID|[Int64][int64] |编辑者  ||
    | appID|[Int32][int32] |来源AppID  ||

  * 返回

   > 返回影响的条数 

## Delete  `停用删除数据`

 方法:  BLL.QuickORMSqlBLL.Delete


> Delete([String][string]  code,[String][string]  _ids,[Int64][int64]  MUser_ID,[Int32][int32]  appID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |对象索引 QF_Model.code  ||
    | _ids|[String][string] |多数据,ID  ||
    | MUser_ID|[Int64][int64] |编辑者  ||
    | appID|[Int32][int32] |来源AppID  ||

  * 返回

   > 返回影响的行数 

## Erase  `物理删除数据`

 方法:  BLL.QuickORMSqlBLL.Erase


> Erase([String][string]  code,[String][string]  _ids,[Int64][int64]  MUser_ID,[Int32][int32]  appID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |对象索引 QF_Model.code  ||
    | _ids|[String][string] |多数据,ID  ||
    | MUser_ID|[Int64][int64] |编辑者  ||
    | appID|[Int32][int32] |来源AppID  ||

  * 返回

   > 返回影响的行数 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[object]:https://docs.microsoft.com/zh-cn/dotnet/api/system.object
[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32


