---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# MSSQL.TableColume
- 引用：Quick.DB.MSSQL.TableColume


## MSSQL.TableColume  `表列信息`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Colume_BitEnum|  | 布尔值显示 |
 | Colume_ShowValue|  | 字段额外提示显示值 |
 | Colume_DefaultShow|  | 外键表中用于关联默认显示的字段 |
 | Colume_DefKeyName|  | 用于显示的关键字段名(标识为def) |
 | Colume_ForeignDispName|  | 用于显示的外键字段名 |
 | Colume_ForeignKeyName|  | 外键名 |
 | Colume_ForeignTableName|  | 外键表名 |
 | Colume_IsForeignKey|  | 是否有外键 |
 | Colume_Code|  | 列代码 |
 | Colume_Name|  | 列名称 |
 | Colume_Type|  | 列类型 |
 | Colume_Length|  | 列长度 |
 | Colume_Scale|  | 小数位数 |
 | Colume_DefaultValue|  | 列默认值 |
 | Colume_IsKey|  | 是否主键 |
 | Colume_IsNullAble|  | 是否必填值 |
 | Colume_Identity|  | 是否自动增长 |
 | Colume_IsAudit|  | 是否启用数据审核 字段说明 aud 启用审核 |
 | Colume_IsEdit|  | 是否允许用户在页面上编辑 字段说明 rad 不允许 |
 | Colume_IsShow|  | 是否在页面上显示 字段说明 hid 不显示 |
 | Colume_Opts|  | 字段自定义Json 属性字符串 /FieldOpts |
- **构造函数**

  - MSSQL.TableColume()
       `构造初始值`





