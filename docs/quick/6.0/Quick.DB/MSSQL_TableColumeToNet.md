---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# MSSQL.TableColumeToNet
- 引用：Quick.DB.MSSQL.TableColumeToNet


## MSSQL.TableColumeToNet  `.net数据对应列`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | NetColume_OracleDbType|  | .net OracleType类型 |
 | ManagerCsSink|  | Manager.aspx.cs获取数据 |
 | ManagerCsSinkDef|  | 定义数据 |
 | DefaultCsSearch|  | default.aspx.cs判断查询数据 |
 | DefaultCsSink|  | default.aspx.cs获得数据 |
 | ScriptCheckTitleSearch|  | 生成查询输入框js提示 |
 | ScriptCheckTitle|  | 生成增加/修改输入框js提示 |
 | NetColume_OleDbType|  | .net oledb类型 |
 | Netcolume_ConvertString|  | Sql字段转换为.net表达式 |
 | NetColume_StringTrim|  | 在数据类型转换时是否增加.Trim() |
 | NetColume_Type|  | .net数据类型 |
 | NetColume_DefaultValue|  | .net数据类型默认值 |
 | NetColume_SqlDbType|  | SqlDbType数据类型 |



