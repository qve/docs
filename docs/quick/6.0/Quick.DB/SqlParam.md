---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# SqlParam
- 引用：Quick.DB.SqlParam


## SqlParam  `存储过程参数             QueryParam`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | pars| DynamicParameters | 定义存储过程查询参数集合 |
- **构造函数**

  - SqlParam()
       `构造`



## Add  `添加`

 方法:  SqlParam.Add


> Add([String][string]  _name,[Object][object]  _value,[ParamType][paramtype]  _paramType)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _name|[String][string] |  ||
    | _value|[Object][object] |  ||
    | _paramType|[ParamType][paramtype] |  ||



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[object]:https://docs.microsoft.com/zh-cn/dotnet/api/system.object
[paramtype]:../Quick.DB/ParamType.md


