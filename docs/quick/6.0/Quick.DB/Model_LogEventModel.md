---
template: 2021.12
version: 2021/12/2 10:38:31
author: quick.docs
date: 2022年03月20日 09:23

---
# Model.LogEventModel
- 引用：Quick.DB.Model.LogEventModel
- version: 2021/12/2 10:38:31


## Model.LogEventModel  `Log_Event`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | ID|  | ID |
 | IP|  | 来源IP |
 | MAC|  | 来源MAC |
 | BrowserInfo|  | 来源浏览器 |
 | FromURL|  | 来源URL |
 | EventBody|  | 事件内容 |
 | SQLTxt|  | 执行SQL |
 | UserName|  | 用户 |
- **构造函数**

  - Model.LogEventModel()
       `默认值构造`





