---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# DB.SqlMyImpl
- 引用：Quick.DB.DB.SqlMyImpl


## DB.SqlMyImpl  `mysql 实现`


- **构造函数**

  - DB.SqlMyImpl([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    connectionString)
       `构造 初始成员`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | connectionString |数据库连接字符串


## GetDbConnection  `获取数据库连接对象`

 方法:  DB.SqlMyImpl.GetDbConnection


> GetDbConnection()


  * 返回

   > IDbConnection 





