---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# Model.RedisConfigModel
- 引用：Quick.DB.Model.RedisConfigModel


## Model.RedisConfigModel  `Redis 配置`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Host|  | 服务器连接 Redis ConnectionString |
 | DBId|  | 默认存储库序号 |



