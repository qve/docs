---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# QuickORMBLL
- 引用：Quick.DB.QuickORMBLL


## QuickORMBLL  `ORM 对象方法`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | qp|  | 数据库指向参数对象 |
- **构造函数**

  - QuickORMBLL()
       `构造ORM`


  - QuickORMBLL([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _modelTable)
       `构造自定义数据结构对象`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _modelTable |QF_Model,QF_API


## Query  `单条查询`

 方法:  QuickORMBLL.Query


> Query([String][string]  code,[Object][object]  safe,[Object][object]  pi)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |QF_Model / QF_API 数据对象索引  ||
    | safe|[Object][object] |请求验证 QuickSafeFilterModel  ||
    | pi|[Object][object] |请求传入参数 PagerInString  ||

## List  `查询 Json 数组列表,qp查询条件`

 方法:  QuickORMBLL.List


> List([String][string]  code,[Object][object]  safe,[PagerInString][pagerinstring]  pi)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |QF_Model / QF_API 数据对象索引  ||
    | safe|[Object][object] |请求验证 QuickSafeFilterModel  ||
    | pi|[PagerInString][pagerinstring] |请求传入参数 PagerInString  ||

  * 返回

   > Json 数组 

  * 备注 

      按输出规则配置替换内容

## ListByJoin  `外键查询数据`

 方法:  QuickORMBLL.ListByJoin


> ListByJoin([String][string]  code,[Object][object]  safe,[PagerInString][pagerinstring]  pi)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |QF_Model / QF_API 数据对象索引  ||
    | safe|[Object][object] |请求验证 QuickSafeFilterModel  ||
    | pi|[PagerInString][pagerinstring] |请求传入参数 PagerInString  ||

## Add  `插入数据`

 方法:  QuickORMBLL.Add


> Add([String][string]  code,[PageCmd][pagecmd]  pc,[Object][object]  safe)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |  ||
    | pc|[PageCmd][pagecmd] |  ||
    | safe|[Object][object] |  ||

  * 返回

   > 返回影响的行数 

## Add  `添加数据，支持批量新增`

 方法:  QuickORMBLL.Add


> Add([String][string]  code,[PageCmd][pagecmd]  pc,[Object][object]  safe,[Int32][int32]  action)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |对象索引 QF_Model.code  ||
    | pc|[PageCmd][pagecmd] |传入参数, obj 单个对象或者数组  ||
    | safe|[Object][object] |请求验证  ||
    | action|[Int32][int32] |数据插入方式1:返回影响行数,5:返回最新插入ID  ||

  * 返回

   > 返回行数或者ID 

## Edit  `数据编辑与批量更新`

 方法:  QuickORMBLL.Edit


> Edit([String][string]  code,[PageCmd][pagecmd]  pc,[Object][object]  safe)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |表索引  ||
    | pc|[PageCmd][pagecmd] |传入参数  ||
    | safe|[Object][object] |请求验证  ||

  * 备注 

      pc.id大于0 为单条编辑



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[object]:https://docs.microsoft.com/zh-cn/dotnet/api/system.object
[pagerinstring]:../Quick.DB/PagerInString.md
[pagecmd]:../Quick.DB/PageCmd.md
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32


