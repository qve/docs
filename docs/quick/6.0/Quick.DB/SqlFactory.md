---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# SqlFactory
- 引用：Quick.DB.SqlFactory


## SqlFactory  `数据工厂`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | dbCode|  | 当前连接命名 |
- **构造函数**

  - SqlFactory([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _dbCode)
       `构造数据库连接`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _dbCode |数据库命名


## CreateInstance  `创建实例dapper`

 方法:  SqlFactory.CreateInstance


> CreateInstance()


  * 备注 

      单例模式保证只存在一个实例





