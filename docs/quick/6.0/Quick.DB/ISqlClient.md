---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# ISqlClient
- 引用：Quick.DB.ISqlClient


## ISqlClient  `数据库交互接口`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Conn| [IDbConnection](https://docs.microsoft.com/zh-cn/dotnet/api/system.data.idbconnection) | 调用数据库连接 |
 | Transaction| [IDbTransaction](https://docs.microsoft.com/zh-cn/dotnet/api/system.data.idbtransaction) | 调用事务 |



