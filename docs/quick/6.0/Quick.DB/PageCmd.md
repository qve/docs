---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# PageCmd
- 引用：Quick.DB.PageCmd


## PageCmd  `提交增删改动态数据`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | cmd| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 操作指令 QF_Control <br /> list,add,edit,del:状态删除,erase:物理删除,audit:审核,resc:超级撤销审核权限 |
 | id| [Int64](https://docs.microsoft.com/zh-cn/dotnet/api/system.int64) | 数据ID |
 | par| !:dynamic | 提交参数 |
 | where| !:dynamic | 条件参数 |
 | obj| !:dynamic | 提交对象数据 <br /> JsonElement o = pc.obj;             o.Equals(null)) |



