---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# Model.TPLSQLSelectModel
- 引用：Quick.DB.Model.TPLSQLSelectModel


## Model.TPLSQLSelectModel  `结构化查询对象`
注意 SQL注入风险

- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | table| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 查询表名 |
 | size| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 取出条数 <br /> 0 取出全部 |
 | field| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 查询字段 <br /> 多个字段用,号隔开 |
 | where| [WhereItem](../Quick.DB/WhereItem.md) | 查询条件 <br /> List |
 | sort| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 排序字段 <br /> 默认值ID 多个值用,号隔开 |
 | desc| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 排序类型 <br /> 1:降序反之升序，需指定Sort字段 |
 | group|  | 查询分组 |



