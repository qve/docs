---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# PagerTplOptsModel
- 引用：Quick.DB.PagerTplOptsModel


## PagerTplOptsModel  `UI字段属性              参阅`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | f| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 字段名称 |
 | t| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 字段类型 |
 | c| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | UI编辑控件名 |
 | g| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 自定义内容编码格式 <br /> encode,base64 |
 | n| [Boolean](https://docs.microsoft.com/zh-cn/dotnet/api/system.boolean) | 必填字段 |
 | r| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 字段只读             1不可编辑 |
 | h| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 标记列不显示字段 <br /> 1不显示 |
 | a| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 标记为审核状态字段 <br /> 1 唯一审核字段 |
 | d| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 标记为外键描述字段 <br /> 1 唯一外键查询显示字段             不是必须 |
 | ls| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 多选项 <br /> 按此规则创建kv下拉选项 |
 | max| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 限制最大长度 <br /> 按此规则创建kv下拉选项 |
 | tip|  | 字段 提示 |
 | api|  | ui 控件数据源 |



