---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# QueryJoin
- 引用：Quick.DB.QueryJoin


## QueryJoin  `连接查询参数`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | outer| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 外键查询的连接方式 <br /> LEFT OUTER JOIN             right outer join |
 | tPrimary| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 主表主键字段名 .t |
 | table| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 外键查询对象表 <br /> 别名为小写字母 j+序号 |
 | primary| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 外键查询字段名 <br /> 默认ID |
 | fields| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 外键查询输出字段 <br /> 多个字段用逗号隔开 |
- **构造函数**

  - QueryJoin()
       `连接查询参数 默认构造`


  - QueryJoin([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _tField,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _cTable)
       `连接查询参数，默认left outer join,ID,title`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _tField |主键
      | _cTable |外键表

  - QueryJoin([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _tField,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _cTable,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _cTitle)
       `连接查询参数，默认left outer join,ID`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _tField |主键
      | _cTable |外键表 as,c1,c2,c3
      | _cTitle |外键描述字段

  - QueryJoin([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _tField,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _cTable,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _cField,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _cTitle)
       `连接查询参数，默认left outer join`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _tField |主键
      | _cTable |外键表
      | _cField |外键字段
      | _cTitle |外键描述字段

  - QueryJoin([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _tField,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _type,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _cTable,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _cField,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _cTitle)
       `连接查询参数`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _tField |主键
      | _type |方式 left outer join,right outer join
      | _cTable |外键表
      | _cField |外键字段
      | _cTitle |外键描述字段




