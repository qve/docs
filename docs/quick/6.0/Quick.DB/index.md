---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23
---

# Quick.DB

template: 2021.12
author: quick.docs
date: 2022 年 03 月 20 日 09:23

## Quick.DB 目录

### [DataSafe](./DataSafe.md)

      数据权限处理方法

### [QuickLogDB](./QuickLogDB.md)

      扩展日志方法

### [QuickORMAPIBLL](./QuickORMAPIBLL.md)

      DI 请求权限处理接口

### [BLL.QuickORMAttrBLL](./BLL_QuickORMAttrBLL.md)

      属性规则方法

### [BLL.QuickORMSqlBLL](./BLL_QuickORMSqlBLL.md)

      平台数据对象SQL方法

### [BLL.QuickServerBLL](./BLL_QuickServerBLL.md)

      数据服务器方法

### [QuickORMBLL](./QuickORMBLL.md)

      ORM 对象方法

### [ISqlClient](./ISqlClient.md)

      数据库交互接口

### [QueryJoin](./QueryJoin.md)

      连接查询参数

### [QueryParam](./QueryParam.md)

      查询参数类

### [QuickDAL](./QuickDAL.md)

      数据库连接层

### [QuickData](./QuickData.md)

      MSSQL 数据库 ORM 读写连接

### [SqlClient_T](./SqlClient_T.md)

      数据库交互方法

### [SqlFactory](./SqlFactory.md)

      数据工厂

### [SqlImpl](./SqlImpl.md)

      sql 连接基础方法

### [SqlParam](./SqlParam.md)

      存储过程参数
            QueryParam

### [ParamType](./ParamType.md)

      存储过程参数类型

### [WhereItem](./WhereItem.md)

      数据表字段查询对象

### [DataType](./DataType.md)

      获取数据类型

### [QueryMode](./QueryMode.md)

      查询条件方式,小写

### [QuickDBEnum](./QuickDBEnum.md)

      数据参数

### [QuickDBEnum.StateFlag](./QuickDBEnum_StateFlag.md)

      数据状态说明

### [QuickDBEnum.StateFlagIcon](./QuickDBEnum_StateFlagIcon.md)

      数据状态 icon-名称

### [Model.DataSafeModel](./Model_DataSafeModel.md)

      通用数据权限对象

### [Model.DBConfigModel](./Model_DBConfigModel.md)

      数据库连接配置

### [Model.LogEventModel](./Model_LogEventModel.md)

      Log_Event

### [Model.RedisConfigModel](./Model_RedisConfigModel.md)

      Redis 配置

### [Model.TPLAPIModel](./Model_TPLAPIModel.md)

      应用接口对象

### [Model.TPLSQLSelectModel](./Model_TPLSQLSelectModel.md)

      结构化查询对象

### [PagerOutModel_T](./PagerOutModel_T.md)

      API 分页查询，返回参数对象

### [PagerIn](./PagerIn.md)

      分页传入参数

### [PagerIn_T](./PagerIn_T.md)

      分页查询自定义where参数对象

### [PagerInString](./PagerInString.md)

      分页传入查询条件where参数为字符串

### [PagerInJson](./PagerInJson.md)

      分页传入查询条件参数为Json

### [PageCmd](./PageCmd.md)

      提交增删改动态数据

### [PageCmd_T](./PageCmd_T.md)

      提交增删改数据对象

### [PagerTplModel](./PagerTplModel.md)

      数据对象模板TPL

### [PagerTplOptsModel](./PagerTplOptsModel.md)

      UI字段属性
            参阅

### [TPLAttrModel](./TPLAttrModel.md)

      数据表管理属性

### [TPLConfigModel](./TPLConfigModel.md)

      QF_Model 数据库配置

### [TPLTableModel](./TPLTableModel.md)

      QF_Model 对象约束

### [DB.SqlMyImpl](./DB_SqlMyImpl.md)

      mysql 实现

### [SqlServerImpl](./SqlServerImpl.md)

      SqlServer 实现

### [MSSQL.SqlDataForTools](./MSSQL_SqlDataForTools.md)

      SQL 构造方法

### [MSSQL.SqlDataTools](./MSSQL_SqlDataTools.md)

      数据库工具

### [MSSQL.SqlHelper](./MSSQL_SqlHelper.md)

      MSSQL 连接

### [MSSQL.TableColume](./MSSQL_TableColume.md)

      表列信息

### [MSSQL.TableColumeToForgeinKey](./MSSQL_TableColumeToForgeinKey.md)

      主外键关系表

### [MSSQL.TableColumeToNet](./MSSQL_TableColumeToNet.md)

      .net数据对应列

### [MSSQL.TableEntity](./MSSQL_TableEntity.md)

      表实体类

### [MSSQL.TableEntityNet](./MSSQL_TableEntityNet.md)

      Table映射.net

### [MSSQL.TableEntityNets](./MSSQL_TableEntityNets.md)

      表实体类

### [MSSQL.Table.TableFieldModel](./MSSQL_Table_TableFieldModel.md)

      表字段明细

### [SqlPostgreImpl](./SqlPostgreImpl.md)

      PostgreSql 实现

### [QuickAppBLL](./QuickAppBLL.md)

      应用架构方法，获取应用配置

### [QuickDBBLL](./QuickDBBLL.md)

      数据库公共工具类

### [RedisBLL](./RedisBLL.md)

      公共方法

### [RedisHelper](./RedisHelper.md)

      封装方法

### [RedisKeys](./RedisKeys.md)

      Redis 统一存储Keys规则

### [RedisKeys.Token](./RedisKeys_Token.md)

      令牌

### [RedisKeys.Cell](./RedisKeys_Cell.md)

      格子库定义
