---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# RedisKeys.Cell
- 引用：Quick.DB.RedisKeys.Cell


## RedisKeys.Cell  `格子库定义`



## key  `标准格子对象`

 方法:  RedisKeys.Cell.key


> key([Int64][int64]  CellID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | CellID|[Int64][int64] |格子id  ||

  * 返回

   > sc:{CellID} 

## keyAll  `取出格子集合ID`

 方法:  RedisKeys.Cell.keyAll


> keyAll([Int32][int32]  GroupID,[Int32][int32]  Channel,[Int32][int32]  SLGroupID,[Int32][int32]  NasID,[Int32][int32]  APID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | GroupID|[Int32][int32] |  ||
    | Channel|[Int32][int32] |  ||
    | SLGroupID|[Int32][int32] |  ||
    | NasID|[Int32][int32] |  ||
    | APID|[Int32][int32] |  ||

  * 返回

   > scg:{0}:{1}:{2}:{3}:{4} 

## GroupEx  `组织禁用的格子id`

 方法:  RedisKeys.Cell.GroupEx


> GroupEx([Int32][int32]  GroupID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | GroupID|[Int32][int32] |组织id  ||

  * 返回

   > ex:g:GroupID 

  * 备注 

      老版本是:  exgid:{0}

## News  `新闻内容格子key`

 方法:  RedisKeys.Cell.News


> News([Int64][int64]  CellID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | CellID|[Int64][int64] |格子ID  ||

  * 返回

   > scn:CellID 

## NewsGroup  `发布新闻组织频道关系`

 方法:  RedisKeys.Cell.NewsGroup


> NewsGroup([Int32][int32]  GroupID,[Int32][int32]  Channel)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | GroupID|[Int32][int32] |发布组织ID，默认1：所有人看  ||
    | Channel|[Int32][int32] |发布频道ID，默认频道5：公共平台  ||

  * 返回

   > scn:g:1:5 



[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32


