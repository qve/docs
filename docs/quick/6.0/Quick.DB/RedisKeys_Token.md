---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# RedisKeys.Token
- 引用：Quick.DB.RedisKeys.Token


## RedisKeys.Token  `令牌`



## UserTokenAccess  `令牌集合 会话缓存子项 u:L:UserID`

 方法:  RedisKeys.Token.UserTokenAccess


> UserTokenAccess([Int32][int32]  AppID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | AppID|[Int32][int32] |应用ID  ||

  * 返回

   > AppID_us 

## UserTokenRefresh  `刷新票据缓存子项`

 方法:  RedisKeys.Token.UserTokenRefresh


> UserTokenRefresh([Int32][int32]  AppID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | AppID|[Int32][int32] |应用ID  ||

  * 返回

   > AppID_ur 

## RefreshToken  `刷新令牌 绑定用户的登陆ID,默认超时30天`

 方法:  RedisKeys.Token.RefreshToken


> RefreshToken([Int32][int32]  AppID,[String][string]  refreshToken)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | AppID|[Int32][int32] |应用ID  ||
    | refreshToken|[String][string] |刷新令牌  ||

  * 返回

   > u:r:{AppID}:{refreshToken} 

## LoginUserKey  `用户已登陆ID 绑定的SessionID`

 方法:  RedisKeys.Token.LoginUserKey


> LoginUserKey([Int64][int64]  LoginID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | LoginID|[Int64][int64] |登陆ID  ||

  * 返回

   > u:L:{LoginID} 

## APUserLoginKey  `wifi登陆账户缓存`

 方法:  RedisKeys.Token.APUserLoginKey


> APUserLoginKey([Int64][int64]  UserID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | UserID|[Int64][int64] |用户ID  ||

  * 返回

   > u:w:UserID 



[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64


