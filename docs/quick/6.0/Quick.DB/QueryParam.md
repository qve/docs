---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# QueryParam
- 引用：Quick.DB.QueryParam


## QueryParam  `查询参数类`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | sql| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 自定义语句 <br /> 慎用，有注入风险 |
 | Trans|  | 多事务执行 <br /> 注意注入风险 |
 | Action| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 执行动作 <br /> 1:插入,2:更新,4:删除,5:插入返回最新ID,6:批量插入json |
 | ORM| [Object](https://docs.microsoft.com/zh-cn/dotnet/api/system.object) | 操作数据对象 |
 | DBName| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 数据库连接命名 <br /> QF_Server |
 | TableName| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 配置表命名 <br /> QF_Model,QF_API |
 | Alias| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 主表设置别名 <br /> 字段返回前缀，默认:t |
 | Output| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 增删改             输出字段名 <br /> Output inserted.ID 返回最新  deleted.返回历史值 |
 | Field| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 查询字段 <br /> 默认* 号取全部，自定义多字段,号隔开 |
 | Join| [QueryJoin](../Quick.DB/QueryJoin.md) | 外键连接查询 <br /> List |
 | Wheres| [WhereItem](../Quick.DB/WhereItem.md) | 表字段对象 <br /> 增删改查,结构化条件 |
 | Outer| [TPLSQLSelectModel](../Quick.DB/Model_TPLSQLSelectModel.md) | 连接查询 <br /> outer apply |
 | GroupBy| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 分组条件 |
 | Sort| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 排序字段 <br /> 默认值ID 多个值用,号隔开 |
 | Desc| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 排序类型 <br /> 1:降序反之升序，需指定Sort字段 |
 | Lock| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | sql 锁 <br /> 默认 with(paglock) |
 | SPName| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 存储过程名 |
 | SParas| [SqlParam](../Quick.DB/SqlParam.md) | 存储过程参数条件 |
 | SPError| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 返回存储过程中的错误信息 |
 | PageTotal| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 总页数 |
 | PageIndex| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 需查询页码 |
 | PageSize| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 分页记录数 <br /> 默认为0 不分页,大于1分页 |
 | ReturnCount| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 查询返回总记录数 |
 | ReturnSQL| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | ORM执行SQL语句 |
- **构造函数**

  - QueryParam()
       `默认查询参数             指向架构数据库：QuickData.DBBase`


  - QueryParam([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _dbCode)
       `指定数据库查询`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _dbCode |数据库连接配置命名




