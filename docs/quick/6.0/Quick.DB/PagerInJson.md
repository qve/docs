---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# PagerInJson
- 引用：Quick.DB.PagerInJson


## PagerInJson  `分页传入查询条件参数为Json`
继承[PagerIn](../Quick.DB/PagerIn.md)

- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | where| [WhereItem](../Quick.DB/WhereItem.md) | 查询条件 <br /> List |



