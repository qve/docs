---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# DataSafe
- 引用：Quick.DB.DataSafe


## DataSafe  `数据权限处理方法`
[示例](../code/safe.html) 


## Change  `数据审核状态变更`

 方法:  DataSafe.Change


> Change([Int32][int32]  SF_ID,[String@][string@]  actStr)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | SF_ID|[Int32][int32] |当前状态  ||
    | actStr|[String@][string@] |输出结果反馈  ||

  * 返回

   > 返回结果1:正常,2:已审,3:撤审 

## GetStateIcon  `返回状态样式`

 方法:  DataSafe.GetStateIcon


> GetStateIcon([Int32][int32]  SF_ID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | SF_ID|[Int32][int32] |状态ID  ||

  * 返回

   > icon-名称 

## GetStateName  `返回状态描述`

 方法:  DataSafe.GetStateName


> GetStateName([Int32][int32]  SF_ID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | SF_ID|[Int32][int32] |状态ID  ||

  * 返回

   > QuickEnum.GetDescription 



[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[string@]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string@


