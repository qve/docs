---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# Model.DBConfigModel
- 引用：Quick.DB.Model.DBConfigModel


## Model.DBConfigModel  `数据库连接配置`
QF_Server

- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | DBCode|  | 数据库连接命名 |
 | DBType|  | 数据库类型 <br /> 1:Redis,2:MSSQL3:PostgreSql |
 | DBConnect|  | 数据库连接 |
 | DBPassType|  | 加密方式 <br />  &#39;ls &#39;: &#39;1:Text,2:DES &#39; |
 | DBPassKey|  | 加密的密钥 |
- **构造函数**

  - Model.DBConfigModel()
       `构造`





