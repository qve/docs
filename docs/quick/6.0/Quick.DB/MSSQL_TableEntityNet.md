---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# MSSQL.TableEntityNet
- 引用：Quick.DB.MSSQL.TableEntityNet


## MSSQL.TableEntityNet  `Table映射.net`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | NameSpace|  | 命名空间 |
 | DateTime|  | 时间 |
 | TableName|  | 表名说明 |
 | TableCode|  | 表名代码 |
 | IsView|  | 是否为视图 |
 | ReturnType|  | 返回值 |
 | TableColume|  | 所有列 |
 | TableColumeKey|  | 主键列 |
 | TableColumeIdentity|  | 自动id列 |
 | TableColumeNoIdentity|  | 非自动id列 |



