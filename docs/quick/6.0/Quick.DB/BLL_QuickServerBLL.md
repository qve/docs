---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# BLL.QuickServerBLL
- 引用：Quick.DB.BLL.QuickServerBLL


## BLL.QuickServerBLL  `数据服务器方法`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | cacheConnPre|  | 连接字符串缓存规则key |

## GetConnect  `取出数据库连接`

 方法:  BLL.QuickServerBLL.GetConnect


> GetConnect([String][string]  _dbCode)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _dbCode|[String][string] |  ||

  * 调试信息


## GetConnectByDBCode  `取出数据库连接字符串`
- version: 6.0.1

 方法:  BLL.QuickServerBLL.GetConnectByDBCode


> GetConnectByDBCode([String][string]  _dbCode)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _dbCode|[String][string] |数据库连接命名  ||

  * 返回

   > QF_Server，并缓存2小时 

  * 备注 

      1. QuickCache 2. QuickSettings 3. QuickServer             cacheKey db:sql:connt:

## GetConfigByDBCode  `取出服务器连接配置`
- version: 6.0.1

 方法:  BLL.QuickServerBLL.GetConfigByDBCode


> GetConfigByDBCode([String][string]  _dbCode)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _dbCode|[String][string] |连接命名  ||

  * 返回

   > QF_Server 

  * 备注 

      



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string


