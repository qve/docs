---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# Model.TPLAPIModel
- 引用：Quick.DB.Model.TPLAPIModel


## Model.TPLAPIModel  `应用接口对象`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | DBSQL|  | SQL语句 \ &quot;c\ &quot;:\ &quot;code\ &quot; |
 | DBSP|  | 存储过程 |



