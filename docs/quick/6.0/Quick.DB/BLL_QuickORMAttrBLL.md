---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# BLL.QuickORMAttrBLL
- 引用：Quick.DB.BLL.QuickORMAttrBLL


## BLL.QuickORMAttrBLL  `属性规则方法`



## GetJoinByField  `外键字段取出数据表配置`

 方法:  BLL.QuickORMAttrBLL.GetJoinByField


> GetJoinByField([String@][string@]  dbName,[String][string]  code,[String][string]  field)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | dbName|[String@][string@] |返回配置的数据库名  ||
    | code|[String][string] |对象索引 QF_Model.code  ||
    | field|[String][string] |主表外键字段  ||

  * 返回

   > 外键配置 

## BindAttrJoin  `绑定外键查询`

 方法:  BLL.QuickORMAttrBLL.BindAttrJoin


> BindAttrJoin([String][string]  _join)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _join|[String][string] |配置字符串  ||

  * 备注 

      QF_Model.AttrJoin

## GetPanelByJoin  `外键查询面板 列表参数`

 方法:  BLL.QuickORMAttrBLL.GetPanelByJoin


> GetPanelByJoin([String][string]  code,[String][string]  field)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |对象索引 QF_Model.code  ||
    | field|[String][string] |配置的外键查询字段  ||

  * 返回

   > listor.th 

## GetListor  `取出列表数据模板`

 方法:  BLL.QuickORMAttrBLL.GetListor


> GetListor([String][string]  code)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |对象索引 QF_Model.code  ||

## GetEditor  `编辑模板参数`

 方法:  BLL.QuickORMAttrBLL.GetEditor


> GetEditor([String][string]  code)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | code|[String][string] |对象索引 QF_Model.code  ||

## GetOuterByReq  `外连查询`

 方法:  BLL.QuickORMAttrBLL.GetOuterByReq


> GetOuterByReq([JsonElement][jsonelement]  o,[Object][object]  _pager,[Object][object]  _safe)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | o|[JsonElement][jsonelement] |  ||
    | _pager|[Object][object] |请求参数参数 pi,pc  ||
    | _safe|[Object][object] |请求验证参数 QuickSafeFilterModel   ||

## GetWhereByReq  `取出配置查询参数 QueryMode`

 方法:  BLL.QuickORMAttrBLL.GetWhereByReq


> GetWhereByReq([JsonElement][jsonelement]  wheresCount,[Int32][int32]  whereList,[Object][object]  _pager,[Object][object]  _safe)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | wheresCount|[JsonElement][jsonelement] |传入查询参数的数量  ||
    | whereList|[Int32][int32] |配置的查询参数  ||
    | _pager|[Object][object] |请求参数参数 pi,pc  ||
    | _safe|[Object][object] |请求验证参数 QuickSafeFilterModel   ||

## GetWhereSqlByParam  `取出请求配置参数对象转为SQL查询`

 方法:  BLL.QuickORMAttrBLL.GetWhereSqlByParam


> GetWhereSqlByParam([JsonElement][jsonelement]  _param,[Object][object]  _pager,[Object][object]  _safe)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _param|[JsonElement][jsonelement] |JsonElement  ||
    | _pager|[Object][object] |请求参数参数 pi,pc  ||
    | _safe|[Object][object] |请求验证参数 QuickSafeFilterModel   ||

## GetValueByParamString  `SQL 请求配置参数取值`

 方法:  BLL.QuickORMAttrBLL.GetValueByParamString


> GetValueByParamString([JsonElement][jsonelement]  _param,[Object][object]  _pager,[Object][object]  _safe)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _param|[JsonElement][jsonelement] |JsonElement  ||
    | _pager|[Object][object] |传入接口请求参数 pi,pc  ||
    | _safe|[Object][object] |请求验证参数 QuickSafeFilterModel   ||

## BindAttrByReq  `正则绑定配置参数与对象值`

 方法:  BLL.QuickORMAttrBLL.BindAttrByReq


> BindAttrByReq([Object][object]  safe,[Object][object]  _pager,[Object][object]  obj,[JsonElement][jsonelement]  reqs)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | safe|[Object][object] |请求安全对象  ||
    | _pager|[Object][object] |请求参数  pi,pc  ||
    | obj|[Object][object] |编辑对象字典  ||
    | reqs|[JsonElement][jsonelement] |请求配置参数指令名 list,add,edit  ||

## BindTransactionByReq  `事务绑定`

 方法:  BLL.QuickORMAttrBLL.BindTransactionByReq


> BindTransactionByReq([Object][object]  _safe,[Object][object]  _pager,[JsonElement][jsonelement]  _trans)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _safe|[Object][object] |请求权限  ||
    | _pager|[Object][object] |传入参数  ||
    | _trans|[JsonElement][jsonelement] |事务  ||



[string@]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string@
[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[jsonelement]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonelement
[object]:https://docs.microsoft.com/zh-cn/dotnet/api/system.object
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32


