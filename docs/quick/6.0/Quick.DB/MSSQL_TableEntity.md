---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# MSSQL.TableEntity
- 引用：Quick.DB.MSSQL.TableEntity


## MSSQL.TableEntity  `表实体类`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | IsIdentity|  | 是否包含自动ID |
 | IsView|  | 是否视图 |
 | DataBaseCode|  | 数据库名称 |
 | TableCode|  | 表名代码 |
 | TableName|  | 表描述 |
 | TableColume|  | 列定义集合 |



