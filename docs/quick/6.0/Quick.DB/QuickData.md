---
template: 2021.12
version: 2021.12.3
author: quick.docs
date: 2022年03月20日 09:23

---
# QuickData
- 引用：Quick.DB.QuickData
- version: 2021.12.3


## QuickData  `MSSQL 数据库 ORM 读写连接`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | DBQuick|  | Quick框架数据库  Quick |
 | DBQuickPlus|  | 旧架构数据库,微信 QuickDB |
 | DBBase|  | 基础数据库,Base |
 | DBPlus|  | 外挂应用数据库 Plus |
 | DBNAS|  | 云设备数据库 NAS |
 | DBLog|  | 日志数据库 Log |
 | DBLib|  | 公共资源类库，图片 Lib |
 | DBMIS|  | 信息集成库 MIS |
 | DBBA|  | 商业数据分析 BA |

## GetConnect  `数据库连接`

 方法:  QuickData.GetConnect


> GetConnect([String][string]  _dbCode)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _dbCode|[String][string] |连接命名  ||

## Query  `查询单条转换为对象`

 方法:  QuickData.Query


> Query&lt;T&gt;([QueryParam][queryparam]  qp)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | qp|[QueryParam][queryparam] |查询条件  ||

  * 返回

   > 返回对象或者Json，无对象返回Null 

## Query  `查询单条Json`

 方法:  QuickData.Query


> Query([QueryParam][queryparam]  qp)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | qp|[QueryParam][queryparam] |查询条件  ||

## QueryByProc  `执行存储过程查询`

 方法:  QuickData.QueryByProc


> QueryByProc([QueryParam][queryparam]  qp)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | qp|[QueryParam][queryparam] |DBName,SPName,SParam  ||

## QueryByProc  `执行存储过程查询`

 方法:  QuickData.QueryByProc


> QueryByProc&lt;T&gt;([QueryParam][queryparam]  qp)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | qp|[QueryParam][queryparam] |查询条件  ||

## List  `数据查询转换为对象数组`

 方法:  QuickData.List


> List&lt;T&gt;([QueryParam][queryparam]  qp)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | qp|[QueryParam][queryparam] |查询条件  ||

  * 返回

   > 返回数据集 

## List  `任意查询为 Json数组`

 方法:  QuickData.List


> List([QueryParam][queryparam]  qp)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | qp|[QueryParam][queryparam] |查询条件  ||

## Action  `ORM 执行动作 1:新增，2:修改，4:删除,5:插入返回最新的ID`

 方法:  QuickData.Action


> Action([QueryParam][queryparam]  qp)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | qp|[QueryParam][queryparam] |参数, 需绑定 qp.ORM，qp.sql 无效  ||

  * 返回

   > 影响条数 

## Execute  `手动执行 (注意： SQL语句注入安全)`

 方法:  QuickData.Execute


> Execute([QueryParam][queryparam]  qp)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | qp|[QueryParam][queryparam] |插入条件  ||

  * 返回

   > 影响条数 -2:语句错误 

## ExecuteByProc  `执行存储过程`

 方法:  QuickData.ExecuteByProc


> ExecuteByProc([QueryParam][queryparam]  qp)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | qp|[QueryParam][queryparam] |传入参数  ||

  * 返回

   > 返回参数 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[queryparam]:../Quick.DB/QueryParam.md


