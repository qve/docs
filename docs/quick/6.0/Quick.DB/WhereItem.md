---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# WhereItem
- 引用：Quick.DB.WhereItem


## WhereItem  `数据表字段查询对象`
qp.Wheres

- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | left| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 左边参数 <br /> QuickDAL.GetWhereFlag |
 | field| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 查询字段名 <br /> 多个用逗号隔开 |
 | value| [Object](https://docs.microsoft.com/zh-cn/dotnet/api/system.object) | 查询值 |
 | param| dynamic | 字段动态映射参数 <br /> JsonElement              TPLSQLSelectModel             String:定义sql语句有注入风险 |
 | mode| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 查询条件 Mode <br /> QuickDAL.GetWhereMode |
 | right| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 右边参数 <br /> QuickDAL.GetWhereFlag |
- **构造函数**

  - WhereItem()
       `构造数据表字段参数对象`


  - WhereItem([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _field,[Object](https://docs.microsoft.com/zh-cn/dotnet/api/system.object)    _value)
       `查询条件，默认=`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _field |字段名称
      | _value |查询值

  - WhereItem([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _field,[Object](https://docs.microsoft.com/zh-cn/dotnet/api/system.object)    _value,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _mode)
       `查询值`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _field |字段名
      | _value |查询值
      | _mode |查询条件

  - WhereItem([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _field,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _mode,[TPLSQLSelectModel](../Quick.DB/Model_TPLSQLSelectModel.md)    _param)
       `查询参数`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _field |字段名
      | _mode |查询条件
      | _param |查询参数




