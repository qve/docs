---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# PagerIn&lt;T&gt;
- 引用：Quick.DB.PagerIn&lt;T&gt;


## PagerIn&lt;T&gt;  `分页查询自定义where参数对象`
继承：[PagerIn](../Quick.DB/PagerIn.md)

- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | where| !:T | 查询条件 |



