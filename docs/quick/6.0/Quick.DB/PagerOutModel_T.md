---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# PagerOutModel&lt;T&gt;
- 引用：Quick.DB.PagerOutModel&lt;T&gt;


## PagerOutModel&lt;T&gt;  `API 分页查询，返回参数对象`
继承 : MessageBaseModel

- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | index| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 第几页 |
 | total| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 总页数 |
 | count| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 总记录数 |
 | tpl| [PagerTplModel](../Quick.DB/PagerTplModel.md) | 前端输出模板 |
- **构造函数**

  - PagerOutModel&lt;T&gt;()
       `分页查询`





