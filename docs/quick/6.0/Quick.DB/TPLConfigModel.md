---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# TPLConfigModel
- 引用：Quick.DB.TPLConfigModel


## TPLConfigModel  `QF_Model 数据库配置`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Reqs|  | 请求参数 <br /> pi:list,pc:请求,safe:验证 |
 | StateFlag|  | 启用审核\ &quot;tip\ &quot;:\ &quot;数据审批\ &quot;,\ &quot;ls\ &quot;:\ &quot;0:未用,1:启用\ &quot; |
 | LogFlag|  | 启用操作日志\ &quot;tip\ &quot;:\ &quot;写入操作日志库\ &quot;,\ &quot;ls\ &quot;:\ &quot;0:未用,1:启用\ &quot; |



