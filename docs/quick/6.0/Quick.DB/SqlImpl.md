---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:23

---
# SqlImpl
- 引用：Quick.DB.SqlImpl


## SqlImpl  `sql 连接基础方法`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | DataType|  | c# 非字符数据类型 用作拼接sql |
- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | DbConnection|  | 数据库连接对象 |
 | DbTransaction|  | 事务对象 |
 | CommandTimeout|  | 连接超时时间 默认15秒 |
 | ConnectionString|  | 数据库连接字符串 |
- **构造函数**

  - SqlImpl([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    connectionString)
       `构造数据库连接`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | connectionString |数据库连接字符串


## GetDbConnection  `获取数据库连接对象`

 方法:  SqlImpl.GetDbConnection


> GetDbConnection()


  * 返回

   > IDbConnection 





