---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickSettings.DB
- 引用：Quick.QuickSettings.DB


## QuickSettings.DB  `数据库`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | PassType|  | 加密方式1:明文,2:DES |
 | PassKey|  | DES密钥 最长8位 <br /> 不建议经常变动，会导致历史解密问题 |

## Connection  `数据库连接配置`

 方法:  QuickSettings.DB.Connection


> Connection&lt;T&gt;([String][string]  dbCode,[String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | dbCode|[String][string] |数据库连接命名  ||
    | key|[String][string] |配置属性  ||

  * 返回

   > 返回内容 

  * 备注 

      DB 数据库配置

## Redis.GetHostByKey  `获取服务器连接`

 方法:  QuickSettings.DB.Redis.GetHostByKey


> Redis.GetHostByKey([String][string]  _key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _key|[String][string] |配置节点名  ||

  * 返回

   > DB:Redis:_key:Host 

## Redis.GetHostByDB  `获取服务器连接`

 方法:  QuickSettings.DB.Redis.GetHostByDB


> Redis.GetHostByDB([String][string]  _key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _key|[String][string] |配置节点名  ||

  * 返回

   > DB:Redis:_key:DB 

## Redis.GetJson  `读取自定义配置 Json对象`

 方法:  QuickSettings.DB.Redis.GetJson


> Redis.GetJson&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |关键词  ||

  * 返回

   > RedisConfigModel 

## Redis.GetValue  `读取配置值`

 方法:  QuickSettings.DB.Redis.GetValue


> Redis.GetValue&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||

  * 返回

   > DB:Redis: 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string


