---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickEnumModel.User
- 引用：Quick.QuickEnumModel.User


## QuickEnumModel.User  `网络会员状态`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | activeNot|  | 未激活 |
 | active|  | 活动 |
 | close|  | 关闭 |
 | deregistration|  | 已注销 |
 | lose|  | 挂失 |
 | overdue|  | 已过期 |



