---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickSettings.Session.Cache
- 引用：Quick.QuickSettings.Session.Cache


## QuickSettings.Session.Cache  `会话redis缓存配置`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Expire|  | 会话超时(分钟数) |
 | ExpireRefresh|  | 票据过期刷新有效期：(分钟数) 默认30天 |



