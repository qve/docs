---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# JsonHelper.JsonType
- 引用：Quick.JsonHelper.JsonType


## JsonHelper.JsonType  `json 数据类型`



  - **示例**

```csharp

            private enum JsonType{
             @string, number, boolean, @object, array, @null 
             }
            
```



