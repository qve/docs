---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# Model.QuickTokenAuthModel
- 引用：Quick.Model.QuickTokenAuthModel


## Model.QuickTokenAuthModel  `返回的认证票据`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | userID|  | 当前用户ID |
 | roleID|  | 角色组ID |
 | groupID|  | 组织ID |
 | nickName|  | 用户昵称 |
 | access_token|  | 接口调用凭证 |
 | expires_in|  | access_token接口调用凭证超时时间，默认（7200秒） |
 | refresh_token|  | 用户刷新access_token |
- **构造函数**

  - Model.QuickTokenAuthModel()
       `用户登陆凭证`





