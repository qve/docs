---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickEnumModel.UserAgentApp
- 引用：Quick.QuickEnumModel.UserAgentApp


## QuickEnumModel.UserAgentApp  `浏览器 请求来源自定义应用`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | notFound|  | 未识别 |
 | micromessenger|  | 微信请求 |
 | qq|  | QQ 请求 |
 | alipayclient|  | 支付宝 |



