---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickSettings
- 引用：Quick.QuickSettings


## QuickSettings  `配置节点`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Log.Path|  | Text存储路径 |
 | Log.IsWrite|  | 是否写入非错误日志，默认写入 |
 | Log.CacheMaxInt|  | 缓冲写入日志的条数，默认1条 |
 | Log.CacheMaxBack|  | 当前文本超过行数就备份,默认8000行 |
 | App.ID|  | 应用 自定义ID |
 | App.Key|  | 应用 Key |
 | App.Secret|  | 应用 加密 密文 |
 | Session.Cookie.Name|  | Session 客户端 cookie 名称 |
 | Session.Cookie.Timeout|  | 客户端超时(分钟数) |
 | Session.Cache.Expire|  | 会话超时(分钟数) |
 | Session.Cache.ExpireRefresh|  | 票据过期刷新有效期：(分钟数) 默认30天 |
 | Session.WithOrigins|  | 跨域授权域名 |
 | DB.PassType|  | 加密方式1:明文,2:DES |
 | DB.PassKey|  | DES密钥 最长8位 <br /> 不建议经常变动，会导致历史解密问题 |

## GetSection  `读取自定义配置 Json节点对象`

 方法:  QuickSettings.GetSection


> GetSection&lt;T&gt;([String][string]  key,[String][string]  Keys)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |取出节点值，一级节点:二级节点，用:号分开  ||
    | Keys|[String][string] |主节点  ||

## GetValueByKey  `获取Quick框架配置节点值`

 方法:  QuickSettings.GetValueByKey


> GetValueByKey([String][string]  key,[String][string]  Keys)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |取出节点值，一级节点:二级节点，用:号分开  ||
    | Keys|[String][string] |主节点  ||

## GetValueByKey  `获取Quick框架配置节点子项`

 方法:  QuickSettings.GetValueByKey


> GetValueByKey&lt;T&gt;([String][string]  key,[String][string]  Keys)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |为空取出全部节点，一级节点:二级节点，用:号分开  ||
    | Keys|[String][string] |主节点  ||

## Log.GetValue  `获取自定义配置`

 方法:  QuickSettings.Log.GetValue


> Log.GetValue&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |  ||

  * 返回

   > Log:key 

## DB.Connection  `数据库连接配置`

 方法:  QuickSettings.DB.Connection


> DB.Connection&lt;T&gt;([String][string]  dbCode,[String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | dbCode|[String][string] |数据库连接命名  ||
    | key|[String][string] |配置属性  ||

  * 返回

   > 返回内容 

  * 备注 

      DB 数据库配置

## DB.Redis.GetHostByKey  `获取服务器连接`

 方法:  QuickSettings.DB.Redis.GetHostByKey


> DB.Redis.GetHostByKey([String][string]  _key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _key|[String][string] |配置节点名  ||

  * 返回

   > DB:Redis:_key:Host 

## DB.Redis.GetHostByDB  `获取服务器连接`

 方法:  QuickSettings.DB.Redis.GetHostByDB


> DB.Redis.GetHostByDB([String][string]  _key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _key|[String][string] |配置节点名  ||

  * 返回

   > DB:Redis:_key:DB 

## DB.Redis.GetJson  `读取自定义配置 Json对象`

 方法:  QuickSettings.DB.Redis.GetJson


> DB.Redis.GetJson&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |关键词  ||

  * 返回

   > RedisConfigModel 

## DB.Redis.GetValue  `读取配置值`

 方法:  QuickSettings.DB.Redis.GetValue


> DB.Redis.GetValue&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||

  * 返回

   > DB:Redis: 

## Service.GetValue  `获取服务器配置的值`

 方法:  QuickSettings.Service.GetValue


> Service.GetValue([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||

  * 返回

   > Service:key 

## Service.GetValue  `读取配置值`

 方法:  QuickSettings.Service.GetValue


> Service.GetValue&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||

  * 返回

   > Service:key 

## Socket.GetValue  `获取IO服务器配置的值`

 方法:  QuickSettings.Socket.GetValue


> Socket.GetValue([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||

  * 返回

   > Socket:key 

## Socket.GetValue  `读取配置值`

 方法:  QuickSettings.Socket.GetValue


> Socket.GetValue&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||

  * 返回

   > Socket:key 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string


