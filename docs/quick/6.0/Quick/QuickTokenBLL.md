---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickTokenBLL
- 引用：Quick.QuickTokenBLL


## QuickTokenBLL  `令牌方法`



## RefreshToken  `生成新的登陆认证令牌`

 方法:  QuickTokenBLL.RefreshToken


> RefreshToken([String][string]  sessionId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | sessionId|[String][string] |会话id Token  ||

  * 返回

   > md5(32) 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string


