---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickEnumModel.MessageLevel
- 引用：Quick.QuickEnumModel.MessageLevel


## QuickEnumModel.MessageLevel  `消息等级`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | System|  | 系统消息 |
 | Two|  | 私聊消息 |
 | Group|  | 群聊消息 |
 | Sevice|  | 服务接口通知 |



