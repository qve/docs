---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# BLL.JsonHelperConvert.DateTimeNullableConverter
- 引用：Quick.BLL.JsonHelperConvert.DateTimeNullableConverter


## BLL.JsonHelperConvert.DateTimeNullableConverter  `json 时间空值转换`



## Read  `读取时间值`

 方法:  BLL.JsonHelperConvert.DateTimeNullableConverter.Read


> Read([Utf8JsonReader@][utf8jsonreader@]  reader,[Type][type]  typeToConvert,[JsonSerializerOptions][jsonserializeroptions]  options)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | reader|[Utf8JsonReader@][utf8jsonreader@] |  ||
    | typeToConvert|[Type][type] |  ||
    | options|[JsonSerializerOptions][jsonserializeroptions] |  ||

## Write  `输出时间值`

 方法:  BLL.JsonHelperConvert.DateTimeNullableConverter.Write


> Write([Utf8JsonWriter][utf8jsonwriter]  writer,[Nullable][nullable]&lt;[DateTime][datetime]&gt;  value,[JsonSerializerOptions][jsonserializeroptions]  options)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | writer|[Utf8JsonWriter][utf8jsonwriter] |  ||
    | value|[Nullable][nullable]&lt;[DateTime][datetime]&gt; |  ||
    | options|[JsonSerializerOptions][jsonserializeroptions] |  ||



[utf8jsonreader@]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.utf8jsonreader@
[type]:https://docs.microsoft.com/zh-cn/dotnet/api/system.type
[jsonserializeroptions]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonserializeroptions
[utf8jsonwriter]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.utf8jsonwriter
[nullable]:https://docs.microsoft.com/zh-cn/dotnet/api/system.nullable
[datetime]:https://docs.microsoft.com/zh-cn/dotnet/api/system.datetime


