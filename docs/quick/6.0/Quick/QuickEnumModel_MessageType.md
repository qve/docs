---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickEnumModel.MessageType
- 引用：Quick.QuickEnumModel.MessageType


## QuickEnumModel.MessageType  `io 消息类型`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Text|  | 明文文字消息 |
 | Json|  | json消息 |
 | Line|  | 连接消息 |
 | cmd|  | 服务指令 |
 | Receipt|  | 回执消息 |



