---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# BLL.TextHelper
- 引用：Quick.BLL.TextHelper


## BLL.TextHelper  `文本读写`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | DirDateTime.s_DateTime|  | 日期 |
- **构造函数**

  - BLL.TextHelper([DateTime](https://docs.microsoft.com/zh-cn/dotnet/api/system.datetime)    d)
       `构造函数`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | d |


## RndNum  `生成0-9随机数`

 方法:  BLL.TextHelper.RndNum


> RndNum([Int32][int32]  VcodeNum)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | VcodeNum|[Int32][int32] |生成长度  ||

## GetFullPath  `获取路径`

 方法:  BLL.TextHelper.GetFullPath


> GetFullPath([String][string]  a)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | a|[String][string] |  ||

## CreatePath  `判断是否存在路径，若没有就创建文件夹`

 方法:  BLL.TextHelper.CreatePath


> CreatePath([String][string]  LogFilePath)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | LogFilePath|[String][string] |  ||

## LoadFile  `返回文件内容.`

 方法:  BLL.TextHelper.LoadFile


> LoadFile([String][string]  path)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | path|[String][string] |文件件物理路径  ||

  * 返回

   > 文件内容 

## DeleteFile  `删除文件`

 方法:  BLL.TextHelper.DeleteFile


> DeleteFile([String][string]  path)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | path|[String][string] |文件物理路径  ||

## DeleteDir  `删除文件夹`

 方法:  BLL.TextHelper.DeleteDir


> DeleteDir([String][string]  path)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | path|[String][string] |路径  ||

## GetFileList  `获取目录下文件列表`

 方法:  BLL.TextHelper.GetFileList


> GetFileList([String][string]  _logpath)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _logpath|[String][string] |目录地址  ||

## DirDateTime.CompareTo  `重写比较类`

 方法:  BLL.TextHelper.DirDateTime.CompareTo


> DirDateTime.CompareTo([Object][object]  o)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | o|[Object][object] |  ||

## IsFileInUse  `判断文件是否被读写锁住`

 方法:  BLL.TextHelper.IsFileInUse


> IsFileInUse([String][string]  fileName)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | fileName|[String][string] |  ||

## getPath  `返回服务器目录下文件路径`

 方法:  BLL.TextHelper.getPath


> getPath([String][string]  path)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | path|[String][string] |指定的文件路径  ||

  * 返回

   > Directory.GetCurrentDirectory 

## Write  `批量写入日志`

 方法:  BLL.TextHelper.Write


> Write([QuickLogModel][quicklogmodel]  qm,[String][string]  fullFilePath,[Int32][int32]  _maxBack)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | qm|[QuickLogModel][quicklogmodel] |批量内容  ||
    | fullFilePath|[String][string] |日志全路径  ||
    | _maxBack|[Int32][int32] |超过最大数就备份  ||

## CopyBackUpFile  `备份超过长度的文本文件`

 方法:  BLL.TextHelper.CopyBackUpFile


> CopyBackUpFile([String][string]  _fileName,[String][string]  _fullFileName,[String][string]  _filePath,[Int32][int32]  _maxBack,[DateTime][datetime]  _dt)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _fileName|[String][string] |文件名  ||
    | _fullFileName|[String][string] |路径+文件全名  ||
    | _filePath|[String][string] |路径  ||
    | _maxBack|[Int32][int32] |最大长度  ||
    | _dt|[DateTime][datetime] |备份时间  ||

## AppendAllLinesAsync  `File 批量追加写入`

 方法:  BLL.TextHelper.AppendAllLinesAsync


> AppendAllLinesAsync([String][string]  fileName,[String][string]  body)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | fileName|[String][string] |  ||
    | body|[String][string] |  ||

## CreateFile  `创建新的文件`

 方法:  BLL.TextHelper.CreateFile


> CreateFile([String][string]  FileTxt,[String][string]  fullFilePath)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | FileTxt|[String][string] |写入内容  ||
    | fullFilePath|[String][string] |文件路径与名称  ||



[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[object]:https://docs.microsoft.com/zh-cn/dotnet/api/system.object
[quicklogmodel]:../Quick/Model_QuickLogModel.md
[datetime]:https://docs.microsoft.com/zh-cn/dotnet/api/system.datetime


