---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# JsonQuick
- 引用：Quick.JsonQuick


## JsonQuick  `前端 Json 语法对象`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | dict|  | 键值集合 |
- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | source|  | json 源字符串 |
 | element|  | json 对象 |
 | document|  | json 文档 |
 | Callback|  | 子级对象递归回调方法 |
 | Item(System.Int32)|  | json 取出数组 子项 |
 | Item(System.String)|  | json 读写元素属性名 <br /> TryGetProperty |
 | ValueKind|  | 值 |
- **构造函数**

  - JsonQuick([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _jsonString)
       `构造json对象或者json数组`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _jsonString |json 标准字符串

  - JsonQuick([JsonDocument](https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsondocument)    doc)
       `构造json对象或者json数组`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | doc |json 文档

  - JsonQuick([JsonElement](https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonelement)    je)
       `构造对象，并绑定键值集合可编辑`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | je |json 对象

  - JsonQuick([JsonDocument](https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsondocument)    _document,[JsonElement](https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonelement)    _element)
       `取出对象数组`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _document |json文档
      | _element |对象

  - JsonQuick([JsonDocument](https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsondocument)    _document,[JsonElement](https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonelement)    _element,[Object](https://docs.microsoft.com/zh-cn/dotnet/api/system.func<system.string,system.object,system.object>)    func)
       `取出元素，并支持回调更新`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _document |json文档
      | _element |对象
      | func |回调方法


## ToString  `重构输出字符串`

 方法:  JsonQuick.ToString


> ToString()


  * 返回

   > json字符串 

## WriteTo  `支持链式`

 方法:  JsonQuick.WriteTo


> WriteTo([Utf8JsonWriter][utf8jsonwriter]  writer)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | writer|[Utf8JsonWriter][utf8jsonwriter] |  ||

## Bind  `KV字典绑定更新当前Json`

 方法:  JsonQuick.Bind


> Bind()


  * 备注 

      批量编辑json 字典内容后，执行此方法更新

## BindDict  `Json对象绑定到KV字典`

 方法:  JsonQuick.BindDict


> BindDict()


  * 备注 

      发生写入编辑时会触发此事件初始化字典

## Serialize  `kv值集合序列化为字符串`

 方法:  JsonQuick.Serialize


> Serialize()


## Remove  `KV 值移除对象`

 方法:  JsonQuick.Remove


> Remove([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |  ||

## Update  `当前字符串更新`

 方法:  JsonQuick.Update


> Update([String][string]  key,[Object][object]  value)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |键名  ||
    | value|[Object][object] |内容  ||

## Update  `json字符串更新值`

 方法:  JsonQuick.Update


> Update([String][string]  _json,[String][string]  key,[Object][object]  value)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _json|[String][string] |指定更新的字符串  ||
    | key|[String][string] |键名  ||
    | value|[Object][object] |更新值  ||

## GetByte  `写入值`

 方法:  JsonQuick.GetByte


> GetByte()


## ValueEquals  `是否为空`

 方法:  JsonQuick.ValueEquals


> ValueEquals([String][string]  text)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | text|[String][string] |  ||

## EnumerateArray  `获取枚举器 , JSON 数组`

 方法:  JsonQuick.EnumerateArray


> EnumerateArray()


  * 返回

   > json 对象集合 

## EnumerateObject  `获取枚举器 , JSON 对象`

 方法:  JsonQuick.EnumerateObject


> EnumerateObject()


  * 返回

   > JSON对象的属性 

## Clone  `获取可在超过原始 JsonDocument 的生存期安全存储的 JsonElement`

 方法:  JsonQuick.Clone


> Clone()


## Dispose  `释放资源`

 方法:  JsonQuick.Dispose


> Dispose()




[utf8jsonwriter]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.utf8jsonwriter
[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[object]:https://docs.microsoft.com/zh-cn/dotnet/api/system.object


