---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# Model.QuickTextLogWebModel
- 引用：Quick.Model.QuickTextLogWebModel


## Model.QuickTextLogWebModel  `文本日志-网页日志`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | LogUserIp|  | 日志引发IP地址 |
 | LogErrorUrl|  | 日志引发Url |
- **构造函数**

  - Model.QuickTextLogWebModel([DateTime](https://docs.microsoft.com/zh-cn/dotnet/api/system.datetime)    Par_LogDateTime,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    Par_LogTxt,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    Par_LogUserIp,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    Par_LogErrorUrl)
       `文本日志-网络版`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | Par_LogDateTime |日志时间
      | Par_LogTxt |日志内容
      | Par_LogUserIp |日志引发IP地址
      | Par_LogErrorUrl |日志引发Url




