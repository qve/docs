---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickSettings.Session.Cookie
- 引用：Quick.QuickSettings.Session.Cookie


## QuickSettings.Session.Cookie  `浏览器会话`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Name|  | Session 客户端 cookie 名称 |
 | Timeout|  | 客户端超时(分钟数) |



