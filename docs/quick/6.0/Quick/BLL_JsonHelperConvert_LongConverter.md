---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# BLL.JsonHelperConvert.LongConverter
- 引用：Quick.BLL.JsonHelperConvert.LongConverter


## BLL.JsonHelperConvert.LongConverter  `json 数字转换`



## Read  `读取数字`

 方法:  BLL.JsonHelperConvert.LongConverter.Read


> Read([Utf8JsonReader@][utf8jsonreader@]  reader,[Type][type]  type,[JsonSerializerOptions][jsonserializeroptions]  options)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | reader|[Utf8JsonReader@][utf8jsonreader@] |  ||
    | type|[Type][type] |  ||
    | options|[JsonSerializerOptions][jsonserializeroptions] |  ||

## Write  `写入数字`

 方法:  BLL.JsonHelperConvert.LongConverter.Write


> Write([Utf8JsonWriter][utf8jsonwriter]  writer,[Int64][int64]  value,[JsonSerializerOptions][jsonserializeroptions]  options)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | writer|[Utf8JsonWriter][utf8jsonwriter] |  ||
    | value|[Int64][int64] |  ||
    | options|[JsonSerializerOptions][jsonserializeroptions] |  ||



[utf8jsonreader@]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.utf8jsonreader@
[type]:https://docs.microsoft.com/zh-cn/dotnet/api/system.type
[jsonserializeroptions]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonserializeroptions
[utf8jsonwriter]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.utf8jsonwriter
[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64


