---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# Model.QuickLogModel
- 引用：Quick.Model.QuickLogModel


## Model.QuickLogModel  `批量日志写入`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | FileName|  | 文件名 |
 | FilePath|  | 文件路径 |
 | Body|  | 批量写入每行的内容 |



