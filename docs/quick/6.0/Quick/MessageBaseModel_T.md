---
template: 2021.12
version: 21.11.18
author: quick.docs
date: 2022年03月20日 09:05

---
# MessageBaseModel&lt;T&gt;
- 引用：Quick.MessageBaseModel&lt;T&gt;
- version: 21.11.18


## MessageBaseModel&lt;T&gt;  `返回消息对象`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | code| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 消息编号默认：1 为正常返回             其它为消息提示 |
 | msg| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 提示消息 |
 | error| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 错误详情 |
 | dataType| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 返回数据类型 <br /> 默认:1,json,2:文本,3:pf,4:图片base64 |
 | data|  | 对象内容 |
- **构造函数**

  - MessageBaseModel&lt;T&gt;()
       `消息传递对象`


  - MessageBaseModel&lt;T&gt;([Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32)    _code,[Int32](https://docs.microsoft.com/zh-cn/dotnet/api/<t>，system.int32)    _data,[Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32)    _dataType,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _statusCode)
       `对象构造`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _code |消息编号默认：1 为正常返回
      | _data |对象
      | _dataType |类别
      | _statusCode |请求状态码
      | _error |错误提示




