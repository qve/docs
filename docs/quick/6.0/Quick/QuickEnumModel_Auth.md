---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickEnumModel.Auth
- 引用：Quick.QuickEnumModel.Auth


## QuickEnumModel.Auth  `验证`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | NotFoundApp|  | 应用不存在 |
 | NotAuthorizedApp|  | 应用未授权 |
 | LoginInfoFail|  | 登陆信息验证失败 |
 | LoginFail|  | 登陆验证失败 |
 | Overrun|  | 验证次数超限，30分钟 |
 | TokenConfigNot|  | App令牌配置不存在 |
 | PhoneFail|  | 请输入正确手机号 |
 | PhoneSMSEmpty|  | 请输入正确短信验证码 |
 | PhoneSMSFail|  | 请输入正确短信验证码 |
 | PassWordSMSEmpty|  | 请输入登录密码 |
 | PhoneIsJoin|  | 手机号已注册 |
 | AppLoginJoinTypeFail|  | 需管理员授权 |
 | TokenEmpty|  | 没有传入base 64 验证参数 |
 | TokenFormatError|  | basic 开头 |
 | TokenAppKeyEmpty|  | 没有按规范传入AppKey |
 | TokenValueEmpty|  | 没有val 值 |
 | TokenNotMatch|  | 前端算法格式不对 |
 | TokenUserEmpty|  | redis 没有缓存 |
 | PathEmpty|  | redis 没有缓存 |



