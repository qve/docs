---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# BLL.TextHelper.DirDateTime
- 引用：Quick.BLL.TextHelper.DirDateTime


## BLL.TextHelper.DirDateTime  `自定义日期类(实现倒排序)`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | s_DateTime|  | 日期 |
- **构造函数**

  - BLL.TextHelper.DirDateTime([DateTime](https://docs.microsoft.com/zh-cn/dotnet/api/system.datetime)    d)
       `构造函数`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | d |


## CompareTo  `重写比较类`

 方法:  BLL.TextHelper.DirDateTime.CompareTo


> CompareTo([Object][object]  o)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | o|[Object][object] |  ||



[object]:https://docs.microsoft.com/zh-cn/dotnet/api/system.object


