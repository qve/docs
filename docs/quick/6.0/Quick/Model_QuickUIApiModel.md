---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# Model.QuickUIApiModel
- 引用：Quick.Model.QuickUIApiModel


## Model.QuickUIApiModel  `UI前端数据模板对象`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | field|  | 数据字段 |
 | attr|  | Ui 字段属性验证规则：QuickUIApiOptsModel |
 | act|  | 自定义 Ui 数据权限 |
 | sort|  | 自定义 排序字段 |
 | th|  | 自定义 表头显示字段 |
 | edit|  | 自定义 可编辑的字段 |

## ToString  `序列化JSON对象，得到返回的JSON代码`

 方法:  Model.QuickUIApiModel.ToString


> ToString()






