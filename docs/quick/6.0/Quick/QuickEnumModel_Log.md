---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickEnumModel.Log
- 引用：Quick.QuickEnumModel.Log


## QuickEnumModel.Log  `日志处理`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | errorWrite|  | 日志写入错误 |



