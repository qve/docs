---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# BLL.JsonHelperConvert.DynamicConverter
- 引用：Quick.BLL.JsonHelperConvert.DynamicConverter


## BLL.JsonHelperConvert.DynamicConverter  `json 泛型 动态转换`



## Read  `读取转换`

 方法:  BLL.JsonHelperConvert.DynamicConverter.Read


> Read([Utf8JsonReader@][utf8jsonreader@]  reader,[Type][type]  typeToConvert,[JsonSerializerOptions][jsonserializeroptions]  options)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | reader|[Utf8JsonReader@][utf8jsonreader@] |  ||
    | typeToConvert|[Type][type] |  ||
    | options|[JsonSerializerOptions][jsonserializeroptions] |  ||

## ReadObject  `Json 转为 IDictionary`

 方法:  BLL.JsonHelperConvert.DynamicConverter.ReadObject


> ReadObject([JsonElement][jsonelement]  je)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | je|[JsonElement][jsonelement] |jsonElement  ||

## ReadValue  `读取值`

 方法:  BLL.JsonHelperConvert.DynamicConverter.ReadValue


> ReadValue([JsonElement][jsonelement]  jsonElement)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | jsonElement|[JsonElement][jsonelement] |  ||



[utf8jsonreader@]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.utf8jsonreader@
[type]:https://docs.microsoft.com/zh-cn/dotnet/api/system.type
[jsonserializeroptions]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonserializeroptions
[jsonelement]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonelement


