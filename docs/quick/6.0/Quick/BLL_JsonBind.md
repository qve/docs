---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# BLL.JsonBind
- 引用：Quick.BLL.JsonBind


## BLL.JsonBind  `正则格式绑定对象值`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | kvs|  | 字段名与值对象 |
- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | bindFlags|  | 用于反射的时候查找类型成员 <br /> 调用方法:InvokeMethod,创建实例:CreateInstance,获取字段的值:GetField,设置字段的值:SetField,获取属性的值:GetProperty,设置属性的值:SetProperty |
- **构造函数**

  - BLL.JsonBind()
       `构造空对象`


  - BLL.JsonBind([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    jsonString)
       `初始构造替换源对象json`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | jsonString |json替换源字符串


## GetValue  `取出值内容`

 方法:  BLL.JsonBind.GetValue


> GetValue([String][string]  index)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | index|[String][string] |序号  ||

## SetValue  `写入内容`

 方法:  BLL.JsonBind.SetValue


> SetValue([String][string]  index,[Object][object]  value)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | index|[String][string] |序号  ||
    | value|[Object][object] |值  ||

## GetValue  `取出多级子节点值`

 方法:  BLL.JsonBind.GetValue


> GetValue([Object][object]  _object,[String][string]  _key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _object|[Object][object] |JSON 需映射的对象  ||
    | _key|[String][string] |映射取值参数  ||

## GetValueByJson  `取出json对象值`

 方法:  BLL.JsonBind.GetValueByJson


> GetValueByJson([JsonElement][jsonelement]  item,[String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | item|[JsonElement][jsonelement] |json对象  ||
    | key|[String][string] |关键字  ||

## Bind  `正则绑定替换值`

 方法:  BLL.JsonBind.Bind


> Bind([String][string]  str,[String][string]  _reg)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | str|[String][string] |需要替换的字符串  ||
    | _reg|[String][string] |正则  ||

  * 返回

   > 替换对应的值 

  * 备注 

      reg正则替换 @ &quot;\{(.*?)\} &quot;

## Bind  `正则绑定替换值`

 方法:  BLL.JsonBind.Bind


> Bind([String][string]  str)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | str|[String][string] |  ||

  * 返回

   > 替换{name}对应的值 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[object]:https://docs.microsoft.com/zh-cn/dotnet/api/system.object
[jsonelement]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonelement


