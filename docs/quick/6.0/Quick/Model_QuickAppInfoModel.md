---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# Model.QuickAppInfoModel
- 引用：Quick.Model.QuickAppInfoModel


## Model.QuickAppInfoModel  `架构应用配置 QF_AppInfo`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | AppID|  | 应用ID，配置应用id相同会话共享 |
 | AppKey|  | 应用Key，禁止重复,命名规则双位年月二位流水号 |
 | AppSecret|  | 应用交互密钥 |
 | AppSuperRole_ID|  | 超级角色组，拥有所有API操作权限 |
 | AppLoginJoinType|  | 注册授权  &#39;ls &#39;: &#39;1:免授权,2:管理员授权,3:用户申请 &#39;, &#39;tip &#39;: &#39;允许其它注册用户登录此应用方式 &#39; |
 | AppDB|  | 应用存储DB\ &quot;tip\ &quot;:\ &quot;应用数据存储\ &quot; |
 | AppConns|  | 应用与用户库服务器连接 |
 | AppEncryption|  | 服务器密钥 |
 | TokenKey|  | 令牌规则 u:s:{0}:{1} |
 | TokenDB|  | 缓存会话库 |
 | TokenConns|  | 令牌数据库服务器连接 |
 | TokenEncryption|  | 服务器密钥 |
 | UserDB|  | 用户缓存数据库序列号 |
 | UserConns|  | 用户数据缓存服务器连接 |
 | UserEncryption|  | 服务器密钥 |
 | Title|  | 应用名称\ &quot;d\ &quot;:1 |



