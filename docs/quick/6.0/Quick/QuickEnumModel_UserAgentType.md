---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickEnumModel.UserAgentType
- 引用：Quick.QuickEnumModel.UserAgentType


## QuickEnumModel.UserAgentType  `浏览器类型`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | NotFound|  | 未识别 |
 | Macintosh|  | mac |
 | AOS|  | 网关传入安卓 |
 | iOS|  | 网关传入 苹果 |
 | Win|  | 网关传入 |



