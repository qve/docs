---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickSettings.Socket
- 引用：Quick.QuickSettings.Socket


## QuickSettings.Socket  `通信服务`



## GetValue  `获取IO服务器配置的值`

 方法:  QuickSettings.Socket.GetValue


> GetValue([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||

  * 返回

   > Socket:key 

## GetValue  `读取配置值`

 方法:  QuickSettings.Socket.GetValue


> GetValue&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||

  * 返回

   > Socket:key 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string


