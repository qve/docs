---
template: 2021.12
version: 2021.12.02
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickLog
- 引用：Quick.QuickLog
- version: 2021.12.02


## QuickLog  `文本日志读写`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | IsWrite|  | 是否写入非错误日志 <br /> 配置 QuickSettings.Log.IsWrite |
 | _logsProducer|  | 日志生产者 |
 | _logsConsumer|  | 日志消费者 |
 | logsCount|  | 日志计数 |
 | maxCount|  | 超过最大数就写入 |
 | maxBack|  | 超过最大数就备份 |
- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | LogPath|  | 获得日志文件存放主目录 |

## GetLogWeb  `读取系统路径下文件日志`

 方法:  QuickLog.GetLogWeb


> GetLogWeb()


## GetLogWeb  `读取文件日志`

 方法:  QuickLog.GetLogWeb


> GetLogWeb([String][string]  FilePath)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | FilePath|[String][string] |文件物理路径  ||

## Add  `写操作日志文件`

 方法:  QuickLog.Add


> Add([String][string]  input)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | input|[String][string] |日志内容  ||

## Add  `写操作日志文件`

 方法:  QuickLog.Add


> Add([String][string]  input,[String][string]  FileName)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | input|[String][string] |日志内容  ||
    | FileName|[String][string] |文件名  ||

## Add  `写操作日志文件`

 方法:  QuickLog.Add


> Add([String][string]  input,[String][string]  FileName,[String][string]  FilePath)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | input|[String][string] |日志内容  ||
    | FileName|[String][string] |文件名  ||
    | FilePath|[String][string] |\\子文件夹\\  ||

## Error  `写入错误日志信息，默认写入\Error 文件夹`

 方法:  QuickLog.Error


> Error([String][string]  input)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | input|[String][string] |日志内容  ||

## Error  `写入错误日志信息，默认写入\Error 文件夹`

 方法:  QuickLog.Error


> Error([String][string]  input,[String][string]  FileName)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | input|[String][string] |日志内容  ||
    | FileName|[String][string] |文件名  ||

## Error  `写入错误日志信息`

 方法:  QuickLog.Error


> Error([String][string]  input,[String][string]  FileName,[String][string]  FilePath)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | input|[String][string] |日志内容  ||
    | FileName|[String][string] |文件名  ||
    | FilePath|[String][string] |\\子文件夹\\  ||

## Write  `缓存写入日志`

 方法:  QuickLog.Write


> Write([String][string]  input,[String][string]  FileName,[String][string]  FilePath)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | input|[String][string] |日志内容  ||
    | FileName|[String][string] |文件名  ||
    | FilePath|[String][string] |\\子文件夹\\  ||

## PushAsync  `异步处理`

 方法:  QuickLog.PushAsync


> PushAsync([IDictionary][idictionary]&lt;[String][string],[QuickLogModel][quicklogmodel]&gt;  _logs)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _logs|[IDictionary][idictionary]&lt;[String][string],[QuickLogModel][quicklogmodel]&gt; |  ||



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[idictionary]:https://docs.microsoft.com/zh-cn/dotnet/api/system.collections.generic.idictionary
[quicklogmodel]:../Quick/Model_QuickLogModel.md


