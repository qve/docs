---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickEnumModel
- 引用：Quick.QuickEnumModel


## QuickEnumModel  `常用枚举对象定义`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | User.activeNot|  | 未激活 |
 | User.active|  | 活动 |
 | User.close|  | 关闭 |
 | User.deregistration|  | 已注销 |
 | User.lose|  | 挂失 |
 | User.overdue|  | 已过期 |
 | Auth.NotFoundApp|  | 应用不存在 |
 | Auth.NotAuthorizedApp|  | 应用未授权 |
 | Auth.LoginInfoFail|  | 登陆信息验证失败 |
 | Auth.LoginFail|  | 登陆验证失败 |
 | Auth.Overrun|  | 验证次数超限，30分钟 |
 | Auth.TokenConfigNot|  | App令牌配置不存在 |
 | Auth.PhoneFail|  | 请输入正确手机号 |
 | Auth.PhoneSMSEmpty|  | 请输入正确短信验证码 |
 | Auth.PhoneSMSFail|  | 请输入正确短信验证码 |
 | Auth.PassWordSMSEmpty|  | 请输入登录密码 |
 | Auth.PhoneIsJoin|  | 手机号已注册 |
 | Auth.AppLoginJoinTypeFail|  | 需管理员授权 |
 | Auth.TokenEmpty|  | 没有传入base 64 验证参数 |
 | Auth.TokenFormatError|  | basic 开头 |
 | Auth.TokenAppKeyEmpty|  | 没有按规范传入AppKey |
 | Auth.TokenValueEmpty|  | 没有val 值 |
 | Auth.TokenNotMatch|  | 前端算法格式不对 |
 | Auth.TokenUserEmpty|  | redis 没有缓存 |
 | Auth.PathEmpty|  | redis 没有缓存 |
 | HttpStatus.AuthenticationFailed|  | 请求验证失败 |
 | HttpStatus.RequestReject|  | 禁止访问服务器拒绝请求 |
 | HttpStatus.PathNotFound|  | 服务器找不到请求连接 |
 | HttpStatus.ApplicationError|  | 服务器发生错误 |
 | DB.connectionNotConfigured|  | 数据库连接未配置 |
 | Log.errorWrite|  | 日志写入错误 |
 | MessageType.Text|  | 明文文字消息 |
 | MessageType.Json|  | json消息 |
 | MessageType.Line|  | 连接消息 |
 | MessageType.cmd|  | 服务指令 |
 | MessageType.Receipt|  | 回执消息 |
 | MessageLevel.System|  | 系统消息 |
 | MessageLevel.Two|  | 私聊消息 |
 | MessageLevel.Group|  | 群聊消息 |
 | MessageLevel.Sevice|  | 服务接口通知 |
 | MessageStatus.Queue|  | 队列等待发送 |
 | MessageStatus.Success|  | 发送成功 |
 | MessageStatus.Revoke|  | 撤销消息 |
 | MessageStatus.Delete|  | 已删除 |
 | MessageStatus.Read|  | 已读 |
 | UserAgentType.NotFound|  | 未识别 |
 | UserAgentType.Macintosh|  | mac |
 | UserAgentType.AOS|  | 网关传入安卓 |
 | UserAgentType.iOS|  | 网关传入 苹果 |
 | UserAgentType.Win|  | 网关传入 |
 | UserAgentApp.notFound|  | 未识别 |
 | UserAgentApp.micromessenger|  | 微信请求 |
 | UserAgentApp.qq|  | QQ 请求 |
 | UserAgentApp.alipayclient|  | 支付宝 |



