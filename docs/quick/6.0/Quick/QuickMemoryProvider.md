---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05
---

# QuickMemoryProvider

- 引用：Quick.QuickMemoryProvider

## QuickMemoryProvider `内存缓存方法 名命前缀 qm:`

- **类变量**

| 名称      | 类型 | 说明                                         |
| --------- | ---- | -------------------------------------------- |
| keyPrefix |      | 来自 AppID 缓存键名 qm <br /> qm:{0}:{1}:{2} |

- **类属性**

| 名称  | 类型                                                                                                       | 说明                             |
| ----- | ---------------------------------------------------------------------------------------------------------- | -------------------------------- |
| Cache | [MemoryCache](https://docs.microsoft.com/zh-cn/dotnet/api/microsoft.extensions.caching.memory.memorycache) | 内存缓存                         |
| AppID | [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32)                                          | 当前应用 ID <br /> QF_AppInfo_ID |

- **构造函数**

  - QuickMemoryProvider()
    `默认内存缓存配置`

  - QuickMemoryProvider([MemoryCacheOptions](https://docs.microsoft.com/zh-cn/dotnet/api/microsoft.extensions.caching.memory.memorycacheoptions) cacheOps)
    `自定义内存缓存方法`

    | 构造参数 | 说明       |
    | -------- | ---------- |
    | cacheOps | 自定义配置 |

## Get `获取对象`

方法: QuickMemoryProvider.Get

> Get&lt;T&gt;([String][string] key)

- **参数**

  | 名称 | 类型             | 说明           | 参见 |
  | ---- | ---------------- | -------------- | ---- |
  | key  | [String][string] | 非规则缓存 Key |      |

* 调试信息

## Set `添加滑动缓存并自动延期`

方法: QuickMemoryProvider.Set

> Set([String][string] key,[Object][object] value,[TimeSpan][timespan] expiresSliding)

- **参数**

  | 名称           | 类型                 | 说明                                                                                                                | 参见 |
  | -------------- | -------------------- | ------------------------------------------------------------------------------------------------------------------- | ---- |
  | key            | [String][string]     | 非规则缓存 Key                                                                                                      |      |
  | value          | [Object][object]     | 缓存 Value                                                                                                          |      |
  | expiresSliding | [TimeSpan][timespan] | TimeSpan.FromHours(QuickSettings.Session.Cache.Expire) 过期时长（如果在过期时间内有操作，则以当前时间点延长过期时间 |      |

## Add `添加缓存，自动延期`

方法: QuickMemoryProvider.Add

> Add([String][string] key,[Object][object] value,[MemoryCacheEntryOptions][memorycacheentryoptions] Opts)

- **参数**

  | 名称  | 类型                                               | 说明                                                     | 参见 |
  | ----- | -------------------------------------------------- | -------------------------------------------------------- | ---- |
  | key   | [String][string]                                   | 缓存 Key                                                 |      |
  | value | [Object][object]                                   | 缓存 Value                                               |      |
  | Opts  | [MemoryCacheEntryOptions][memorycacheentryoptions] | 自定义配置（到期回调方法：RegisterPostEvictionCallback） |      |

## Remove `通过Key删除缓存数据`

方法: QuickMemoryProvider.Remove

> Remove([String][string] key)

- **参数**

  | 名称 | 类型             | 说明           | 参见 |
  | ---- | ---------------- | -------------- | ---- |
  | key  | [String][string] | 非规则缓存 Key |      |

## Count `当前存储总数`

方法: QuickMemoryProvider.Count

> Count()

## TryGetValue `是否存在`

方法: QuickMemoryProvider.TryGetValue

> TryGetValue([String][string] key)

- **参数**

  | 名称 | 类型             | 说明           | 参见 |
  | ---- | ---------------- | -------------- | ---- |
  | key  | [String][string] | 非规则缓存 Key |      |

## GetAll `获取缓存集合`

方法: QuickMemoryProvider.GetAll

> GetAll([String][string] keys)

- **参数**

  | 名称 | 类型                                               | 说明          | 参见 |
  | ---- | -------------------------------------------------- | ------------- | ---- |
  | keys | [IEnumerable][ienumerable]&lt;[String][string]&gt; | 缓存 Key 集合 |      |

## GetKeyName `当前AppID缓存键名规则`

方法: QuickMemoryProvider.GetKeyName

> GetKeyName([String][string] keyName,[String][string] version)

- **参数**

  | 名称    | 类型             | 说明                            | 参见 |
  | ------- | ---------------- | ------------------------------- | ---- |
  | keyName | [String][string] | 模块名称:缓存主键命名，不能重复 |      |
  | version | [String][string] | 版本号,默认 0.1                 |      |

* 返回

> qm:当前应用 ID:版本:key

- 备注

  规则[keyPrefix](./QuickMemoryProvider.md)

## GetKeyName `创建AppId缓存名`

方法: QuickMemoryProvider.GetKeyName

> GetKeyName([Int32][int32] \_AppId,[String][string] keyName)

- **参数**

  | 名称    | 类型             | 说明                            | 参见 |
  | ------- | ---------------- | ------------------------------- | ---- |
  | \_AppId | [Int32][int32]   | 应用 ID                         |      |
  | keyName | [String][string] | 模块名称:缓存主键命名，不能重复 |      |

* 备注

  规则[keyPrefix](./QuickMemoryProvider.md)

## GetKeyName `来自appKey缓存名规qmk`

方法: QuickMemoryProvider.GetKeyName

> GetKeyName([String][string] keyName,[String][string] appKey,[String][string] version)

- **参数**

  | 名称    | 类型             | 说明                                       | 参见 |
  | ------- | ---------------- | ------------------------------------------ | ---- |
  | keyName | [String][string] | 模块名称:缓存主键命名，不能重复（ui:home） |      |
  | appKey  | [String][string] | 应用配置注册 QuickSettings.App.Key         |      |
  | version | [String][string] | 存储版本 0.1                               |      |

* 返回

> qmk:appKey:version:keyName

## GetKeys `获取所有缓存名`

方法: QuickMemoryProvider.GetKeys

> GetKeys()

## KeyExists `验证缓存名是否存在`

方法: QuickMemoryProvider.KeyExists

> KeyExists([String][string] key)

- **参数**

  | 名称 | 类型             | 说明     | 参见 |
  | ---- | ---------------- | -------- | ---- |
  | key  | [String][string] | 缓存 Key |      |

## KeySearch `搜索匹配到的缓存名`

方法: QuickMemoryProvider.KeySearch

> KeySearch([String][string] pattern)

- **参数**

  | 名称    | 类型             | 说明     | 参见 |
  | ------- | ---------------- | -------- | ---- |
  | pattern | [String][string] | 匹配正则 |      |

## GetCache `获取key值缓存`

方法: QuickMemoryProvider.GetCache

> GetCache&lt;T&gt;([String][string] key)

- **参数**

  | 名称 | 类型             | 说明 | 参见 |
  | ---- | ---------------- | ---- | ---- |
  | key  | [String][string] |      |      |

## GetCacheAll `获取非标缓存集合`

方法: QuickMemoryProvider.GetCacheAll

> GetCacheAll([String][string] keys)

- **参数**

  | 名称 | 类型                                               | 说明          | 参见 |
  | ---- | -------------------------------------------------- | ------------- | ---- |
  | keys | [IEnumerable][ienumerable]&lt;[String][string]&gt; | 缓存 Key 集合 |      |

## SetCache `添加滑动缓存，自动延期`

方法: QuickMemoryProvider.SetCache

> SetCache([String][string] key,[Object][object] value,[TimeSpan][timespan] expiresSliding)

- **参数**

  | 名称           | 类型                 | 说明                                                                               | 参见 |
  | -------------- | -------------------- | ---------------------------------------------------------------------------------- | ---- |
  | key            | [String][string]     | 缓存 Key                                                                           |      |
  | value          | [Object][object]     | 缓存 Value                                                                         |      |
  | expiresSliding | [TimeSpan][timespan] | TimeSpan.FromHours(1) 过期时长（如果在过期时间内有操作，则以当前时间点延长过期时间 |      |

## AddCache `添加缓存，自动延期`

方法: QuickMemoryProvider.AddCache

> AddCache([String][string] key,[Object][object] value,[MemoryCacheEntryOptions][memorycacheentryoptions] Opts)

- **参数**

  | 名称  | 类型                                               | 说明                                                     | 参见 |
  | ----- | -------------------------------------------------- | -------------------------------------------------------- | ---- |
  | key   | [String][string]                                   | 缓存 Key                                                 |      |
  | value | [Object][object]                                   | 缓存 Value                                               |      |
  | Opts  | [MemoryCacheEntryOptions][memorycacheentryoptions] | 自定义配置（到期回调方法：RegisterPostEvictionCallback） |      |

## RemoveCache `通过Key删除缓存数据`

方法: QuickMemoryProvider.RemoveCache

> RemoveCache([String][string] key)

- **参数**

  | 名称 | 类型             | 说明 | 参见 |
  | ---- | ---------------- | ---- | ---- |
  | key  | [String][string] | Key  |      |

## RemoveCache `批量删除缓存`

方法: QuickMemoryProvider.RemoveCache

> RemoveCache([String][string] keys)

- **参数**

  | 名称 | 类型                                               | 说明                       | 参见 |
  | ---- | -------------------------------------------------- | -------------------------- | ---- |
  | keys | [IEnumerable][ienumerable]&lt;[String][string]&gt; | 关键字集合（按规则存储键） |      |

## RemoveByContain `删除所有缓存来自包含字符串`

方法: QuickMemoryProvider.RemoveByContain

> RemoveByContain([String][string] contain)

- **参数**

  | 名称    | 类型             | 说明         | 参见 |
  | ------- | ---------------- | ------------ | ---- |
  | contain | [String][string] | 包含的字符串 |      |

* 备注

  [KeySearch](./QuickMemoryProvider.md)

## RemoveAll `批量删除缓存`

方法: QuickMemoryProvider.RemoveAll

> RemoveAll([String][string] keys)

- **参数**

  | 名称 | 类型                                               | 说明                       | 参见 |
  | ---- | -------------------------------------------------- | -------------------------- | ---- |
  | keys | [IEnumerable][ienumerable]&lt;[String][string]&gt; | 关键字集合（非规则存储键） |      |

## Clear `删除所有的缓存`

方法: QuickMemoryProvider.Clear

> Clear()

[string]: https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[object]: https://docs.microsoft.com/zh-cn/dotnet/api/system.object
[timespan]: https://docs.microsoft.com/zh-cn/dotnet/api/system.timespan
[memorycacheentryoptions]: https://docs.microsoft.com/zh-cn/dotnet/api/microsoft.extensions.caching.memory.memorycacheentryoptions
[ienumerable]: https://docs.microsoft.com/zh-cn/dotnet/api/system.collections.generic.ienumerable
[int32]: https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
