---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickEnumModel.MessageStatus
- 引用：Quick.QuickEnumModel.MessageStatus


## QuickEnumModel.MessageStatus  `消息状态`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Queue|  | 队列等待发送 |
 | Success|  | 发送成功 |
 | Revoke|  | 撤销消息 |
 | Delete|  | 已删除 |
 | Read|  | 已读 |



