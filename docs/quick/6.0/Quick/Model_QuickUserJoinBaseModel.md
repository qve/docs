---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# Model.QuickUserJoinBaseModel
- 引用：Quick.Model.QuickUserJoinBaseModel


## Model.QuickUserJoinBaseModel  `注册用户基本信息`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Phone|  | 注册手机号码 |
 | QF_Group_ID|  | 注册组织 |
 | QF_AppInfo_ID|  | 注册应用 ID |
 | QF_Role_ID|  | 注册角色组 ID （用户权限必填） |
 | LastFirst|  | 注册姓名 |
 | LoginPwd|  | 注册 设置的密码 |
 | SourceTypeID|  | 来源类别\ &quot;ls\ &quot;:\ &quot;1:自己注册,2:推荐注册,3:后台注册\ &quot;,4:\ &quot;app注册\ &quot; |
 | Source|  | 注册来源，默认填写 AppID |
 | SourceUser_ID|  | 推荐人用户ID |
 | MUser_ID|  | 注册编辑者，平台自主注册默认1 |
- **构造函数**

  - Model.QuickUserJoinBaseModel()
       `注册用户基本信息`





