---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickSettings.Session
- 引用：Quick.QuickSettings.Session


## QuickSettings.Session  `会话`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Cookie.Name|  | Session 客户端 cookie 名称 |
 | Cookie.Timeout|  | 客户端超时(分钟数) |
 | Cache.Expire|  | 会话超时(分钟数) |
 | Cache.ExpireRefresh|  | 票据过期刷新有效期：(分钟数) 默认30天 |
 | WithOrigins|  | 跨域授权域名 |



