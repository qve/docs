---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05
---

# Quick

template: 2021.12
author: quick.docs
date: 2022 年 03 月 20 日 09:05

## Quick 目录

### [BLL.JsonBind](./BLL_JsonBind.md)

      正则格式绑定对象值

### [BLL.JsonHelperConvert](./BLL_JsonHelperConvert.md)

      json 类型转换自定义方法

### [BLL.JsonHelperConvert.DynamicConverter](./BLL_JsonHelperConvert_DynamicConverter.md)

      json 泛型 动态转换

### [BLL.JsonHelperConvert.DateTimeConverter](./BLL_JsonHelperConvert_DateTimeConverter.md)

      Json 时间格式化，避免ISO转换js.toISOString()

### [BLL.JsonHelperConvert.DateTimeNullableConverter](./BLL_JsonHelperConvert_DateTimeNullableConverter.md)

      json 时间空值转换

### [BLL.JsonHelperConvert.LongConverter](./BLL_JsonHelperConvert_LongConverter.md)

      json 数字转换

### [BLL.JsonHelperConvert.Int32Converter](./BLL_JsonHelperConvert_Int32Converter.md)

      数字转换

### [BLL.JsonHelperConvert.DecimalConverter](./BLL_JsonHelperConvert_DecimalConverter.md)

      金额转换

### [BLL.JsonHelperConvert.StringConverter](./BLL_JsonHelperConvert_StringConverter.md)

      文本与数字转换

### [BLL.QuickImg](./BLL_QuickImg.md)

      图片处理类

### [BLL.TextHelper](./BLL_TextHelper.md)

      文本读写

### [BLL.TextHelper.DirDateTime](./BLL_TextHelper_DirDateTime.md)

      自定义日期类(实现倒排序)

### [JsonHelper](./JsonHelper.md)

      json 转换序列化

### [JsonHelper.JsonType](./JsonHelper_JsonType.md)

      json 数据类型

### [JsonQuick](./JsonQuick.md)

      前端 Json 语法对象

### [QuickBLL](./QuickBLL.md)

      基础方法

### [QuickCache](./QuickCache.md)

      本机缓存管理

### [QuickEnum](./QuickEnum.md)

      全局输出代码

### [QuickLog](./QuickLog.md)

      文本日志读写

### [QuickMemoryProvider](./QuickMemoryProvider.md)

      内存缓存方法
            名命前缀 qm:

### [QuickTokenBLL](./QuickTokenBLL.md)

      令牌方法

### [XMLHelper](./XMLHelper.md)

      解析XML

### [ZipHelper](./ZipHelper.md)

      压缩库

### [MessageBaseModel_T](./MessageBaseModel_T.md)

      返回消息对象

### [MessageModel_T](./MessageModel_T.md)

      前端模板消息传递实体

### [Model.QuickAppInfoModel](./Model_QuickAppInfoModel.md)

      架构应用配置 QF_AppInfo

### [Model.QuickLogModel](./Model_QuickLogModel.md)

      批量日志写入

### [Model.QuickTextLogModel](./Model_QuickTextLogModel.md)

      标准文本日志

### [Model.QuickTextLogWebModel](./Model_QuickTextLogWebModel.md)

      文本日志-网页日志

### [Model.QuickTokenAuthModel](./Model_QuickTokenAuthModel.md)

      返回的认证票据

### [Model.QuickUIApiModel](./Model_QuickUIApiModel.md)

      UI前端数据模板对象

### [Model.QuickUserJoinBaseModel](./Model_QuickUserJoinBaseModel.md)

      注册用户基本信息

### [Model.QuickUserModel](./Model_QuickUserModel.md)

      用户基础对象

### [Model.QuickUserMoreModel](./Model_QuickUserMoreModel.md)

      扩展第三方

### [QuickEnumModel](./QuickEnumModel.md)

      常用枚举对象定义

### [QuickEnumModel.User](./QuickEnumModel_User.md)

      网络会员状态

### [QuickEnumModel.Auth](./QuickEnumModel_Auth.md)

      验证

### [QuickEnumModel.HttpStatus](./QuickEnumModel_HttpStatus.md)

      请求状态

### [QuickEnumModel.DB](./QuickEnumModel_DB.md)

      数据库提示

### [QuickEnumModel.Log](./QuickEnumModel_Log.md)

      日志处理

### [QuickEnumModel.MessageType](./QuickEnumModel_MessageType.md)

      io 消息类型

### [QuickEnumModel.MessageLevel](./QuickEnumModel_MessageLevel.md)

      消息等级

### [QuickEnumModel.MessageStatus](./QuickEnumModel_MessageStatus.md)

      消息状态

### [QuickEnumModel.UserAgentType](./QuickEnumModel_UserAgentType.md)

      浏览器类型

### [QuickEnumModel.UserAgentApp](./QuickEnumModel_UserAgentApp.md)

      浏览器 请求来源自定义应用

### [QuickSettings](./QuickSettings.md)

      配置节点

### [QuickSettings.Log](./QuickSettings_Log.md)

      日志

### [QuickSettings.App](./QuickSettings_App.md)

      应用配置

### [QuickSettings.Session](./QuickSettings_Session.md)

      会话

### [QuickSettings.Session.Cookie](./QuickSettings_Session_Cookie.md)

      浏览器会话

### [QuickSettings.Session.Cache](./QuickSettings_Session_Cache.md)

      会话redis缓存配置

### [QuickSettings.DB](./QuickSettings_DB.md)

      数据库

### [QuickSettings.DB.Redis](./QuickSettings_DB_Redis.md)

      缓存数据库

### [QuickSettings.Service](./QuickSettings_Service.md)

      后台服务

### [QuickSettings.Socket](./QuickSettings_Socket.md)

      通信服务

### [QuickSettingsManager](./QuickSettingsManager.md)

      appsettings.json 配置读取

### [QuickToken](./QuickToken.md)

      会话身份令牌
