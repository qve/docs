---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickSettings.Log
- 引用：Quick.QuickSettings.Log


## QuickSettings.Log  `日志`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Path|  | Text存储路径 |
 | IsWrite|  | 是否写入非错误日志，默认写入 |
 | CacheMaxInt|  | 缓冲写入日志的条数，默认1条 |
 | CacheMaxBack|  | 当前文本超过行数就备份,默认8000行 |

## GetValue  `获取自定义配置`

 方法:  QuickSettings.Log.GetValue


> GetValue&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |  ||

  * 返回

   > Log:key 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string


