---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickEnum
- 引用：Quick.QuickEnum


## QuickEnum  `全局输出代码`



## GetEnumName  `根据枚举的值获取枚举名称`

 方法:  QuickEnum.GetEnumName


> GetEnumName&lt;T&gt;([Int32][int32]  status)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | status|[Int32][int32] |枚举的值  ||

## GetNamesArr  `获取枚举名称集合`

 方法:  QuickEnum.GetNamesArr


> GetNamesArr&lt;T&gt;()


## GetEnumDic  `将枚举转换成字典集合`

 方法:  QuickEnum.GetEnumDic


> GetEnumDic&lt;T&gt;()


## GetDic  `将枚举转换成字典`

 方法:  QuickEnum.GetDic


> GetDic&lt;T&gt;()


## Result  `返回值与描述`

 方法:  QuickEnum.Result


> Result([Enum][enum]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[Enum][enum] |变量名称  ||

  * 返回

   > 返回code_{值}:{变量名} 

## GetDescription  `获取到对应枚举的描述-没有描述信息，返回枚举名称`

 方法:  QuickEnum.GetDescription


> GetDescription([Enum][enum]  enum,[Boolean][boolean]  isName)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | enum|[Enum][enum] |枚举对象  ||
    | isName|[Boolean][boolean] |默认没有描述信息，返回枚举名称  ||

  * 返回

   > 返回描述 

## GetBy  `根据变量的名，取出对象`

 方法:  QuickEnum.GetBy


> GetBy&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |变量名称  ||

  * 返回

   > 返回枚举 



[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[enum]:https://docs.microsoft.com/zh-cn/dotnet/api/system.enum
[boolean]:https://docs.microsoft.com/zh-cn/dotnet/api/system.boolean
[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string


