---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# XMLHelper
- 引用：Quick.XMLHelper


## XMLHelper  `解析XML`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | elements|  | 解析返回的对象 |
- **构造函数**

  - XMLHelper([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    text)
       `解析xml`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | text |内容


## Deserialize  `反序列化为对象`

 方法:  XMLHelper.Deserialize


> Deserialize&lt;T&gt;([String][string]  xml)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | xml|[String][string] |  ||

## Serialize  `序列化xml`

 方法:  XMLHelper.Serialize


> Serialize&lt;T&gt;(T  t)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | t|T |  ||

## ToHtmlByXSL  `转换xml和xsl为html文档`

 方法:  XMLHelper.ToHtmlByXSL


> ToHtmlByXSL([String][string]  xsl_name,[String][string]  xml_name)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | xsl_name|[String][string] |xsl路径  ||
    | xml_name|[String][string] |xml内容  ||

  * 返回

   > 转换后字符 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string


