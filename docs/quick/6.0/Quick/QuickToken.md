---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickToken
- 引用：Quick.QuickToken


## QuickToken  `会话身份令牌`
继承[QuickUserMoreModel](../Quick/Model_QuickUserMoreModel.md)

- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Token| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 用户会话令牌 AccessToken <br /> 来自登录的会话 SessionID |
 | RefreshToken| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 刷新票据，用于换取Token |
 | LoginID| [Int64](https://docs.microsoft.com/zh-cn/dotnet/api/system.int64) | 登录账号 ID |



