---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickSettings.DB.Redis
- 引用：Quick.QuickSettings.DB.Redis


## QuickSettings.DB.Redis  `缓存数据库`



## GetHostByKey  `获取服务器连接`

 方法:  QuickSettings.DB.Redis.GetHostByKey


> GetHostByKey([String][string]  _key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _key|[String][string] |配置节点名  ||

  * 返回

   > DB:Redis:_key:Host 

## GetHostByDB  `获取服务器连接`

 方法:  QuickSettings.DB.Redis.GetHostByDB


> GetHostByDB([String][string]  _key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _key|[String][string] |配置节点名  ||

  * 返回

   > DB:Redis:_key:DB 

## GetJson  `读取自定义配置 Json对象`

 方法:  QuickSettings.DB.Redis.GetJson


> GetJson&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |关键词  ||

  * 返回

   > RedisConfigModel 

## GetValue  `读取配置值`

 方法:  QuickSettings.DB.Redis.GetValue


> GetValue&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||

  * 返回

   > DB:Redis: 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string


