---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickSettings.App
- 引用：Quick.QuickSettings.App


## QuickSettings.App  `应用配置`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | ID|  | 应用 自定义ID |
 | Key|  | 应用 Key |
 | Secret|  | 应用 加密 密文 |



