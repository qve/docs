---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickEnumModel.HttpStatus
- 引用：Quick.QuickEnumModel.HttpStatus


## QuickEnumModel.HttpStatus  `请求状态`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | AuthenticationFailed|  | 请求验证失败 |
 | RequestReject|  | 禁止访问服务器拒绝请求 |
 | PathNotFound|  | 服务器找不到请求连接 |
 | ApplicationError|  | 服务器发生错误 |



