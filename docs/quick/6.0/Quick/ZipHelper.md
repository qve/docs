---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# ZipHelper
- 引用：Quick.ZipHelper


## ZipHelper  `压缩库`



## Create  `创建压缩`

 方法:  ZipHelper.Create


> Create([String][string]  filePathName,[String][string]  zipPathName)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | filePathName|[String][string] |源文件路径名称  ||
    | zipPathName|[String][string] |生成压缩包的路径名称  ||

  * 返回

   > Replace(@"\", "/") 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string


