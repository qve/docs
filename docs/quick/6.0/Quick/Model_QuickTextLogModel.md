---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# Model.QuickTextLogModel
- 引用：Quick.Model.QuickTextLogModel


## Model.QuickTextLogModel  `标准文本日志`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | FileName|  | 文件名 |
 | FilePath|  | 文件路径 |
 | LogDateTime|  | 日志时间 |
 | LogTxt|  | 日志内容 |
- **构造函数**

  - Model.QuickTextLogModel([DateTime](https://docs.microsoft.com/zh-cn/dotnet/api/system.datetime)    Par_LogDateTime,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    Par_LogTxt)
       `标准文本日志`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | Par_LogDateTime |日志时间
      | Par_LogTxt |日志内容




