---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# JsonHelper
- 引用：Quick.JsonHelper


## JsonHelper  `json 转换序列化`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | options|  | 转换属性 |
 | xml|  | xml 节点 |
 | jsonType|  | 类别 |
- **构造函数**

  - JsonHelper([XElement](https://docs.microsoft.com/zh-cn/dotnet/api/system.xml.linq.xelement)    element,[JsonType](../Quick/JsonHelper_JsonType.md)    type)
       `xml 数组节点转换`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | element |xml 节点
      | type |xml 类别


## CreateOptions  `转换属性配置`

 方法:  JsonHelper.CreateOptions


> CreateOptions()


## Serialize  `序列化为字符串`

 方法:  JsonHelper.Serialize


> Serialize([Object][object]  obj)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | obj|[Object][object] |JsonSerializerOptions  ||

  * 备注 

      JsonSerializer

## Deserialize  `反序列化为实体对象`

 方法:  JsonHelper.Deserialize


> Deserialize&lt;T&gt;([String][string]  strJson)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | strJson|[String][string] |json 字符串  ||

  * 备注 

      支持DataTable和List集合对象

## Deserialize  `反序列化为泛型对象`

 方法:  JsonHelper.Deserialize


> Deserialize([String][string]  strJson)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | strJson|[String][string] |json 字符串  ||

  * 返回

   > dynamic 

## Parse  `反序列化为Json文档`

 方法:  JsonHelper.Parse


> Parse([String][string]  strJson)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | strJson|[String][string] |字符串  ||

  * 返回

   > JsonDocument 

## ParseElement  `返回序列化为Json节点`

 方法:  JsonHelper.ParseElement


> ParseElement([String][string]  strJson)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | strJson|[String][string] |json 字符串  ||

  * 返回

   > RootElement 

## ParseObject  `反序列化为Json对象`

 方法:  JsonHelper.ParseObject


> ParseObject([String][string]  strJson)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | strJson|[String][string] |json 字符串  ||

  * 返回

   > EnumerateObject 

## ParseArray  `反序列化为Json数组`

 方法:  JsonHelper.ParseArray


> ParseArray([String][string]  strJson)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | strJson|[String][string] |json 字符串  ||

  * 返回

   > EnumerateArray 

## DeserializeJsonTime  `转换为对象,对时间格式化`

 方法:  JsonHelper.DeserializeJsonTime


> DeserializeJsonTime&lt;T&gt;([String][string]  str)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | str|[String][string] |时间格式  ||

## JsonTime  `Json 转换时间格式`

 方法:  JsonHelper.JsonTime


> JsonTime([String][string]  str)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | str|[String][string] |需转换的字符串  ||

  * 返回

   > 返回标准时间格式串 

## StringFormat 

 方法:  JsonHelper.StringFormat


> StringFormat([String][string]  s)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | s|[String][string] |初始字符串  ||

  * 返回

   > 过滤处理 

## JsonKeys  `json 取出字段主键`

 方法:  JsonHelper.JsonKeys


> JsonKeys([JsonElement][jsonelement]  obj)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | obj|[JsonElement][jsonelement] |EnumerateObject 对象  ||

## ToObject  `json 转换为KV对象`

 方法:  JsonHelper.ToObject


> ToObject([JsonElement][jsonelement]  jsonElement)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | jsonElement|[JsonElement][jsonelement] |jsonElement.EnumerateObject  ||

  * 返回

   > string,jsonElement 

## Reader  `json 反序列化到XML`

 方法:  JsonHelper.Reader


> Reader([String][string]  json)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | json|[String][string] |json字符串  ||

## Reader  `反序列化到XML`

 方法:  JsonHelper.Reader


> Reader([String][string]  json,[Encoding][encoding]  encoding)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | json|[String][string] |字符串  ||
    | encoding|[Encoding][encoding] |编码  ||

  * 返回

   > JsonReaderWriterFactory 

## ToValue  `xml 值转换`

 方法:  JsonHelper.ToValue


> ToValue([XElement][xelement]  element)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | element|[XElement][xelement] |XML对象  ||



[object]:https://docs.microsoft.com/zh-cn/dotnet/api/system.object
[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[jsonelement]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonelement
[encoding]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.encoding
[xelement]:https://docs.microsoft.com/zh-cn/dotnet/api/system.xml.linq.xelement


