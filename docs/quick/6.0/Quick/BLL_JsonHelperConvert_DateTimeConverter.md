---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# BLL.JsonHelperConvert.DateTimeConverter
- 引用：Quick.BLL.JsonHelperConvert.DateTimeConverter


## BLL.JsonHelperConvert.DateTimeConverter  `Json 时间格式化，避免ISO转换js.toISOString()`



## Read  `读取加载`

 方法:  BLL.JsonHelperConvert.DateTimeConverter.Read


> Read([Utf8JsonReader@][utf8jsonreader@]  reader,[Type][type]  typeToConvert,[JsonSerializerOptions][jsonserializeroptions]  options)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | reader|[Utf8JsonReader@][utf8jsonreader@] |  ||
    | typeToConvert|[Type][type] |  ||
    | options|[JsonSerializerOptions][jsonserializeroptions] |  ||

## Write  `json 输出 时间`

 方法:  BLL.JsonHelperConvert.DateTimeConverter.Write


> Write([Utf8JsonWriter][utf8jsonwriter]  writer,[DateTime][datetime]  value,[JsonSerializerOptions][jsonserializeroptions]  options)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | writer|[Utf8JsonWriter][utf8jsonwriter] |写入  ||
    | value|[DateTime][datetime] |时间值  ||
    | options|[JsonSerializerOptions][jsonserializeroptions] |配置参数  ||



[utf8jsonreader@]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.utf8jsonreader@
[type]:https://docs.microsoft.com/zh-cn/dotnet/api/system.type
[jsonserializeroptions]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonserializeroptions
[utf8jsonwriter]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.utf8jsonwriter
[datetime]:https://docs.microsoft.com/zh-cn/dotnet/api/system.datetime


