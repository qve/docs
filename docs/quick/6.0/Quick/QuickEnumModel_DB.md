---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickEnumModel.DB
- 引用：Quick.QuickEnumModel.DB


## QuickEnumModel.DB  `数据库提示`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | connectionNotConfigured|  | 数据库连接未配置 |



