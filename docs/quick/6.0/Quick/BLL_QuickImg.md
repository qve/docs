---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# BLL.QuickImg
- 引用：Quick.BLL.QuickImg


## BLL.QuickImg  `图片处理类`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | authcode|  | 随机验证码 |

## CodeImg  `随机码图片生成`

 方法:  BLL.QuickImg.CodeImg


> CodeImg([Int32][int32]  _long,[String][string]  _code)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _long|[Int32][int32] |长度  ||
    | _code|[String][string] |范围  ||

  * 返回

   > FileContentResult 



[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string


