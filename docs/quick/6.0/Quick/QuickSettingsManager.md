---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickSettingsManager
- 引用：Quick.QuickSettingsManager


## QuickSettingsManager  `appsettings.json 配置读取`
注意发布文件路径             路径不能含有netcoreapp文件名             windows 服务模式，将读取根目路径             需要从Nuget安装并引用             Microsoft.Extensions.Configuration.Binder

- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | _iConfiguration|  | 获取根节点 |

## GetSectionValue  `读取自定义配置子节点  Json对象`

 方法:  QuickSettingsManager.GetSectionValue


> GetSectionValue([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |关键词:路径  ||

  * 返回

   > 对象 

## GetSection  `读取自定义配置子节点  Json对象`

 方法:  QuickSettingsManager.GetSection


> GetSection&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |关键词:路径  ||

  * 返回

   > 对象 

## GetValue  `获取配置节点子项`

 方法:  QuickSettingsManager.GetValue


> GetValue&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |节点路径名称  ||



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string


