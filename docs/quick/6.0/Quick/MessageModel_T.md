---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# MessageModel&lt;T&gt;
- 引用：Quick.MessageModel&lt;T&gt;


## MessageModel&lt;T&gt;  `前端模板消息传递实体`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | url|  | 跳转地址 |

## ToString  `序列化JSON对象，得到返回的JSON代码`

 方法:  MessageModel&lt;T&gt;.ToString


> ToString()






