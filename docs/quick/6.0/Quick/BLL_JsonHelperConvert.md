---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# BLL.JsonHelperConvert
- 引用：Quick.BLL.JsonHelperConvert


## BLL.JsonHelperConvert  `json 类型转换自定义方法`



## DynamicConverter.Read  `读取转换`

 方法:  BLL.JsonHelperConvert.DynamicConverter.Read


> DynamicConverter.Read([Utf8JsonReader@][utf8jsonreader@]  reader,[Type][type]  typeToConvert,[JsonSerializerOptions][jsonserializeroptions]  options)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | reader|[Utf8JsonReader@][utf8jsonreader@] |  ||
    | typeToConvert|[Type][type] |  ||
    | options|[JsonSerializerOptions][jsonserializeroptions] |  ||

## DynamicConverter.ReadObject  `Json 转为 IDictionary`

 方法:  BLL.JsonHelperConvert.DynamicConverter.ReadObject


> DynamicConverter.ReadObject([JsonElement][jsonelement]  je)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | je|[JsonElement][jsonelement] |jsonElement  ||

## DynamicConverter.ReadValue  `读取值`

 方法:  BLL.JsonHelperConvert.DynamicConverter.ReadValue


> DynamicConverter.ReadValue([JsonElement][jsonelement]  jsonElement)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | jsonElement|[JsonElement][jsonelement] |  ||

## DateTimeConverter.Read  `读取加载`

 方法:  BLL.JsonHelperConvert.DateTimeConverter.Read


> DateTimeConverter.Read([Utf8JsonReader@][utf8jsonreader@]  reader,[Type][type]  typeToConvert,[JsonSerializerOptions][jsonserializeroptions]  options)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | reader|[Utf8JsonReader@][utf8jsonreader@] |  ||
    | typeToConvert|[Type][type] |  ||
    | options|[JsonSerializerOptions][jsonserializeroptions] |  ||

## DateTimeConverter.Write  `json 输出 时间`

 方法:  BLL.JsonHelperConvert.DateTimeConverter.Write


> DateTimeConverter.Write([Utf8JsonWriter][utf8jsonwriter]  writer,[DateTime][datetime]  value,[JsonSerializerOptions][jsonserializeroptions]  options)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | writer|[Utf8JsonWriter][utf8jsonwriter] |写入  ||
    | value|[DateTime][datetime] |时间值  ||
    | options|[JsonSerializerOptions][jsonserializeroptions] |配置参数  ||

## DateTimeNullableConverter.Read  `读取时间值`

 方法:  BLL.JsonHelperConvert.DateTimeNullableConverter.Read


> DateTimeNullableConverter.Read([Utf8JsonReader@][utf8jsonreader@]  reader,[Type][type]  typeToConvert,[JsonSerializerOptions][jsonserializeroptions]  options)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | reader|[Utf8JsonReader@][utf8jsonreader@] |  ||
    | typeToConvert|[Type][type] |  ||
    | options|[JsonSerializerOptions][jsonserializeroptions] |  ||

## DateTimeNullableConverter.Write  `输出时间值`

 方法:  BLL.JsonHelperConvert.DateTimeNullableConverter.Write


> DateTimeNullableConverter.Write([Utf8JsonWriter][utf8jsonwriter]  writer,[DateTime][datetime]  value,[JsonSerializerOptions][jsonserializeroptions]  options)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | writer|[Utf8JsonWriter][utf8jsonwriter] |  ||
    | value|[Nullable][nullable]&lt;[DateTime][datetime]&gt; |  ||
    | options|[JsonSerializerOptions][jsonserializeroptions] |  ||

## LongConverter.Read  `读取数字`

 方法:  BLL.JsonHelperConvert.LongConverter.Read


> LongConverter.Read([Utf8JsonReader@][utf8jsonreader@]  reader,[Type][type]  type,[JsonSerializerOptions][jsonserializeroptions]  options)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | reader|[Utf8JsonReader@][utf8jsonreader@] |  ||
    | type|[Type][type] |  ||
    | options|[JsonSerializerOptions][jsonserializeroptions] |  ||

## LongConverter.Write  `写入数字`

 方法:  BLL.JsonHelperConvert.LongConverter.Write


> LongConverter.Write([Utf8JsonWriter][utf8jsonwriter]  writer,[Int64][int64]  value,[JsonSerializerOptions][jsonserializeroptions]  options)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | writer|[Utf8JsonWriter][utf8jsonwriter] |  ||
    | value|[Int64][int64] |  ||
    | options|[JsonSerializerOptions][jsonserializeroptions] |  ||



[utf8jsonreader@]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.utf8jsonreader@
[type]:https://docs.microsoft.com/zh-cn/dotnet/api/system.type
[jsonserializeroptions]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonserializeroptions
[jsonelement]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.jsonelement
[utf8jsonwriter]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json.utf8jsonwriter
[datetime]:https://docs.microsoft.com/zh-cn/dotnet/api/system.datetime
[nullable]:https://docs.microsoft.com/zh-cn/dotnet/api/system.nullable
[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64


