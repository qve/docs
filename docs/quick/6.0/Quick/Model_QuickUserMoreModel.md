---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# Model.QuickUserMoreModel
- 引用：Quick.Model.QuickUserMoreModel


## Model.QuickUserMoreModel  `扩展第三方`
继承[QuickUserModel](../Quick/Model_QuickUserModel.md)

- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | WeChatOpenId| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 微信公众号用户识别码 openid |



