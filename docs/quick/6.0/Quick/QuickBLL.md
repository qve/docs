---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickBLL
- 引用：Quick.QuickBLL


## QuickBLL  `基础方法`



## GetMD5  `MD5 加密`

 方法:  QuickBLL.GetMD5


> GetMD5([String][string]  str)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | str|[String][string] |内容  ||

  * 返回

   > 默认大写输出 

## GetMD5  `MD5加密`

 方法:  QuickBLL.GetMD5


> GetMD5([String][string]  str,[Boolean][boolean]  lower)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | str|[String][string] |内容  ||
    | lower|[Boolean][boolean] |true是小写输出  ||

  * 返回

   > 默认大写输出 

## Base64Encode  `将字符串转换成base64格式,使用UTF8字符集`

 方法:  QuickBLL.Base64Encode


> Base64Encode([String][string]  content)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | content|[String][string] |加密内容  ||

## Base64Decode  `将base64格式，转换utf8`

 方法:  QuickBLL.Base64Decode


> Base64Decode([String][string]  content)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | content|[String][string] |解密内容  ||

## Match  `正则取出指定内容`

 方法:  QuickBLL.Match


> Match([String][string]  inputStr,[String][string]  patternStr)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | inputStr|[String][string] |输入字符串  ||
    | patternStr|[String][string] |正则表达式  ||

  * 返回

   > Match 

  * 备注 

      

## Match  `正则取出指定内容`

 方法:  QuickBLL.Match


> Match([String][string]  inputStr,[String][string]  patternStr,[RegexOptions][regexoptions]  ro)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | inputStr|[String][string] |输入字符串  ||
    | patternStr|[String][string] |正则表达式  ||
    | ro|[RegexOptions][regexoptions] |参数  ||

## MatchValue  `获取字符中指定html标签的属性`

 方法:  QuickBLL.MatchValue


> MatchValue([String][string]  str,[String][string]  tag)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | str|[String][string] |字符串  ||
    | tag|[String][string] |html标签  ||

  * 返回

   > 返回属性 

## MatchValue  `获取字符中指定html标签的值,第一个内容,忽略大小写`

 方法:  QuickBLL.MatchValue


> MatchValue([String][string]  str,[String][string]  start,[String][string]  end,[String][string]  startTag,[String][string]  endTag)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | str|[String][string] |字符串  ||
    | start|[String][string] |开始位置标签  ||
    | end|[String][string] |结束第一个匹配?的标签  ||
    | startTag|[String][string] |匹配开始第一个标签?  ||
    | endTag|[String][string] |匹配结束第一个标签?  ||

  * 返回

   > 返回内容 

## IsMatch  `验证字符串是否匹配正则表达式描述的规则`

 方法:  QuickBLL.IsMatch


> IsMatch([String][string]  inputStr,[String][string]  patternStr)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | inputStr|[String][string] |待验证的字符串 不能为空,不区分大大小写  ||
    | patternStr|[String][string] |正则表达式字符串  ||

  * 返回

   > 是否匹配 

## IsMatch  `验证字符串是否匹配正则表达式描述的规则`

 方法:  QuickBLL.IsMatch


> IsMatch([String][string]  inputStr,[String][string]  patternStr,[Boolean][boolean]  ifIgnoreCase)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | inputStr|[String][string] |待验证的字符串 不能为空  ||
    | patternStr|[String][string] |正则表达式字符串  ||
    | ifIgnoreCase|[Boolean][boolean] |匹配时是否不区分大小写  ||

  * 返回

   > 是否匹配 

## IsPhone  `正则验证手机号合法`

 方法:  QuickBLL.IsPhone


> IsPhone([String][string]  Phone,[String][string]  _regex)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | Phone|[String][string] |手机号  ||
    | _regex|[String][string] |验证规则  ||

## ToHide  `正则隐藏内容`

 方法:  QuickBLL.ToHide


> ToHide([String][string]  str,[String][string]  reg,[String][string]  tag)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | str|[String][string] |源内容  ||
    | reg|[String][string] |正则，默认手机号分组  ||
    | tag|[String][string] |替换标签，默认保留1和3组,其它用*号代替  ||

  * 备注 

      默认为手机号隐藏正则

## HtmlMin  `压缩css Js 格式，清除注释与空格，保留/**/ 块注释`

 方法:  QuickBLL.HtmlMin


> HtmlMin([String][string]  _html)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _html|[String][string] |html 代码  ||

## GetHtmlTagContent 

 方法:  QuickBLL.GetHtmlTagContent


> GetHtmlTagContent([String][string]  str,[String][string]  tag)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | str|[String][string] |字符串  ||
    | tag|[String][string] |html标签  ||

  * 返回

   > 值 

## GetHtmlTagContent  `获取html指定标签之间的内容`

 方法:  QuickBLL.GetHtmlTagContent


> GetHtmlTagContent([String][string]  str,[String][string]  tag,[String][string]  attr)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | str|[String][string] |字符串  ||
    | tag|[String][string] |html标签  ||
    | attr|[String][string] |属性  ||

## GetValueByRegex  `获取指定规则间的内容`

 方法:  QuickBLL.GetValueByRegex


> GetValueByRegex([String][string]  str,[String][string]  tag,[String][string]  end)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | str|[String][string] |初始字符串  ||
    | tag|[String][string] |开始标签  ||
    | end|[String][string] |最后的结束标签  ||

  * 返回

   > 返回之间的值 

## IsIP  `检查IP V4 地址格式`

 方法:  QuickBLL.IsIP


> IsIP([String][string]  ip)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | ip|[String][string] |  ||

## IPToNumber  `将IPv4格式的字符串转换为int型表示`

 方法:  QuickBLL.IPToNumber


> IPToNumber([String][string]  strIPAddress)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | strIPAddress|[String][string] |IPv4格式的字符  ||

## ReplaceLineBreaks  `替换一个字符串中换行符（一般为“\r\n”）成特定的字符串`

 方法:  QuickBLL.ReplaceLineBreaks


> ReplaceLineBreaks([String][string]  lines,[String][string]  replacement)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | lines|[String][string] |当前字符串  ||
    | replacement|[String][string] |要替换成的字符串  ||

## UbbDecode  `ubb转html`

 方法:  QuickBLL.UbbDecode


> UbbDecode([String][string]  str)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | str|[String][string] |包含 BBCode 的字符串  ||

## RndNum  `生成0-9随机数`

 方法:  QuickBLL.RndNum


> RndNum([Int32][int32]  Num)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | Num|[Int32][int32] |生成长度  ||

## RandNum  `两数之间随机数`

 方法:  QuickBLL.RandNum


> RandNum([Int32][int32]  min,[Int32][int32]  max)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | min|[Int32][int32] |  ||
    | max|[Int32][int32] |  ||

## RandCode  `生成随机字符 认证码`

 方法:  QuickBLL.RandCode


> RandCode([Int32][int32]  codeNum,[String][string]  _char)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | codeNum|[Int32][int32] |认证码长度  ||
    | _char|[String][string] |认证码生成范围  ||

  * 返回

   > 认证码 

## TimeStamp  `当前时间戳兼容js时间数字`

 方法:  QuickBLL.TimeStamp


> TimeStamp()


  * 返回

   > 获取1970-01-01至当前时间的毫秒数 

## TimeFormat  `当前时间格式化`

 方法:  QuickBLL.TimeFormat


> TimeFormat([String][string]  rule)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | rule|[String][string] |格式规则  ||

## TimeFormat  `时间格式化`

 方法:  QuickBLL.TimeFormat


> TimeFormat([DateTime][datetime]  dt,[String][string]  rule)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | dt|[DateTime][datetime] |默认时间为当前  ||
    | rule|[String][string] |规则  ||

## BuildZero  `补0函数`

 方法:  QuickBLL.BuildZero


> BuildZero([Int32][int32]  j)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | j|[Int32][int32] |需要补的数量  ||

  * 返回

   > 文本0 

## CheckUserAgent  `根据 UserAgent 判断浏览器类型`

 方法:  QuickBLL.CheckUserAgent


> CheckUserAgent([String][string]  agent)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | agent|[String][string] |Request.Headers["User-Agent"]  ||

## CheckUserAgentApp  `判断是否来自微信请求`

 方法:  QuickBLL.CheckUserAgentApp


> CheckUserAgentApp([String][string]  agent)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | agent|[String][string] |是否含有 micromessenger  ||

## QRCodeUrlEncrypt  `微信扫码与网关参数请求 加密算法`

 方法:  QuickBLL.QRCodeUrlEncrypt


> QRCodeUrlEncrypt([String][string]  val,[Int64][int64]  _ts)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | val|[String][string] |明文内容  ||
    | _ts|[Int64][int64] |自定义密钥，net 4.8 默认是时间戳  ||

  * 返回

   > 密文，QRCodeUrlDecrypt 

  * 备注 

      兼容net 4.8 保持一致，微信扫码参数绑定             建议net 5.0 密钥使用应用Id取出应用密文

## QRCodeUrlEncrypt  `微信扫码与网关参数请求 加密算法`

 方法:  QuickBLL.QRCodeUrlEncrypt


> QRCodeUrlEncrypt([String][string]  val,[String][string]  _AppSecret)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | val|[String][string] |明文内容  ||
    | _AppSecret|[String][string] |传入应用密钥  ||

  * 备注 

      建议net 5.0 密钥使用应用Id取出应用密文

## QRCodeUrlDecrypt  `微信扫码与网关参数请求 解密算法`

 方法:  QuickBLL.QRCodeUrlDecrypt


> QRCodeUrlDecrypt([String][string]  val,[Int64][int64]  _ts)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | val|[String][string] |密文内容  ||
    | _ts|[Int64][int64] |自定义密钥，net 4.8 默认是时间戳  ||

  * 返回

   > 明文 QRCodeUrlEncrypt 

  * 备注 

      兼容net 4.8 保持一致，微信扫码参数绑定             建议net 5.0 密钥使用应用Id取出应用密文

## QRCodeUrlDecrypt  `微信扫码与网关参数请求 解密算法`

 方法:  QuickBLL.QRCodeUrlDecrypt


> QRCodeUrlDecrypt([String][string]  val,[String][string]  _AppSecret)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | val|[String][string] |密文内容  ||
    | _AppSecret|[String][string] |传入应用密钥  ||

  * 备注 

      建议net 5.0 密钥使用应用Id取出应用密文

## EncryptTextToMemory  `加密文本内存模式  Encrypt`

 方法:  QuickBLL.EncryptTextToMemory


> EncryptTextToMemory([String][string]  Data,[Byte][byte]  Key,[Byte][byte]  IV)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | Data|[String][string] |加密文本  ||
    | Key|[Byte][byte] |  ||
    | IV|[Byte][byte] |  ||

  * 调试信息


## DecryptTextFromMemory  `解密文本来自内存 Decrypt`

 方法:  QuickBLL.DecryptTextFromMemory


> DecryptTextFromMemory([Byte][byte]  Data,[Byte][byte]  Key,[Byte][byte]  IV)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | Data|[Byte][byte] |需解密文本  ||
    | Key|[Byte][byte] |  ||
    | IV|[Byte][byte] |  ||

## Encrypt  `url 参数 加密DES`

 方法:  QuickBLL.Encrypt


> Encrypt([String][string]  strToEncrypt,[String][string]  strEncryptKey)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | strToEncrypt|[String][string] |要加密的字符串  ||
    | strEncryptKey|[String][string] |密钥 8位数  ||

  * 返回

   > ToBase64String 

  * 备注 

      QuickSettings.DB.PassKey

## Decrypt  `url 参数 解密DES`

 方法:  QuickBLL.Decrypt


> Decrypt([String][string]  strToDecrypt,[String][string]  strEncryptKey)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | strToDecrypt|[String][string] |要解密的字符串 FromBase64String  ||
    | strEncryptKey|[String][string] |密钥，8位数  ||

  * 备注 

      QuickSettings.DB.PassKey

## UrlEncode  `js 前端编码 encodeURIComponent`

 方法:  QuickBLL.UrlEncode


> UrlEncode([String][string]  body)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | body|[String][string] |  ||

## UrlDecode  `js 前端 解码 decodeURIComponent`

 方法:  QuickBLL.UrlDecode


> UrlDecode([String][string]  body)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | body|[String][string] |  ||



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[boolean]:https://docs.microsoft.com/zh-cn/dotnet/api/system.boolean
[regexoptions]:https://docs.microsoft.com/zh-cn/dotnet/api/system.text.regularexpressions.regexoptions
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[datetime]:https://docs.microsoft.com/zh-cn/dotnet/api/system.datetime
[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64
[byte]:https://docs.microsoft.com/zh-cn/dotnet/api/system.byte


