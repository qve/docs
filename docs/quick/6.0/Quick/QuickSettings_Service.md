---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# QuickSettings.Service
- 引用：Quick.QuickSettings.Service


## QuickSettings.Service  `后台服务`



## GetValue  `获取服务器配置的值`

 方法:  QuickSettings.Service.GetValue


> GetValue([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||

  * 返回

   > Service:key 

## GetValue  `读取配置值`

 方法:  QuickSettings.Service.GetValue


> GetValue&lt;T&gt;([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |主键  ||

  * 返回

   > Service:key 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string


