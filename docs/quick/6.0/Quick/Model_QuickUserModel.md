---
template: 2021.12
author: quick.docs
date: 2022年03月20日 09:05

---
# Model.QuickUserModel
- 引用：Quick.Model.QuickUserModel


## Model.QuickUserModel  `用户基础对象`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | UserID| [Int64](https://docs.microsoft.com/zh-cn/dotnet/api/system.int64) | 用户ID |
 | UserName| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 用户名称 |
 | Phone| [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) | 用户绑定手机号 |
 | GroupID| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 所属组织ID |
 | RoleID| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 角色组 |
 | Level| [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32) | 用户等级 |



