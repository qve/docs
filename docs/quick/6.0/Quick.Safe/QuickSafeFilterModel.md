---
template: 2021.12
author: quick.docs
date: 2021年12月31日 23:46
---

# QuickSafeFilterModel

- 引用：Quick.Safe.QuickSafeFilterModel

## QuickSafeFilterModel `请求权限验证`

- **类属性**

| 名称      | 类型                                                                  | 说明                                                                                                  |
| --------- | --------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------- |
| isPass    | [Boolean](https://docs.microsoft.com/zh-cn/dotnet/api/system.boolean) | 是否验证通过                                                                                          |
| code      | [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32)     | 返回验证的类别编码                                                                                    |
| msg       | [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)   | 返回的验证消息                                                                                        |
| appKey    | [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)   | 请求应用 Key                                                                                          |
| appID     | [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32)     | 应用 ID                                                                                               |
| path      | [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)   | 当前请求路径 QF_Page.Path                                                                             |
| pathID    | [Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32)     | 访问请求 QF_Page.ID 每个需要验证的页面设置 ID                                                         |
| cmd       | [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)   | 操作指令 QF_Control <br /> list,add,edit,del:状态删除,erase:物理删除,audit:审核,resc:超级撤销审核权限 |
| method    | [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)   | 请求方式 <br /> GET POST Delete Put                                                                   |
| action    | [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)   | 可用的权限列表 <br /> QF_Control.Code 1,2,3,4,5,6,7,8,9,10                                            |
| TokenUser | QuickToken                                                            | 当前验证票据 <br /> 令牌用户                                                                          |

- **构造函数**

  - QuickSafeFilterModel()
    `验证返回对象`
