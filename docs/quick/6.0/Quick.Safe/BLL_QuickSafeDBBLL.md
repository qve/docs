---
template: 2021.12
author: quick.docs
date: 2021年12月31日 23:46

---
# BLL.QuickSafeDBBLL
- 引用：Quick.Safe.BLL.QuickSafeDBBLL


## BLL.QuickSafeDBBLL  `权限数据库请求方法 Quick`



## GetAppPathID  `查询当前 Url的 数据库授权ID`

 方法:  BLL.QuickSafeDBBLL.GetAppPathID


> GetAppPathID([String][string]  _Url)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _Url|[String][string] |连接地址  ||

  * 返回

   > 已审核 QF_Page.ID 

## GetControlCode  `取出cmd全部API操作权限命令集合`

 方法:  BLL.QuickSafeDBBLL.GetControlCode


> GetControlCode([Int32][int32]  _AppId,[Int64][int64]  _PageID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _AppId|[Int32][int32] |专属应用id  ||
    | _PageID|[Int64][int64] |专属页面id,小于2取出，所有应用下命令  ||

  * 返回

   > CMD命名 

  * 备注 

      QF_Control              \ &quot;list\ &quot;:1,\ &quot;add\ &quot;:2,\ &quot;edit\ &quot;:3,\ &quot;del\ &quot;:4

## GetPageRoleRules  `读取页面角色权限`

 方法:  BLL.QuickSafeDBBLL.GetPageRoleRules


> GetPageRoleRules([Int32][int32]  _AppId,[Int32][int32]  _RoleID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _AppId|[Int32][int32] |所属应用  ||
    | _RoleID|[Int32][int32] |所属角色组  ||

  * 备注 

      PageRules from [QF_Rules] where [Quick].[dbo].[QF_RoleRules] and(QF_AppInfo_ID=1 or  QF_AppInfo_ID=7) and QF_Role_ID

## GetRoleMenuRules  `角色组菜单规则`

 方法:  BLL.QuickSafeDBBLL.GetRoleMenuRules


> GetRoleMenuRules([Int32][int32]  _AppId,[Int32][int32]  _RoleID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _AppId|[Int32][int32] |应用id  ||
    | _RoleID|[Int32][int32] |角色组  ||

  * 备注 

      取出当前请求应用，角色组=1和当前角色组

## GetLoginUser  `用户名+密码 登陆DBBase`

 方法:  BLL.QuickSafeDBBLL.GetLoginUser


> GetLoginUser([Int32][int32]  AppID,[Int32][int32]  AppLoginJoinType,[String][string]  LoginCode,[String][string]  LoginPwd,[String][string]  loginInfo)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | AppID|[Int32][int32] |登陆应用配置ID  ||
    | AppLoginJoinType|[Int32][int32] |应用登录授权模式  ||
    | LoginCode|[String][string] |账户  ||
    | LoginPwd|[String][string] |密码  ||
    | loginInfo|[String][string] |登录描述信息,默认:pwd  ||

  * 返回

   > 存储过程 MP_APP_LoginByPwd 

## GetLoginBySMS  `取出登录信息，来至于手机短信登录`

 方法:  BLL.QuickSafeDBBLL.GetLoginBySMS


> GetLoginBySMS([Int32][int32]  AppID,[Int32][int32]  AppLoginJoinType,[Int64][int64]  QF_User_ID,[String][string]  LoginCode,[String][string]  LoginPwd,[String][string]  loginInfo)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | AppID|[Int32][int32] |应用ID  ||
    | AppLoginJoinType|[Int32][int32] |应用登录授权模式  ||
    | QF_User_ID|[Int64][int64] |登录用户  ||
    | LoginCode|[String][string] |登录手机号  ||
    | LoginPwd|[String][string] |设置新的登录密码密文  ||
    | loginInfo|[String][string] |登录描述信息,默认:sms  ||

## GetLoginUser  `根据登录ID 取出登录用户信息DBBase`

 方法:  BLL.QuickSafeDBBLL.GetLoginUser


> GetLoginUser([Int32][int32]  AppID,[Int64][int64]  QF_Login_ID,[String][string]  loginInfo)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | AppID|[Int32][int32] |应用ID  ||
    | QF_Login_ID|[Int64][int64] |登录ID  ||
    | loginInfo|[String][string] |登录描述信息，默认:token  ||

  * 返回

   > 存储过程 MP_APP_LoginByID 

## IsRegisteredByPhone  `判断手机号是否注册`

 方法:  BLL.QuickSafeDBBLL.IsRegisteredByPhone


> IsRegisteredByPhone([String][string]  phone)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | phone|[String][string] |注册号码  ||

  * 备注 

      存储过程 QP_IsJoinByPhone

## JoinPhoneBySMS  `新手机号码，短信认证记录临时表 QF_Phone`

 方法:  BLL.QuickSafeDBBLL.JoinPhoneBySMS


> JoinPhoneBySMS([Int32][int32]  QF_AppInfo_ID,[String][string]  phone)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | QF_AppInfo_ID|[Int32][int32] |注册来源应用  ||
    | phone|[String][string] |注册手机号  ||

  * 返回

   > 返回注册ID 

  * 备注 

      Quick 存储过程：QP_JoinPhone

## JoinUserByPhone  `注册用户  QP_JoinUserByPhone`

 方法:  BLL.QuickSafeDBBLL.JoinUserByPhone


> JoinUserByPhone(QuickUserJoinBaseModel  o)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | o|QuickUserJoinBaseModel |Phone,LoginPwd  ||

  * 返回

   > 返回注册用户ID 

  * 备注 

      Quick 存储过程：QP_JoinUserByPhone



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64


