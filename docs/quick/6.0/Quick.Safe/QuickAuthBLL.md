---
template: 2021.12
author: quick.docs
date: 2021年12月31日 23:46

---
# QuickAuthBLL
- 引用：Quick.Safe.QuickAuthBLL


## QuickAuthBLL  `请求连接身份安全验证方法`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | rs|  | 会话存储 |
- **构造函数**

  - QuickAuthBLL([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _appKey)
       `连接请求验证方法`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _appKey |当前应用Key


## SetCodeTimeOut  `设置验证码有效期`

 方法:  QuickAuthBLL.SetCodeTimeOut


> SetCodeTimeOut([String][string]  key,[String][string]  val,[Int64][int64]  TimeOut)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |code:方法名:关键字 （注意不要重复）  ||
    | val|[String][string] |内容  ||
    | TimeOut|[Int64][int64] |秒数  ||

  * 返回

   > code:key 

## GetCodeTimeOut  `获取超时验证码`

 方法:  QuickAuthBLL.GetCodeTimeOut


> GetCodeTimeOut([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |方法名:关键字（禁止重复）  ||

  * 返回

   > code:key 

## RemoveCodeTimeOut  `删除验证码缓存`

 方法:  QuickAuthBLL.RemoveCodeTimeOut


> RemoveCodeTimeOut([String][string]  key)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | key|[String][string] |方法名:关键字（禁止重复）  ||

  * 返回

   > code:key 

## SetCodeSMSAuth  `缓存登录与注册短信`

 方法:  QuickAuthBLL.SetCodeSMSAuth


> SetCodeSMSAuth([String][string]  phone,[String][string]  val,[Int64][int64]  TimeOut)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | phone|[String][string] |手机号  ||
    | val|[String][string] |短信验证码  ||
    | TimeOut|[Int64][int64] |默认300秒  ||

## GetCodeSMSAuth  `取出登录与注册短信`

 方法:  QuickAuthBLL.GetCodeSMSAuth


> GetCodeSMSAuth([String][string]  phone)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | phone|[String][string] |验证手机号  ||

## RemoveCodeSMSAuth  `删除登录与注册短信`

 方法:  QuickAuthBLL.RemoveCodeSMSAuth


> RemoveCodeSMSAuth([String][string]  phone)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | phone|[String][string] |验证手机号  ||

## BasicTokenUser  `基本验证，取出用户信息`

 方法:  QuickAuthBLL.BasicTokenUser


> BasicTokenUser([QuickAuthBasicModel][quickauthbasicmodel]  qabm)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | qabm|[QuickAuthBasicModel][quickauthbasicmodel] |base64 验证字符串  ||

  * 返回

   > 返回用户信息ID>0 

## AuthLoginCode  `用户登陆认证，当前账户登陆次数最多5次`

 方法:  QuickAuthBLL.AuthLoginCode


> AuthLoginCode([QuickLoginModel][quickloginmodel]  model,[String][string]  SessionId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | model|[QuickLoginModel][quickloginmodel] |传入认证参数 登陆类型,1:账户密码登陆,2:动态短信码登录,3:票据登陆带刷新用户  ||
    | SessionId|[String][string] |当前会话id  ||

## LoginByUser  `用户名+密码 方法登陆`

 方法:  QuickAuthBLL.LoginByUser


> LoginByUser([String][string]  AppID,[String][string]  LoginCode,[String][string]  LoginPwd)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | AppID|[String][string] |登陆应用配置ID  ||
    | LoginCode|[String][string] |账户  ||
    | LoginPwd|[String][string] |密码  ||

  * 返回

   > MP_APP_LoginByPwd 

## LoginToken  `用户凭令牌登陆`

 方法:  QuickAuthBLL.LoginToken


> LoginToken([String][string]  refreshToken,[String][string]  SessionId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | refreshToken|[String][string] |刷新登陆凭证  ||
    | SessionId|[String][string] |当前需要绑定的会话  ||

## LoginByPhone  `手机短信验证登录，并且根据平台配置，判断是否注册新用户`

 方法:  QuickAuthBLL.LoginByPhone


> LoginByPhone([String][string]  loginCode,[String][string]  loginPwd,[String][string]  sessionId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | loginCode|[String][string] |手机号  ||
    | loginPwd|[String][string] |短信验证码  ||
    | sessionId|[String][string] |当前会话ID  ||

## LoginByUserID  `短信登录来自用户ID`

 方法:  QuickAuthBLL.LoginByUserID


> LoginByUserID([Int64][int64]  _UserID,[String][string]  loginCode,[String][string]  loginPwd,[String][string]  sessionId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _UserID|[Int64][int64] |用户ID  ||
    | loginCode|[String][string] |登录用户名  ||
    | loginPwd|[String][string] |登录密码  ||
    | sessionId|[String][string] |当前会话ID  ||

## BindTokenLogin  `绑定登陆用户更新票据`

 方法:  QuickAuthBLL.BindTokenLogin


> BindTokenLogin([Int32][int32]  _AppID,[String][string]  SessionId,[QuickLoginCacheModel][quicklogincachemodel]  cpt)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _AppID|[Int32][int32] |应用配置ID  ||
    | SessionId|[String][string] |用户会话  ||
    | cpt|[QuickLoginCacheModel][quicklogincachemodel] |用户登陆信息  ||

## IsRegisteredByPhone  `判断手机号是否已经注册`

 方法:  QuickAuthBLL.IsRegisteredByPhone


> IsRegisteredByPhone([String][string]  phone)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | phone|[String][string] |手机号  ||

## JoinPhoneBySMS  `记录发送短信的手机号`

 方法:  QuickAuthBLL.JoinPhoneBySMS


> JoinPhoneBySMS([Int32][int32]  QF_AppInfo_ID,[String][string]  phone)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | QF_AppInfo_ID|[Int32][int32] |来源应用  ||
    | phone|[String][string] |发送手机号  ||

  * 备注 

      QuickSafeDBBLL.JoinPhoneBySMS

## JoinUserByPhone  `注册用户来自手机号`

 方法:  QuickAuthBLL.JoinUserByPhone


> JoinUserByPhone([String][string]  phone,[String][string]  loginPwd)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | phone|[String][string] |注册手机号  ||
    | loginPwd|[String][string] |重置密码  ||

  * 返回

   > 返回注册用户ID 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64
[quickauthbasicmodel]:../Quick.Safe/Model_QuickAuthBasicModel.md
[quickloginmodel]:../Quick.Safe/Model_QuickLoginModel.md
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[quicklogincachemodel]:../Quick.Safe/Model_QuickLoginCacheModel.md


