---
template: 2021.12
author: quick.docs
date: 2021年12月31日 23:46

---
# QuickSafeEnum
- 引用：Quick.Safe.QuickSafeEnum


## QuickSafeEnum  `权限参数`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Auth.All|  | 默认权限验证全部,token,user,role,cmd权限 |
 | Auth.token|  | 只验证token合法 |
 | Auth.user|  | 验证Token并取出用户信息 |
 | Auth.role|  | 验证Token并验证角色组权限 |
 | Auth.No|  | 不验证当前请求 |
 | Auth.NoToken|  | 不验证当前请求，如有票据取出认证令牌 |
 | Auth.NoUser|  | 不验证当前请求，如有票据取出用户信息 |



