---
template: 2021.12
author: quick.docs
date: 2021年12月31日 23:46

---
# Model.QuickSafeBasicModel
- 引用：Quick.Safe.Model.QuickSafeBasicModel


## Model.QuickSafeBasicModel  `前端参数验证返回对象`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | AppId|  | 应用ID |
 | AppSecret|  | 应用密钥 |



