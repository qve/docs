---
template: 2021.12
author: quick.docs
date: 2021年12月31日 23:46

---
# Model.QuickAuthBasicModel
- 引用：Quick.Safe.Model.QuickAuthBasicModel


## Model.QuickAuthBasicModel  `Token Basic 前端传入参数需保持一致`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | key|  | App Key 配置 |
 | val|  | 验证的值，默认token 令牌 |
 | tamp|  | 请求时间戳，默认半个小时有效（注意：客户端与服务器端时间差）             时间戳(从1970.1.1开始的毫秒数)/1000, 可以转变成Unix时间戳 |
 | sign|  | 请求的验证签名算法md5(时间戳/token) |
 | title|  | 描述，不是必须，不参与验证算法 |



