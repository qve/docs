---
template: 2021.12
author: quick.docs
date: 2021年12月31日 23:46

---
# Model.QuickSaleRulesModel
- 引用：Quick.Safe.Model.QuickSaleRulesModel


## Model.QuickSaleRulesModel  `页面路径权限 QF_Page`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | p|  | path 请求路径 ID |
 | c|  | 授权使用的控件Id |



