---
template: 2021.12
author: quick.docs
date: 2021年12月31日 23:46

---
# QuickSafeBLL
- 引用：Quick.Safe.QuickSafeBLL


## QuickSafeBLL  `验证公共方法`



## GetLoginInt  `获取登陆重试次数`

 方法:  QuickSafeBLL.GetLoginInt


> GetLoginInt([String][string]  _LoginCode)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _LoginCode|[String][string] |登陆账户  ||

  * 返回

   > 当前已经重试登陆次数 

## SetLoginInt  `记录登陆重试次数，超时 15分钟`

 方法:  QuickSafeBLL.SetLoginInt


> SetLoginInt([String][string]  _LoginCode,[Int32][int32]  _value)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _LoginCode|[String][string] |登录账户  ||
    | _value|[Int32][int32] |已登录次数  ||

## RemoveLoginInt  `登录成功后删除记录`

 方法:  QuickSafeBLL.RemoveLoginInt


> RemoveLoginInt([String][string]  _LoginCode)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _LoginCode|[String][string] |  ||

## GetSmsInt  `从缓存获取发送短信密码次数`

 方法:  QuickSafeBLL.GetSmsInt


> GetSmsInt([String][string]  _LoginCode)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _LoginCode|[String][string] |登陆账户  ||

  * 备注 

      建议限制15分钟 最多5次修改             QuickCache

## SetSmsInt  `记录发送短信密码次数,15分钟有效`

 方法:  QuickSafeBLL.SetSmsInt


> SetSmsInt([String][string]  _LoginCode,[Int32][int32]  _value)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _LoginCode|[String][string] |登陆账户  ||
    | _value|[Int32][int32] |当前登陆次数  ||

## RemoveSmsInt  `登录成功后删除记录`

 方法:  QuickSafeBLL.RemoveSmsInt


> RemoveSmsInt([String][string]  _LoginCode)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _LoginCode|[String][string] |  ||

## CreateLoginPwd  `生成用户登录验证码,二次加密`

 方法:  QuickSafeBLL.CreateLoginPwd


> CreateLoginPwd([String][string]  pwdMd5)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | pwdMd5|[String][string] |密码Md5  ||

  * 返回

   > 32位MD5密码 

## CreateLoginPwdByCode  `后端匹配前端登录密码加密算法`

 方法:  QuickSafeBLL.CreateLoginPwdByCode


> CreateLoginPwdByCode([String][string]  loginCode,[String][string]  LoginPwd)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | loginCode|[String][string] |登录明文  ||
    | LoginPwd|[String][string] |登录明文密码  ||

  * 返回

   > 返回大写前端加密Md5(Md5(LoginPwd)+loginCode) 

## BindAuthBasic  `生成验证参数`

 方法:  QuickSafeBLL.BindAuthBasic


> BindAuthBasic([QuickAuthBasicModel][quickauthbasicmodel]  qabm,[String][string]  secret)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | qabm|[QuickAuthBasicModel][quickauthbasicmodel] |验证对象  ||
    | secret|[String][string] |应用配置安全验证码  ||

  * 返回

   > QuickBLL.Base64Encode(8,16) 

## BasicAuthSign  `生成请求的验证签名算法`

 方法:  QuickSafeBLL.BasicAuthSign


> BasicAuthSign([QuickAuthBasicModel][quickauthbasicmodel]  qabm,[Int32@][int32@]  _AppSuperRole_ID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | qabm|[QuickAuthBasicModel][quickauthbasicmodel] |App.Secret 没配置，默认为ap  ||
    | _AppSuperRole_ID|[Int32@][int32@] |架构配置超级角色组  ||

  * 返回

   > 返回验证码 md5小写(时间戳/token) 

  * 备注 

      应用Key对应的通信的密码 , 默认是ap，对应前端算法              注意：js substring(8,16) 匹配

## BasicAuthorization  `1 .验证是否平台授权，不取出用户`

 方法:  QuickSafeBLL.BasicAuthorization


> BasicAuthorization([Int32@][int32@]  _AppSuperRole_ID,[String][string]  _Authorization,[String][string]  _code)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _AppSuperRole_ID|[Int32@][int32@] |架构配置超级角色组  ||
    | _Authorization|[String][string] |base64  Token编码加密  ||
    | _code|[String][string] |编码前缀  ||

## BasicAuthorizationUser  `2. 验证并取出用户信息`

 方法:  QuickSafeBLL.BasicAuthorizationUser


> BasicAuthorizationUser([Int32@][int32@]  _AppSuperRole_ID,[String][string]  _Authorization,[String][string]  _code)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _AppSuperRole_ID|[Int32@][int32@] |应用超级权限角色组  ||
    | _Authorization|[String][string] |请求token  ||
    | _code|[String][string] |验证方式  ||

## BasicAuthorizationRole  `3. 用户角色组，权限验证`

 方法:  QuickSafeBLL.BasicAuthorizationRole


> BasicAuthorizationRole([Int32][int32]  Role_ID,[Int32][int32]  Page_ID,[Int32][int32]  _AppId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | Role_ID|[Int32][int32] |QuickUser.RoleID 角色组规则  ||
    | Page_ID|[Int32][int32] |当前请求页面ID，小于1：未设置页面编码将退出控件权限判断  ||
    | _AppId|[Int32][int32] |当前应用ID  ||

## BasicAuthorizationAction  `4.角色组 API CMD 动作验证`

 方法:  QuickSafeBLL.BasicAuthorizationAction


> BasicAuthorizationAction([String@][string@]  _control,[Int32][int32]  Role_ID,[Int64][int64]  Page_ID,[String][string]  _PageCMD,[Int32][int32]  _AppId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _control|[String@][string@] |Page_Control : 当前页面可用权限  ||
    | Role_ID|[Int32][int32] |角色组ID  ||
    | Page_ID|[Int64][int64] |连接页面ID  ||
    | _PageCMD|[String][string] |操作命令  ||
    | _AppId|[Int32][int32] |当前应用ID  ||

## CachePathKey  `缓存路径主键`

 方法:  QuickSafeBLL.CachePathKey


> CachePathKey([String][string]  _path,[String][string]  _Appkey)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _path|[String][string] |请求路径  ||
    | _Appkey|[String][string] |请求应用  ||

  * 备注 

      appid_0.1_safe_path

## CacheCMDKey  `缓存的页面指令json集合`

 方法:  QuickSafeBLL.CacheCMDKey


> CacheCMDKey([Int32][int32]  _AppId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _AppId|[Int32][int32] |请求应用ID  ||

  * 备注 

      qm:cacheCmdkey

## CacheCMDIdsKey  `缓存指令的id集合`

 方法:  QuickSafeBLL.CacheCMDIdsKey


> CacheCMDIdsKey([Int32][int32]  _AppId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _AppId|[Int32][int32] |请求应用id  ||

  * 备注 

      qm:cacheCmdkey:ids

## GetPathAuthID  `取出当前Url 的配置ID`

 方法:  QuickSafeBLL.GetPathAuthID


> GetPathAuthID([String][string]  _Url,[String][string]  appKey)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _Url|[String][string] |请求地址  ||
    | appKey|[String][string] |请求的应用  ||

  * 返回

   > 处理路径待参数 并返回本机缓存的 UrlID 

## BindPageCmd  `合并页面cmd 命令集合`

 方法:  QuickSafeBLL.BindPageCmd


> BindPageCmd([String][string]  _List,[String@][string@]  _cmdIds)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _List|[String][string] |多个页面命令json数组  ||
    | _cmdIds|[String@][string@] |返回命令的id集合  ||

  * 返回

   > 整合为一个json 

## GetPageCmd  `获取全部页面cmd与控件配置`

 方法:  QuickSafeBLL.GetPageCmd


> GetPageCmd([Int32][int32]  _AppId,[Int64][int64]  _PageID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _AppId|[Int32][int32] |应用key  ||
    | _PageID|[Int64][int64] |专属页面id,小于2取出，所有应用下命令  ||

  * 返回

   > QuickCache小写列表 

  * 备注 

      本机缓存 safe:cmd:             QF_Control 请求命令[CMD]的编码

## GetPageCMDAction  `取出页面控制器所有的操作指令的id`

 方法:  QuickSafeBLL.GetPageCMDAction


> GetPageCMDAction([Int32][int32]  _AppId,[Int64][int64]  _PageID,[Int32][int32]  _type)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _AppId|[Int32][int32] |应用id  ||
    | _PageID|[Int64][int64] |指定页面id  ||
    | _type|[Int32][int32] |指令类型 0:button,1:api  ||

  * 返回

   > 1,2,3,4,5,6,7,8,9,10 

## BindPageRoleRules  `合并多个角色组权限组合去重复`

 方法:  QuickSafeBLL.BindPageRoleRules


> BindPageRoleRules([String][string]  _List)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _List|[String][string] |json 规则数组集合  ||

  * 返回

   > 有权限返回json，无返回空值 

## GetPageRoleRules  `获取接口权限配置`

 方法:  QuickSafeBLL.GetPageRoleRules


> GetPageRoleRules([Int32][int32]  _RoleID,[Int32][int32]  _AppId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _RoleID|[Int32][int32] |角色组  ||
    | _AppId|[Int32][int32] |应用ID  ||

## GetRoleMenuRules  `获取角色组菜单规则来自Memory 缓存`

 方法:  QuickSafeBLL.GetRoleMenuRules


> GetRoleMenuRules([Int32][int32]  _RoleID,[Int32][int32]  _AppId)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _RoleID|[Int32][int32] |角色组  ||
    | _AppId|[Int32][int32] |  ||



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[quickauthbasicmodel]:../Quick.Safe/Model_QuickAuthBasicModel.md
[int32@]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32@
[string@]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string@
[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64


