---
template: 2021.12
author: quick.docs
date: 2021年12月31日 23:46

---
# SessionRedisConfigModel
- 引用：Quick.Safe.SessionRedisConfigModel


## SessionRedisConfigModel  `会话数据库配置`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Prefix|  | 会话存储  Redis key 前缀  &quot;u:s &quot; |
 | Timeout|  | Session超时 分钟数 |
 | AppID|  | 会话应用ID |
 | AppLoginJoinType|  | 注册授权 <br /> 允许其它注册用户登录此应用方式 1:免授权,2:管理员授权,3:用户申请 |



