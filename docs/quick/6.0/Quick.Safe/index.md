---
template: 2021.12
author: quick.docs
date: 2021年12月31日 23:46
---

# Quick.Safe

template: 2021.12
author: quick.docs
date: 2021 年 12 月 31 日 23:46

## Quick.Safe 目录

### [QuickAuthBLL](./QuickAuthBLL.md)

      请求连接身份安全验证方法

### [BLL.QuickSafeDBBLL](./BLL_QuickSafeDBBLL.md)

      权限数据库请求方法 Quick

### [Filters.QuickSafeFilterNoToken](./Filters_QuickSafeFilterNoToken.md)

      不验证当前请求
             如有票据取出认证令牌

### [Filters.QuickSafeFilterNoUser](./Filters_QuickSafeFilterNoUser.md)

      不验证当前请求用户
            如有票据取出用户信息

### [QuickSafeFilter](./QuickSafeFilter.md)

      http 请求身份安全验证

### [QuickSafeFilterNo](./QuickSafeFilterNo.md)

      不验证当前请求

### [QuickSafeFilterRole](./QuickSafeFilterRole.md)

      验证Token并验证角色组权限

### [QuickSafeFilterToken](./QuickSafeFilterToken.md)

      只验证token合法

### [QuickSafeFilterUser](./QuickSafeFilterUser.md)

      验证Token并取出用户信息

### [Model.QuickAuthBasicModel](./Model_QuickAuthBasicModel.md)

      Token Basic 前端传入参数需保持一致

### [Model.QuickLoginCacheModel](./Model_QuickLoginCacheModel.md)

      登录用户信息缓存
             QF_Login

### [Model.QuickLoginModel](./Model_QuickLoginModel.md)

      请求登录认证的信息

### [Model.QuickSafeBasicModel](./Model_QuickSafeBasicModel.md)

      前端参数验证返回对象

### [Model.QuickSaleRulesModel](./Model_QuickSaleRulesModel.md)

      页面路径权限 QF_Page

### [QuickSafeEnum](./QuickSafeEnum.md)

      权限参数

### [QuickSafeEnum.Auth](./QuickSafeEnum_Auth.md)

      请求身份验证方式

### [QuickSafeFilterModel](./QuickSafeFilterModel.md)

      请求权限验证

### [QuickSafeBLL](./QuickSafeBLL.md)

      验证公共方法

### [SessionRedis](./SessionRedis.md)

      session 数据读写

### [SessionRedisConfigModel](./SessionRedisConfigModel.md)

      会话数据库配置
