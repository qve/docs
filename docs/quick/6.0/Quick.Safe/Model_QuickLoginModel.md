---
template: 2021.12
author: quick.docs
date: 2021年12月31日 23:46

---
# Model.QuickLoginModel
- 引用：Quick.Safe.Model.QuickLoginModel


## Model.QuickLoginModel  `请求登录认证的信息`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | loginType|  | 登陆方式，默认1:账户密码,2:手机号,3:刷新票据 |
 | appKey|  | 应用认证编码 |
 | loginCode|  | 登陆帐户 / refresh_token临时验证码 |
 | loginPwd|  | 登录密码 QuickSafeBLL.CreateLoginPwdByCode |
 | vailCode|  | 登录图文验证码,短信验证码，加密传输 |



