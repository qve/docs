---
template: 2021.12
author: quick.docs
date: 2021年12月31日 23:46

---
# SessionRedis
- 引用：Quick.Safe.SessionRedis


## SessionRedis  `session 数据读写`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | TokenConn|  | 会话服务连接 |
 | UserConn|  | 用户服务连接 |
- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | _Config|  | session 扩展配置 |
- **构造函数**

  - SessionRedis([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    _appKey)
       `session 配置数据读写封装`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _appKey |指定应用key


## GetSessionIdKey  `获取session Us 规范定义的 Key`

 方法:  SessionRedis.GetSessionIdKey


> GetSessionIdKey([Int32][int32]  _AppID,[String][string]  _SessionID)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _AppID|[Int32][int32] |指定操作的应用ID  ||
    | _SessionID|[String][string] |用户Session  ||

  * 返回

   > u:s:AppID:SessionID 

## UpSession  `更新用户Session，默认配置缓存`

 方法:  SessionRedis.UpSession


> UpSession([String][string]  _SessionRedisKey,[String][string]  pairs)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _SessionRedisKey|[String][string] |sessionkey  ||
    | pairs|[String][string] |批量写入值  ||

## GetUserInfo  `根据Token读取redis 用户缓存信息`

 方法:  SessionRedis.GetUserInfo


> GetUserInfo([Int32][int32]  _AppId,[String][string]  token)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _AppId|[Int32][int32] |请求应用  ||
    | token|[String][string] |请求令牌  ||

  * 返回

   > 绑定的用户对象 

## BindLoginSession  `绑定Session用户信息`

 方法:  SessionRedis.BindLoginSession


> BindLoginSession([Int32][int32]  _AppID,[String][string]  sessionId,[QuickLoginCacheModel][quicklogincachemodel]  cpt)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | _AppID|[Int32][int32] |当前登录应用Id  ||
    | sessionId|[String][string] |当前登录会话id  ||
    | cpt|[QuickLoginCacheModel][quicklogincachemodel] |绑定的对象  ||

  * 返回

   > 写入的用户库 



[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[quicklogincachemodel]:../Quick.Safe/Model_QuickLoginCacheModel.md


