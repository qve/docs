---
template: 2021.12
author: quick.docs
date: 2021年12月31日 23:46

---
# QuickSafeEnum.Auth
- 引用：Quick.Safe.QuickSafeEnum.Auth


## QuickSafeEnum.Auth  `请求身份验证方式`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | All|  | 默认权限验证全部,token,user,role,cmd权限 |
 | token|  | 只验证token合法 |
 | user|  | 验证Token并取出用户信息 |
 | role|  | 验证Token并验证角色组权限 |
 | No|  | 不验证当前请求 |
 | NoToken|  | 不验证当前请求，如有票据取出认证令牌 |
 | NoUser|  | 不验证当前请求，如有票据取出用户信息 |



