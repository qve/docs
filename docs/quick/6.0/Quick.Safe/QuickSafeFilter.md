---
template: 2021.12
version: 2021.01
author: quick.docs
date: 2021年12月31日 23:46

---
# QuickSafeFilter
- 引用：Quick.Safe.QuickSafeFilter
- version: 2021.01


## QuickSafeFilter  `http 请求身份安全验证`
[使用示例](../code/safe.html) [QuickSafeFilterNo 不验证请求](../Quick.Safe/QuickSafeFilterNo.md)

- **构造函数**

  - QuickSafeFilter([QuickSafeFilterModel](../Quick.Safe/QuickSafeFilterModel.md)    filter,[Auth](../Quick.Safe/QuickSafeEnum_Auth.md)    _authType)
       `安全验证`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | filter |当前请求参数
      | _authType |验证方式


## OnActionExecuting  `在控制器执行之前调用`

 方法:  QuickSafeFilter.OnActionExecuting


> OnActionExecuting([ActionExecutingContext][actionexecutingcontext]  context)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | context|[ActionExecutingContext][actionexecutingcontext] |  ||

  * 备注 

      执行父类的异步方法  异步方法  OnActionExecuting同时执行



[actionexecutingcontext]:https://docs.microsoft.com/zh-cn/dotnet/api/microsoft.aspnetcore.mvc.filters.actionexecutingcontext


