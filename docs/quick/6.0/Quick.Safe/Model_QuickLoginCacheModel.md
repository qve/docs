---
template: 2021.12
author: quick.docs
date: 2021年12月31日 23:46

---
# Model.QuickLoginCacheModel
- 引用：Quick.Safe.Model.QuickLoginCacheModel


## Model.QuickLoginCacheModel  `登录用户信息缓存              QF_Login`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | ID|  | 登录账户 ID |
 | QF_Group_ID|  | 所属 组织 |
 | QF_User_ID|  | 联系人 |
 | AP_User_ID|  | 网络会员ID |
 | QF_Role_ID|  | 所属角色组 |
 | NickName|  | 会员昵称 |
 | LoginCode|  | 登陆账户,默认手机号 |
 | LoginAppLimit|  | 登录限制 1:单端,2:多端登录,3:多端会话 |
- **构造函数**

  - Model.QuickLoginCacheModel()
       `初始化设置默认值构造`





