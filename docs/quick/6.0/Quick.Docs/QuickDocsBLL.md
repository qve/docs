---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:28

---
# QuickDocsBLL
- 引用：Quick.Docs.QuickDocsBLL


## QuickDocsBLL  `公共方法`



 --- 
## RandCode

 方法:  QuickDocsBLL.RandCode

### `生成随机字符 认证码`


> RandCode([Int32][int32]  codeNum,[String][string]  _char)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | codeNum|[Int32][int32] |认证码长度  ||
    | _char|[String][string] |认证码生成范围  ||

  - **返回**

   > 认证码 

 --- 
## Deserialize

 方法:  QuickDocsBLL.Deserialize

### `反序列化为对象`


> Deserialize&lt;T&gt;([String][string]  xml)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | xml|[String][string] |  ||

  - **返回**

   > 

 --- 
## GetVowelCount

 方法:  QuickDocsBLL.GetVowelCount

### `统计字符出现的次数`


> GetVowelCount([String][string]  str,[String][string]  val)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | str|[String][string] |源字符串  ||
    | val|[String][string] |需统计的字符  ||

  - **返回**

   > 



[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string


