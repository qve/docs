---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:28

---
# XML.Example
- 引用：Quick.Docs.XML.Example


## XML.Example  `扩展`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Text|  | 扩展说明 |
 | code|  | 示例代码 |



