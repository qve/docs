---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:28

---
# MarkDown.MDHeadModel
- 引用：Quick.Docs.MarkDown.MDHeadModel


## MarkDown.MDHeadModel  `文档头部定义`
参考：[frontmatter](https://vitepress.vuejs.org/guide/frontmatter.html) 

- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | template|  | md 生成模板版本 |
 | title|  | 页面标题 |
 | editLink|  | 是否允许编辑 |
 | author|  | 作者 |



