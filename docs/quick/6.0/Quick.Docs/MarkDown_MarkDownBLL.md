---
template: 2021.12
version: 2021.7.1
author: quick.docs
date: 2021年12月31日 15:28

---
# MarkDown.MarkDownBLL
- 引用：Quick.Docs.MarkDown.MarkDownBLL
- version: 2021.7.1


## MarkDown.MarkDownBLL  `公共方法`



 --- 
## AsCatalog

 方法:  MarkDown.MarkDownBLL.AsCatalog

### `新的类目录名`


> AsCatalog([String][string]  name,[String][string]  assemblyName)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | name|[String][string] |类名  ||
    | assemblyName|[String][string] |项目名  ||

  - **返回**

   > 

 --- 
## GenerateHead

 方法:  MarkDown.MarkDownBLL.GenerateHead

### `创建头部`


> GenerateHead([MDHeadModel][mdheadmodel]  mh,[String][string]  classVersion)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | mh|[MDHeadModel][mdheadmodel] |头部信息  ||
    | classVersion|[String][string] |类备注定义的版本  ||

  - **返回**

   > md 编辑者与版本说明 

 --- 
## ConvertToMD

 方法:  MarkDown.MarkDownBLL.ConvertToMD

### `xml注释转换为md`


> ConvertToMD([String][string]  vs)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | vs|[String][string] |vs注释xml格式  ||

  - **返回**

   > 替换符号 

 --- 
## Escape

 方法:  MarkDown.MarkDownBLL.Escape

### `字符转换ubb 处理`


> Escape([String][string]  sInput)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | sInput|[String][string] |传入参数  ||

  - **返回**

   > 转为ubb 

 --- 
## EscapeNoN

 方法:  MarkDown.MarkDownBLL.EscapeNoN

### `换行转换为空`


> EscapeNoN([String][string]  sInput)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | sInput|[String][string] |  ||

  - **返回**

   > 

 --- 
## EscapeNoFlag

 方法:  MarkDown.MarkDownBLL.EscapeNoFlag

### `去除头部标记`


> EscapeNoFlag([String][string]  sInput)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | sInput|[String][string] |需要替换的内容  ||

  - **返回**

   > 

 --- 
## AsString

 方法:  MarkDown.MarkDownBLL.AsString

### `字符串空处理`


> AsString([String][string]  str)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | str|[String][string] |原内容  ||

  - **返回**

   > 为空输出 

 --- 
## WriteFile

 方法:  MarkDown.MarkDownBLL.WriteFile

### `写入文件`


> WriteFile([String][string]  directory,[String][string]  fileName,[String][string]  info)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | directory|[String][string] |路径  ||
    | fileName|[String][string] |文件名  ||
    | info|[String][string] |内容  ||

 --- 
## WriteFile

 方法:  MarkDown.MarkDownBLL.WriteFile

### `写入文件`


> WriteFile([String][string]  filePath,[String][string]  info)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | filePath|[String][string] |完整文件路径  ||
    | info|[String][string] |内容  ||

 --- 
## WriteLogFile

 方法:  MarkDown.MarkDownBLL.WriteLogFile

### `写入日志文件`


> WriteLogFile([String][string]  directory,[String][string]  fileName,[String][string]  info)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | directory|[String][string] |文件路径_log  ||
    | fileName|[String][string] |文件名  ||
    | info|[String][string] |文件内容  ||

 --- 
## ConvertPar

 方法:  MarkDown.MarkDownBLL.ConvertPar

### `参数名处理`


> ConvertPar([String][string]  par)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | par|[String][string] |参数  ||

  - **返回**

   > 取出最后.参数名 

 --- 
## GetCatelog

 方法:  MarkDown.MarkDownBLL.GetCatelog

### `生成文档路径`


> GetCatelog([String][string]  str,[String][string]  path)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | str|[String][string] |类型  ||
    | path|[String][string] |类源地址  ||

  - **返回**

   > 

 --- 
## GetParamTypes

 方法:  MarkDown.MarkDownBLL.GetParamTypes

### `读取参数类型`


> GetParamTypes([String][string]  name)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | name|[String][string] |  ||

  - **返回**

   > 

 --- 
## GenerateExample

 方法:  MarkDown.MarkDownBLL.GenerateExample

### `生成 示例节点`


> GenerateExample([Example][example]  example)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | example|[Example][example] |  ||

  - **返回**

   > 

 --- 
## GenerateParsBody

 方法:  MarkDown.MarkDownBLL.GenerateParsBody

### `构造函数 参数表格`


> GenerateParsBody([String][string]  paras,[List][list]<[Param][param]>  param,[String][string]  assemblyName,[String][string]  methodName,[String][string]  summaryText,[String][string]  parText,[String][string]  docsRoot,[String][string]  docsUrlNet)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | paras|[List][list]<[String][string]> |参数集合  ||
    | param|[List][list]<[Param][param]> |参数描述  ||
    | assemblyName|[String][string] |程序集  ||
    | methodName|[String][string] |方法名  ||
    | summaryText|[String][string] |方法描述  ||
    | parText|[String][string] |参数名  ||
    | docsRoot|[String][string] |根目录  ||
    | docsUrlNet|[String][string] |net api 连接地址  ||

  - **返回**

   > 

 --- 
## GenerateRemarkText

 方法:  MarkDown.MarkDownBLL.GenerateRemarkText

### `组合备注内容`


> GenerateRemarkText([RemarksModel][remarksmodel]  Remarks,[String][string]  assemblyName,[String][string]  docsUrlNet)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | Remarks|[RemarksModel][remarksmodel] |备注  ||
    | assemblyName|[String][string] |程序集  ||
    | docsUrlNet|[String][string] |net api 连接地址  ||

  - **返回**

   > 

 --- 
## GetRemarksSee

 方法:  MarkDown.MarkDownBLL.GetRemarksSee

### `组合描述带连接`


> GetRemarksSee([List][list]<[See][see]>  see,[String][string]  assemblyName,[String][string]  docsUrlNet)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | see|[List][list]<[See][see]> |连接集合  ||
    | assemblyName|[String][string] |程序集名  ||
    | docsUrlNet|[String][string] |net api 连接地址  ||

  - **返回**

   > 

 --- 
## GetParLink

 方法:  MarkDown.MarkDownBLL.GetParLink

### `多参数创建 md 软连接`


> GetParLink([String][string]  parLinks,[String][string]  par,[String][string]  assemblyName,[String][string]  docsUrlNet)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | parLinks|[IDictionary][idictionary]<[String][string],[String][string]> |软连接集合  ||
    | par|[String][string] |传入参数  ||
    | assemblyName|[String][string] |程序集  ||
    | docsUrlNet|[String][string] |net api 连接地址  ||

  - **返回**

   > {par,links} 

 --- 
## GetParLinkUrl

 方法:  MarkDown.MarkDownBLL.GetParLinkUrl

### `参数转为url 连接`


> GetParLinkUrl([String][string]  par,[String][string]  assemblyName,[String][string]  docsUrlNet)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | par|[String][string] |cref 参数  ||
    | assemblyName|[String][string] |集合文件夹名  ||
    | docsUrlNet|[String][string] |net 文档地址  ||

  - **返回**

   > 

  - **备注**

      例如List方法参数： System.Collections.Generic.List{Quick.Docs.XML.Member}



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[mdheadmodel]:../Quick.Docs/MarkDown_MDHeadModel.md
[example]:../Quick.Docs/XML_Example.md
[list]:https://docs.microsoft.com/zh-cn/dotnet/api/system.collections.generic.list
[param]:../Quick.Docs/XML_Param.md
[remarksmodel]:../Quick.Docs/XML_RemarksModel.md
[see]:../Quick.Docs/XML_See.md
[idictionary]:https://docs.microsoft.com/zh-cn/dotnet/api/system.collections.generic.idictionary


