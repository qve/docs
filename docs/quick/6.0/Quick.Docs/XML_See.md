---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:28

---
# XML.See
- 引用：Quick.Docs.XML.See


## XML.See  `标记使您得以从文本内指定链接`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | text|  | 文本 |
 | cref|  | 属性定义参见 |
 | href|  | 连接地址 |



