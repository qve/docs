---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:28

---
# XML.RemarksModel
- 引用：Quick.Docs.XML.RemarksModel


## XML.RemarksModel  `备注`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Text|  | 备注文本 |
 | obsolete|  | 弃用说明 |
 | Paramref|  | 参数 |
 | See|  | 连接 |
 | Seealso|  | 另请参见 , 本项目仅用于定义属性类型 |
 | Version|  | 版本 |
 | group|  | 分组 |
 | appliesTo|  | 适用于 |
 | nameSpace|  | 命名空间 |
 | assembly|  | 程序集 |
 | className|  | 类名 |



