---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:28

---
# XML.Summary
- 引用：Quick.Docs.XML.Summary


## XML.Summary  `私有定义注释`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Text|  | 注释内容 |



