---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:28

---
# MarkDown.MDAuthorModel
- 引用：Quick.Docs.MarkDown.MDAuthorModel


## MarkDown.MDAuthorModel  `编辑者信息`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | name|  | 来自编辑者 |
 | email|  | 编辑者邮箱 |
 | remark|  | 备注说明 |



