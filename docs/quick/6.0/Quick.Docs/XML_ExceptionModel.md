---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:28

---
# XML.ExceptionModel
- 引用：Quick.Docs.XML.ExceptionModel


## XML.ExceptionModel  `异常的说明`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | See|  | 连接 |
 | Seealso|  | 参阅 |



