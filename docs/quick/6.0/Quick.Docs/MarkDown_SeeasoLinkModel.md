---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:28

---
# MarkDown.SeeasoLinkModel
- 引用：Quick.Docs.MarkDown.SeeasoLinkModel


## MarkDown.SeeasoLinkModel  `连接`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | url|  | 连接地址 |
 | fileName|  | 文件名 |



