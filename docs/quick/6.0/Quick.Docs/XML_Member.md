---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:28

---
# XML.Member
- 引用：Quick.Docs.XML.Member


## XML.Member  `集合`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Name|  | 节点名称<br />T:类名,M:构造函数,F:属性,P:变量 |
 | Summary|  | 注释 |
 | TypeParam|  | 泛型变量说明 |
 | Param|  | 参数 |
 | Returns|  | 返回 |
 | Remarks|  | 备注 |
 | Example|  | 案例 |
 | Exception|  | 例外 |



