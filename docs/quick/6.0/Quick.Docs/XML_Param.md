---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:28

---
# XML.Param
- 引用：Quick.Docs.XML.Param


## XML.Param  `参数注释`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Name|  | 参数名 |
 | Text|  | 描述 |
 | See|  | 连接 |
 | Seealso|  | 参考 |



