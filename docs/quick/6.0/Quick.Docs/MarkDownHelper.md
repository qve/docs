---
template: 2021.12
version: 2021.01
author: quick.docs
date: 2021年12月31日 15:28

---
# MarkDownHelper
- 引用：Quick.Docs.MarkDownHelper
- version: 2021.01


## MarkDownHelper  `md 助手`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | docsRoot|  | 文档保存的路径目标 |
 | docsTmpName|  | 临时文档目录名 |
 | docsCatalog|  | 生成文档目录名 |
 | docsUrlNet|  | dotnet/api 连接地址 |
 | docModel|  | 当前文档 |
 | docHead|  | 文档头部 |
 | isWrite|  | 每个方法都独立页面，默认集合到类的页面 |
- **构造函数**

  - MarkDownHelper()
       `默认日期随机临时文件夹`


  - MarkDownHelper([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    docsTmpName)
       `自定义临时文件夹名`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | docsTmpName |指定临时文件夹名


 --- 
## ByXMLPath

 方法:  MarkDownHelper.ByXMLPath

### `转换xml 来自文件`


> ByXMLPath([String][string]  path)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | path|[String][string] |文件路径  ||

  - **返回**

   > 

 --- 
## ByXMLBody

 方法:  MarkDownHelper.ByXMLBody

### `转换xml 内容`


> ByXMLBody([String][string]  body)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | body|[String][string] |vs 注释 xml 内容  ||

  - **返回**

   > 

 --- 
## ByXML

 方法:  MarkDownHelper.ByXML

### `vs注释生成md文档`


> ByXML([DocModel][docmodel]  doc)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | doc|[DocModel][docmodel] |xml注释文件内容  ||

  - **返回**

   > 程序集页面 [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) 

 --- 
## GenerateBody

 方法:  MarkDownHelper.GenerateBody

### `生成类 方法页面`


> GenerateBody([List][list]<[Member][member]>  members,[String][string]  className,[String][string]  methodName,[String][string]  assemblyName,[String][string]  filename,[Member][member]  currMember)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | members|[List][list]<[Member][member]> |程序集合  ||
    | className|[String][string] |命名空间  ||
    | methodName|[String][string] |  ||
    | assemblyName|[String][string] |  ||
    | filename|[String][string] |文件路径  ||
    | currMember|[Member][member] |  ||

  - **返回**

   > 类页面 [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) 

 --- 
## GenerateExample

 方法:  MarkDownHelper.GenerateExample

### `生成方法详情说明`


> GenerateExample([Member][member]  member,[String][string]  assemblyName,[String][string]  className,[String][string]  methodName,[String][string]  filename,[String][string]  methodFileName,[Member][member]  currMember,[String][string]  methodcontent,[Dictionary][dictionary]<[String][string],[String][string]>  parLink)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | member|[Member][member] |  ||
    | assemblyName|[String][string] |命名空间  ||
    | className|[String][string] |类名  ||
    | methodName|[String][string] |方法  ||
    | filename|[String][string] |  ||
    | methodFileName|[String][string] |  ||
    | currMember|[Member][member] |  ||
    | methodcontent|[String][string] |  ||
    | parLink|[Dictionary][dictionary]<[String][string],[String][string]> |页面软链接  ||

  - **返回**

   > 方法页面 [String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string) 



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[docmodel]:../Quick.Docs/XML_DocModel.md
[list]:https://docs.microsoft.com/zh-cn/dotnet/api/system.collections.generic.list
[member]:../Quick.Docs/XML_Member.md
[dictionary]:https://docs.microsoft.com/zh-cn/dotnet/api/system.collections.generic.dictionary


