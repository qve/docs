---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:28

---
# XML.DocModel
- 引用：Quick.Docs.XML.DocModel


## XML.DocModel  `vs 注释文档对象`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | Assembly|  | 程序集 |
 | Members|  | 属性集合 |
 | Text|  | 描述 |



