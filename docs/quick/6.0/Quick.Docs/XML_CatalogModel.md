---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:28

---
# XML.CatalogModel
- 引用：Quick.Docs.XML.CatalogModel


## XML.CatalogModel  `对象目录`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | catalog|  | 程序集目录名称 |
 | named|  | 引用命名 |
 | fileName|  | 文件名 |
 | filePathName|  | 文件路径全名 |
 | obsolete|  | 弃用的 |
 | obsoleteText|  | 弃用说明 |
 | title|  | 描述内容 |
 | group|  | 分组 |
 | appliesTo|  | 适用版本 |
 | nameSpace|  | 命名空间 |
 | assembly|  | 程序集 |
 | className|  | 类名 |
 | version|  | 自定义版本 |



