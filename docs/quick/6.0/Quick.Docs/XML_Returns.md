---
template: 2021.12
author: quick.docs
date: 2021年12月31日 15:28

---
# XML.Returns
- 引用：Quick.Docs.XML.Returns


## XML.Returns  `返回参数`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | See|  | 连接 |
 | Seealso|  | 另请参见 |
 | Text|  | 说明 |



