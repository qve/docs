---
template: 2021.12
author: quick.docs
date: 2021年12月31日 22:47

---
# QuickORMBuildJsonBLL
- 引用：Quick.Code.QuickORMBuildJsonBLL


## QuickORMBuildJsonBLL  `ORM 生成工具`



## GetDataTableViewList  `获取数据库表`

 方法:  QuickORMBuildJsonBLL.GetDataTableViewList


> GetDataTableViewList([String][string]  ConnStr,[String][string]  _tablename,[Boolean][boolean]  _isView,[Boolean][boolean]  _isColume)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | ConnStr|[String][string] |连接字符串，不加密  ||
    | _tablename|[String][string] |表名称 (多表用,号隔开；为空:全部表)   ||
    | _isView|[Boolean][boolean] |是否包含视图  ||
    | _isColume|[Boolean][boolean] |是否读取列与外键  ||

## GetDataTableColume  `获取表的字段明细`

 方法:  QuickORMBuildJsonBLL.GetDataTableColume


> GetDataTableColume([String][string]  ConnStr,[String][string]  _tablename)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | ConnStr|[String][string] |数据库连接  ||
    | _tablename|[String][string] |多表名  ||

## GetDataTableColumeJson  `加载表 字段属性Json`

 方法:  QuickORMBuildJsonBLL.GetDataTableColumeJson


> GetDataTableColumeJson([String][string]  ConnStr,[String][string]  _tablename)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | ConnStr|[String][string] |数据库连接字符串  ||
    | _tablename|[String][string] |需要加载的多个表名  ||

## ToJsonString  `字段描述转为TPL模板Json`

 方法:  QuickORMBuildJsonBLL.ToJsonString


> ToJsonString([List][list]&lt;TableColume&gt;  columes)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | columes|[List][list]&lt;TableColume&gt; |表字段  ||

## ToJsonAttrString  `字段属性转换`

 方法:  QuickORMBuildJsonBLL.ToJsonAttrString


> ToJsonAttrString(TableColumeToNet  tc)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | tc|TableColumeToNet |字段属性  ||

## ConvertToTableColumeToNet  `将表结构转为.net数据对应列`

 方法:  QuickORMBuildJsonBLL.ConvertToTableColumeToNet


> ConvertToTableColumeToNet([List][list]&lt;TableColume&gt;  List)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | List|[List][list]&lt;TableColume&gt; |  ||

## ConvertToColumeToNet  `转为.net列`

 方法:  QuickORMBuildJsonBLL.ConvertToColumeToNet


> ConvertToColumeToNet(TableColumeToNet  tc)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | tc|TableColumeToNet |  ||



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[boolean]:https://docs.microsoft.com/zh-cn/dotnet/api/system.boolean
[list]:https://docs.microsoft.com/zh-cn/dotnet/api/system.collections.generic.list


