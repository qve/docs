---
template: 2021.12
author: quick.docs
date: 2021年12月31日 22:47

---
# Model.QuickORMBindModel
- 引用：Quick.Code.Model.QuickORMBindModel


## Model.QuickORMBindModel  `ORM 代码生成参数`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | connStr|  | 数据库连接字符串 |
 | dbCode|  | Server连接命名 |
 | dbSql|  | 传入SQL 语句 |
 | tableCode|  | 指定生成的多个表代码 |
 | isView|  | 是否读取视图 |
 | nameSpace|  | 空间命名 |
 | templatePath|  | 模板路径 <br /> \\Plus\\ORM\\Template\\Core\\ |
 | path|  | 存储路径 <br /> \\wwwroot\\_cdn\\orm\\bind\\ |
 | fileName|  | 目录文件名 <br /> 当前时间双位秒_随机数 |
- **构造函数**

  - Model.QuickORMBindModel()
       `构造`



## getFliePathName  `完整路径`

 方法:  Model.QuickORMBindModel.getFliePathName


> getFliePathName()






