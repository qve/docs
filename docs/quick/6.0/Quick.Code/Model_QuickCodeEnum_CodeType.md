---
template: 2021.12
author: quick.docs
date: 2021年12月31日 22:47

---
# Model.QuickCodeEnum.CodeType
- 引用：Quick.Code.Model.QuickCodeEnum.CodeType


## Model.QuickCodeEnum.CodeType  `代码类别`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | SC|  | 源代码 |
 | CP|  | 执行代码 |



