---
template: 2021.12
author: quick.docs
date: 2021年12月31日 22:47

---
# Model.QuickCodeEnum
- 引用：Quick.Code.Model.QuickCodeEnum


## Model.QuickCodeEnum  `源码定义`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | CodeType.SC|  | 源代码 |
 | CodeType.CP|  | 执行代码 |
 | SqlExecType.addTitle|  | 添加描述 |
 | SqlExecType.editTitle|  | 修改描述 |
 | SqlExecType.delTitle|  | 删除描述 |
 | SqlExecType.editField|  | 修改字段名 |



