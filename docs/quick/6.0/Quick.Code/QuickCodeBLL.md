---
template: 2021.12
author: quick.docs
date: 2021年12月31日 22:47

---
# QuickCodeBLL
- 引用：Quick.Code.QuickCodeBLL


## QuickCodeBLL  `开发代码方法`



## CreateKey  `源代码缓存Key`

 方法:  QuickCodeBLL.CreateKey


> CreateKey([CodeType][codetype]  type,[String][string]  path,[String][string]  version)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | type|[CodeType][codetype] |源码:sc / 执行代码:cp  ||
    | path|[String][string] |请求路径  ||
    | version|[String][string] |代码版本，主版默认为 0.0.1  ||

## GetCodeCompileByPath  `读取编译执行代码，默认缓存24小时`

 方法:  QuickCodeBLL.GetCodeCompileByPath


> GetCodeCompileByPath([String][string]  path,[String][string]  version)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | path|[String][string] |路径  ||
    | version|[String][string] |模板版本为空为主版本 0.0.1  ||

## SaveCode  `保存代码并缓存代码 QF_Code`

 方法:  QuickCodeBLL.SaveCode


> SaveCode([Int32][int32]  cmd,[Int64][int64]  _userId,[QuickCodeModel][quickcodemodel]  qcm)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | cmd|[Int32][int32] |命令 1:表示新增,2:新增并设置为主版  ||
    | _userId|[Int64][int64] |编辑者  ||
    | qcm|[QuickCodeModel][quickcodemodel] |页面参数  ||

  * 返回

   > 默认版本：0.0.1 



[codetype]:../Quick.Code/Model_QuickCodeEnum_CodeType.md
[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64
[quickcodemodel]:../Quick.Code/Model_QuickCodeModel.md


