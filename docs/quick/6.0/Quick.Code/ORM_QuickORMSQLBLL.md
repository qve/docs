---
template: 2021.12
author: quick.docs
date: 2021年12月31日 22:47

---
# ORM.QuickORMSQLBLL
- 引用：Quick.Code.ORM.QuickORMSQLBLL


## ORM.QuickORMSQLBLL  `ORM SQL 管理方法`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | qp|  | 数据库指向参数对象 |
- **构造函数**

  - ORM.QuickORMSQLBLL()
       `默认 Quick 数据库`


  - ORM.QuickORMSQLBLL(QueryParam    _qp)
       `自定义查询`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | _qp |

  - ORM.QuickORMSQLBLL([String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    dbCode,[String](https://docs.microsoft.com/zh-cn/dotnet/api/system.string)    sqlconns,[Int32](https://docs.microsoft.com/zh-cn/dotnet/api/system.int32)    passType)
       `自定义数据连接`

      |    构造参数   |   说明   |  
      | ---- | ---- |
      | dbCode |数据库连接名命名
      | sqlconns |数据连接字符串
      | passType |加密方式1:明文,2:DES


## EditTable  `修改数据表`

 方法:  ORM.QuickORMSQLBLL.EditTable


> EditTable([Int64][int64]  MUser_ID,[Int32][int32]  appID,[SqlExecType][sqlexectype]  type,[String][string]  tableCode,[String][string]  title)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | MUser_ID|[Int64][int64] |编辑者  ||
    | appID|[Int32][int32] |来源应用  ||
    | type|[SqlExecType][sqlexectype] |增删改  ||
    | tableCode|[String][string] |表名称  ||
    | title|[String][string] |描述内容  ||

## EditTableField  `编辑表字段`

 方法:  ORM.QuickORMSQLBLL.EditTableField


> EditTableField([Int64][int64]  MUser_ID,[Int32][int32]  appID,[SqlExecType][sqlexectype]  type,[String][string]  tableCode,[List][list]&lt;TableFieldModel&gt;  fields)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | MUser_ID|[Int64][int64] |编辑者  ||
    | appID|[Int32][int32] |来源应用  ||
    | type|[SqlExecType][sqlexectype] |命令  ||
    | tableCode|[String][string] |表名  ||
    | fields|[List][list]&lt;TableFieldModel&gt; |字段集合  ||



[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[sqlexectype]:../Quick.Code/Model_QuickCodeEnum_SqlExecType.md
[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[list]:https://docs.microsoft.com/zh-cn/dotnet/api/system.collections.generic.list


