---
template: 2021.12
author: quick.docs
date: 2021年12月31日 22:47
---

# Quick.Code

template: 2021.12
author: quick.docs
date: 2021 年 12 月 31 日 22:47

## Quick.Code 目录

### [BLL.QuickCodeSQLBLL](./BLL_QuickCodeSQLBLL.md)

      数据库读写

### [Model.QuickCodeEnum](./Model_QuickCodeEnum.md)

      源码定义

### [Model.QuickCodeEnum.CodeType](./Model_QuickCodeEnum_CodeType.md)

      代码类别

### [Model.QuickCodeEnum.SqlExecType](./Model_QuickCodeEnum_SqlExecType.md)

      sql 编辑命令类别

### [Model.QuickCodeModel](./Model_QuickCodeModel.md)

      快速编码对象

### [Model.QuickORMBindModel](./Model_QuickORMBindModel.md)

      ORM 代码生成参数

### [Model.QuickORMSQLModel](./Model_QuickORMSQLModel.md)

      数据库管理

### [QuickORMBuildBLL](./QuickORMBuildBLL.md)

      数据库 ORM 生成工具, 逐步切换到JSon

### [QuickORMBuildJsonBLL](./QuickORMBuildJsonBLL.md)

      ORM 生成工具

### [ORM.QuickORMSQLBLL](./ORM_QuickORMSQLBLL.md)

      ORM SQL 管理方法

### [QuickCodeBLL](./QuickCodeBLL.md)

      开发代码方法
