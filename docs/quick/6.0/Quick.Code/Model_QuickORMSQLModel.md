---
template: 2021.12
author: quick.docs
date: 2021年12月31日 22:47

---
# Model.QuickORMSQLModel
- 引用：Quick.Code.Model.QuickORMSQLModel


## Model.QuickORMSQLModel  `数据库管理`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | connStr|  | 数据库连接字符串 |
 | dbCode|  | Server连接命名 |
 | tableCode|  | 指定生成的多个表代码 |
 | tableTitle|  | 表描述 |
 | fields|  | 编辑表字段列表 |
 | cmd|  | 执行命令 |
- **构造函数**

  - Model.QuickORMSQLModel()
       `构造数据库 参数`





