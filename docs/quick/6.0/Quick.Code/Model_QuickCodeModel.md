---
template: 2021.12
author: quick.docs
date: 2021年12月31日 22:47

---
# Model.QuickCodeModel
- 引用：Quick.Code.Model.QuickCodeModel


## Model.QuickCodeModel  `快速编码对象`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | id|  | 代码ID |
 | pageId|  | 所属页面ID |
 | title|  | 组件描述 |
 | description|  | 提交说明 |
 | appKey|  | 所属应用 |
 | path|  | 请求路径 |
 | version|  | 代码版本 |
 | lang|  | 代码语言 vue,js,html |
 | body|  | 原代码内容 |
 | code|  | 编译执行代码 <br /> 未提交压缩代码，就执行后端压缩代码 |



