---
template: 2021.12
author: quick.docs
date: 2021年12月31日 22:47

---
# QuickORMBuildBLL
- 引用：Quick.Code.QuickORMBuildBLL


## QuickORMBuildBLL  `数据库 ORM 生成工具, 逐步切换到JSon`


- **类属性**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | NameSpace|  | 项目命令空间 |

## GetDataTableViewList  `获取数据库表明细`

 方法:  QuickORMBuildBLL.GetDataTableViewList


> GetDataTableViewList([String][string]  ConnStr,[String][string]  _tablename,[Boolean][boolean]  _isView,[Boolean][boolean]  _isColume)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | ConnStr|[String][string] |连接字符串,非加密  ||
    | _tablename|[String][string] |表名称 (多表用,号隔开；为空:全部表)   ||
    | _isView|[Boolean][boolean] |是否包含视图  ||
    | _isColume|[Boolean][boolean] |是否读取列与外键  ||

## BuildFile  `生成对象代码`

 方法:  QuickORMBuildBLL.BuildFile


> BuildFile([List][list]&lt;TableEntity&gt;  TableList,[String][string]  TemplateUrl,[String][string]  SavePath,[String][string]  nSpace)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | TableList|[List][list]&lt;TableEntity&gt; |表实体集  ||
    | TemplateUrl|[String][string] |模板文件所在根目录名  ||
    | SavePath|[String][string] |保存代码路径  ||
    | nSpace|[String][string] |  ||

## BuildComponents  `生成实体类`

 方法:  QuickORMBuildBLL.BuildComponents


> BuildComponents([String][string]  TemplateDir,TableEntityNets  TableList,[String][string]  SavePath)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | TemplateDir|[String][string] |模板目录  ||
    | TableList|TableEntityNets |表结构  ||
    | SavePath|[String][string] |保存路径  ||

## BuildDataCS  `XSL 模板生成代码`

 方法:  QuickORMBuildBLL.BuildDataCS


> BuildDataCS([String][string]  templateDir,[String][string]  xmlTableList,[String][string]  savePath,[Int32][int32]  index,[String][string]  tabcode)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | templateDir|[String][string] |模板路径  ||
    | xmlTableList|[String][string] |模板绑定数据  ||
    | savePath|[String][string] |保存路径  ||
    | index|[Int32][int32] |序号  ||
    | tabcode|[String][string] |数据表对象名  ||

## ConvertToTableEntityNets  `将多个表转换为Table对应.net类`

 方法:  QuickORMBuildBLL.ConvertToTableEntityNets


> ConvertToTableEntityNets([List][list]&lt;TableEntity&gt;  lst)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | lst|[List][list]&lt;TableEntity&gt; |  ||

## ConvertToTableColumeToNet  `将表结构转为.net数据对应列`

 方法:  QuickORMBuildBLL.ConvertToTableColumeToNet


> ConvertToTableColumeToNet([List][list]&lt;TableColume&gt;  List)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | List|[List][list]&lt;TableColume&gt; |  ||

## ConvertToColumeToNet  `转为.net列`

 方法:  QuickORMBuildBLL.ConvertToColumeToNet


> ConvertToColumeToNet(TableColumeToNet  tc)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | tc|TableColumeToNet |  ||

## GetIdentityType 

 方法:  QuickORMBuildBLL.GetIdentityType


> GetIdentityType([List][list]&lt;TableColumeToNet&gt;  List)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | List|[List][list]&lt;TableColumeToNet&gt; |  ||

## GetTableColumeToNet_Key  `获得主键列`

 方法:  QuickORMBuildBLL.GetTableColumeToNet_Key


> GetTableColumeToNet_Key([List][list]&lt;TableColumeToNet&gt;  List)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | List|[List][list]&lt;TableColumeToNet&gt; |  ||

## GetTableColumeToNet_Identity  `获得自动ID列`

 方法:  QuickORMBuildBLL.GetTableColumeToNet_Identity


> GetTableColumeToNet_Identity([List][list]&lt;TableColumeToNet&gt;  List,[Boolean][boolean]  isView)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | List|[List][list]&lt;TableColumeToNet&gt; |集合  ||
    | isView|[Boolean][boolean] |是否视图  ||

## GetTableColumeToNet_NoIdentity  `获得不包含自动ID列`

 方法:  QuickORMBuildBLL.GetTableColumeToNet_NoIdentity


> GetTableColumeToNet_NoIdentity([List][list]&lt;TableColumeToNet&gt;  List)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | List|[List][list]&lt;TableColumeToNet&gt; |  ||



[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[boolean]:https://docs.microsoft.com/zh-cn/dotnet/api/system.boolean
[list]:https://docs.microsoft.com/zh-cn/dotnet/api/system.collections.generic.list
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32


