---
template: 2021.12
author: quick.docs
date: 2021年12月31日 22:47

---
# BLL.QuickCodeSQLBLL
- 引用：Quick.Code.BLL.QuickCodeSQLBLL


## BLL.QuickCodeSQLBLL  `数据库读写`



## GetItemByPageID  `查询当前页面所有版本代码`

 方法:  BLL.QuickCodeSQLBLL.GetItemByPageID


> GetItemByPageID([Int64][int64]  id)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | id|[Int64][int64] |页面 QF_Page_ID   ||

  * 返回

   > List[ID,State,Title,CodeVersion,CodeTime] 

## GetCodeSourceByID  `读取组件源代码`

 方法:  BLL.QuickCodeSQLBLL.GetCodeSourceByID


> GetCodeSourceByID([Int64][int64]  id)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | id|[Int64][int64] |QF_Code_ID  ||

## GetCodeSourceByPath  `读取组件源代码`

 方法:  BLL.QuickCodeSQLBLL.GetCodeSourceByPath


> GetCodeSourceByPath([String][string]  path,[String][string]  version)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | path|[String][string] |路径  ||
    | version|[String][string] |版本  ||

## GetCodeCompileByPath  `读取组件执行编译的代码`

 方法:  BLL.QuickCodeSQLBLL.GetCodeCompileByPath


> GetCodeCompileByPath([String][string]  path,[String][string]  version)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | path|[String][string] |QF_Page.Path  ||
    | version|[String][string] |模板版本为空为主版本 0.0.1  ||

  * 返回

   > QF_Code.CodeCompile 

## SetCodeSourceByPath  `保存代码 QF_Code`

 方法:  BLL.QuickCodeSQLBLL.SetCodeSourceByPath


> SetCodeSourceByPath([Int32][int32]  cmd,[Int64][int64]  _userId,[QuickCodeModel][quickcodemodel]  qcm)


  - **参数**

    | 名称 | 类型 | 说明 | 参见 |
    | ---- | ---- | ---- | ---- |
    | cmd|[Int32][int32] |命令 1:表示新增,2:新增并设置为主版  ||
    | _userId|[Int64][int64] |编辑者  ||
    | qcm|[QuickCodeModel][quickcodemodel] |页面参数  ||

  * 返回

   > 存储过程执行结果 



[int64]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int64
[string]:https://docs.microsoft.com/zh-cn/dotnet/api/system.string
[int32]:https://docs.microsoft.com/zh-cn/dotnet/api/system.int32
[quickcodemodel]:../Quick.Code/Model_QuickCodeModel.md


