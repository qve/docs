---
template: 2021.12
author: quick.docs
date: 2021年12月31日 22:47

---
# Model.QuickCodeEnum.SqlExecType
- 引用：Quick.Code.Model.QuickCodeEnum.SqlExecType


## Model.QuickCodeEnum.SqlExecType  `sql 编辑命令类别`


- **类变量**

|  名称    |   类型   |   说明   |
| ---- | ---- | ---- |
 | addTitle|  | 添加描述 |
 | editTitle|  | 修改描述 |
 | delTitle|  | 删除描述 |
 | editField|  | 修改字段名 |



