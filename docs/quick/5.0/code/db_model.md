# 数据对象

## QF_Server 获取数据库配置

- `QF_Server` 全局配置服务器加密连接，方便动态切换服务器

```csharp
// 根据数据库命名取出服务器配置连接字符串
QuickORMBLL qob = new QuickORMBLL();
string connStr = qob.GetConnectByDBName("QuickDB");
connStr =  qob.GetFieldByDBName("QuickDB","DBConnect")
```

## QF_Model 数据对象

数据表 `QF_Model` 存储了对象的字段与外键关系，与所在数据库。
`code` 字段用于索引查询

## QuickORMBLL 增删改查

| 事件名           | 说明                                                         |
| ---------------- | ------------------------------------------------------------ |
| getDBName        | 根据索引取出所在的数据库名，并缓存在 QuickCache.Memory1 小时 |
| getJoinCache     | 根据索引取出外键配置，并缓存在 QuickCache.Memory1 小时       |
| GetJoinAttrBySql | 根据索引取出外键配置，并缓存在 QuickCache.Memory1 小时       |

### 根据索引取出外键关系

- QuickORMBLL

```csharp

    /// <summary>
    /// 取出外键配置
    /// </summary>
    /// <param name="code">对象索引 QF_Model.code</param>
    /// <returns></returns>
    string _joinStr = getJoinCache(code);

    string _DBName="";

    /// <summary>
    /// 根据索引取出外键配置
    /// </summary>
    /// <param name="dbName"></param>
    /// <param name="code">对象索引 QF_Model.code</param>
    /// <param name="field">外键字段</param>
    /// <returns>外键配置</returns>
    QueryJoin qj = getJoin(out _DBName, Table, pj.par);

```

### Join 外键查询

```csharp

        /// <summary>
        /// 查询 Json 数组列表,qp查询条件
        /// </summary>
        /// <returns>Json 数组</returns>
        public dynamic ListJson(string Table, PagerInStr pj)
        {
            qp.DBName = getDBNameCache(Table);
            qp.TableName = Table;

            // 查询条件参数
            if (!string.IsNullOrWhiteSpace(pj.where))
            {
                qp.TParams.AddRange(pj.JsonParams(pj.where));
            }

            // 查询数据，取出平台外键配置
            pj.join = getJoinCache(Table);
            // 外键字段查询
            if (!string.IsNullOrWhiteSpace(pj.join))
            { qp.Join = bindJoin(pj.join); }

            //自定义查询字段
            if (!string.IsNullOrWhiteSpace(pj.field))
            { qp.Field = pj.field; }

            qp.PageSize = pj.size;
            qp.PageIndex = pj.index;
            qp.Orderfld = pj.sort;
            qp.OrderType = pj.desc;

            //if (qp.Join != null)
            //{
            //    qp.Join.Add(new QueryJoin("MUser_ID", QuickData.SqlLink("[QF_User]", QuickData.DBBase, false), "LastFirst"));
            //}
            return QuickData.List(qp);
        }

```

## Api 权限

- QF_Model 数据对象管理表

## QuickDBEnum 数据状态

```csharp

    /// <summary>
    /// 数据参数
    /// </summary>
    public class QuickDBEnum
    {
        /// <summary>
        /// 数据状态说明
        /// </summary>
        public enum StateFlag
        {
            #region 定义
            /// <summary>
            /// 异常状态
            /// </summary>
            [Description("未配置")]
            empty = 0,
            /// <summary>
            /// 默认 所有人可正常编辑
            /// </summary>
            [Description("正常")]
            normal = 1,

            /// <summary>
            /// 已审核，不可编辑
            /// </summary>
            [Description("已审")]
            audited = 2,
            /// <summary>
            /// 已撤销审核，可本人编辑
            /// </summary>
            [Description("撤审")]
            revoked = 3,
            /// <summary>
            /// 删除，所有人禁用，等待物理删除
            /// </summary>
            [Description("删除")]
            deleted = 4,

            /// <summary>
            /// 已暂停，所有人停止访问与编辑
            /// </summary>
            [Description("暂停")]
            paused = 5

            #endregion

        }

        /// <summary>
        /// 数据状态 icon-名称
        /// </summary>
        public enum StateFlagIcon : int
        {
            /// <summary>
            /// 未配置
            /// </summary>
            gantan = 0,
            /// <summary>
            /// 正常
            /// </summary>
            bianji = 1,
            /// <summary>
            /// 已审
            /// </summary>
            suo = 2,
            /// <summary>
            /// 撤审
            /// </summary>
            suokai = 3,
            /// <summary>
            /// 删除
            /// </summary>
            del = 4,
            /// <summary>
            /// 暂停
            /// </summary>
            clear = 5
        }
    }
```
