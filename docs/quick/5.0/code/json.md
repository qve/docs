# Json

## JsonDocument 为只读

`System.Text.Json` `DOM` 无法添加、删除或修改 JSON 元素

## JsonQuick 原生语法

基于接近 js 语法，可代替方法 JsonHelper

```csharp

JsonQuick jq = new JsonQuick("[{\"list\":1,\"add\":2},{\"itemAdd\":20}]");

// 取出 add 的值
jq[0]["add"]

// 取出 itemAdd 的值
jq[0]["itemAdd"]

StringBuilder _control = new StringBuilder();
// 取出js数组 第一个对象，循环取出值
foreach (var pp in jq[0].EnumerateObject())
{
  if (pp.Name == "list")
  {
   _control.Append("," + pp.Value.GetString());
  }
}

```

## Json 转换去重

```csharp

// 转为 json 对象
JsonQuick jqList = new JsonQuick("[{\"493\":\"1,2\"},{\"493\":\"1,2,3,4,5,6\",\"534\":\"1,2,3,4,5,6,7,8\"}]");

// 定义新的临时变量
Dictionary<string, string> ld = new Dictionary<string, string>();

for (int i = 0; i < jqList.GetArrayLength(); i++)
{
  foreach (var pp in jqList[i].EnumerateObject())
  {
    // Console.WriteLine(pp);
    //判断是否有重复键名
     if (ld.TryGetValue(pp.Name, out var value))
     {
        // Console.WriteLine(value);
        string[] _arr = value.Split(",");
        string[] _arr2 = pp.Value.ToString().Split(",");
        string[] x = _arr.Union(_arr2).ToArray<string>(); //剔除重复项
        //Console.WriteLine(x);
        ld[pp.Name] = String.Join(",", x);

     }else{
        ld.Add(pp.Name, pp.Value.ToString());
     }

  }
}

// 转为json
Console.WriteLine(JsonHelper.Serialize(ld));
```

## JsonHelper

建议使用跟接近 js 语法的 JsonQuick

## json 反序列化

```csharp

var _jDoc = JsonHelper.Parse("{\"list\":1}");
var _obj = _jDoc.RootElement.GetProperty("list");
string _actionId = _obj.ToString();

```

## json 数组

```csharp
                // 分页查询参数
foreach (JsonElement po in JsonHelper.ParseArray(par))
{

    foreach (var pp in po.EnumerateObject())
    {

        if (pp.Name == "q")
        {
          pp.Value.GetString();
        }

        // 判断属性取值
        if (pp.TryGetProperty("list", out var value))
        {
          _actionId = value.ToString();
          break;
        }
    }
}
```

## json 动态泛型

```csharp
  JsonElement _obj = pc.obj;
  int a = _obj.GetProperty("a").GetInt64();
```

## JsonHelper 方法

```js
  /// <summary>
  ///  转换属性
  /// </summary>
  /// <returns></returns>
  public static JsonSerializerOptions Options()

  /// <summary>
  /// JsonSerializer 序列化为字符串
  /// </summary>
  /// <param name="obj">JsonSerializerOptions</param>
  /// <returns></returns>
  public static string Serialize(object obj)


  /// <summary>
  /// 将json反序列化为实体对象(包含DataTable和List集合对象)
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <param name="strJson"></param>
  /// <returns></returns>
  public static T Deserialize<T>(string strJson)

  /// <summary>
  ///  使用 Parse 方法返回内容
  /// </summary>
  /// <param name="strJson"></param>
  /// <returns>RootElement</returns>
  public static dynamic Deserialize(string strJson){
    JsonSerializerOptions options = new JsonSerializerOptions();
    options.Converters.Add(new JsonHelperConvert.DynamicConverter());
    return JsonSerializer.Deserialize<dynamic>(strJson, options);
  }



  /// <summary>
  ///  反序列化到对象
  /// </summary>
  /// <param name="strJson">字符串</param>
  /// <returns></returns>
  public static JsonDocument Parse(string strJson)
  {
     return JsonDocument.Parse(strJson);
  }

  /// <summary>
  /// 返回 对象
  /// </summary>
  /// <param name="strJson"></param>
  /// <returns>EnumerateObject</returns>
  public static dynamic ParseObject(string strJson)
  {
     return JsonDocument.Parse(strJson).RootElement.EnumerateObject();
  }

  /// <summary>
  /// 返回 数组
  /// </summary>
  /// <param name="strJson"></param>
  /// <returns>EnumerateArray</returns>
  public static dynamic ParseArray(string strJson)
  {
     return JsonDocument.Parse(strJson).RootElement.EnumerateArray();
  }


 /// <summary>
 /// 转换为对象,对时间格式化
 /// </summary>
 /// <typeparam name="T">对象</typeparam>
 /// <param name="str">时间格式</param>
 /// <returns></returns>
 public static T DeserializeJsonTime<T>(string str)
 {
     return Deserialize<T>(JsonTime(str));
 }

  /// <summary>
 /// Json 转换时间格式
 /// </summary>
 /// <param name="str">需转换的字符串</param>
 /// <returns>返回标准时间格式串</returns>
 public static string JsonTime(string str)

  /// <summary>
 /// Json过滤特殊字符
 /// </summary>
 /// <param name="s">初始字符串</param>
 /// <returns>过滤处理</returns>
 public static string StringFormat(string s)


  /// <summary>
 /// json 取出字段主键
 /// </summary>
 /// <param name="jsonElement">EnumerateObject 对象</param>
 /// <returns></returns>
 public string JsonToKey(JsonElement jsonElement)


 /// <summary>
 /// json 对象转换
 /// </summary>
 /// <param name="jsonElement">jsonElement.EnumerateObject</param>
 /// <returns>string,jsonElement</returns>
 public IDictionary<string, dynamic> ToObject(JsonElement jsonElement)


  /// <summary>
 /// 反序列化到对象
 /// </summary>
 /// <param name="json"></param>
 /// <returns></returns>
 public static dynamic Reader(string json)

  /// <summary>
 /// 反序列化到对象
 /// </summary>
 /// <param name="json">字符串</param>
 /// <param name="encoding">编码</param>
 /// <returns>JsonReaderWriterFactory</returns>
 public static dynamic Reader(string json, Encoding encoding)
```
