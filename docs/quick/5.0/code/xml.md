# XMLHelper

## XML 格式支持

```csharp
public class Startup
{
  public void ConfigureServices(IServiceCollection services)
  {
            services.AddControllersWithViews(options =>
            {
                // 全局注册权限验证过滤器
                options.Filters.Add(typeof(QuickSafeFilter));
            }).AddJsonOptions(configure =>
            {
                // 全局Json 时间格式化，避免前端ISO转换
                configure.JsonSerializerOptions.Converters.Add(new JsonHelperConvert.DateTimeConverter());
            }).AddXmlDataContractSerializerFormatters();// 增加对xml 支持
  }
}
```

## Produces 过滤器返回 xml/json

- [Produces]过滤器

```csharp
[HttpGet]

// [Produces("application/json")]
// [Produces("text/plain")]
// [Produces("text/xml")]
[Produces("application/xml")]
public IActionResult Get([FromQuery] LoginModel o)
{
   return Ok(o);
}
```

## Consumes 响应请求 xml

- [Consumes]XML 格式数据请求则使用,若没有该属性，则直接识别请求头中的 Content-Typ

```csharp
[Consumes("application/xml")]
public IActionResult Get(LoginModel obj)
{
    return obj;
}
```

## XMLHelper 使用
