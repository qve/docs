# API 数据接口

- [官方 入门](https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/first-web-api?view=aspnetcore-5.0&tabs=visual-studio)
- [官方 响应数据的格式](https://docs.microsoft.com/zh-cn/aspnet/core/web-api/advanced/formatting?view=aspnetcore-5.0)

- [QuickSafeFilter](./safe.md) 接口权限验证，取出身份信息

- [QF_Model 数据对象](./db_model.md)

## API 控制器

### http get 参数

```c#
public class TestController : ControllerBase
{
        /// <summary>
        /// 当前请求的令牌
        /// </summary>
        private QuickSafeFilterModel safe;

        /// <summary>
        /// 构造注入
        /// </summary>
        /// <param name="_fiter"></param>
        public TestController(QuickSafeFilterModel _fiter)
        {
            safe = _fiter;
        }


        [HttpGet]
        public IActionResult Get([FromQuery] PagerInStr pj)
        {
          MessageModel<dynamic> mm = new MessageModel<dynamic>();

            try
            {
                switch (pj.tpl)
                {
                  case 11:
                  // 自定义tpl 类型建议10以上开始
                  // 取出当前请求的令牌用户ID
                    mm.data ="返回的数据"+ safe.TokenUser.UserID
                    mm.code=1; //表示取出成功
                   break;
                }
            }
            catch (Exception ex)
            {
                mm.msg = ex.Message;

            }

            return Ok(mm);
        }
}
```

### http where 参数

```c#
public class TestController : ControllerBase
{
        /// <summary>
        /// 当前请求的令牌
        /// </summary>
        private QuickSafeFilterModel safe;

        /// <summary>
        /// 构造注入
        /// </summary>
        /// <param name="_fiter"></param>
        public TestController(QuickSafeFilterModel _fiter)
        {
            safe = _fiter;
        }


        [HttpGet]
        public IActionResult Get([FromQuery] Pager<dynamic> pj)
        {
          MessageModel<dynamic> mm = new MessageModel<dynamic>();

            try
            {
                switch (pj.tpl)
                {
                  case 11:
                    // 自定义tpl 类型建议10以上开始
                    // 取出当前请求的自定义where 参数
                    mm.data ="返回的数据"+ JsonHelper.Deserialize(pj.where);
                    mm.code=1;//表示取出成功
                   break;
                }
            }
            catch (Exception ex)
            {
                mm.msg = ex.Message;
            }

            return Ok(mm);
        }
}
```

## API 前端参数

Join 外键

- 前端传入

```json
{
  "sort": "ID",
  "desc": 1,
  "tpl": 2,
  "index": 1,
  "size": 5,
  "join": "[{\"tField\":\"MUser_ID\",\"cTable\":\"AP10000DB.[dbo].[QF_User]\",\"cTitle\":\"LastFirst\"}]",
  "where": "[{\"ID\":\"5\",\"q\":\">\"},{\"ID\":\"20\",\"q\":\"<\"}]"
}
```

## Http Get

-查询接口

```csharp
[HttpGet]
public JsonResult Index([FromQuery] PagerInStr pj)
{
  PagerOutModel<dynamic> pm = new PagerOutModel<dynamic>();

  try
  {
    // 分页查询数量
    if (pj.size > 0)
    {
      QueryParam qp = new QueryParam(QuickData.DBQuick);

      // 查询条件参数
      if (!string.IsNullOrWhiteSpace(pj.where))
      {
        qp.TParams.AddRange(pj.JsonParams(pj.where));
      }

      // 外键字段查询
      if (!string.IsNullOrWhiteSpace(pj.join))
      { qp.Join = JsonHelper.Deserialize<List<QueryJoin>>(pj.join); }

      //自定义查询字段
      if (!string.IsNullOrWhiteSpace(pj.field))
      { qp.Field = pj.field; }

      qp.PageSize = pj.size;
      qp.PageIndex = pj.index;
      qp.Orderfld = pj.sort;
      qp.OrderType = pj.desc;

      // 查询列表
      pm.obj = QuickData.List(qp);
      pm.index = qp.PageIndex;
      pm.total = qp.PageTotal;
      pm.count = qp.RecordCount;

      pm.code = 1;
    }
  }
  catch (Exception ex)
  {
    pm.msg = ex.Message;
    pm.obj = "[]";
  }

  return Json(pm);
}
```

## http post 增删改

- ajax 请求类型
  `Content-Type:application/json; charset=utf-8`

  ```json
  { "cmd": "resc", "par": "494", "obj": "编辑的对象" }
  ```

- Post

架构配置表，通用的增删改

```csharp
  ///<summary>
  ///(组织)  /Api/Group/
  ///</summary>
  [Route("api/[controller]")]
  [ApiController]
  // [TypeFilter(typeof(QuickSafeFilter))]
  public class GroupController : ControllerBase
  {

      /// <summary>
      /// 当前请求的令牌
      /// </summary>
      private QuickSafeFilterModel safe;

      /// <summary>
      /// 初始化
      /// </summary>
      /// <param name="_fiter">DI注入请求令牌</param>
      public GroupController(QuickSafeFilterModel _fiter){
        safe=_fiter
      }

      [HttpPost]
      public IActionResult Post(PageCmd<QuickCodeModel> pc)
      {
          MessageModel<bool> mm = new MessageModel<bool>();

          try
          {
            // 本地方法
             QuickORMBLL bll = new QuickORMBLL();
             // 需要编辑表的索引编码
              string code ="";
              switch (pc.cmd)
              {
                  case "add":
                      mm.data = bll.add(code, pc.obj, safe.TokenUser.UserID);
                      break;
                  case "edit":
                      mm.obj = bll.edit(code, pc.id, pc.obj, safe.TokenUser.UserID);
                      break;
                  case "del":
                      mm.data = bll.delete(code, pc.par.ToString(), safe.TokenUser.UserID);
                      break;
                  case "audit": //标准审核
                      mm.data = bll.audit(code, pc.par.ToString(), safe.TokenUser.UserID);
                      break;
                  case "resc": //超级撤销审核权限
                      mm.data = bll.audit(code, pc.par.ToString(), safe.TokenUser.UserID, 1);
                      break;
                  case "erase": //物理删除
                     mm.obj = bll.erase(pc.par.ToString(), TokenUser.UserID);
                       break;
              }
              mm.code = 1;
          }
          catch (Exception ex)
          {
              mm.msg = ex.Message;
          }
          return Ok(mm);
      }
  }
```

## DBController

通用数据接口

- Startup 需要定义动态路由

```csharp
 public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
 {
    app.UseEndpoints(endpoints =>
    {
                // 自定义 数据编辑接口
        endpoints.MapControllerRoute("DB", "/DB/{*code}", new
        {
            controller = "DB",
            action = "Index", //指定接收动作
        });

    }
 }
```

## PagerTplModel

返回给前端组件 `Listor` 配置参数

```csharp
 /// <summary>
    /// 数据模板对象
    /// </summary>
    public class PagerTplModel
    {
        /// <summary>
        /// 查询字段
        /// </summary>
        public string sql { get; set; }

        /// <summary>
        /// 数据字段
        /// </summary>
        public dynamic field { get; set; }

        /// <summary>
        /// 外键查询
        /// </summary>
        public dynamic join { get; set; }

        /// <summary>
        /// Ui 字段属性定义：TplOptsModel  DynamicObject
        /// </summary>
        public dynamic attr { get; set; }

        /// <summary>
        /// 自定义 Ui 数据权限
        /// </summary>
        public dynamic act { get; set; }

        /// <summary>
        ///  UI 主键primary 与审核键定义 auth
        /// </summary>
        public dynamic keys { get; set; }

        /// <summary>
        ///  自定义 排序字段
        /// </summary>
        public dynamic sort { get; set; }

        /// <summary>
        /// 自定义 表头显示字段
        /// </summary>
        public dynamic th { get; set; }

        /// <summary>
        /// 自定义 可编辑的字段
        /// </summary>
        public dynamic edit { get; set; }

        /// <summary>
        /// 序列化JSON对象，得到返回的JSON代码
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("field:" + field);
            sb.Append(",attr:" + attr);
            sb.Append(",act:" + act);
            sb.Append(",sort:" + sort);
            sb.Append(",th:" + th);
            sb.Append(",edit:" + edit);
            return "{" + sb.ToString().Substring(1) + "}";
        }
    }
```

## PagerTplOptsModel

```csharp
    /// <summary>
    /// 字段自定义 UI属性
    /// </summary>
    public class PagerTplOptsModel
    {

        #region 系统保留定义

        /// <summary>
        /// 字段名称
        /// </summary>
        public string f { get; set; }

        /// <summary>
        /// 字段类型
        /// </summary>
        public string t { get; set; }

        /// <summary>
        /// 自定义编辑控件类型名称
        /// </summary>
        public string c { get; set; }

        /// <summary>
        /// 必填字段
        /// </summary>
        public bool n { get; set; }

        /// <summary>
        /// 字段只读，前端不可编辑,rad
        /// </summary>
        public int r { get; set; }

        /// <summary>
        /// 字段表头不显示,hid
        /// </summary>
        public int h { get; set; }

        /// <summary>
        /// 字段内容需审核,aut
        /// </summary>
        public int a { get; set; }

        /// <summary>
        /// 外键字段描述,def
        /// </summary>
        public int d { get; set; }

        /// <summary>
        /// 多选项
        /// </summary>
        public string ls { get; set; }


        /// <summary>
        /// 限制最大长度
        /// </summary>
        public Int32 max { get; set; }
        #endregion

        #region 数据表字段 可选自定义描述

        /// <summary>
        /// 字段 提示
        /// </summary>
        public string tip { get; set; }

        /// <summary>
        /// ui 控件数据源
        /// </summary>
        public string api { get; set; }
        #endregion
    }
```

## CRUD Api 增删改

```csharp
  //获取当前请求的类型
string _Method = context.HttpContext.Request.Method;
switch (_Method)
{
    case "OPTIONS":
       //请求预检，默认通过
       qsf.isPass = true;
      throw new Exception(string.Format("预检请求:{0}", _Method));
    case "GET":
       qsf.cmd = "list";
      break;
    case "DELETE":
      // 真正删除
      qsf.cmd = "erase";
    break;
      case "PUT": // 整体更新
      qsf.cmd = "edit";
    break;
      case "PATCH": // 局部字段更新
      qsf.cmd = "audit";
    break;
    default:
      //获取请求参数 POST
}
```

## Join 外键查询配置

```json
{ "MUser_ID": { "server": 2, "table": "QF_User", "fields": "LastFirst" } }
```

- 外键查询字段 MUser_ID

| 属性   | 说明                             | 类型   | 默认值 |
| ------ | -------------------------------- | ------ | ------ |
| sever  | 如需跨域请配置服务器 ID          | int    | -      |
| table  | 外键查询表                       | String | -      |
| fields | 外键输出的字段 多个字段用,号隔开 | String | -      |

## DI 数据应用接口

### Startup 接口配置

```csharp
public class Startup{
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
            app.UseEndpoints(endpoints =>
            {

                // 自定义 数据编辑接口
                endpoints.MapControllerRoute("DB", "/DB/{*path}", new
                {
                    controller = "DB",
                    action = "Index", //指定接收动作
                });

                // 自定义 数据编辑接口
                endpoints.MapControllerRoute("DI", "/DI/{*path}", new
                {
                    controller = "DI",
                    action = "Index", //指定接收动作
                });

                // 标准MVC 控制器
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
    }
}
```
