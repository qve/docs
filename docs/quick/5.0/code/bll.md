# QuickBLL 基础库方法

## HttpUtility Url 编码和解码

js 前端参数 encodeURIComponent

```csharp
//URL 编码测试
string result1 = HttpUtility.UrlEncode("张三丰");
Console.WriteLine(result1); // %e5%bc%a0%e4%b8%89%e4%b8%b0
string result2 = HttpUtility.UrlDecode(result1);
Console.WriteLine(result2); // 张三丰
```

### URL 参数键值对

```csharp
string path = "name=zhangsan&age=13";
NameValueCollection values = HttpUtility.ParseQueryString(path);
Console.WriteLine(values.Get("name"));// zhangsan
Console.WriteLine(values.Get("age")); // 13
```

### HTML 编码解码

```csharp
string html = "<h1>张三丰</h1>";
string html1 = HttpUtility.HtmlEncode(html);
Console.WriteLine(html1); // &lt;h1&gt;张三丰&lt;/h1&gt;
string html2 = HttpUtility.HtmlDecode(html1);
Console.WriteLine(html2); // <h1>张三丰</h1>
```
