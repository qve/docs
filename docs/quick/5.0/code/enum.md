# QuickEnum

- enums 枚举是值类型，数据直接存储在栈中，而不是使用引用和真实数据的隔离方式来存储
- 默认情况下，枚举中的第一个变量被赋值为 0，其他的变量的值按定义的顺序来递增(0,12,3...)
- enum 枚举类型的变量的名称不能相同，但是值可以相同

## QuickEnum 枚举

- 枚举可以使代码更易于维护，有助于确保给变量指定合法的、期望的值。

- 枚举使代码更清晰，允许用描述性的名称表示整数值，而不是用含义模糊的数来表示。

- 枚举使代码更易于键入。在给枚举类型的实例赋值时，VS.NET IDE 会通过 IntelliSense 弹出一个包含可接受值的列表框，减少了按键次数，并能够让我们回忆起可能的值

```csharp
        /// <summary>
        /// 日志处理
        /// </summary>
        public enum Log
        {
            /// <summary>
            /// 日志写入错误
            /// </summary>
            [Description("日志写入错误")]
            errorWrite = 900
        }
```

## userAgent 支付宝,微信,qq

```csharp
  string agent = Request.Headers["User-Agent"];
  if (agent.ToLower().Contains("micromessenger")){
    // 是微信
  }

  QuickEnum.UserAgentApp uaa =QuickEnum.GetBy<QuickEnum.UserAgentApp>(agent);
  //  uaa= QuickBLL.CheckUserAgentApp(agent);
  switch (uaa)
  {
    case QuickEnum.UserAgentApp.notFound:
       //不是 微信，qq，支付宝，判断其它浏览器
    break;
    default:
    // 默认
  }
```

## Result 枚举返回

```csharp
try{

}catch (Exception ex)
{
  string _errMsg = QuickEnum.Result(QuickEnum.Log.日志写入错误);
  // "code_900：日志写入错误"

  _errMsg += ";" + ex.Message;
  //将错误抛向上一层
  throw new Exception(_errMsg);
}
```

## GetDescription 枚举描述

获取到对应枚举的描述-没有描述信息，返回枚举值

```csharp

string _title=QuickEnum.GetDescription((DBStateFlagEnum)SF_ID);

//日志写入错误
```

```csharp
    /// <summary>
    /// 常用枚举对象定义
    /// </summary>
    public class QuickEnumModel
    {

        #region 用户

        /// <summary>
        /// 网络会员状态
        /// </summary>
        public enum User
        {
            /// <summary>
            /// 未激活
            /// </summary>
            [Description("未激活")]
            activeNot = 1,
            /// <summary>
            /// 活动
            /// </summary>
            [Description("活动")]
            active = 2,
            /// <summary>
            /// 关闭
            /// </summary>
            [Description("关闭")]
            close = 3,
            /// <summary>
            /// 已注销
            /// </summary>
            [Description("已注销")]
            deregistration = 4,
            /// <summary>
            /// 挂失
            /// </summary>
            [Description("挂失")]
            lose = 5,
            /// <summary>
            /// 已过期
            /// </summary>
            [Description("已过期")]
            overdue = 6,
        }

        /// <summary>
        /// 验证
        /// </summary>
        public enum Auth
        {
            /// <summary>
            /// 应用不存在
            /// </summary>
            [Description("应用不存在")]
            NotFoundApp = 300,
            /// <summary>
            /// 应用未授权
            /// </summary>
            [Description("应用未授权")]
            NotAuthorizedApp = 303,
            /// <summary>
            /// 登陆信息验证失败
            /// </summary>
            [Description("登陆验证错误")]
            LoginInfoFail = 304,
            /// <summary>
            /// 登陆验证失败
            /// </summary>
            [Description("登陆失败")]
            LoginFail = 305,

            /// <summary>
            /// 验证次数超限，30分钟
            /// </summary>
            [Description("请30分钟后重试")]
            Overrun = 306,



            /// <summary>
            /// App令牌配置不存在
            /// </summary>
            [Description("App令牌配置不存在")]
            TokenConfigNot = 307,


            /// <summary>
            /// 请输入正确手机号
            /// </summary>
            [Description("请输入正确手机号")]
            PhoneFail = 320,

            /// <summary>
            /// 请输入正确短信验证码
            /// </summary>
            [Description("请输入短信验证码")]
            PhoneSMSEmpty = 321,

            /// <summary>
            /// 请输入正确短信验证码
            /// </summary>
            [Description("请输入正确短信验证码")]
            PhoneSMSFail = 322,


            /// <summary>
            /// 请输入登录密码
            /// </summary>
            [Description("请输入登录密码")]
            PassWordSMSEmpty = 323,

            /// <summary>
            /// 手机号已注册
            /// </summary>
            [Description("手机号已注册")]
            PhoneIsJoin = 324,

            /// <summary>
            /// 需管理员授权
            /// </summary>
            [Description("需管理员授权")]
            AppLoginJoinTypeFail = 325,

            /// <summary>
            /// 没有传入base 64 验证参数
            /// </summary>
            [Description("令牌为空")]
            TokenEmpty = 330,

            /// <summary>
            /// basic 开头
            /// </summary>
            [Description("令牌格式错误")]
            TokenFormatError = 331,

            /// <summary>
            /// 没有按规范传入AppKey
            /// </summary>
            [Description("令牌AppKey为空")]
            TokenAppKeyEmpty = 332,

            /// <summary>
            /// 没有val 值
            /// </summary>
            [Description("令牌Value为空")]
            TokenValueEmpty = 333,

            /// <summary>
            /// 前端算法格式不对
            /// </summary>
            [Description("令牌参数格式不匹配")]
            TokenNotMatch = 334,

            /// <summary>
            /// redis 没有缓存
            /// </summary>
            [Description("令牌用户信息加载失败")]
            TokenUserEmpty = 335,

            /// <summary>
            /// redis 没有缓存
            /// </summary>
            [Description("请求验证路径为空")]
            PathEmpty = 350,
        }

        /// <summary>
        /// 请求状态
        /// </summary>
        public enum HttpStatus
        {
            /// <summary>
            /// 请求验证失败
            /// </summary>
            [Description("请求验证失败")]
            AuthenticationFailed = 401,

            /// <summary>
            /// 禁止访问服务器拒绝请求
            /// </summary>
            [Description("禁止访问服务器拒绝请求")]
            RequestReject = 403,

            /// <summary>
            /// 服务器找不到请求连接
            /// </summary>
            [Description("服务器找不到请求连接")]
            PathNotFound = 404,

            /// <summary>
            /// 服务器发生错误
            /// </summary>
            [Description("服务器发生错误")]
            ApplicationError = 500
        }

        #endregion

        #region 数据库

        /// <summary>
        /// 数据库提示
        /// </summary>
        public enum DB
        {
            /// <summary>
            /// 数据库连接未配置
            /// </summary>
            [Description("数据库连接未配置")]
            connectionNotConfigured = 800
        }


        #endregion

        #region Log

        /// <summary>
        /// 日志处理
        /// </summary>
        public enum Log
        {
            /// <summary>
            /// 日志写入错误
            /// </summary>
            [Description("日志写入错误")]
            errorWrite = 900
        }
        #endregion

        #region io
        /// <summary>
        /// io 消息类型
        /// </summary>
        public enum MessageType
        {
            /// <summary>
            /// 明文文字消息
            /// </summary>
            Text = 1,
            /// <summary>
            /// json消息
            /// </summary>
            Json = 2,
            /// <summary>
            /// 连接消息
            /// </summary>
            Line = 3,

            /// <summary>
            ///  服务指令
            /// </summary>
            cmd = 6,

            /// <summary>
            /// 回执消息
            /// </summary>
            Receipt = 100
        }

        /// <summary>
        ///  消息等级
        /// </summary>
        public enum MessageLevel
        {
            /// <summary>
            /// 系统消息
            /// </summary>
            System = 1,
            /// <summary>
            ///私聊消息
            /// </summary>
            Two = 2,
            /// <summary>
            /// 群聊消息
            /// </summary>
            Group = 3,

            /// <summary>
            /// 服务接口通知
            /// </summary>
            Sevice = 5,
        }


        /// <summary>
        ///  消息状态
        /// </summary>
        public enum MessageStatus
        {
            /// <summary>
            /// 队列等待发送
            /// </summary>
            Queue = 1,
            /// <summary>
            /// 发送成功
            /// </summary>
            Success = 2,
            /// <summary>
            /// 撤销消息
            /// </summary>
            Revoke = 3,

            /// <summary>
            /// 已删除
            /// </summary>
            Delete = 4,

            /// <summary>
            /// 已读
            /// </summary>
            Read = 5
        }
        #endregion

        #region 浏览器

        /// <summary>
        /// 浏览器类型
        /// </summary>
        public enum UserAgentType
        {
            /// <summary>
            /// 未识别
            /// </summary>
            NotFound = 0,
            Android = 2,
            Windows = 3,
            iPhone = 4,
            iPod = 5,
            iPad = 6,
            /// <summary>
            /// mac
            /// </summary>
            Macintosh = 7,
            /// <summary>
            ///  网关传入安卓
            /// </summary>
            AOS = 8,
            /// <summary>
            ///  网关传入 苹果
            /// </summary>
            iOS = 9,
            /// <summary>
            ///  网关传入
            /// </summary>
            Win = 10,
            WP = 11,
            XP = 12
        }

        /// <summary>
        /// 浏览器 请求来源自定义应用
        /// </summary>
        public enum UserAgentApp
        {
            /// <summary>
            /// 未识别
            /// </summary>
            notFound = 0,
            /// <summary>
            /// 微信请求
            /// </summary>
            micromessenger = 1,
            /// <summary>
            /// QQ 请求
            /// </summary>
            qq = 2,
            /// <summary>
            /// 支付宝
            /// </summary>
            alipayclient = 3,

        }
        #endregion
    }
```
