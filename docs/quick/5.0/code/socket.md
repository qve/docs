# Quick.Socket

## 通信连接

## SocketClient 客户端

- QuickSocket.DLL

## SocketClient 注入 DI 服务

启动注册，全局引用

```csharp
  // 项目启动文件
  public class Startup
  {
        public void ConfigureServices(IServiceCollection services)
        {
            #region 注入消息推送客户端
            // 使用客户端服务
            services.AddSingleton<IQuickSocketService, QuickSocketService>();
            #endregion
        }
  }
```

- 注入接口定义

```csharp
    /// <summary>
    /// 通信接口定义
    /// </summary>
    public interface IQuickSocketService
    {

        /// <summary>
        /// 获取当前客户端连接,uuid,gpid
        /// </summary>
        /// <returns></returns>
        SocketClient GetClient();

        /// <summary>
        /// 客户端初始化
        /// </summary>
        bool SocketInit();


        /// <summary>
        /// 异步推送消息
        /// </summary>
        /// <param name="body">消息内容</param>
        /// <param name="uuid">指定收信用户</param>
        /// <param name="gpid">指定收信群组，可为空</param>
        /// <returns></returns>
        Task PushAsync(string body, string uuid, string gpid);

        /// <summary>
        /// 异步推送消息
        /// </summary>
        /// <param name="hm"></param>
        /// <returns></returns>
        Task PushAsync(MessageHubModel hm);
    }
```

- 接口实现

```csharp
/// <summary>
/// 通信服务实现
/// </summary>
public class QuickSocketService : IQuickSocketService
{
        /// <summary>
        /// 客户端
        /// </summary>
        public static SocketClient client;

        /// <summary>
        /// 客户端初始化
        /// </summary>
        public bool SocketInit()
        {
            //配置服务器连接
            //client = new SocketClient("ws://localhost:5006","/TestHub","刷新令牌可以不填");
            client = new SocketClient(FlowBLL.Setting.SocketHostUrl, FlowBLL.Setting.SocketHub, FlowBLL.Setting.RefreshToken);

             // 服务器上线通知事件，返回用户信息等，具体参考后台端消息定义
            client.connection.On<MessageHubModel>("Online", (message) =>
            {
                string _message = JsonHelper.Serialize(message);
                Console.WriteLine("{0}|{1}", "Online",_message);
            }

            // 群组通知事件
            client.connection.On<MessageHubModel>("GroupEvent", (message) =>
            {
                string _message = JsonHelper.Serialize(message);
                Console.WriteLine("{0}|{1}", "GroupEvent",_message);
            });

            // 私聊推送通知
            client.connection.On<MessageHubModel>("Push", (message) =>
            {
                string _message = JsonHelper.Serialize(message);
                Console.WriteLine("{0}|{1}", "Push",_message);
            });

            // 启动连接
            client.connection.StartAsync();

            // 判断启动状态
            if (client.GetState().ToString() == "Disconnected")
            {
                // 重新连接
                client.connection.StartAsync();
            }
        }


                /// <summary>
        /// 异步推送消息
        /// </summary>
        /// <param name="body">消息内容</param>
        /// <param name="uuid">指定收信用户</param>
        /// <param name="gpid">指定收信群组，可为空</param>
        /// <returns></returns>
        public async Task PushAsync(string body, string uuid, string gpid)
        {
            MessageHubModel hm = new MessageHubModel();
            hm.GroupId = gpid;
            hm.ToUser.UUID = uuid;
            hm.Body.Tip = "WorkFlowSocketService";
            hm.Body.Detail = body;
            try
            {
                // 异步发送启动消息
                await client.PushAsync(hm);
            }
            catch (Exception ex)
            {
                QuickLog.Add(JsonHelper.Serialize(hm), "QuickSocket.PushAsync");
                QuickLog.Error(ex.Message, "QuickSocket.PushAsync");
            }
        }

        public async Task PushAsync(MessageHubModel hm)
        {
            try
            {
                // 异步发送启动消息
                await client.PushAsync(hm);
            }
            catch (Exception ex)
            {
                QuickLog.Add(JsonHelper.Serialize(hm), "QuickSocket.PushAsync");
                QuickLog.Error(ex.Message, "QuickSocket.PushAsync");
            }

        }
}

```

## SocketClient 调用

### Api 调用及时消息

```csharp
    /// <summary>
    /// 通信接口
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class SocketController : ControllerBase
    {
        private IQuickSocketService _socket;

        public SocketController(IQuickSocketService socketService)
        {
            _socket = socketService;
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get(string msg)
        {

            MessageHubModel hm = new MessageHubModel();
            hm.GroupId =  _socket.GetClient().GroupID;
           // hm.ToUser.UUID = uuid;
            hm.Body.Tip = "WorkFlowSocketService";
            hm.Body.Detail = msg;
            _socket.PushAsync(hm);

          return Ok('push');
        }
    }
```

## 工作流发送消息

```csharp

    /// <summary>
    /// 异步推送通知消息
    /// </summary>
    public class PushMessageStep : StepBody
    {
        private IQuickSocketService _socket;

        public PushMessageStep(IQuickSocketService socket)
        {
            _socket = socket;
        }

        /// <summary>
        /// 输入消息参数
        /// </summary>
        public string Message { get; set; }

        public override ExecutionResult Run(IStepExecutionContext context)
        {
            MessageHubModel hm = new MessageHubModel();
            hm.GroupId = _socket.GetClient().UUID;
            hm.Body.Tip = "WorkFlowSocketService";
            hm.Body.Detail = Message;

            _socket.PushAsync(hm);

            return ExecutionResult.Next();
        }
    }

```

## udp sockets 10054 错误

执行 Send 导致 Receive 抛出异常，而且导致 next Receive 救也救不回来.

- 处理办法接收与发送都采用异步模式（目前已验证解决）。

- UDP 的 IOControl（低级操作模式,未实际测试）

[sockets](https://docs.microsoft.com/zh-cn/dotnet/api/system.net.sockets.iocontrolcode?view=net-5.0)

```csharp
private IPEndPoint LocaIPEP; //udp 本地节点
private IPEndPoint RemoteIPEP; //对方节点
private Socket MyClient; //当前对象
public UdpHandle(IPEndPoint local, IPEndPoint remote)
{
  LocaIPEP = local;
  RemoteIPEP = remote;
  MyClient = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
  uint IOC_IN = 0x80000000;
  uint IOC_VENDOR = 0x18000000;
  uint SIO_UDP_CONNRESET = IOC_IN | IOC_VENDOR | 12;
  MyClient.IOControl((int)SIO_UDP_CONNRESET, new byte[] { Convert.ToByte(false) }, null);
  MyClient.Bind(local);
}
```
