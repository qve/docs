# QuickSafe

> Api 请求的安全认证库，缓存 Redis

- [官方文档 ActionFilterAttribute](https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/filters)

- [Quick.DB](./db.md) 数据库组件，获取数据连接配置

## 全局权限认证

> 全局权限认证默认核验全部,token,user,role,cmd 权限

- [QuickSafeFilterModel]请求参数与验证用户身份信息
- [QuickSafeFilter] 验证拦截方法

### Startup

```csharp
  // 项目启动文件
  public class Startup
  {
        public void ConfigureServices(IServiceCollection services)
        {

            #region 中间件
            // 注册QuickCache 缓存
            QuickCache.Memory = new QuickMemoryProvider();
             // 注入（DI）在同一个请求发起到结束，在服务容器中注册自定义中间件用户类
            services.AddScoped(typeof(QuickSafeFilterModel));
            #endregion

            services.AddControllersWithViews(options =>
            {
                // 全局注册权限验证过滤器
                options.Filters.Add(typeof(QuickSafeFilter));
            }).AddJsonOptions(configure =>
            {
                // 全局Json 时间格式化，避免前端ISO转换
                configure.JsonSerializerOptions.Converters.Add(new JsonHelperConvert.DateTimeConverter());
            });
        }
  }

```

### 不验证请求 QuickSafeFilterNo

```csharp
 [QuickSafeFilterNo]
 public class HomeController : Controller
```

### 验证令牌 QuickSafeFilterToken

- 过期令牌也可以放行

```csharp
 [QuickSafeFilterToken]
 public class HomeController : Controller
```

### 验证登录用户 QuickSafeFilterUser

- 令牌验证，并取出用户登录信息，不验证角色组与接口操作权限

```csharp
 [QuickSafeFilterUser]
 public class HomeController : Controller
```

### 验证登录用户 QuickSafeFilterRole

- 令牌验证取出用户登录信息并验证角色组是否有当前请求权限，不验证操作动作权限

```csharp
 [QuickSafeFilterRole]
 public class HomeController : Controller
```

## 局部权限认证

> 全局不注册，只在单个接口启用验证类型

```csharp
  // 项目启动文件
  public class Startup
  {
        public void ConfigureServices(IServiceCollection services)
        {

            #region 中间件
            // 注册QuickCache 缓存
            QuickCache.Memory = new QuickMemoryProvider();
             // 注入（DI）在同一个请求发起到结束，在服务容器中注册自定义中间件用户类
            services.AddScoped(typeof(QuickSafeFilterModel));
            #endregion

            services.AddControllersWithViews(options =>
            {
                // 全局注册权限验证过滤器
               // options.Filters.Add(typeof(QuickSafeFilter));
            }).AddJsonOptions(configure =>
            {
                // 全局Json 时间格式化，避免前端ISO转换
                configure.JsonSerializerOptions.Converters.Add(new JsonHelperConvert.DateTimeConverter());
            });
        }
  }

```

### 控制器权限

- `SafeAuthEnum` 验证类型定义

```csharp
    /// <summary>
    /// 请求身份验证方式
    /// </summary>
    public enum SafeAuthEnum
    {
        /// <summary>
        /// 默认权限验证全部,token,user,role,cmd权限
        /// </summary>
        All = 1,
        /// <summary>
        /// 只验证token合法
        /// </summary>
        token = 2,
        /// <summary>
        /// 验证Token并取出用户信息
        /// </summary>
        user = 3,
        /// <summary>
        /// 验证Token并验证角色组权限
        /// </summary>
        role = 5,
        /// <summary>
        /// 不验证当前请求
        /// </summary>
        No = 4,
    }

```

- 启用权限全部验证

```csharp
 [TypeFilter(typeof(QuickSafeFilter))]
 public class HomeController : Controller
```

- 启用权限验证，取出用户信息

```csharp
 [TypeFilter(typeof(QuickSafeFilter), Arguments = new object[] { SafeAuthEnum.user })]
 public class HomeController : Controller
```

- 启用权限验证，取出用户信息,验证角色组

```csharp
[ApiController, TypeFilter(typeof(QuickSafeFilter), Arguments = new object[] { SafeAuthEnum.role })]
public class HomeController : Controller
```

## 获取请求令牌用户

```csharp

/// <summary>
/// 当前请求的参数
/// </summary>
private QuickSafeFilterModel safe;

[ApiController, TypeFilter(typeof(QuickSafeFilter), Arguments = new object[] { SafeAuthEnum.role })]
public class HomeController : Controller
{
    public HomeController(QuickSafeFilterModel _safe)
    {
      // 获取DI启动配置注入的对象
            safe = _safe;
    }

    public IActionResult Index()
    {
         Int64 _userId= safe.TokenUser.UserID
            return View();
    }
}
```

### QuickSafeFilterModel 请求权限

```csharp

    /// <summary>
    /// 请求权限验证
    /// </summary>
    public class QuickSafeFilterModel
    {
        /// <summary>
        /// 访问请求ID
        /// 每个需要验证的页面设置ID
        /// </summary>
        public Int32 urlID { get; set; }

        /// <summary>
        /// 是否验证通过
        /// </summary>
        public bool isPass { get; set; }

        /// <summary>
        /// 返回验证的类别编码
        /// </summary>
        public Int32 code { get; set; }

        /// <summary>
        /// 返回的验证消息
        /// </summary>
        public string msg { get; set; }

        /// <summary>
        ///  请求应用Key
        /// </summary>
        public string appKey { get; set; }

        /// <summary>
        /// 应用ID
        /// </summary>
        public Int32 appID { set; get; }

        /// <summary>
        ///  当前请求路径
        /// </summary>
        public string path { get; set; }

        /// <summary>
        /// 当前请求操作动作
        /// </summary>
        public string cmd { get; set; }

        /// <summary>
        ///  请求方式 get post
        /// </summary>
        public string method { get; set; }

        /// <summary>
        /// 可用的权限列表 [1,2,3,4,5,6]
        /// </summary>
        public string action { get; set; }

        /// <summary>
        /// 当前验证票据用户
        /// </summary>
        public QuickToken TokenUser { get; set; }

        /// <summary>
        ///  验证返回对象
        /// </summary>
        public QuickSafeFilterModel()
        {
            isPass = false;
            TokenUser = new QuickToken();
            action = "";
        }
    }

```

## QuickSafe 安全认证

> 按模板应用，配置`QF_Rules`规则，添加角色组继承模块应用的权限规则

### 权限数据表

- QuickSafeDBBLL 数据请求方法

  - 数据表

    - [QF_Role] 架构角色组
    - [QF_Rules] 架构`APi` 权限规则,多个 api 用逗号隔开
      `{"p":QF_Page_ID,"c":"QF_Control.code"}`
      `{"p":109,"c":"1,2,3,4,5,6,7"}`

    - [QF_RoleRules] 架构角色组与 `Api` 权限继承配置
    - [QF_Page] 请求 `APi` 权限配置表
    - [QF_Control] 请求 `APi` 操作权限配置表

  - 存储过程

    - [MP_APP_LoginByPwd] 用户名+密码+AppID
    - [MP_APP_LoginByID] 根据登录用户 ID 取出登录用户信息

### 登录认证

- 用户账户+密码登录
  令牌缓存在架构应用配置的 Token 服务器与 db 序号

- 令牌缓存规则

  - hash key: u:L:9526 (QF_Login_ID)
    - key:'7_ur',value:'刷新令牌'
    - key:'7_us',value:'当前会话 ssonid'

- 用户登录信息缓存
  缓存在架构应用配置的 User 服务器与 db 序号

## 拦截器

ActionFilterAttribute 请求拦截器几个方法进行重写。

1. OnActionExecuting：在执行请求开始之前进行调用

2. OnActionExecuted：在执行请求开始之后进行调用

3. OnResultExecuting：在执行请求结束前进行调用

4. OnResultExecuted：在执行请求结束后进行调用

## 角色组权限

- 用户角色组权限配置

`QF_RoleRules.QF_AppInfo_ID`
角色组指定应用的权限配置，默认 1 表示通用应用 ID

## 验证信息缓存

短信验证码发送次数限制。

### 验证次数缓存

- QuickSafeBLL

```cs
        /// <summary>
        /// 获取登陆重试次数
        /// 限制15分钟 最多5次，code:login:l:登陆账户
        /// </summary>
        /// <param name="_LoginCode">登陆账户</param>
        /// <returns>当前已经重试登陆次数</returns>
        public static int GetLoginInt(string _LoginCode)
        {
            int rint = QuickCache.Memory.Get<int>(RedisKeys.LoginInit(_LoginCode));
            //     return Convert.ToInt16(RedisBLL.getCodeTimeOut(RedisKeys.LoginInit(_LoginCode)));
            return rint;
        }

        /// <summary>
        /// 记录登陆重试次数，超时 15分钟
        /// </summary>
        /// <param name="_LoginCode">登录账户</param>
        /// <param name="_value">已登录次数</param>
        /// <returns></returns>
        public static bool SetLoginInt(string _LoginCode, int _value)
        {
            QuickCache.Memory.Set(RedisKeys.LoginInit(_LoginCode), _value, TimeSpan.FromMinutes(15));
            return true;
            //RedisBLL.setCodeTimeOut(RedisKeys.LoginInit(_LoginCode), _value.ToString(), 900);
        }

        /// <summary>
        /// 登录成功后删除记录
        /// </summary>
        /// <param name="_LoginCode"></param>
        /// <returns></returns>
        public static bool RemoveLoginInt(string _LoginCode)
        {
            QuickCache.Memory.Remove(RedisKeys.LoginInit(_LoginCode));
            return true;
        }


        /// <summary>
        /// 获取发送短信密码次数,15分钟有效
        /// 限制15分钟 最多5次修改，code:login:s:登陆账户
        /// <param name="_LoginCode">登陆账户</param>
        /// </summary>
        public int GetSmsInt(string _LoginCode)
        {
            return QuickCache.Memory.Get<int>(RedisKeys.PassWordSmsInt(_LoginCode));
            // return Convert.ToInt16(RedisBLL.getCodeTimeOut(RedisKeys.PassWordSmsInt(LoginCode)));
        }

        /// <summary>
        /// 记录发送短信密码次数,15分钟有效
        /// </summary>
        /// <param name="_LoginCode">登陆账户</param>
        /// <param name="_value">当前登陆次数</param>
        /// <returns></returns>
        public bool SetSmsInt(string _LoginCode, int _value)
        {
            QuickCache.Memory.Set(RedisKeys.PassWordSmsInt(_LoginCode), _value, TimeSpan.FromMinutes(15));
            return true;
            //  return RedisBLL.setCodeTimeOut(RedisKeys.PassWordSmsInt(_LoginCode), _value.ToString(), 900);
        }

        /// <summary>
        /// 登录成功后删除记录
        /// </summary>
        /// <param name="_LoginCode"></param>
        /// <returns></returns>
        public bool RemoveSmsInt(string _LoginCode)
        {
            QuickCache.Memory.Remove(RedisKeys.PassWordSmsInt(_LoginCode));
            return true;
        }

```

### Redis 验证码

采用 redis 缓存，降低部署更新导致信息丢失的影响，速度会比本机慢

- QuickAuthBLL

```cs


```
