# 插件

## 阿里云短信

- [阿里云在线调试接口](https://next.api.aliyun.com/api/Dysmsapi/2017-05-25/SendSms?spm=5176.12207334.0.0.1d1c1cbeMj0SrL&sdkStyle=dara&params={}&tab=DEMO&lang=CSHARP)

- [阿里云 Api](https://github.com/aliyun/alibabacloud-csharp-sdk)


## 短信签名不合法

由于项目配置文件`appsettings.json`默认字符编码导致中文乱码问题，用`vscode`打开保存为 utf8编码即可。