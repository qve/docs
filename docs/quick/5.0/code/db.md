# Quick.DB 数据库组件

> 基于 Dapper 封装方法

- namespace Quick.DB
- Quick.DB
- System.Data.SqlClient `4.8.2`

- [Api 数据接口](./db_api.md)
- [Model 数据对象](./db_model.md)
- [MSSQL 数据库常用语句](../../../guide/db/mssql.md)

## 数据库配置

- QuickSettings

```json
{
  "QuickSettings": {
    "DB": {
      "MSSQL": {
        //新架构数据库
        "Quick": "server=127.0.0.1,1000;database=Quick;User ID=User;Password=10000;Max Pool Size =6000;Min Pool Size =5;",
        //默认基础数据库
        "Base": "server=127.0.0.1,1000;database=Quick;User ID=User;Password=10000;Max Pool Size =6000;Min Pool Size =5;",
        //外挂数据库，wechat
        "Plus": "server=127.0.0.1,1000;database=Quick;User ID=User;Password=10000;Max Pool Size =6000;Min Pool Size =5;",
        //网关设备数据库
        "NAS": "server=127.0.0.1,1000;database=Quick;User ID=User;Password=10000;Max Pool Size =6000;Min Pool Size =5;"
      }
    }
  }
}
```

## App 配置

应用的令牌配置与系统应用配置缓存在本机，若没有就去数据库,对应数据表 `QF_AppInfo`

```csharp
// 参考配置文件 QuickSettings.App.Key
// 加载指定
QuickAppBLL qab= new QuickAppBLL(_appKey);
// 应用缓存数据库连接
qab.AppConns
```

## QuickDAL

> 数据库连接层

```csharp
// 建议使用 QuickData
QuickDAL qda=new QuickDAL();
string _connectionString="数据库连接";

return qda.Open(_connectionString, (conn) =>
{
   return conn.Query<T>("sql").ToList();
});

```

## QuickData

> 对象与 SQL 语句封装处理

### 连接配置

```csharp
// Quick框架数据库配置
QuickData.DBQuick = "Quick";
// 基础数据库
QuickData.DBBase = "Base";
// 外挂数据库
QuickData.DBPlus = "Plus";
// CRM数据库
QuickData.DBCRM = "CRM";
// 云设备数据库
QuickData.DBNAS = "NAS";
// 日志数据库
QuickData.DBLog = "Log";
// 公共数据库
QuickData.DBLib = "Lib";
```

### GetQuerySql 查询插入语句

```csharp
  QueryParam qp = new QueryParam();

  qp.TableName = "QF_Table";//操作数据表
 // qp.Field = "NameCn"; //查询字段
  qp.PageSize = 10; //返回条数
  qp.Orderfld = "ID"; //排序字段
  qp.OrderType = 1; //升序或者降序

  // 结构化条件转换 Where 语句
  qp.TParams = new List<TableParams>();
  qp.TParams.Add(new TableParams("SF_ID", 2, "<>"));

  //外键
  qp.Join = new List<QueryJoin>();
  // 跨数据库外键查询
  qp.Join.Add(new QueryJoin("MUser_ID", QuickData.SqlLink("[QF_User]", QuickData.DBBase, false), "LastFirst"));

  QuickData qd = new QuickData();
  // 生成查询语句
  qp.ReturnSQL = qd.GetQuerySql(qp);

      // 只生产插入语句
      // qp.ReturnSQL = qd.GetInsertSql(qp.TableName, qp.ORM);
```

## QueryParam

> 结构化查询条件封装

### ORM 增删改

```csharp

  // 数据对象
  QF_TableCompent o=new QF_TableCompent();
  o.title="测试";

  // 更新条件
  QueryParam qp = new QueryParam();

  qp.ORM =o; // 更新的对象

  // 结构化条件转换 Where 语句
  qp.TParams = new List<TableParams>();
  qp.TParams.Add(new TableParams("ID", o.ID));
  qp.TParams.Add(new TableParams("SF_ID", 2, "<>"));
  qp.TParams.Add(new TableParams("MUser_ID", MUser_ID));

  //操作动作 1:插入,2:更新,4:删除
  qp.Action = 2;



  // 执行数据库
  QuickData.Action(qp);

```

### ORM 查询

```csharp

  QueryParam qp = new QueryParam();

  qp.TableName = "QF_Table";//操作数据表
  qp.Field = "NameCn"; //查询字段
  qp.PageSize = 1; //返回条数
  qp.Orderfld = "ID"; //排序字段
  qp.OrderType = 1; //升序或者降序
   //查询 条件字符串写法
  qp.Where = string.Format("{0}={1}", QuickData.inSQL(_field), QuickData.inSQL(_val));
  // 查询转换
  QuickData.Query<string>(qp);
```

### ORM 分页查询

```csharp
  QueryParam qp = new QueryParam();

  qp.TableName = "QF_Table";//操作数据表
 // qp.Field = "NameCn"; //查询字段
  qp.PageSize = 10; //返回条数
  qp.Orderfld = "ID"; //排序字段
  qp.OrderType = 1; //升序或者降序

  // 结构化条件转换 Where 语句
  qp.TParams = new List<TableParams>();
  qp.TParams.Add(new TableParams("SF_ID", 2, "<>"));

  //外键
  qp.Join = new List<QueryJoin>();
  // 跨数据库外键查询
  qp.Join.Add(new QueryJoin("MUser_ID", QuickData.SqlLink("[QF_User]", QuickData.DBBase, false), "LastFirst"));
  // 查询转换为Json
 // QuickData.list(qp);
  // 查询转换为对象
  QuickData.List<QF_TableCompent>(qp);

```

### 跨数据库处理

- QuickData.SqlLink

```csharp
  QueryParam qp = new QueryParam();
  // 跨数据库外键查询
  qp.Join.Add(new QueryJoin("MUser_ID", QuickData.SqlLink("[QF_User]", QuickData.DBBase, false), "LastFirst"));

```

- sql 语句

```sql
select [AP_NAS_ID]
      ,[savetime]
      ,[natcount] from [服务器名].[数据库名].[dbo].[表名] where savetime>'2020-01-01'
```

### QueryJoin 外键查询

- [QuickORMBLL](./db_model.md) 更多平台外键配置查询

```csharp

public dynamic ListJoin(string Table, PagerInStr pj)
{
  QueryParam qp = new QueryParam();
  // 主表
  qp.TableName =Table;// "QF_Page";
  // 外键表
  qp.Join = new List<QueryJoin>();

   QueryJoin qj=new QueryJoin();
   qj.tPrimary = "App_ID";
   qj.cTable="QF_AppInfo";
   //跨库查询，可以用
  // qj.cTable=QuickData.SqlLink("[QF_User]", QuickData.DBBase, false);
   qj.cPrimary="ID";
   qj.cFields="Title,Path";
    // 添加外键
   qp.Join.Add(qj);

  // 跨数据库外键查询
   qp.Join.Add(new QueryJoin("MUser_ID", QuickData.SqlLink("[QF_User]", QuickData.DBBase, false), "LastFirst"));

  qp.PageSize = pj.size;
  qp.PageIndex = pj.index;
  qp.Orderfld = pj.sort;
  qp.OrderType = pj.desc;

  // 取出数据，返回为 Json
  return QuickData.List(qp);
  // 生成的sql 语句
  // qp.ReturnSQL

  // 不查询，直接生成语句
  // QuickData qd = new QuickData();
  // 生成查询语句
  // qd.GetQuerySql(qp);

/** 返回数据库SQL



  "select t.ID,t.Code,t.MUser_ID ,c1.LastFirst as [t.MUser_ID.LastFirst],c2.Title as [t.App_ID.Title],c2.AppKey as [t.App_ID.AppKey] from QF_Page as t  left outer join QF_User as c1 on t.MUser_ID=c1.ID left outer join  QF_AppInfo as c2 on t.App_ID=c2.ID  order by ID desc OFFSET 0 ROWS FETCH NEXT 5 ROWS ONLY"
*
*/
}
```

### 查询 Json

```csharp

  QueryParam qp = new QueryParam();

  qp.TableName = "QF_Table";//操作数据表
  qp.Field = "NameCn"; //查询字段
  qp.PageSize = 1; //返回条数
  qp.Orderfld = "ID"; //排序字段
  qp.OrderType = 1; //升序或者降序
   //查询 条件字符串写法
  qp.Where = string.Format("{0}={1}", QuickData.inSQL(_field), QuickData.inSQL(_val));
  // 查询转换
  QuickData.Query(qp);
```

### 查询 Json 数组

```csharp

  QueryParam qp = new QueryParam();

  qp.TableName = "QF_Table";//操作数据表
  qp.Field = "NameCn"; //查询字段
  qp.PageSize = 1; //返回条数
  qp.Orderfld = "ID"; //排序字段
  qp.OrderType = 1; //升序或者降序
   //查询 条件字符串写法
  qp.Where = string.Format("{0}={1}", QuickData.inSQL(_field), QuickData.inSQL(_val));
  // 查询转换
  QuickData.List(qp);
```

### SQL 列表查询

> 注入风险，需要使用过滤 QuickData.inSQL(传入的文本值)

```csharp
QueryParam qp = new QueryParam(QuickData.DBQuick);
// 直接写语句有注入风险
qp.sql = string.Format("select * from Test where title={0}",QuickData.inSQL(title));
QuickData.List<QF_TableCompent>(qp)
```

### SQL 执行语句

```csharp
QueryParam qp = new QueryParam(QuickData.DBQuick);
qp.sql = string.Format("update Test set SF_ID={1},MUser_ID={2},U_Time=GETDATE() where ID={0}",123);
QuickData.Execute(qp)
```

## 存储过程

```csharp
QueryParam qp = new QueryParam(QuickData.DBAP);
//存储过程名称
qp.SPName = "";
// 传入参数
qp.SParas = new DynamicParameters();
qp.SParas.Add("@paymentId", paymentId);
qp.SParas.Add("@OPUser_ID", QuickUser.UserID);
//返回参数
qp.SParas.Add("@error", dbType: DbType.Int16, direction: ParameterDirection.Output);
qp = QuickData.ExecutePro(qp);
retInt = qp.SParas.Get<Int16>("@error");
```

## Dapper 存储过程

- [Dapper](https://www.cnblogs.com/c7jie/p/12971352.html)

- 原方法，不建议使用

```csharp
  QuickLoginCacheModel o = null;
  var tParas = new DynamicParameters();
  tParas.Add("@LoginID", Convert.ToInt64(LoginId));
  using (var conn = new SqlConnection("数据库连接字符串"))
  {
    // 取出对象
    o = conn.Query<QuickLoginCacheModel>("MP_APP_LoginByID", tParas, commandType: CommandType.StoredProcedure).FirstOrDefault();
  }
```

- 封装方法

```csharp
  var tParas = new DynamicParameters();
  tParas.Add("@LoginID", Convert.ToInt64(LoginId));
  tParas.Add("@res", "", DbType.String, ParameterDirection.Output);
  int rint= QuickData.Execute(QuickData.DBBase, (conn) =>
  {
     return conn.Execute("QP_PageCodeEdit", qp.SParas, commandType: CommandType.StoredProcedure);
      //return conn.Query<QuickLoginCacheModel>("MP_APP_LoginByID", tParas, commandType: CommandType.StoredProcedure).FirstOrDefault();
  });
  //获取参数
  return tParas.Get<string>("@res");
```

## 事务操作

- [MSSQL 事务介绍](https://www.cnblogs.com/xinysu/p/7860605.html)

同时插入两张表的业务逻辑，将 transaction 作为回调函数的参数，业务逻辑部分直接将 transaction 塞入到各自的业务代码中即可

```csharp

  return QuickData.Execute(QuickData.DBBase, (conn, trans) =>
  {
     var execnum = conn.Execute("insert into xxx ", transaction: trans);
    if (execnum == 0) return false;
    var execnum2 = conn.Execute("update xxx set xxx", transaction: trans);
    if (execnum2 > 0) trans.Commit();
    return execnum > 0;
  });

```

## QueryParam 查询条件

```csharp

    /// <summary>
    ///  查询参数类
    /// </summary>
    public class QueryParam
    {

        #region "Public Variables"
        /// <summary>
        /// 手动 SQL语句，有注入风险，设置PageSize 大于1自动分页并且设置order by
        /// </summary>
        public string sql { get; set; }

        /// <summary>
        /// 操作动作 1:插入,2:更新,4:删除,5:插入返回最新ID
        /// </summary>
        public int Action { get; set; }

        /// <summary>
        /// 操作对象 增删改，
        /// </summary>
        public object ORM { set; get; }

        /// <summary>
        /// 返回查询的总记录数
        /// </summary>
        public Int32 RecordCount { get; set; }

        /// <summary>
        /// 外键连接查询{}
        /// </summary>
        public List<QueryJoin> Join { get; set; }

        /// <summary>
        /// 外键字段返回前缀，默认:为空，建议:t_
        /// </summary>
        public string JoinPrefix { get; set; }

        /// <summary>
        /// 表字段对象 Where（要增删改查，结构化条件模式）
        /// </summary>
        public List<TableParams> TParams { get; set; }

        /// <summary>
        ///  配置的数据库连接名称
        /// </summary>
        public string DBName{ get; set; }

        /// <summary>
        /// 自定义连接数据库
        /// </summary>
        public string DBConnect { get; set; }

        /// <summary>
        /// 字符串是否加密
        /// </summary>
        public bool DBPass { get; set; }

        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 默认为 * 全部，自定义的字段名多个用,号隔开
        /// </summary>
        public string Field { get; set; }

        /// <summary>
        /// 查询条件 需带Where
        /// </summary>
        public string Where { get; set; }

        /// <summary>
        /// 分组条件 Group
        /// </summary>
        public string GroupBy { get; set; }

        /// <summary>
        /// 排序字段,分页必须设置
        /// </summary>
        public string Orderfld { get; set; }

        /// <summary>
        /// 排序类型 1:降序反之升序，需要指定排序字段
        /// </summary>
        public int OrderType { get; set; }

        /// <summary>
        /// sql 锁 默认 with(paglock)
        /// </summary>
        public string Lock { get; set; }

        /// <summary>
        /// 总页数
        /// </summary>
        public int pageTotalCount { get; set; }

        /// <summary>
        /// 当前页码
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 每页记录数，默认为0 不分页
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        ///存储过程名
        /// </summary>
        public string SPName { set; get; }

        /// <summary>
        /// 存储过程 参数条件
        /// </summary>
        public DynamicParameters SParas { get; set; }

        /// <summary>
        /// 返回字段
        /// </summary>
        public string ReturnFields { get; set; }

        /// <summary>
        /// 返回执行SQL语句
        /// </summary>
        public string ReturnSQL { get; set; }

        /// <summary>
        /// 返回最新插入ID  qp.Action =5 为  SCOPE_IDENTITY
        /// </summary>
        public Int64 ReturnID { get; set; }

        /// <summary>
        /// 返回执行查询过程中的错误信息
        /// </summary>
        public string OutErr { set; get; }

        /// <summary>
        /// 输出字段进行,html转码  decodeURIComponent
        /// </summary>
        public bool ValUrlEncode { set; get; }
        #endregion
    }
```

## where 查询条件

### TableParams 查询参数

| 属性   | 说明                                           | 类型   |
| ------ | ---------------------------------------------- | ------ |
| PField | 表字段                                         | string |
| PValue | 字段值                                         | object |
| PQMode | 查询条件`GetPQMode`,跟前端 UI 配置一致,默认`=` | string |

```csharp
QueryParam qp=new QueryParam();
// where 处理条件
qp.TParams.Add(new TableParams("ID", 2, "<>"));
qp.TParams.Add(new TableParams("User_ID", User_ID)); //自己

```

### GetPQMode 字段类型

QuickData.GetPQMode
根据 TParams 传入参数生成查询条件

```csharp

QuickData qd=new QuickData();
/// <summary>
/// 按字段类型生成查询条件
/// </summary>
/// <param name="_field">字段类型</param>
/// <param name="_mode">查询条件</param>
/// <param name="oValue">查询值</param>
/// <returns></returns>
qd.GetPQMode("ID","1,2,3","in");


/**
 * 查询符号，与UI配合使用
 *
 */
const querySymbol = [
  {
    value: '=',
    label: '=',
  },
  {
    value: '≠',
    label: '<>',
  },
  {
    value: '<',
    label: '<',
  },
  {
    value: '<=',
    label: '<=',
  },
  {
    value: '>',
    label: '>',
  },
  {
    value: '>=',
    label: '>=',
  },
  {
    value: 'likeNot',
    label: '≠%',
  },
  {
    value: 'like',
    label: '%',
  },
  {
    value: 'likeE',
    label: '<%',
  },
  {
    value: 'likeB',
    label: '>%',
  },
  {
    value: 'in',
    label: 'in',
  },
  {
    value: 'between',
    label: 'T+1',
  },
];
```
