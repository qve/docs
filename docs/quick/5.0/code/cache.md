# QuickCache 缓存

MemoryCache 是.Net Framework 4.0 开始提供的内存缓存类，使用该类型可以方便的在程序内部缓存数据并对于数据的有效性进行方便的管理, 它通过在内存中缓存数据和对象来减少读取数据库的次数，从而减轻数据库负载，加快数据读取速度，提升系统的性能。

NuGet 引用 Microsoft.Extensions.Caching.Memory 包

## QuickCache 本地缓存

- 全局启动配置 `Startup.cs`

```csharp
using Quick;

public void ConfigureServices(IServiceCollection services)
{

  #region 中间件
    // 注册QuickCache 缓存
    QuickCache.Memory = new QuickMemoryProvider();


            // 自定义配置参考，using Microsoft.Extensions.Caching.Memory;
            //QuickCache.Memory = new QuickMemoryProvider(new MemoryCacheOptions()
            //{
            //    //缓存最大为100份
            //    //##注意netcore中的缓存是没有单位的，缓存项和缓存的相对关系
            //    SizeLimit = 100,
            //    //缓存满了时，压缩20%（即删除20份优先级低的缓存项）
            //    CompactionPercentage = 0.2,
            //    //3秒钟查找一次过期项
            //    ExpirationScanFrequency = TimeSpan.FromSeconds(3)
            //});
  #endregion
}
```

## 按规则增删查

存储字段规则：`qm:应用ID:版本:实际key`
存储字段规则：`qmk:应用key:版本:实际key`

```csharp

 // 按规则缓存内容
 QuickCache.Memory.SetCache("test", "4567", TimeSpan.FromHours(1));

 // 按规则取出内容
 string _val = QuickCache.Memory.GetCache<string>("test");

 // 按规则删除
 QuickCache.Memory.RemoveCache("test");

// 存储是否存在
 QuickCache.Memory.KeyExists("test");

 List<string> ls = new List<string>();
 ls.Add("test1");
 ls.Add("test2");

  // 批量获取
 IDictionary<string, object> lo = QuickCache.Memory.GetCacheAll(ls);

```

## QuickCache 缓存

```csharp

 // 按规则缓存内容
  QuickCache.Memory.Set("test", "4567", TimeSpan.FromHours(1));

 // 按规则取出内容
  string _val = QuickCache.Memory.Get<string>("test");

 // 按规则删除
  QuickCache.Memory.Remove("test");

  // 存储是否存在
  QuickCache.Memory.TryGetValue("test");

  // 存储数量
  QuickCache.Memory.Count();

  List<string> ls=new List<string>();
  // 批量获取
  QuickCache.Memory.GetAll(ls)

```

## 过期删除回调

```csharp

 // 设置过期时间


 //单个缓存项的配置
 MemoryCacheEntryOptions Opts = new MemoryCacheEntryOptions()
 {
    //绝对过期时间1
    //AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddSeconds(2)),
    //绝对过期时间2
    //AbsoluteExpirationRelativeToNow=TimeSpan.FromSeconds(3),
    //相对过期时间
    SlidingExpiration = TimeSpan.FromSeconds(3),
    //优先级，当缓存压缩时会优先清除优先级低的缓存项
    Priority = CacheItemPriority.Low,//Low,Normal,High,NeverRemove
    //缓存大小占1份
    Size = 1
  };

 // 绝对过期
 // Opts.SetAbsoluteExpiration(TimeSpan.FromSeconds(3));
 // 滑动过期
 // Opts.SlidingExpiration = TimeSpan.FromSeconds(3);

 // 过期回掉
 Opts.RegisterPostEvictionCallback((keyInfo, valueInfo, reason, state) =>{
     Console.WriteLine($"回调函数输出【键:{keyInfo},值:{valueInfo},被清除的原因:{reason}】");
 })

 // 按规则存储
 QuickCache.Memory.AddCache('key','value',Opts)
 //标准存储
 QuickCache.Memory.Add('key','value',Opts)
```

## 内存缓存 qm

```csharp
    /// <summary>
    /// 内存缓存方法，名命前缀 qm:
    /// </summary>
    public class QuickMemoryProvider
    {
        #region 属性

        /// <summary>
        /// 内存缓存
        /// </summary>
        private MemoryCache Cache { get; set; }

        /// <summary>
        /// 当前应用 Id
        /// </summary>
        private Int32 AppID { get; set; }

        /// <summary>
        /// 自定义内存缓存方法
        /// </summary>
        /// <param name="cacheOps">自定义配置</param>
        public QuickMemoryProvider(MemoryCacheOptions cacheOps){}

        #endregion

        #region 原生方法
        /// <summary>
        /// 获取对象
        /// </summary>
        /// <param name="key">非规则缓存Key</param>
        /// <returns></returns>
        public T Get<T>(string key){}


        /// <summary>
        /// 添加滑动缓存，自动延期
        /// </summary>
        /// <param name="key">非规则缓存Key</param>
        /// <param name="value">缓存Value</param>
        /// <param name="expiresSliding"> TimeSpan.FromHours(1) 过期时长（如果在过期时间内有操作，则以当前时间点延长过期时间</param>
        /// <returns></returns>
        public object Set(string key, object value, TimeSpan expiresSliding){}

        /// <summary>
        /// 添加缓存，自动延期
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <param name="value">缓存Value</param>
        /// <param name="Opts">自定义配置（到期回调方法：RegisterPostEvictionCallback）</param>
        /// <returns></returns>
        public object Add(string key, object value, MemoryCacheEntryOptions Opts){}

        /// <summary>
        ///通过Key删除缓存数据
        /// </summary>
        /// <param name="key">非规则缓存Key</param>
        public void Remove(string key){}

        /// <summary>
        /// 当前存储总数
        /// </summary>
        /// <returns></returns>
        public int Count(){}

        /// <summary>
        /// 是否存在
        /// </summary>
        ///  <param name="key">非规则缓存Key</param>
        /// <returns></returns>
        public bool TryGetValue(string key){}

        /// <summary>
        /// 获取缓存集合
        /// </summary>
        /// <param name="keys">缓存Key集合</param>
        /// <returns></returns>
        public IDictionary<string, object> GetAll(IEnumerable<string> keys){}
        #endregion

        #region 存储规范

        /// <summary>
        /// 存储字段规则
        /// </summary>
        public string keyPrefix = "qm:{0}:{1}:{2}";

        /// <summary>
        /// 产生存储规则名称 keyPrefix
        /// </summary>
        /// <param name="keyName">模块名称:缓存主键命名，不能重复（ui:home）</param>
        /// <param name="version">版本号,默认0.1</param>
        /// <returns>qm:应用ID:版本:key</returns>
        public string GetKeyName(string keyName, string version = "0.1"){}

        /// <summary>
        /// 产生存储规则名称 keyPrefix
        /// </summary>
        /// <param name="keyName">模块名称:缓存主键命名，不能重复（ui:home）</param>
        /// <param name="appKey">应用配置注册 QuickSettings.App.Key</param>
        /// <param name="version">存储版本 0.1</param>
        /// <returns>keyPrefix</returns>
        public string GetKeyName(string keyName, string appKey, string version){}


        /// <summary>
        /// 获取所有缓存键
        /// </summary>
        /// <returns></returns>
        public List<string> GetKeys(){}

        /// <summary>
        /// 验证缓存项是否存在
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        public bool KeyExists(string key){}


        /// <summary>
        /// 搜索 匹配到的缓存
        /// </summary>
        /// <param name="pattern"></param>
        /// <returns>SearchCacheRegex</returns>
        public IList<string> KeySearch(string pattern){}
        #endregion



        #region 扩展方法


        /// <summary>
        /// 获取key值缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetCache<T>(string key){}

        /// <summary>
        /// 获取非标缓存集合
        /// </summary>
        /// <param name="keys">缓存Key集合</param>
        /// <returns></returns>
        public IDictionary<string, object> GetCacheAll(IEnumerable<string> keys){}



        /// <summary>
        /// 添加滑动缓存，自动延期
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <param name="value">缓存Value</param>
        /// <param name="expiresSliding"> TimeSpan.FromHours(1) 过期时长（如果在过期时间内有操作，则以当前时间点延长过期时间</param>
        /// <returns></returns>
        public object SetCache(string key, object value, TimeSpan expiresSliding){}

        /// <summary>
        /// 添加缓存，自动延期
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <param name="value">缓存Value</param>
        /// <param name="Opts">自定义配置（到期回调方法：RegisterPostEvictionCallback）</param>
        /// <returns></returns>
        public object AddCache(string key, object value, MemoryCacheEntryOptions Opts){}

        /// <summary>
        ///通过Key删除缓存数据
        /// </summary>
        /// <param name="key">Key</param>
        public void RemoveCache(string key){}

        /// <summary>
        ///  批量删除缓存
        /// </summary>
        /// <param name="keys">关键字集合（按规则存储键）</param>
        public void RemoveCache(IEnumerable<string> keys){}



        /// <summary>
        /// 删除所有包含字符串的缓存
        /// </summary>
        /// <param name="contain">包含的字符串</param>
        public void RemoveByContain(string contain){}


        /// <summary>
        /// 批量删除缓存
        /// </summary>
        /// <param name="keys">关键字集合（非规则存储键）</param>
        public void RemoveAll(IEnumerable<string> keys){}

        /// <summary>
        /// 删除所有的缓存
        /// </summary>
        public void Clear(){}

        #endregion
    }
```
