# 认证流程

## 用户登录流程

登陆过程中，小程序、服务器、微信服务器三者交互的时序图：

```mermaid
sequenceDiagram

    应用 ->> + 服务器 : 发送 loginType+appKey+LoginCode+LoginPwd
    服务器 ->> 服务器 : 进行权限验证 AuthLoginCode
    服务器 -->> - 应用 : token
```

### 用户登录应用限制

- Quick.Model.QuickAppInfoModel
- QF_AppInfo.LoginAuthType

- 开放授权

所有平台注册用户，都可以登录使用此应用。

- 应用授权

需要后台管理员，授权注册用户才可以登录此应用。

- 用户授权

需要用户自行先申请授权，才可以登录此应用。
