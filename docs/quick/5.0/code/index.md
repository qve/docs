# Quick 组件库

## QuickSettings 配置

appsettings.json

```js
{
 "QuickSettings": {
    "App": {
      // 应用 自定义Id
      "Id": "0000",
      // 当前应用,
      "Key": "20200000",
      // 应用密码
      "Secret": "89631CFB17F0C9"
    },
    "Log": {
      //日志存储的路径
      "Path": "\\Log\\",
      // 是否写入
      "IsWrite": true,
      // 是否缓存满后实际写入
      "CacheMaxInt": 50,
      // 超过800kb行就备份独立文件
      "CacheMaxBack": 800000
    },
    "DB": {
      //是否加密
      "Pass": false,
      // 密钥
      "PassKey": "",
      "Redis": {
        //工作流服务器
        "Flows": {
          "Host": "127.0.0.1,password=1234567",
          // 缓存数据库
          "DBId": 0,
          "AppName": "work-flows",
          "HubName": "work-flows-hub"
        }
      },
      "MSSQL": {
        //新架构数据库
        "Quick": "server=127.0.0.1;database=Quick;User ID=User;Password=123;Max Pool Size =6000;Min Pool Size =5;",
        //默认基础数据库
        "Base": "server=127.0.0.1;database=Base;User ID=User;Password=123;",
        //外挂数据库，QuickDB,wechat 旧架构
        "Plus": "server=127.0.0.1;database=Plus;User ID=User;Password=123;",
        //网关设备数据库
        "NAS": "server=127.0.0.1;database=NAS;User ID=User;Password=123;",
        // 日志服务器
        "Log": "server=127.0.0.1;database=Log;User ID=User;Password=123;"
      }
    },
    "Session": {
      // 跨域
      "Cors": {
        // 授权域名
        "Origins": [ "http://*.apwlan.com", "http://localhost:8080", "http://localhost:3000", "http://localhost:5000" ]
      },
      // 客户端 cookie 名称
      "Cookie": {
        "Name": "自定义",
        //超时(分钟数)
        "Timeout": 4320
      },
      "Cache": {
        //会话超时(分钟数，默认3天)
        "Expire": 4320,
        // 票据过期刷新有效期：30天
        "ExpireRefresh": 43200
      }
    }
  }
}
```

## 全局跨域配置

```csharp
  // 项目启动文件
  public class Startup
  {
        public void ConfigureServices(IServiceCollection services)
        {

            #region 中间件
            // 注册QuickCache 缓存
            QuickCache.Memory = new QuickMemoryProvider();
             // 注入（DI）在同一个请求发起到结束，在服务容器中注册自定义中间件用户类
            services.AddScoped(typeof(QuickSafeFilterModel));
            #endregion

           #region 跨域

            // 跨域规则的名称 APi 禁止跨域  [DisableCors] 单独跨域 [EnableCors("AllowCors")]
            services.AddCors(options => options.AddPolicy("AllowCors", builder =>
            {
                //builder.AllowAnyOrigin() //允许任意origin
                //.AllowCredentials(); // 可以接收cookie ;

                builder.AllowAnyHeader() //允许任意header
                          .AllowAnyMethod()//允许任意请求方式
                                           //.AllowAnyOrigin() //允许任意origin
                        .WithOrigins("http://localhost:8080", "http://localhost:3000") //指定特定域名才能访问
                        .SetPreflightMaxAge(TimeSpan.FromSeconds(60))    //允许验证OPTIONS请求进行检测时间
                        .SetIsOriginAllowedToAllowWildcardSubdomains()
                       .AllowCredentials(); // 指定接收cookie
            }));

            #endregion

            #region Session 跨域会话保持

            services.AddSession(option =>
            {
                option.IOTimeout = TimeSpan.FromMinutes(QuickSettings.Session.Cookie.Timeout);
                // 会话超时分钟配置
                option.IdleTimeout = TimeSpan.FromMinutes(QuickSettings.Session.Cookie.Timeout);
                option.Cookie.Name = QuickSettings.Session.Cookie.Name;
                //js无法获得cookie ，防XSS攻击
                option.Cookie.HttpOnly = true;
                // 使用
                option.Cookie.IsEssential = true;
                  // 使用宽松模式，同一站点保持会话
                //  option.Cookie.SameSite = SameSiteMode.Unspecified; //.Lax.None.Strict;
                option.Cookie.SameSite = SameSiteMode.None;
                //设置存储用户登录信息（用户Token信息）的Cookie，只会通过HTTPS协议传递，如果是HTTP协议，Cookie不会被发送。
                // 注意，option.Cookie.SecurePolicy属性的默认值是Microsoft.AspNetCore.Http.CookieSecurePolicy.SameAsRequest
                option.Cookie.SecurePolicy = CookieSecurePolicy.Always;
            });

            #endregion

            services.AddControllersWithViews(options =>
            {
                // 全局注册权限验证过滤器
                options.Filters.Add(typeof(QuickSafeFilter));
            }).AddJsonOptions(configure =>
            {
                // 全局Json 时间格式化，避免前端ISO转换
                configure.JsonSerializerOptions.Converters.Add(new JsonHelperConvert.DateTimeConverter());
            });

            // 运行时编译view页面，方便调试
            // services.AddRazorPages().AddRazorRuntimeCompilation();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

           #region 自定义配置

            // 支持默认文件 index.html
           // app.UseDefaultFiles();

            // 静态文件
           // app.UseStaticFiles();

            // 跨域
            app.UseCors("AllowCors");

            // Session
            app.UseSession();
            #endregion

           app.UseEndpoints(endpoints =>
            {
                // 自定义 数据编辑接口
                endpoints.MapControllerRoute("DB", "/DB/{*path}", new
                {
                    controller = "DB",
                    action = "Index", //指定接收动作
                });

                // 自定义 数据编辑接口
                endpoints.MapControllerRoute("DI", "/DI/{*path}", new
                {
                    controller = "DI",
                    action = "Index", //指定接收动作
                });

                // 标准MVC 控制器
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
  }
```

## 跨域请求

- [SameSite cookie 跨域示例](https://docs.microsoft.com/zh-cn/aspnet/core/security/samesite/rp31?view=aspnetcore-3.1&viewFallbackFrom=aspnetcore-5.0)

### SameSite SecurePolicy 兼容

- SameSite 属性可以让 Cookie 在跨站请求时不会被发送，从而可以阻止跨站请求伪造攻击（CSRF）。至于什么是 CSRF 这里就不具体说了。
  SameSite 可以有下面三种值：
  1. Strict 仅允许一方请求携带 Cookie，即浏览器将只发送相同站点请求的 Cookie，即当前网页 URL 与请求目标 URL 完全一致。
  2. Lax 允许部分第三方请求携带 Cookie
  3. None 无论是否跨站都会发送 Cookie，支持跨域发送 cookie， None 指令需要搭配 Secure 指令 SecurePolicy
  4. Unspecified，这意味着，不会向 cookie 中添加 SameSite 属性，客户端将使用其默认行为（对于新浏览器而言，对于旧浏览器则为 "无"）
     造成现在无法获取 cookie 是因为之前默认是 None 的，Chrome80 后默认是 Lax。

## 下载配置 UseStaticFiles

- [官方文档](https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/static-files?view=aspnetcore-5.0)

```csharp
  // 项目启动文件
  public class Startup
  {
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseStaticFiles(new StaticFileOptions
            {
                //FileProvider = new PhysicalFileProvider(Directory.GetCurrentDirectory()),
                //设置不限制content-type 该设置可以下载所有类型的文件，但是不建议这么设置，因为不安全
                //下面设置可以下载apk和nupkg类型的文件
                ContentTypeProvider = new FileExtensionContentTypeProvider(new Dictionary<string, string>
                {
                      { ".apk", "application/vnd.android.package-archive" }
                })
            });
        }
  }
```

### 404.8 拒绝 hiddenSegment

IIS7 url 路径禁止包含 bin 的文件夹
