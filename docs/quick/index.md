# Quick 快框架

## 版本

dotnet --version

## 插件

- Dapper
- System.Data.SqlClient
- Microsoft.Extensions.Configuration;

## net 注释转 Markdown

- [Quick.Docs.MarkDownHelper](./6.0/Quick.Docs/MarkDownHelper.md)

1. 类库属性> 生成>勾选输出 xml 文档。
2. 将输出的注释 xml 文件内容，拷贝到： `http://docs.apwlan.com` 进行 MD 文档转换

## summary 注释语法规范

- [微软注释 语法](https://docs.microsoft.com/zh-cn/dotnet/csharp/language-reference/xmldoc/)
- [vitepress 语法](https://vitepress.vuejs.org/guide/frontmatter.html)
- [MarkDown 语法](https://qve.gitee.io/docs/plugin/markdown.html)

::: warning
编译后正常，调试模式下表格中有大括弧`{}`会导致报错，前后加标签 ` 解决。
:::

```csharp

namespace Quick
{
    /// <summary>
    /// 请使用新的 DataSafe
    /// </summary>
    /// <remarks>
    /// <version>2019.02</version>
    /// <obsolete>弃用说明</obsolete>
    /// 继承 <see cref="QuickUserModel" />
    /// </remarks>
    [Obsolete] //标记该方法已弃用
    public class QFD: QuickUserModel
    {
        // Debug.LogError("你应该调用本类的 OpenMessageBox 方法");
    }

    /// <summary>
    /// 数据权限处理方法
    /// </summary>
    /// <remarks>
    /// <version>2021.01</version>
    /// <see href="../code/safe.html">使用示例 </see>
    /// <see cref="QuickSafeFilterNo">QuickSafeFilterNo 不验证请求参数</see>
    /// </remarks>
    public class DataSafe
    {

        /// <summary>
        /// 是否验证通过
        /// </summary>
        /// <remarks>
        ///  <seealso cref="bool"/>
        /// </remarks>
        public bool isPass { get; set; }

        /// <summary>
        /// 返回验证的类别编码
        /// </summary>
        /// <remarks>
        /// 创建类型连接用 seealso
        /// <seealso cref="Int32" />
        /// </remarks>
        public Int32 code { get; set; }

        #region 配置
        /// <summary>
        /// 当前连接配置
        /// </summary>
        /// <remarks>
        ///  <seealso cref="RedisConfigModel" />
        /// </remarks>
        public RedisConfigModel Config { get; set; }

        /// <summary>
        /// 连接对象
        /// </summary>
        /// <remarks>
        /// type 不创建连接
        /// <type>StackExchange.Redis.ConnectionMultiplexer</type>
        /// </remarks>
        public ConnectionMultiplexer Conn;

        /// <summary>
        /// 自定义 主键Key前缀:
        /// </summary>
        /// <remarks>
        ///  <seealso cref="string" />
        /// </remarks>
        public string CustomKey;

        /// <summary>
        /// 返回数据连接对象
        /// </summary>
        /// <remarks>
        /// <see cref="ConnectionMultiplexer.GetDatabase(int, object)"/>
        /// <type>StackExchange.Redis.IDatabase</type>
        /// </remarks>
        public IDatabase db
        {
            get { return Conn.GetDatabase(Config.DBId); }
        }


        /// <summary>
        /// 不等于
        /// </summary>
        ///  <remarks>
        /// 注释包含特殊符号按以下方式书写
        /// <![CDATA[ <> ]]>
        /// </remarks>
        public const string NotEqualTo = "<>";

        #endregion

        /// <summary>
        /// This sample shows how to specify
        /// </summary>
        /// <remarks>
        /// the  attribute.
        /// </remarks>
        public DataSafe(string _CustomKey)
        {
           CustomKey=_CustomKey
        }
    }



    /// <summary>
    /// 代码示例说明
    /// </summary>
    /// <example>
    /// This sample shows how to call the <see cref="GetZero"/> method.
    /// <code>
    /// class TestClass
    /// {
    ///     static int Main()
    ///     {
    ///         return GetZero();
    ///     }
    /// }
    /// </code>
    /// </example>
    public static int GetZero()
    {
        return 0;
    }
}
```

### 注释参数

| 命名     | 说明     | 描述                 |
| -------- | -------- | -------------------- |
| summary  | 注释     | 简要说明             |
| remarks  | 备注     | 说明组件的作用       |
| param    | 参数     |                      |
| returns  | 返回     |                      |
| obsolete | 标记弃用 | 此方法，用于提示更新 |

### remarks 注释备注

| 命名     | 说明                                      | 描述                                                                                                    |
| -------- | ----------------------------------------- | ------------------------------------------------------------------------------------------------------- |
| version  | 版本标记                                  | 用于常用函数更新标记                                                                                    |
| obsolete | 弃用说明                                  | 建议跟版本配合，用于区别那个版本被弃用                                                                  |
| see      | cref                                      | [cref 代码引用](https://docs.microsoft.com/zh-cn/dotnet/csharp/programming-guide/xmldoc/cref-attribute) |
| see      | href                                      | 帮助连接等                                                                                              |
| seealso  | 方法跟 see 一致，本项目仅用于定义属性类型 | 指定希望在“请参见”一节中出现                                                                            |
| type     | 定义返回的数据类型，注释默认为空          | 引用全路径                                                                                              |

## 语法规范

::: warning
文档标题由参数名与注释第一行组成，建议功能描述换行
:::

命名使用驼峰形式

- 变量定义
  - 类变量小写开头
  - 方法内临时变量，用\_开头
- 大写开头名称
  - 类名
  - 方法名

### Model 对象类规范

定义实体对象建议名称结尾加入 Model

```csharp
public class TestModel
{

   public string name{get;set}

   public TestModel() : this("默认名")
   {

   }

   public TestModel(string _name){
     name=_name
   }
}
```
