# DB 数据源组件

## FieldListor TPL 字段列表组件

- [将 TPL 参数转为字段列表](./tpl.md)

<CodeRun auto editable>

```vue
<template>
  <div>
    <FieldListor :tpl="the.tpl" @onEvent="FieldEvent" />
  </div>
</template>
<script>
export default {
  setup() {
    const { toColumnByTpl } = window.$plus.qve;
    const { reactive } = window.$plus.vue;

    const the = reactive({
      tpl: {
        sql: 'ID,QF_Appinfo_ID,Title,OnTime,OnType',
        field: {
          ID: 'ID',
          QF_Appinfo_ID: '所属应用',
          Title: '描述',
          OnTime: '当前时间',
          OnType: '打卡类别'
        },
        attr: {
          ID: {
            t: 'Int64',
            pk: 1,
            r: 1
          },
          QF_Appinfo_ID: {
            t: 'Int32',
            n: true,
            v: '1',
            c: 'panel'
          },
          Title: {
            t: 'String',
            max: 50
          },
          OnTime: {
            t: 'DateTime',
            n: true,
            v: 'getdate()'
          },
          OnType: {
            t: 'Int32',
            n: true,
            v: '1',
            ls: '1:打卡,2:外勤,3:其它'
          }
        },
        keys: {
          primary: 'ID'
        }
      }
    });

    const FieldEvent = (resp) => {};

    return { the, FieldEvent };
  }
};
</script>
```

</CodeRun>

## FieldEditor 字段属性编辑器
