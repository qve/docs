# editor 动态表单

根据 `TPL` 配置参数，创建表单。

## editor 动态表单

<CodeRun editable>

```vue
<script>
const testTpl = {
  //  是否从服务器取模板查询参数
  isHttp: false,

  tpl: {
    sql: '',
    field: {
      ID: 'ID',
      Code: '索引编码',
      App_ID: '应用ID',
      PageType: '类别',
      State: '页面状态',
      Path: '请求路径',
      Title: '页面标题',
      Description: '功能说明',
      MenuTitle: '菜单标题',
      MenuIcon: '菜单图标',
      MenuOpen: '菜单打开模式',
      CodeLang: '代码语言',
      CodeVersion: '代码版本',
      CodeTime: '代码更新',
      body: '源JSONbody',
      ContUbb: '源Ubb',
      CodeSource: '源代码',
      CodeCompile: '编译代码',
      SF_ID: '编辑状态',
      MUser_ID: '编辑者',
      U_Time: '编辑时间'
    },
    join: null,
    attr: {
      ID: {
        t: 'Int64',
        pk: 1,
        r: 1
      },
      Code: {
        t: 'String',
        n: true,
        v: '0',
        max: 50
      },
      App_ID: {
        t: 'Int32',
        n: true,
        v: '1',
        c: 'panel',
        api: '/Api/QF_Group/'
      },
      PageType: {
        t: 'Int32',
        n: true,
        v: '1',
        ls: '1:api接口,2:menu菜单,3:vue组件,5:html'
      },
      State: {
        t: 'Int32',
        n: true,
        v: '1',
        ls: '1:暂停,2:启用,3:停用,4:删除'
      },
      Path: {
        t: 'String',
        max: 2147483647,
        tip: '请求时进行权限验证'
      },
      Title: {
        t: 'String',
        n: true,
        max: 200,
        c: 'text'
      },
      Description: {
        t: 'String',
        max: 1000
      },
      MenuTitle: {
        t: 'String',
        max: 50,
        d: 1
      },
      MenuIcon: {
        t: 'String',
        max: 50
      },
      MenuOpen: {
        t: 'String',
        max: 200
      },
      CodeLang: {
        t: 'String',
        n: true,
        v: 'vue',
        max: 50
      },
      CodeVersion: {
        t: 'String',
        n: true,
        v: '0.0.1',
        max: 50
      },
      CodeTime: {
        t: 'DateTime',
        n: true,
        v: 'getdate()'
      },

      body: {
        t: 'String',
        c: 'json'
      },
      ContUbb: {
        t: 'String',
        c: 'ubb'
      },
      CodeSource: {
        t: 'String',
        g: 'encode',
        max: 2147483647,
        h: 1,
        c: 'code'
      },
      CodeCompile: {
        t: 'String',
        max: 2147483647,
        h: 1
      },
      SF_ID: {
        t: 'Int32',
        n: true,
        v: '1',
        r: 1,
        e: 1,
        a: 1
      },
      MUser_ID: {
        t: 'Int64',
        n: true,
        v: '1',
        r: 1,
        e: 1
      },
      U_Time: {
        t: 'DateTime',
        n: true,
        v: 'getdate()',
        r: 1,
        e: 1
      }
    },
    act: [1, 2, 3, 4, 5, 6, 7],
    keys: {
      primary: 'ID',
      auth: 'SF_ID'
    },
    sort: null,
    th: null,
    edit: null
  }
};

const testApiData = {
  ID: 497,
  Code: '497',
  App_ID: 1,
  PageType: 3,
  State: 2,
  Path: '/Page/work/place',
  Title: '\u5DE5\u4F5C\u53F0',
  Description: null,
  MenuTitle: null,
  MenuIcon: null,
  MenuOpen: null,
  CodeLang: 'vue',
  CodeVersion: '0.0.1',
  CodeTime: '2021-03-22 16:28:59',
  body: { MUser_ID: { table: 'QF_User', title: 'LastFirst' } },
  ContUbb: '&lt;d&gt;asfasf&lt;/d&gt;',
  CodeSource:
    '\u003C!-- \u5DE5\u4F5C\u53F0 --\u003E\n\u003Ctemplate\u003E\n  \u003Cdiv class=\u0022work-place-page\u0022\u003E\n    \u003CButton @click=\u0022btnEvent\u0022\u003E\n      \u6D4B\u8BD5\n    \u003C/Button\u003E\n  \u003C/div\u003E\n\u003C/template\u003E\n\n\u003Cscript\u003E\nexport default {\n  setup (props, { emit }) {\n\n    const $plus = window.$plus;\n\n    const { reactive } = $plus.vue;\n\n    // \u5916\u6302\u5199\u6CD5\n    const { useGlobalStore } = $plus.store;\n    //\u8C03\u7528 inject \u51FD\u6570\u65F6\uFF0C\u901A\u8FC7\u6307\u5B9A\u7684\u6570\u636E\u540D\u79F0\uFF0C\u83B7\u53D6\u5230\u7236\u7EA7\u5171\u4EAB\u7684\u6570\u636E\n    const store = useGlobalStore();\n    // console.log(\u0027place:store\u0027, JSON.stringify(store.user))\n\n    const the = reactive({\n      user: store.user\n    });\n\n    // // \u5F15\u5165\u4E0A\u4E0B\u6587\n    // const { ctx } = $vue.getCurrentInstance()\n    // console.log(\u0027page\u0027, router.currentRoute.value)\n\n    const btnEvent = () =\u003E {\n      // \u4E0A\u62A5\u6570\u636E\n      emit(\u0027onEvent\u0027, { cmd: \u0027test\u0027 })\n    };\n\n    return { the, btnEvent }\n  },\n}\n\n\u003C/script\u003E',
  CodeCompile:
    '%3Ctemplate%3E%3Cdiv%20class=%22work-place-page%22%3E%3CButton%20@click=%22btnEvent%22%3E%E6%B5%8B%E8%AF%95%3C/Button%3E%3C/div%3E%20%3C/template%3E%3Cscript%3E%20export%20default%20%7B%20%20%20setup%20(props,%20%7B%20emit%20%7D)%20%7B%20%20%20%20%20%20const%20$plus%20=%20window.$plus;%20%20%20%20%20%20const%20%7B%20reactive%20%7D%20=%20$plus.vue;%20%20%20%20%20%20%20%20%20%20const%20%7B%20useGlobalStore%20%7D%20=%20$plus.store;%20%20%20%20%20%20%20%20%20const%20store%20=%20useGlobalStore();%20%20%20%20%20%20%20%20%20%20const%20the%20=%20reactive(%7B%20%20%20%20%20%20%20user:%20store.user%20%20%20%20%20%7D);%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20const%20btnEvent%20=%20()%20=%3E%20%7B%20%20%20%20%20%20%20%20%20%20%20%20%20emit(\u0027onEvent\u0027,%20%7B%20cmd:%20\u0027test\u0027%20%7D)%20%20%20%20%20%7D;%20%20%20%20%20%20return%20%7B%20the,%20btnEvent%20%7D%20%20%20%7D,%20%7D%20%20%3C/script%3E',
  SF_ID: 1,
  MUser_ID: 5,
  U_Time: '2021-03-23 09:46:34'
};

export default {
  setup() {
    const { reactive } = window.$plus.vue;
    const the = reactive({
      tabs: {
        list: [
          {
            title: '配置'
          },
          {
            title: '内容'
          }
        ]
      },
      /** 对话框 */
      modal: {
        title: '请勾选',
        show: false
      },
      /** 面板 */
      panel: {
        // 数据源地址
        api: '',
        // 返回绑定的主键
        named: '',
        // 查询字段
        field: {},
        // 查询数据
        list: []
      },
      cmd: 'edit', //是否编辑
      keys: {
        cmd: ['read', 'list', 'add', 'edit']
      },
      tpl: testTpl.tpl, // JSON.stringify(testTpl),
      // 编辑对象
      obj: {},

      config: {
        api: '',
        ui: {}
      },
      editor: {}
    });

    the.obj = testApiData;
    // 格式转换
    const onJsonEvent = (resp) => {
      //  console.log('json', resp)
      // if (resp.cmd == 'format' && resp.format) {
      //   the.config.ui = JSON.parse(resp.data)
      //   // the.config.api = new Date
      // }
    };

    /** 编辑窗口外键查询回调 */
    let onEditorCallback;

    const onEditorEvent = (resp) => {
      console.log('onEditorEvent', resp);

      switch (resp.cmd) {
        case 'InputPlus':
          // 扩展图标
          if (resp.attr && resp.attr.api) {
            the.panel.api = resp.attr.api;
            the.panel.named = resp.named;
            the.modal.show = true;
            onEditorCallback = resp.callback;
          }

          break;

        case 'save':
          break;
      }
    };

    /** 面板触发事件 */
    const onPanelEvent = (resp) => {
      // console.log('onPanelEvent:', resp);
      switch (resp.cmd) {
        case 'check':
          // if (resp.item) {
          //   // 选择的数据选项
          //   onEditorCallback(resp.item.ID)
          //   //  resp.item.ID
          //   // console.log('onPanelEvent:' + the.obj[resp.named], resp.item.ID);
          // } else {
          //   if (resp.index > -1) {
          //     // 是否全选
          //     the.obj[resp.named] = the.panel.list[resp.index].ID
          //   }
          // }
          // 回调写入
          onEditorCallback(resp.value);
          the.modal.show = false;

          break;
        case 'close':
          // 关闭窗口
          the.modal.show = false;
          break;
      }
    };

    return { the, onJsonEvent, onEditorEvent, onPanelEvent };
  }
};
</script>

<template>
  <div class="form-test-page">
    <div>
      <Select v-model="the.cmd">
        <option
          v-for="(item, index) in the.keys.cmd"
          :key="index"
          :value="item"
        >
          {{ item }}
        </option>
      </Select>
    </div>
    <Tabs :active="0" :list="the.tabs.list">
      <template #qv-tabs-0>
        <TextJson
          :value="the.tpl"
          spellcheck="false"
          rows="6"
          @onEvent="onJsonEvent"
        />
      </template>
      <template #qv-tabs-1>
        <TextJson v-model="the.obj" spellcheck="false" rows="6" />
      </template>
    </Tabs>

    <Editor
      style="margin:1rem"
      :ui="the.tpl"
      :data="the.obj"
      @onEvent="onEditorEvent"
    />

    <Modal
      :title="the.modal.title"
      :show="the.modal.show"
      footer-hide
      @onEvent="onPanelEvent"
    >
      <!-- <Panel :field="the.panel.field"
             :list="the.panel.list"
             @onEvent="onPanelEvent" /> -->

      <PanelListor
        :named="the.panel.named"
        :api="the.panel.api"
        :field="the.panel.field"
        :list="the.panel.list"
        @onEvent="onPanelEvent"
      />
    </Modal>
  </div>
</template>

<style lang="less">
.form-test-page {
  .qv-input-json {
    width: 90%;
  }
}
</style>
```

</CodeRun>

## Editor props

| 属性  | 说明                                                | 类型   | 默认值 |
| ----- | --------------------------------------------------- | ------ | ------ |
| named | 组件命名                                            | String | editor |
| cmd   | 编辑指令 'read':只读, 'list', 'add', 'edit', 'save' | String | list   |
| ui    | 编辑数据字段模板 TPL                                | Object |        |
| data  | 初始编辑数据                                        | Object |        |

## Editor onEvent

| 属性         | 说明                                                | 类型   | 默认值 |
| ------------ | --------------------------------------------------- | ------ | ------ |
| named        | 组件命名                                            | String | editor |
| cmd          | 编辑指令 'read':只读, 'list', 'add', 'edit', 'save' | String | list   |
| data         | 编辑数据                                            | Object |        |
| mode `0.3.3` | 当前编辑模式,与 `cmd.save` 配合使用                 | String |        |

- 输出案例

```json
{
  "cmd": "save",
  "mode": "add",
  "named": "editor",
  "data": { "State": 2 },
  "primary": "ID",
  "primaryValue": 0
}
```
