# PanelTree

基于 `Tree` 组件封装 `http` 后端请求

## PanelTree 树外键查询面板

<CodeRun auto editable>

```vue
<template>
  <div>
    <Input
      v-model="the.App_ID"
      named="App_ID"
      placeholder="请选中应用ID"
      type="number"
      readonly
      icon="icon-jiantouxia"
      cmd="InputPlusTree"
      @onEvent="onInputEvent"
    />
    <Modal :show="the.show.modal" footer-hide>
      <PanelTree
        :config="the.config"
        :primary="the.primary"
        @onEvent="onPanelTreeEvent"
      />
    </Modal>
  </div>
</template>

<script>
export default {
  setup() {
    const { reactive } = window.$plus.vue;
    const { message, confirm } = window.$plus;
    const { bll } = window.$plus.quick;

    const the = reactive({
      show: {
        modal: false
      },
      primary: 'current_ID', //自定义主键
      config: {
        title: '架构配置',
        key: '4',
        http: {
          url: '/Api/Tree/', //自定义Api
          data: {
            tpl: 4,
            index: 4,
            sort: '4.10.1.1'
          }
        },
        children: []
      }
    });

    /**
     * 树面板
     * @param {*} resp 交互参数
     */
    const onPanelTreeEvent = (resp) => {
      // console.log('onPanelTreeEvent', resp);
      switch (resp.cmd) {
        case 'click':
          // 回调处理选择的数据选项
          // console.log('onPanelTreeEvent', JSON.stringify(resp.item));
          if (resp.item && resp.item.node) {
            // 取出树节点绑定的值
            console.log('onPanelTreeEvent', resp.item.node[resp.primary]);
          }
          break;
        case 'close':
          // 关闭窗口
          resp.close && resp.close();
          break;
      }
    };

    const onInputEvent = (resp) => {
      switch (resp.cmd) {
        case 'InputPlusTree':
          // 自定义事件名与参数
          let _par = bll.parse("{tpl:4,index:4,sort: '4.10.1.1'}");
          console.log('list.InputPlusTree', _par);
          resp.attr = resp.attr || {};
          confirm({
            footerHide: true,
            title: '查询',
            model: {
              props: {
                primary: _par.primary || 'current_ID', //自定义主键
                config: {
                  // title: '架构配置',
                  // key: '4',
                  http: {
                    url: resp.attr.api || '/Api/Tree/', //自定义Api
                    data: _par
                    //  {
                    //   tpl: 4,
                    //   index: 4,
                    //   sort: '4.10.1.1'
                    // }
                  },
                  children: []
                }
              },
              component: 'PanelTree' //绑定组件参数
            },
            onEvent: onPanelTreeEvent
          });

          break;
        case 'InputPlus':
          // 默认事件名
          break;
      }
    };

    return { the, onPanelTreeEvent, onInputEvent };
  }
};
</script>
```

</CodeRun>
