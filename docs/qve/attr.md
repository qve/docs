# TPL Attr 属性方法

- tpl 属性转换封装公共方法

  `import { BindValue } from 'qve';`

## Attr 属性

测试参数

<CodeRun auto editable>

```vue
<template>
  <div>
    <TextJson
      v-model="the.tpl"
      placeholder="tpl json"
      rows="6"
      style="width:70%"
    />

    <Button type="info" @click="btnEvent">参数测试</Button>

    <Table
      border
      :columns="the.columns"
      :data="the.list"
      @onEvent="TableEvent"
    />
  </div>
</template>

<script>
export default {
  setup() {
    const { toColumnByTpl } = window.$plus.qve;
    const { reactive } = window.$plus.vue;

    const the = reactive({
      tpl: {
        sql: 'ID,QF_Appinfo_ID,Title,OnTime,OnType',
        field: {
          ID: 'ID',
          QF_Appinfo_ID: '所属应用',
          Title: '描述',
          OnTime: '当前时间',
          OnType: '打卡类别'
        },
        attr: {
          ID: {
            t: 'Int64',
            pk: 1,
            r: 1
          },
          QF_Appinfo_ID: {
            t: 'Int32',
            n: true,
            v: '1',
            c: 'panel'
          },
          Title: {
            t: 'String',
            max: 50
          },
          OnTime: {
            t: 'DateTime',
            n: true,
            v: 'getdate()'
          },
          OnType: {
            t: 'Int32',
            n: true,
            v: '1',
            ls: '1:打卡,2:外勤,3:其它'
          }
        },
        keys: {
          primary: 'ID'
        }
      },
      /** 模板表格头部 */
      columns: [
        // {
        //   type: 'check',
        //   eventKey: 'check',
        //   width: 40
        // },
        // {
        //   title: '序',
        //   type: 'index'
        // },
        {
          key: 'field',
          eventKey: 'field',
          title: '字段',
          lock: 0
        },
        {
          key: 'title',
          title: '说明',
          width: 100
        },
        {
          key: 'tip',
          title: '提示.tip',
          width: 120
        },
        {
          key: 't',
          title: 'net类型.t',
          width: 80
        },
        {
          key: 'k',
          title: '触发事件.k',
          tip: '默认小写字段名',
          width: 80
        },
        {
          key: 'c',
          title: '编辑控件.c',
          tip: '编辑界面显示控件名',
          width: 60
        },
        {
          key: 'cp',
          title: '控件参数.cp',
          tip: '编辑界面显示控件参数'
        },
        {
          key: 'api',
          title: '自定义Api',
          tip: '请求参数'
        },
        {
          key: 'j',
          title: '多外键指定显示.j',
          tip: '默认空取最后'
        },
        {
          key: 'n',
          title: '必填.n'
        },
        {
          key: 'v',
          title: '默认值.v',
          width: 60
        },
        {
          key: 'w',
          title: 'px列宽.w',
          width: 60
        },
        {
          key: 'h',
          title: '列隐藏.h',
          width: 60
        },
        {
          key: 'e',
          title: '列扩展.e',
          width: 60
        },
        {
          key: 'a',
          title: '列审核.a',
          width: 60
        },
        {
          key: 'r',
          title: '只读.r',
          width: 60
        },
        {
          key: 'g',
          title: '编码.g',
          tip: 'encode'
        },
        {
          key: 'ls',
          title: '多选ls',
          width: 200
        }
      ],
      list: []
    });

    const TableEvent = () => {};

    const btnEvent = () => {
      the.list = toColumnByTpl(the.tpl)[0];
    };
    return { the, btnEvent, TableEvent };
  }
};
</script>
```

</CodeRun>

## toColumnByTpl 属性转换

TPL 模板字段属性转换为行级格式

```js
/**
 * TPL 模板字段属性转换为行级格式
 * @param {*} tpl 数据模板
 * 返回[行数据集合,外挂表显示字段]
 */
const toColumnByTpl = tpl;

// 返回[字段集合,表外键查询字段名]
//tpl 模板字段属性转为对象集合，用于属性配置编辑
```

## toFieldTplByColumn 转换

字段属性行级格式转换为后端修改表字段参数

```js
//tpl 模板字段属性转为对象集合，用于属性配置编辑
let _attr = {
  field: 'DBSQL',
  title: 'SQL查询',
  t: 'String',
  max: 2147483647,
  h: 1,
  c: 'code',
  cp: 'sql'
};
let _field = toFieldTplByColumn(_attr);
// 返回[字段配置对象,新的表外键查询字段名]
// [{"code":"DBSQL","title":"SQL查询{'field':DBSQL,'title':SQL查询,'h':1,'c':'code','cp':'code','d':1,'cp':'sql','d':1}"},"cp"]
```

## modeSymbol 查询符号

- `0.3.4` 原 `querySymbol` 更名为 `modeSymbol` 与后端一致

```js
const modeSymbol = [
  {
    value: '=',
    label: '='
  },
  {
    value: '≠',
    label: '<>'
  },
  {
    value: '<',
    label: '<'
  },
  {
    value: '<=',
    label: '<='
  },
  {
    value: '>',
    label: '>'
  },
  {
    value: '>=',
    label: '>='
  },
  {
    value: 'likeNot',
    label: '≠%'
  },
  {
    value: 'like',
    label: '%'
  },
  {
    value: 'likeE',
    label: '<%'
  },
  {
    value: 'likeB',
    label: '>%'
  },
  {
    value: 'in',
    label: 'in'
  },
  {
    value: 'between',
    label: 'T+1'
  }
];
```

## BindValue 数据类型转换内容

提交数据时，根据数据类型进行转换内容

```js
/**
 * 提交数据时，根据数据类型进行转换内容
 * @param {*} val 默认值
 * @param {*} _attr 数据规则
 */
const BindValue = (val, _attr);

import { BindValue } from 'qve';
```

## BindControlValue 读取自定义控件值

```js
/**
 * 读取自定义控件值
 * @param {*} _value 内容
 * @param {*} _class 自定义控件
 * @returns
 */
const BindControlValue = (_value, _class);
```

## BindCodeValue 内容编码

```js
/**
 * 内容编码
 * @param {*} _value 原文内容
 * @param {*} _code 是否编码
 * @returns
 */
const BindCodeValue = (_value, _code);
```

## EditDisplay 编辑状态值的显示处理

```js
/**
 * 编辑状态值的显示处理
 * @param {*} val
 * @param {*} _attr
 * @returns
 */
const EditDisplay = (val, _attr);
```

## ReadDisplay 值转换 html 显示

```js
/**
 * 值转换html显示
 * @param {*} val 值内容
 * @param {*} _attr 数据规则
 */
const ReadDisplay = (val, _attr);
```

## ReadControlValue 读取自定义控件值

```js
/**
 * 读取自定义控件值
 * @param {*} _value 内容
 * @param {*} _class 自定义控件
 * @returns
 */
const ReadControlValue = (_value, _class);
```

## ReadCodeValue 读取内容并解码

```js
/**
 * 读取内容并解码
 * @param {*} _value
 * @param {*} _code
 * @returns
 */
const ReadCodeValue = (_value, _code);
```

## getJoinPrefix 生成外键查询组合字段前缀

```js
/**
 * 生成外挂前缀字段
 * @param {*} key 主建
 * @param {*} field 外键表显示字段名
 * @returns t.主建.外键表字段名
 */
const getJoinPrefix = (key, field);
```

## getJoinPrefixReg 分割外键查询

```js
/**
 * 分割外键查询t.索引名.外键字段名
 * @param {*} val 字段名称
 * @returns  Array[t,外键字段名,外键表显示字段名]
 */
const getJoinPrefixReg = val;
```

```js
/**
 * 取出审核对象
 * @param {*} key 1:正常,2:已审,3:撤审,4:删除
 */
const getSafeFlag = (key) => {};
```

```js
/**
 * 取出审核输出html
 * @param {*} key 1:正常,2:已审,3:撤审,4:删除
 */
const getSafeFlagHtml = (key) => {};
```

```js
/**
 * 模板规则转换绑定
 * @param {*} _ar 字段规则定义
 */
const bindAttrRlus = (_ar) {};
```

```js
/**
 * 规则多选列表,取出对应的描述
 * @param {*} _ls 多选列表  attr.ls,
 * @param {*} val 内容
 */
const getAttrListValue = (_ls, val);
```

```js
/**
 * 数据库选项定义参数 ls 格式转为 json
 * @param {*} _ls
 * @returns
 */
const getAttrListToJson = _ls;
```

```js
/**
 * 根据模板规则转换数据内容
 * @param {*} _attr 数据规则
 * @param {*} val 输入内容
 */
const getAttrValue = (_attr, val);
```

```js
/**
 * 初始化字段默认值
 * @param {*} data 传入初始内容
 * @param {*} field 字段定义
 * @param {*} attr 字段规则
 * @returns
 */
const getAttrInitValue = (data, field, attr);
```

```js
/**
 * 返回查询对象参数
 * @param {*} select {field,word,cate}
 * @param {*} attr_type attr.t
 * @returns
 */
const getSearchItem = (select, attr_type);
```

```js
/**
 * 查询参数
 * @param {*} pager 分页参数
 * @param {*} select 查找条件
 * @param {*} attr 字段属性
 */
const getSearchPars = (pager, select, attr);
```

```js
/**
 * 数据属性转为tpl字段说明
 * @param {*} _attr 字段属性定义行格式
 * @returns
 */
const toFieldTplByColumn = _attr;
```

```js
/**
 * 转为数库默认存储值格式
 * @param {*} val 内容
 * @param {*} type 数据类型,小写
 */
const getValueByType = (val, type);
```

```js
/**
 * 生成表头
 * @param {*} _key 主键
 * @param {*} _value 内容
 * @param {*} _attr 属性
 */
const bindTh = (_key, _value, _attr);
```

```js
/**
 * 绑定表头,来自模板定义
 * @param {Array} cut the.row.cut 指定不显示字段[]
 * @param {Array} _th the.tpl.listor.th 指定不显示字段[]
 * @param {object} _field the.tpl.field 字段[]
 * @param {object} attr the.tpl.attr 字段规则
 * @param {string} _primary the.tpl.keys.primary 勾选值主键
 * @param {boolean} _isCheck the.show.check 列表是否显示勾选
 */
const bindThByTpl = (_cut, _th, _field, attr, _primary, _isCheck);
```
