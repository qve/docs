# PanelListor

## PanelListor 外键查询面板

基于 [Listor](./listor.md) 组件，参数配置一致

<CodeRun editable>

```vue
<template>
  <div>
    <Input
      v-model="the.App_ID"
      named="App_ID"
      placeholder="组件内调用"
      type="number"
      readonly
      icon="icon-jiantouxia"
      @onEvent="onInputEvent"
    />

    <Input
      v-model="the.User_ID"
      named="User_ID"
      placeholder="全局对话框调用"
      type="number"
      readonly
      icon="icon-jiantouxia"
      @onEvent="onInputEvent"
    />

    <Modal :show="the.show.modal" footer-hide>
      <PanelListor
        :named="the.panel.named"
        :config="the.panel.config"
        :list="the.panel.list"
        @onEvent="onPanelEvent"
      />
    </Modal>
  </div>
</template>

<script>
export default {
  setup() {
    const { confirm, message } = window.$plus;
    const { reactive } = window.$plus.vue;

    const the = reactive({
      show: {
        modal: false
      },
      App_ID: 1,
      User_ID: 2,
      panel: {
        named: 'App_ID',
        // 数据请求源配置
        config: {
          http: {
            // 请求地址
            url: '',
            // 请求参数
            data: {
              // 外挂面板
              tpl: 6
            }
          },
          field: {
            ID: 'ID',
            Title: '应用'
          }
        },
        list: [
          { ID: 1, Title: '应用1' },
          { ID: 2, Title: '应用1' },
          { ID: 3, Title: '应用1' }
        ]
      }
    });

    const openPanel = () => {
      // 请求服务器端参数
      let _config = {
        http: {
          // 请求地址
          url: '/',
          // 请求参数
          data: {
            // 直接请求, 同数据源，tpl:6,par:'Group'
            tpl: 1
          }
        }
      };

      // 全局对话框
      confirm({
        footerHide: true,
        title: '查询',
        model: {
          props: {
            // 传入配置参数
            //config: _config
            config: the.panel.config,
            list: the.panel.list
          },
          component: 'PanelListor' //绑定组件参数
        },
        onEvent: (resp) => {
          console.log('openPanel', resp);
          switch (resp.cmd) {
            case 'check':
              // 回调处理选择的数据选项
              // console.log('onPanelTreeEvent', JSON.stringify(resp));
              the.User_ID = resp.value;
              break;
            case 'close':
              // 关闭窗口
              resp.close && resp.close();
              break;
          }
        }
      });
    };

    const onInputEvent = (resp) => {
      // console.log('onInputEvent', JSON.stringify(resp));
      switch (resp.cmd) {
        case 'InputPlus':
          switch (resp.named) {
            case 'App_ID':
              // 显示面板
              the.show.modal = true;
              break;
            case 'User_ID':
              openPanel(resp.named);
              break;
          }
          break;
      }
    };

    const onPanelEvent = (resp) => {
      // console.log('onControlEvent', JSON.stringify(resp));
      message.info(JSON.stringify(resp));
    };

    return { the, onInputEvent, onPanelEvent };
  }
};
</script>
```

</CodeRun>
