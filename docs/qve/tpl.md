# TPL 数据组件模板

前端组件配置参数

## tpl 组件参数

```json
{
  //  是否从服务器取模板查询参数
  "isHttp": false,
  "tpl": {
    //字段集合
    "sql": "",
    "field": {
      "ID": "ID",
      "Code": "索引编码",
      "App_ID": "应用ID",
      "PageType": "类别",
      "State": "页面状态",
      "Path": "请求路径",
      "Title": "页面标题",
      "Description": "功能说明",
      "MenuTitle": "菜单标题",
      "MenuIcon": "菜单图标",
      "MenuOpen": "菜单打开模式",
      "CodeLang": "代码语言",
      "CodeVersion": "代码版本",
      "CodeTime": "代码更新",
      "CodeSource": "源代码",
      "CodeCompile": "编译代码",
      "SF_ID": "编辑状态",
      "MUser_ID": "编辑者",
      "U_Time": "编辑时间"
    },
    // 字段属性集合
    "attr": {
      "ID": {
        "t": "Int64",
        "pk": 1,
        "r": 1
      },
      "Code": {
        "t": "String",
        "n": true,
        "v": "0",
        "max": 50
      },
      "App_ID": {
        "t": "Int32",
        "n": true,
        "v": "1",
        "c": "panel",
        "api": "/Api/QF_Group/"
      },
      "PageType": {
        "t": "Int32",
        "n": true,
        "v": "1",
        "ls": "1:<s>api接口</s>,2:menu菜单,3:vue组件,5:html"
      },
      "State": {
        "t": "Boolean",
        "n": true,
        "v": 0,
        // bool 类型按以下格式配置
        "ls": "<em>停用</em>:启用"
      },
      "Path": {
        "t": "String",
        "max": 2147483647,
        "tip": "请求时进行权限验证"
      },
      "Title": {
        "t": "String",
        "n": true,
        "max": 200
      },
      "Description": {
        "t": "String",
        "max": 1000,
        "w": 500
      },
      "MenuTitle": {
        "t": "String",
        "max": 50
      },
      "MenuIcon": {
        "t": "String",
        "max": 50
      },
      "MenuOpen": {
        "t": "String",
        "max": 200
      },
      "CodeLang": {
        "t": "String",
        "n": true,
        "v": "vue",
        "max": 50
      },
      "CodeVersion": {
        "t": "String",
        "n": true,
        "v": "0.0.1",
        "max": 50
      },
      "CodeTime": {
        "t": "DateTime",
        "n": true,
        "v": "getdate()"
      },
      "CodeSource": {
        "t": "String",
        "g": "encode",
        "max": 2147483647,
        "h": 1,
        "c": "code"
      },
      "CodeCompile": {
        "t": "String",
        "max": 2147483647,
        "h": 1
      },
      "SF_ID": {
        "t": "Int32",
        "n": true,
        "v": "1",
        "r": 1,
        "e": 1,
        "a": 1
      },
      "MUser_ID": {
        "t": "Int64",
        "n": true,
        "v": "1",
        "r": 1,
        "e": 1
      },
      "U_Time": {
        "t": "DateTime",
        "n": true,
        "v": "getdate()",
        "r": 1,
        "e": 1
      }
    },
    // 前端自定义外键查询
    "join": null,
    //join: {"MUser_ID":{"table":"QF_User","title":"LastFirst"}}
    // join: [
    //   {
    //     tField: 'QF_Group_ID',
    //     cTable: 'AP10000DB.[dbo].[QF_Group]',
    //     cTitle: 'NameShort'
    //   }
    // ],
    // 按钮权限参数
    "act": "1,2,3,4,5,6,7",
    // 自定义 默认字段名
    "keys": {
      // 主键
      "primary": "ID",
      // 审核字段 a
      "auth": "SF_ID",
      // 编辑者
      "user": "MUser_ID",
      // 编辑时间 默认字段名
      "time": "U_Time"
    },
    // 自定义排序
    "sort": null,
    "th": null,
    // 列表界面配置
    "listor": null,
    // 编辑窗体配置
    "editor": null
  }
}
```

## tpl.attr 组件规则

| 属性名    | 说明                                                                                     | 类型    | 列表值 | 编辑值  |
| --------- | ---------------------------------------------------------------------------------------- | ------- | ------ | ------- |
| h         | 控件表头字段不显示,1                                                                     | Number  | -      |         |
| a         | 标记字段为审核字段,                                                                      | Number  |        |         |
| e         | 列表扩展显示,1                                                                           | Number  |        |         |
| r         | 只读,1                                                                                   | Number  |        |         |
| n         | 必填字段,非空格                                                                          | Boolean |        |         |
| v         | 新增定义默认值                                                                           | String  |        |         |
| t         | 数据库字段类型                                                                           | String  |        |         |
| ls        | 多选项 `0.3.8`支持`html` 标签内容`<s>停用</s>:启用`                                      | String  |        |         |
| tip       | 内容提示                                                                                 | Number  |        | ？      |
| max       | 输入内容最大值                                                                           | Number  |        |         |
| api       | 自定义外键查询面板数据接口，默认为空使用 `listor` 请求路径,后端参数配置                  | String  |        | c:panel |
| c         | 自定义编辑控件命名如 code:代码编辑,panel:外键面板,file:上传控件                          | String  |        |         |
| cp        | 自定义控件 props 参数，非标 json 格式,具体参考各实际参数                                 | String  |        |         |
| g         | 自定义内容编码格式：`encode`,`base64`                                                    | String  |        |         |
| w         | 列表宽度                                                                                 | Number  |        |         |
| pk        | 配置为全表唯一主键，值为 1 点击触发打开编辑页面                                          | Number  | 1      |         |
| k `0.4.3` | 自定义点击触发的事件名，默认为字段名                                                     | String  | listor |         |
| j `0.4.4` | 外键映射取值的字段名,不配置:匹配`fields`的第一个字段，或配置为`当前字段名`不显示外键内容 | String  | listor |         |

- 停用属性

| 属性名 | 说明                                            | 类型   | 列表控件 | 编辑控件 |
| ------ | ----------------------------------------------- | ------ | -------- | -------- |
| f      | 自定义数据库字段名，`停用` `field`,由后台配置   | String | -        | -        |
| d      | 自定义数据库字段名，`停用` 由后台配置自由取字段 | Number | -        | -        |

## attr.g 内容编码

自定义内容编码格式，`encode`

- encode 使用 URL 编码方式 encodeURI
- base64 `0.6.5` 支持编码方式

## attr.k eventKey 事件触发

点击触发事件名，用于接收事件 cmd 响应

- 配置示例

```js
// 以下示例，j:指定关联外键字段
const tpl = {
  // 字段属性定义触发事件
  attr: {
    App_ID: {
      // 定义触发事件名
      k: 'App_ID'
    },
    User_ID: {
      // 定义触发事件名
      k: 'userClick'
    }
  },
  // 列表配置
  listor: {
    // 新增自定义按钮
    btns: {
      // 按钮事件名
      btnCacheDel: {
        title: '清除数据对象配置缓存',
        type: 'info'
      }
    },
    // 监听事件触发
    events: {
      // 路由跳转
      userClick:
        "(res)=>{const {router} = window.$plus;router.push({'path':'/DB/AP_User/','query':{'title':'AP:'+res.data.ID}});}",
      // 按钮触发事件执行的代码
      btnCacheDel: `(resp) => { 
        console.log('btnCacheDel', resp.table.checkeds); 
        const { http,message } = window.$plus; 
        for (let _ck in resp.table.checkeds) { 
          let _index = resp.table.checkeds[_ck]; 
          let _code = resp.table.data[_index].Code; 
          if (_code) { 
            http({ url: '/Caches/DB/', data: { code: _code }}).then(res => { 
              let _msg=res.code==1?'缓存已清除，请重新打开':res.msg; 
               message.info(_msg); })
            }
        }}`,
      // 事件名不能重复，tpl远程下发配置传入是 function 函数代码，本地配置直接为函数
      App_ID: `  
        (resp) => {
          console.log('td.App_ID', resp);
          console.log('传入的参数集合：', arguments);

          //  console.log('td.App_ID', resp);
          const { message } = window.$plus.ui;
          message.info(resp.data.ID);

        } `
    }
  }
};
```

- 以下为内部参数处理

```js
let _th = {
  title: _value,
  key: _key
  //  resizable: true,
  //  width: the.row.width,
};

if (_attr.k) {
  // 点击触发事件名，用于接收事件cmd响应
  _th.eventKey = _attr.k;
}

// 主建 the.tpl.keys.primary
if (_attr.pk) {
  _th.width = _th.width || 50;
  _th.className = 'qv-table-index';
  _th.eventKey = _key;
  // 主建
  _th.primary = true;
  // console.log(key, _attr);
}
```

## attr.j 索引外键字段

- 外键查询数据返回字段规则

t.索引键.外键显示字段名

- 同表外键多个查询返回值，需要指定外键显示字段，不指定默认匹配第一个值

```js
// 以下示例，j:指定关联外键字段
const tpl = {
  // 属性规则
  attr: {
    App_ID: {
      t: 'Int32',
      n: true,
      v: '1',
      c: 'panel',
      api: '/DI/QF_Group/', //默认不配置，由后端参数配置
      j: 'Title' //默认不需配置，由后端参数配置显示的关联外键字段
    },
    MUser_ID: {
      t: 'Int64',
      n: true,
      v: '1',
      r: 1,
      e: 1
    }
  }
};

// 外键查询返回的数据结构
const list = [
  {
    ID: 5,
    App_ID: 1,
    Title: '客户关系管理',
    SF_ID: 2,
    MUser_ID: 2,
    U_Time: '2012-05-06 10:23:30',
    't.MUser_ID.LastFirst': '架构管理员',
    't.App_ID.Title': '初始化',
    't.App_ID.AppKey': null
  }
];
```

## join 外键查询

- QF_Model 数据对象的外键配置如下：
  listor

```js
const join = {
  // 外键字段名，table:关联表名QF_Model的定义编码，fields 外键表显示字段, 默认空就取最后一个字段 Title
  QF_User_ID: { table: 'QF_User', fields: 'LastFirst' },
  QF_Group_ID: { table: 'QF_Group', fields: 'NameShort' },
  MUser_ID: {
    table: 'QF_UserContact',
    primary: 'QF_User_ID',
    fields: 'NickName'
  },
  // 主表外键关联字段
  LoginCode: {
    // 需要跨服务器查询
    server: true,
    // 外键表QF_Model的定义编码
    table: 'QF_UserPhoneJoin',
    // 外键表关联字段
    primary: 'Phone',
    // 外键表显示字段
    fields: 'Department,NickName'
  },
  QF_AppInfo_ID: {
    table: 'QF_AppInfo',
    fields: 'AppKey,AppWeb,Title',
    primary: 'ID' //可自定义外键表关联字段
  },
  QF_Page_ID: {
    table: 'QF_Page',
    fields: 'Title,Path,State'
  }
};
```

## attr.c 组件传参 cp

| 控件名 | 说明                                           | 编辑控件    |
| ------ | ---------------------------------------------- | ----------- |
| panel  | 外键查询                                       | PanelListor |
| code   | 代码编辑组件                                   | CodeEditor  |
| tree   | 树状查询                                       | PanelTree   |
| ubb    | ubb 组件 format:text 支持 ASCII 码格式转为 ubb | PanelTree   |

- 外键查询面板

```js
const tpl = {
  // 服务器配置，无需下发前端
  attr: {
    App_ID: {
      t: 'Int32',
      n: true,
      v: '1',
      c: 'panel' //定义外键查询面板
      // api: '/Api/QF_Group/', // 自定义数据源，默认为空，取当前表格数据源
    },
    CodeSource: {
      t: 'String',
      max: 2147483647,
      h: 1,
      g: 'encode', // 编码解码格式
      c: 'code' //代码编辑器
      cp: 'lang:\'sql\'' //代码编辑器的语言参数为sql
    },
    Body: {
      "t": "String",
      "n": true,
      "max": 2147483647,
      "h": 1,
      "c": "ubb", // ubb 组件支持accii
      "cp": "format:\"text\""
    },
  }
};
```

## attr.c.tree 树面板

- 支持树节点展示

```json
{
  // 服务器配置，无需下发前端
  attr: {
    QF_Book_ID: {
      t: 'Int32',
      n: true,
      v: '1',
      c: 'tree' //定义Tree 面板
      cp: 'tpl:4,index:4,sort:\'4.10.1.1\'', // 节点参数
    }
  }
}

```

## tpl.listor.paged 分页

```js
const tpl = {
  // 列表配置
  listor: {
    /** 查询分页参数 */
    paged: {
      // 当前页码
      index: 1,
      // 每页取数据条数
      size: 5,
      // 分页数
      sizes: [5, 10, 20, 30, 50, 100],
      /** 总条数 */
      count: 0
    }
  }
};
```
