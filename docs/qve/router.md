# Router 路由

- [vue 3.x 路由](../guide/vue/start.md)
- [vue 路由官方文档](https://router.vuejs.org/zh/guide/)

## Router 路由 $plus

```js
// 外挂写法
const { router } = window.$plus;

// 字符串路径
router.push('/users/eduardo');

// 带有路径的对象
router.push({ path: '/users/eduardo' });

// 命名的路由，并加上参数，让路由建立 url
router.push({ name: 'user', params: { username: 'eduardo' } });

// 带查询参数，结果是 /register?plan=private
router.push({ path: '/register', query: { plan: 'private' } });

// 带 hash，结果是 /about#team
router.push({ path: '/about', hash: '#team' });

// 跳转地址，并传入query参数
router.push({
  path: '跳转地址',
  query: {
    title: '标签名称'
  }
});
```
