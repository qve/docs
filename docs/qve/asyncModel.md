# AsyncModel

动态根据 vue 代码创建组件

## AsyncModel 异步组件

```vue
<!-- 动态组件 -->
<template>
  <AsyncModel
    :url="the.url"
    :body="the.body"
    :cached="the.isCache"
    :query="query"
    @onEvent="asyncEvent"
  />
</template>

<script>
export default {
  props: {
    /** 连接地址 */
    url: {
      type: String,
      // 指定默认的值
      default: null
    },
    /** 传入参数 */
    query: {
      type: Object
    }
  },
  setup(props, { emit }) {
    // 全局引用
    const { reactive } = window.$plus.vue;
    const { router } = window.$plus;
    const _router = router.currentRoute.value;
    const the = reactive({
      url: props.url || _router.path,
      body: '',
      isCache: false
    });

    return { the };
  }
};
</script>
```

### AsyncModel 参数

| 属性   | 说明                  | 类型    | 默认值         |
| ------ | --------------------- | ------- | -------------- |
| url    | 代码连接地址          | String  |                |
| body   | 代码内容              | String  | 服务器默认编码 |
| query  | 组件传入参数          | Object  |                |
| cached | 组件代码是否缓存本地  | Boolean | false          |
| decode | 代码内容解码,默认转码 | Boolean | true           |

## AsyncPage 动态页面

- 路由请求路径：`/Page/Quick/Main/`
  从后端读取页面代码并编译执行

```vue
<!-- 远程页面 -->
<template>
  <AsyncPage :url="the.url" :query="the.query" @onEvent="asyncEvent" />
</template>

<script>
import { reactive } from '@/components/vue.api.js';
import router from '@/router/index.js';

export default {
  setup() {
    // 当前路由请求
    const _router = router.currentRoute.value;
    // console.log('Page', _router.path)
    const the = reactive({
      url: _router.path,
      query: _router.query || null
    });

    /** 页面事件 */
    const asyncEvent = (resp) => {
      console.log('Page:' + the.url, resp);
      // emit('onEvent', resp)
    };

    return { the, asyncEvent };
  }
};
</script>
```

## AsyncPage props

| 属性  | 说明         | 类型   | 默认值 |
| ----- | ------------ | ------ | ------ |
| url   | 代码连接地址 | String |        |
| query | 组件传入参数 | Object |        |
