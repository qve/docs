# 响应式数据列表

- 根据参数配置动态创建响应式查询列表
- [请参考 tpl 参数配置](./tpl.md)

## Listor 响应式数据列表

- 服务器端配置参数 `PagerTplModel`

<CodeRun auto editable>

```vue
<!-- 列表组件 -->
<template>
  <div class="test-list-page">
    <Listor :config="the.config" @onEvent="onListorEvent">
      <template #more>
        <ButtonGroup>
          <Input v-model="the.data.text" />
          <Button type="info" @click="btnEvent('pwd')">演示扩展插槽</Button>
        </ButtonGroup>
      </template>
      <dl class="qf-06">
        <dt>提示：</dt>
        <dd>点击编号ID进入编辑</dd>
      </dl>
    </Listor>
    <div>试试修改以下配置参数更新列表</div>
    <TextJson
      v-model="the.config"
      class="test-json"
      rows="10"
      @onEvent="onEvent"
    />
  </div>
</template>

<script>
export default {
  setup(props, context) {
    const { message } = window.$plus;
    const { reactive } = window.$plus.vue;

    // 模板配置参数
    const _tpl = {
      field: {
        ID: 'ID',
        Code: '索引编码',
        App_ID: '应用ID',
        PageType: '类别',
        State: '页面状态',
        Path: '请求路径',
        Title: '页面标题',
        Description: '功能说明',
        MenuTitle: '菜单标题',
        MenuIcon: '菜单图标',
        MenuOpen: '菜单打开模式',
        CodeLang: '代码语言',
        CodeVersion: '代码版本',
        CodeTime: '代码更新',
        CodeSource: '源代码',
        CodeCompile: '编译代码',
        SF_ID: '编辑状态',
        MUser_ID: '编辑者',
        U_Time: '编辑时间'
      },
      attr: {
        ID: {
          t: 'Int64',
          pk: 1,
          r: 1
        },
        Code: {
          t: 'String',
          n: true,
          v: '0',
          max: 50
        },
        App_ID: {
          t: 'Int32',
          n: true,
          v: '1',
          j: 1, //外键查询
          c: 'panel' // 调用的面板
        },
        PageType: {
          t: 'Int32',
          n: true,
          v: '1',
          ls: '1:api接口,2:menu菜单,3:vue组件,5:html'
        },
        State: {
          t: 'Int32',
          n: true,
          v: '1',
          ls: '1:暂停,2:启用,3:停用,4:删除'
        },
        Path: {
          t: 'String',
          max: 2147483647,
          tip: '请求时进行权限验证',
          w: 200
        },
        Title: {
          t: 'String',
          n: true,
          max: 200,
          w: 120
        },
        Description: {
          t: 'String',
          max: 1000
        },
        MenuTitle: {
          t: 'String',
          max: 50,
          d: 1
        },
        MenuIcon: {
          t: 'String',
          max: 50
        },
        MenuOpen: {
          t: 'String',
          max: 200
        },
        CodeLang: {
          t: 'String',
          n: true,
          v: 'vue',
          max: 50
        },
        CodeVersion: {
          t: 'String',
          n: true,
          v: '0.0.1',
          max: 50
        },
        CodeTime: {
          t: 'DateTime',
          n: true,
          v: 'getdate()'
        },
        CodeSource: {
          t: 'String',
          max: 2147483647,
          g: 'encode',
          h: 1,
          c: 'code'
        },
        CodeCompile: {
          t: 'String',
          max: 2147483647,
          g: 'encode',
          h: 1
        },
        SF_ID: {
          t: 'Int32',
          n: true,
          v: '1',
          r: 1,
          e: 1,
          a: 1
        },
        MUser_ID: {
          t: 'Int64',
          n: true,
          v: '1',
          r: 1,
          e: 1
        },
        U_Time: {
          t: 'DateTime',
          n: true,
          v: 'getdate()',
          r: 1,
          e: 1
        }
      },
      /** 数据与按钮权限*/
      act: '1,2,3,4,5,6,7,8,9,10,16',
      keys: {
        primary: 'ID',
        auth: 'SF_ID'
      },
      /** 响应编辑配置参数 */
      editor: null,
      /** 响应列表配置参数 */
      listor: {
        show: {
          // 2新增
          btnNew: true,
          // 4删除
          btnDel: false,
          // 6审核
          btnAudit: false,
          // 7 超级撤审
          btnRevoke: false,
          // 8擦除
          btnErase: false,
          // 多个条件查询框状态
          dialog: false,
          /* 表格是否允许勾选*/
          check: true,
          // 是否显示编辑器
          editor: false
        }
      }
    };

    /** 组件参数 */
    const the = reactive({
      data: {
        // 列表勾选的数据
        list: [],
        // 文本内容
        text: ''
      },
      config: {
        // 查询数据源, 默认true 取服务器
        isHttp: false,
        // 数据请求源配置
        http: {
          // 请求地址
          url: '',
          // 请求参数
          data: {
            tpl: 1
          }
        },
        // 自定义界面
        tpl: _tpl
      }
    });

    /**
     * 列表编辑器事件监听
     */
    const onListorEvent = (resp) => {
      // console.log('onListorEvent', resp);
      switch (resp.cmd) {
        case 'check':
          //更新触发勾选列表
          the.data.list = resp.list;
          //   console.log('onListorEvent', the.listor.login.more);
          break;
      }
    };

    const onEvent = (resp) => {
      switch (resp.cmd) {
        case 'jsonUpdate':
          // 更新表格传入参数
          // 深度拷贝创建新的内存对象，才会更新生效
          // let _config = JSON.parse(JSON.stringify(the.config));
          the.config = resp.data;
          break;
      }
      console.log('onEvent', resp);
    };

    const btnEvent = (cmd) => {
      switch (cmd) {
        case 'pwd':
          //console.log('修改密码', the.data);
          message.info('修改密码为' + the.data.text, { text: the.data.list });
          break;
      }
    };

    return { the, onListorEvent, onEvent, btnEvent };
  }
};
</script>
<style lang="less">
.test-list-page {
  margin: 1rem;
  .test-json {
    width: 70%;
  }
}
</style>
```

</CodeRun>

## Listor props

| 属性   | 说明             | 类型   | 默认值 |
| ------ | ---------------- | ------ | ------ |
| named  | 组件命名         | string | listor |
| config | tpl 配置模板     | Object |        |
| list   | 需表格绑定的数据 | Array  |        |

## Listor slot 插槽

| 名称  | 插入位置     |
| ----- | ------------ |
| -     | 主体内容     |
| tools | 工具栏       |
| more  | 扩展选项底部 |

## Listor onEvent 事件

| 事件名  | 说明             | 返回值 |
| ------- | ---------------- | ------ |
| onEvent | 点击触发回调事件 | json   |

### check 点击表格

- 点击表格数据触发

  | 参数名   | 说明                                       | 类型          | 返回值 |
  | -------- | ------------------------------------------ | ------------- | ------ |
  | cmd      | 指令                                       | string        | check  |
  | named    | 组件命名                                   | string        | listor |
  | id       | `dom` 随机动态名                           | string        |        |
  | index    | 当前行的索引                               | Number        |        |
  | checked  | 是否选中                                   | Boolean       |        |
  | checkeds | 返回所有勾选序号数组                       | Array         |        |
  | value    | 当前勾选主键定义值                         | string,Number |        |
  | list     | 当前勾选主键定义值数组 `tpl.keys.primary ` | Array         |        |
  | data     | 当前勾选的行数据                           | Object        |        |

- 输出的交互数据

  ```json
  {
    "cmd": "check",
    "named": "check_2",
    "id": "check_2_cMYk5LFr",
    "index": 2,
    "checked": true,
    "checkeds": [2],
    "value": 554025,
    "list": [554025],
    "data": {}
  }
  ```

## tpl.listor 配置

参考以下

- 后端配置参数
- onEvent res 输出取值

```js
return {
  /** 数据源请求配置*/
  http: {
    /** 请求地址 */
    url: '',
    /** 请求参数 */
    data: {
      /** 查询模板类型 1:接口属性,2:只查询数据，不返回属性,其它自定义 */
      tpl: 1,
      /** 查询条件 */
      where: [],
      /** 查询扩展参数 */
      par: null,
      /** 自定义查询字段 */
      field: null,
      /** 扩展外键查询 */
      join: null
      // join: [
      //   {
      //     tField: 'QF_Group_ID',
      //     cTable: 'AP10000DB.[dbo].[QF_Group]',
      //     cTitle: 'NameShort'
      //   }
      // ],
    }
  },
  /** 来自服务器的数据模板*/
  tpl: {},
  /** 查询分页参数 */
  paged: {
    // 当前页码
    index: 1,
    // 每页取数据条数
    size: 5,
    // 分页数
    sizes: [5, 10, 20, 30, 50, 100],
    /** 总条数 */
    count: 0
    // 是否小型分页
    // small: true,
    // 最大页码按钮数
    // max: 4,
    // 页码按钮
    // layout: 'count, sizes, prev, pager, next, jumper',
  },
  /** 查询条件符号*/
  options: modeSymbol,
  /** 查询组合配置*/
  select: {
    /** 输入的查询内容 */
    value: null,
    /** 查询排序的字段，默认是ID */
    sort: ['ID'],
    /**  查询模式：降序还是升序*/
    desc: true,
    /** 选择的查询条件符号 */
    mode: '=',
    /** 是否开启多条件查询 */
    mode_more: false,
    /** 指定默认查询字段 */
    field: 'ID',
    /** 自定义查询的字段配置,无配置默认取tpl.field */
    terms: {},
    /** 锁定不可修改的查询字段，查询不显示 */
    lock: [],
    /** 传入查询组合条件 */
    where: []
  },
  /* 控件是否显示*/
  show: {
    // 显示工具栏
    toolbar: true,
    // 显示更多按钮
    btnMore: true,
    /* 表格是否允许勾选*/
    check: true,
    // 是否显示编辑器
    editor: true,
    // 分页栏
    paged: true,
    // 更多选项框
    dialog: false
  },
  // 表格行级参数
  row: {
    /** 锁定显示的列, 多个用逗号隔开 */
    lock: '',
    /** 自定义显示列，多个逗号隔开 */
    th: [],
    /* 不显示的数据列*/
    cut: [],
    // 默认列宽
    width: 120,
    // 行数据绑定字段
    field: {}
  },
  /** 表格 */
  table: {
    // 标题定义
    columns: [],
    // 表格数据
    data: [],
    /** 合计字段 */
    sumField: [],
    /** 表格底部行 */
    foots: [],
    /** 跨页所有勾选缓存的主键 */
    checkList: [],
    /** 当前页勾选序号 */
    checkeds: []
  },
  // 当前编辑对象
  edit: {
    //缓存当前编辑数据，用于编辑的内容进行对比
    obj: {}
  },
  /** 对话框 */
  modal: {
    title: '请勾选',
    show: false
  },
  /** 面板 */
  panel: {
    /** 返回绑定的主键名 */
    named: '',
    http: {
      /** 数据源地址 */
      url: '',
      /** 数据源请求参数 */
      data: {
        tpl: 6
      }
    }
  },
  /** 基础功能按钮 */
  btnGroupSafe: {},
  /** 自定义扩展按钮 */
  btnGroupMore: {}
};
```

### width 列宽度

- tpl.listor 所有列自定义平均宽度

```js
let listor = {
  // 表格列自定义平均宽度
  row: {
    width: 100
  }
};
```

- tpl 字段属性可设置单列宽度

```js
let tpl = {
  // 属性参数
  attr: {
    // 自定义字段属性
    title: {
      w: 500
    }
  }
};
```

### listor.select 查询配置

- `0.3.4` 自定义默认查询

  - `cate` 改名为 `mode` 跟后端同名
  - `word` 改名为 `value` 跟后端同名

  ```json
  {
    "select": {
      //字段名
      "field": "Code",
      // 查询条件
      "mode": "like",
      // 默认查询值
      "value": "1"
    }
  }
  ```

### listor.select.where 查询

- `0.4.1` 支持初始化 tpl 配置或者 url 传入 where 条件

  ```json
  {
    // 字段属性
    "attr": {
      "OnTime": {
        // 默认值
        "v": "now",
        // 类型
        "t": "datetime"
      }
    },
    "listor": {
      "select": {
        // 默认开启多条件查询
        "mode_more": true,
        // 默认查询
        "field": "AP_NAS_ID",
        // 自定义查询参数
        "terms": {
          "AP_NAS_ID": "网关ID",
          "OnTime": "上网开始时间"
        },
        // 自定义查询条件，设置后field失效
        "where": [
          {
            // 查询值为空，将会取attr字段属性，返回默认值
            "OnTime": null
          }
        ]
      },
      "show": {
        // 关闭分页
        "paged": false
      }
    }
  }
  ```

### lock 自定义查询锁定

- `1.0.9` 项目运行中，经常会根据不通的权限，锁定前端查询的条件。
- 注意服务器交互接口验证需另外参阅 `ORM` 配置
- `terms`与`lock` 建议不重复配置

```js
/** 列表编辑 */
let listor = {
  /* 查询组合*/
  select: {
    /** 自定义查询的条件配置,无配置默认取tpl.field */
    terms: { gid: '组织ID', start: '开始时间' },
    /** 需锁定不可修改的查询字段 */
    lock: ['QF_Group_ID'],
    // 组合查询, 条件q必须放最后,无此参数默认为'='
    where: [{ QF_Group_ID: 256, q: '<' }, { start: '2022/8/1' }]
  }
};
```

### 列显示于底部求和

```js
/** 列表编辑 */
return {
  row: {
    // 指定显示的列
    th: ['ID', 'QFGroupID', 'QFUserID', 'GiftConfId', 'Account']
  },
  table: {
    // 自定义底部,优先于sum
    foots: [
      {
        ID: '合', //列描述
        Code: (data) => {
          //自定义计算方式
          let value = 0;
          data.map((item) => {
            value += parseInt(item.Code);
          });
          //求和
          return value;
        }
      }
    ],
    // 传入合计列, 无需自定义计算方式
    sum: ['Account', 'Code']
  }
};
```

### Listor http 查询

```vue
<!-- 列表组件 -->
<template>
  <div class="test-list-page">
    <Listor :config="the.config.role" />
  </div>
</template>

<script>
export default {
  setup(props, context) {
    const the = reactive({
      config: {
        role: {
          http: {
            // 请求地址
            url: '/DB/QF_RoleRules',
            // c# 后端Get请求参数，具体参考后端参数需求 tpl.cp
            data: {
              tpl: 1,
              par: '传入自定义参数',
              size: 5 //分页
            }
          },
          tpl: {
            /*查询组合*/
            listor: {
              select: {
                /** 初始查询字段 */
                field: 'QF_Role_ID',
                /** 初始查询内容 */
                value: 5
              }
            }
          }
        }
      }
    });
  }
};
</script>
```

### 外键显示

- listor 外键关系配置

```json
{
  "MUser_ID": { "table": "QF_User", "fields": "LastFirst" },
  "LoginCode": {
    "table": "QF_UserPhoneJoin",
    "primary": "Phone",
    "fields": "Department,NickName"
  }
}
```

- 外键列表显示配置

```js
let tpl = {
  // 显示字段
  field: {
    ID: 'ID',
    LoginCode: '登陆账户',
    // 自定义显示的字段与显示名称
    't.LoginCode.Department': '部门'
  },
  // 字段属性
  attr: {
    ID: {
      t: 'Int64',
      pk: 1,
      r: 1
    },
    LoginCode: {
      t: 'String',
      n: true,
      max: 50,
      r: 1,
      // 默认不配置，将用外键查询最后字段内容进行替换显示， 设置为LoginCode，不进行外键替换显示
      j: 'NickName',
      tip: '默认会员手机号码'
    }
  },
  // 列表参数配置
  listor: {
    row: {
      // 自定义显示字段列表
      th: ['ID', 'LoginCode', 't.LoginCode.NickName', 't.LoginCode.Department']
    },
    // 查询配置
    select: {
      lock: ['QF_Group_ID'],
      where: [
        {
          QF_Group_ID: 256
        }
      ]
    }
  }
};
```

## Listor 内置事件按钮

- 基础功能按钮事件，可以直接调用

```js
/** 控件内全局事件定义，自定义事件名请勿重名 */
let btnGroupSafe = {
  /**
   * 添加按钮事件
   */
  btnAdd: () => {
    const _win = openEditor(
      '新增',
      {
        cmd: 'add',
        ui: the.tpl,
        data: {}
      },
      (_resp) => {
        // console.log('onEditorEvent', _win)
        // 窗口回调
        onEditorEvent(_resp, () => {
          // 关闭窗口
          _win.close();
          // 查询最新数据
          httpSearch();
        });
      }
    );
  },
  /** 审核按钮 */
  btnAudit: () => {
    httpApi('audit');
  },
  /** 撤销审核 */
  btnResc,
  /** 标记删除按钮 */
  btnDel,
  /** 物理删除 */
  btnErase,
  /** 搜索按钮 */
  btnSearch,
  /** 显示更多按钮 */
  btnMore,
  /** 重置初始化按钮 */
  btnInit,
  /** 表头列关闭显示 */
  btnThCut,
 /**
       * 求和事件
       */
  btnTdSum:,
  /**
   * 锁定列显示
   */
  btnThLock,
  /**
   * 表头置顶交换位置
   */
  btnThSwap,
  /**
   * 集合查询勾选列表
   */
  btnMoreChecks,
  /** 清除已勾选 */
  btnCheckClean
};
```

### 勾选按钮事件

自定义按钮设置按钮样式 type,并且触发按钮事件

```js

/** 列表编辑 */
const tpl = {
  // 列表扩展配置
  listor: {
    // 自定义按钮
    btns: {
      btnAddTree: {
        title: '添加树节点', //按钮描述
        type: 'info' //按钮样式
      },
      btnStateClose: {
        title: "关闭账户",
        type: "info"
      }
    },
    // 监听全局事件触发
    events: {
      /**
       * 自定义按钮触发 (接收resp)
       * @param {*} resp  返回listor配置参数对象
       */
      btnAddTree: (resp) => {
        //checkList 勾选的主键值
        console.log('btnAddTree', the.tree.onItem, resp.table.checkList);
        // checkeds 勾选的序号，可以取出整条数据
        for (let _ck in resp.table.checkeds) {
          // 取出列表序号
          let _index = resp.table.checkeds[_ck];
          // 取出整条数据
          let _item = resp.table.data[_index];
        }
      },
      btnStateClose:(resp) => {
        console.log('btnCacheDel', resp.table.checkList);
        if(resp.table.checkList.length<1){ message.info("请勾选更新行");return;}
        const { http, message } = window.$plus;
        http({
          url: '请求地址',
          data: {
            cmd: 'edit',
            where: [{ ID: resp.table.checkList.toString(), q: 'in' }]
          }
        }).then((res) => {
          message.info(res.code == 1 ? '更新成功，请查询' : res.msg);
        });
      };
    }
  }
};
```

### 自定义事件触发监听

```js
/** 列表编辑 */
const tpl = {
  listor: {
    // 监听事件触发，事件名不能重复
    events: {
      // 后台配置定义json 传入是 function 函数字符串代码
      App_ID: `  
        (resp) => {
          console.log('td.App_ID', resp);
          console.log('传入的参数集合：', arguments);

          //  console.log('td.App_ID', resp);
          const { message } = window.$plus.ui;
          message.info(resp.data.ID);

        } `,
      // 前端可直接写函数
      Code: (resp) => {
        // console.log('td.App_ID', resp);
        //  console.log('传入的参数集合：', arguments);

        //  console.log('td.Code', resp);
        const { message } = window.$plus.ui;
        message.info(resp.data.ID);
      }
    }
  }
};
```
