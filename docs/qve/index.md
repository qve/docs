# Quick-Vue-Engineering

> Quick Engineering UI for Vue 3.x

一个极简稳健的中后台项目开发框架

- [qve 发布包更新说明](https://www.npmjs.com/package/qve)
- [开箱即用模板 qve-admin-template](https://gitee.com/qve/qve-admin-template)

## QVE 组件

- [PanelListor](./panelListor.md)
- [listor](./listor.md)
- [Editor](./editor.md)
- [Control](./control.md)
- [AsyncModel](./asyncModel.md)
- [CodeEditor](./codeEditor.md)

## QVE Api

- clearCacheTpl
- codeUnpackTpl
- config
- default
- deleteCacheState
- getCacheState
- getCacheTpl
- getSession
- http
- loadCacheTplByHttp
- removeCacheState
- removeCacheTpl
- removeSession
- setCacheState
- setCacheTpl
- setSession

## $plus 组件引入

- 全局默认变量：window.$plus
- 挂载方法：components/index.js

```js
/** 引入基础库 */
import * as quick from 'quick.lib';

/** 引入Ui组件库 */
import 'qveui/dist/fonts/iconfont.css';
import 'qveui/dist/styles/index.less';
import * as ui from 'qveui';

/** 引入项目组件库 */
import qve from 'qve';
import * as $qve from 'qve';

/** vue原生方法 */
import * as $vue from './vue.api';

// 全局状态引入
import * as $store from '../store';

// 引入网络请求
import { request, fail } from './utils/request.js';

// 请求地址配置
import { host, url } from '../router/url.js';

// 本地页面集合
import * as $Views from '../router/views';

// 全局路径方法
import * as $router from '../router/method.js';

/**
 * vue 组件挂载
 * @param {*} app
 * @param {*} opts
 */
const install = function (app, opts) {
  //  console.log('install');
  // 判断是否安装
  if (install.installed) return;

  opts = opts || {
    //输出日志
    log: { isPrint: true },
    // 是否绑定外挂 window
    plus: true,
    // 初始化 quick.lib 原生JS函数扩展
    init: true
  };

  // 当前项目外部配置
  if (opts.config) {
    //console.log('install.host', opts.config.host);
    url.setHost(opts.config.host);
  }

  // 注册组件
  Object.keys(components).forEach((key) => {
    app.component(key, components[key]);
  });

  // 使用install方法引入组件库
  app.use(ui.install, opts);

  // 2种方法都可以引入项目库
  app.use(qve, {
    ...opts,
    config: {
      app: _configApp,
      http: {
        setting: {
          // 跨域时是否发送cookie
          withCredentials: true,
          // 服务器地址
          baseURL: host.base
        },
        // 请求库
        request,
        // 请求失败处理
        fail
      },
      // 外挂脚本地址
      script: {
        // 在线代码编辑器路径,initMonacoEditorScript 自动加载
        monacoEditorPath: '/static/plus/monaco/min/vs',
        lessUrl: '/static/plus/less/less.min.js'
      }
    }
  });

  // 全局挂载
  let $plus = {
    vue: $vue,
    // 自定义挂载
    quick,
    url,
    store: $store,
    router: $router,
    ui,
    loading: ui.loading,
    confirm: ui.confirm, //对话
    message: ui.message, //消息
    frame: ui.frame, //弹窗
    qve: $qve,
    http: $qve.http,
    config: $qve.config,
    views: $Views //页面调用
  };

  // 日志输出
  let _resp = `version:${$plus.ui.version},lib:` + quick.version;

  if (typeof window !== 'undefined' && opts.plus) {
    if (window.$plus) {
      // 添加其它自定义外挂
      for (let key in window.$plus) {
        $plus[key] = window.$plus[key];
      }
    }

    // 全局原生挂载
    window.$plus = $plus;
    _resp += ';window.$plus';
  } else {
    // 挂载
    /** 全局绑定调用 */
    app.config.globalProperties.$plus = $plus;
    _resp += ';ctx';
  }

  // 读取当前外挂的组件
  _resp += '[';
  // 显示外挂的key
  for (let _key in $plus) {
    _resp += ',' + _key;
  }
  _resp += `];`;

  // 调试日志
  if (typeof opts.log !== 'undefined') {
    //调试日志输出
    console.log('quick-vue-ui', _resp);
  }
};

/** vue原生方法 */
export * from './vue.api';
// 导出UI组件库
export * from 'qveui';
/** 导出项目组件库*/
export * from 'qve';

// 导出该组件
export default {
  install
};
```

## views 路由页面

- 项目需要跨页面引入调用路由注册的其它页面

```js
// 除了使用 import DBTableFrame from '页面地址'

// 项目可以用外挂方式引入

const { DBTableFrame } = window.$plus.views;

const { frame } = window.$plus;

// 新窗口打开
frame.open({
  title: '添加' + title,
  model: {
    props: {
      //workID: 11577
    },
    // 进行组件标记
    component: markRaw(DBTableFrame)
  },
  // onEvent: (_res) => {
  //   console.log('win.on', _res);
  // },
  onClose: (_resp) => {
    console.log(reloadNamed);
    if (reloadNamed) {
      getInfoData(reloadNamed);
    }
    // 自定义关闭方法
    _resp.close();
  }
});

const open = (res) => {
  const { router } = window.$plus;
  router.push(
    '/frame/orm/dbTable?db=' + res.data.DBCode + '&table=' + res.data.TableCode
  );
};

const open = (res) => {
  const { frame } = window.$plus;
  const { markRaw } = window.$plus.vue;
  const { DBTableFrame } = window.$plus.views;
  frame.open({
    title: '编辑',
    model: {
      props: {},
      component: () => {
        return markRaw(DBTableFrame);
      }
    }
  });
};
```
