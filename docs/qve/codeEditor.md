tags:[CodeEditor,代码编辑器]
categories:[技术]

# CodeEditor 代码编辑器

基于 monaco 实现的轻量级代码编辑器,支持自定义引用样式与语法。

特点：

- 语法高亮
- 行号
- 动态编辑

- [vetur 插件](https://github.com/vuejs/vetur)
- [monaco-editor 官方](https://github.com/microsoft/monaco-editor)
- [monaco-editor 代码编辑库 0.33.0](https://www.npmjs.com/package/monaco-editor)
- 代码编辑样式加载 less 4.1.2
- [terser js 代码压缩支持 es6](https://terser.org/docs/api-reference)

## CodeEditor vue 代码编辑

- 目前 ESLint 有两种流派`Airbnb`和`Standard` 前者推荐加分号，后者不推荐

::: warning
注意 JS 代码结尾必须加分号
需要拷贝插件 `node_modules`>`qve`>`dist`>`plus` 到项目根目下的 `pulic`>`plus`
:::

```vue
<template>
  <div class="test-code-page">
    <CodeEditor
      class="body"
      v-model="the.code.body"
      :lang="the.code.lang"
      @onEvent="onCodeEditorEvent"
    >
      <template>
        <Icon type="icon-shezhi" title="设置" @click="the.show = !the.show" />

        <Select v-model="the.code.lang" @onEvent="onLangEvent">
          <option
            v-for="(key, index) in the.code.langList"
            :key="index"
            :value="key"
          >
            {{ key }}
          </option>
        </Select>
      </template>
    </CodeEditor>
  </div>
</template>

<script>
export default {
  setup(props, context) {
    const { reactive } = window.$plus.vue;

    const the = reactive({
      code: {
        /** 代码语言类别 */
        lang: 'html',
        langList: [
          'vue',
          'c#',
          'js',
          'css',
          'less',
          'html',
          'md',
          'ts',
          'py',
          'sql',
          'xml-doc',
          'json',
          'bash',
          'c'
        ],
        title: '在线帮助',
        path: '/page/help/index',
        // 版本
        version: '0.0.1',
        // 编译后代码
        code: '',
        // 源代码
        body: '',
        css: ''
      }
    });

    const onCodeEditorEvent = (resp) => {
      console.log('onCodeEditorEvent', resp);

      switch (resp.cmd) {
        case 'output':
          // 输出命令
          the.body = resp.value;

          if (resp.lang == 'less') {
            // 格式转换的代码
            the.css = resp.output.css;
          }

          // console.log('Code.body', the.edit.body);
          //  the.edit.body = vue_code_tpl.body;

          break;
      }
    };

    const onLangEvent = (data) => {
      console.log('onLangEvent', the.code.lang, data);
    };

    return { the, onCodeEditorEvent, onLangEvent };
  }
};
</script>
```

## CodeEditor props

| 属性              | 说明                         | 类型   | 默认值 |
| ----------------- | ---------------------------- | ------ | ------ |
| named             | 组件命名                     | String | input  |
| v-model           | 双向绑定数据                 | String | -      |
| lang              | 代码语言,less 支持输出为 css | String | html   |
| opts              | monaco.editor 配置参数       | Object | null   |
| btnOutput `0.3.6` | 输出按钮文本                 | String | 输出   |
| btnReset `0.3.6`  | 重置按钮文本                 | String | 重置   |

## CodeEditor events

| 事件名  | 说明                         | 值     |
| ------- | ---------------------------- | ------ |
| onEvent | 点击触发回调事件             |        |
| cmd     | 图标触发指令 `output` 更新值 | string |
| named   | 自定义组件命名               | string |
| value   | 代码内容                     | string |
| lang    | 代码语言                     | string |
| output  | 编译输出对象                 | object |

## CodeEditor slot

| 名称  | 说明     |
| ----- | -------- |
| tools | 工具栏   |
|       | 代码顶部 |

## lang 语言 less 转为 css

= 在 index.html 直接引入 less.js 用于编译 less 为 css
`<script id="loadLessScript" src="/static/plus/less/less.min.js"></script>`

- 组件运行代码需编译为 css,才能正常加载样式
- 支持 less 编译输出为 output.css

## monaco.editor opts 配置

- [官方 API](https://microsoft.github.io/monaco-editor/api/modules/monaco.editor.html)

```json
{
  "acceptSuggestionOnCommitCharacter": true, // 接受关于提交字符的建议
  "acceptSuggestionOnEnter": "on", // 接受输入建议 "on" | "off" | "smart"
  "accessibilityPageSize": 10, // 辅助功能页面大小 Number 说明：控制编辑器中可由屏幕阅读器读出的行数。警告：这对大于默认值的数字具有性能含义。
  "accessibilitySupport": "on", // 辅助功能支持 控制编辑器是否应在为屏幕阅读器优化的模式下运行。
  "autoClosingBrackets": "always", // 是否自动添加结束括号(包括中括号) "always" | "languageDefined" | "beforeWhitespace" | "never"
  "autoClosingDelete": "always", // 是否自动删除结束括号(包括中括号) "always" | "never" | "auto"
  "autoClosingOvertype": "always", // 是否关闭改写 即使用insert模式时是覆盖后面的文字还是不覆盖后面的文字 "always" | "never" | "auto"
  "autoClosingQuotes": "always", // 是否自动添加结束的单引号 双引号 "always" | "languageDefined" | "beforeWhitespace" | "never"
  "autoIndent": "None", // 控制编辑器在用户键入、粘贴、移动或缩进行时是否应自动调整缩进
  "automaticLayout": true, // 自动布局
  "codeLens": false, // 是否显示codeLens 通过 CodeLens，你可以在专注于工作的同时了解代码所发生的情况 – 而无需离开编辑器。 可以查找代码引用、代码更改、关联的 Bug、工作项、代码评审和单元测试。
  "codeLensFontFamily": "", // codeLens的字体样式
  "codeLensFontSize": 14, // codeLens的字体大小
  "colorDecorators": false, // 呈现内联色彩装饰器和颜色选择器
  "comments": {
    "ignoreEmptyLines": true, // 插入行注释时忽略空行。默认为真。
    "insertSpace": true // 在行注释标记之后和块注释标记内插入一个空格。默认为真。
  }, // 注释配置
  "contextmenu": true, // 启用上下文菜单
  "columnSelection": false, // 启用列编辑 按下shift键位然后按↑↓键位可以实现列选择 然后实现列编辑
  "autoSurround": "never", // 是否应自动环绕选择
  "copyWithSyntaxHighlighting": true, // 是否应将语法突出显示复制到剪贴板中 即 当你复制到word中是否保持文字高亮颜色
  "cursorBlinking": "Solid", // 光标动画样式
  "cursorSmoothCaretAnimation": true, // 是否启用光标平滑插入动画  当你在快速输入文字的时候 光标是直接平滑的移动还是直接"闪现"到当前文字所处位置
  "cursorStyle": "UnderlineThin", // "Block"|"BlockOutline"|"Line"|"LineThin"|"Underline"|"UnderlineThin" 光标样式
  "cursorSurroundingLines": 0, // 光标环绕行数 当文字输入超过屏幕时 可以看见右侧滚动条中光标所处位置是在滚动条中间还是顶部还是底部 即光标环绕行数 环绕行数越大 光标在滚动条中位置越居中
  "cursorSurroundingLinesStyle": "all", // "default" | "all" 光标环绕样式
  "cursorWidth": 2, // <=25 光标宽度
  "minimap": {
    "enabled": false // 是否启用预览图
  }, // 预览图设置
  "folding": true, // 是否启用代码折叠
  "links": true, // 是否点击链接
  "overviewRulerBorder": false, // 是否应围绕概览标尺绘制边框
  "renderLineHighlight": "gutter", // 当前行突出显示方式
  "roundedSelection": false, // 选区是否有圆角
  "scrollBeyondLastLine": false, // 设置编辑器是否可以滚动到最后一行之后
  "readOnly": false, // 是否为只读模式
  "theme": "vs" // vs, hc-black, or vs-dark
}
```

## terser js es6 代码压缩

- uglify-js 不支持 es6 的语法
- terser js 的编译和压缩 支持 es6 语法

使用案例

```html
<html>
   <head>
    <title>test</title>
  </head>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/source-map@0.7.3/dist/source-map.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/terser/dist/bundle.min.js"></script>
    <script>

      async function minifyCode() {
        // 需压缩的源代码
        let code = 'function add(first, second) { return first + second; }';
        let minify = Terser.minify;
        var result = await minify(code, { sourceMap: true });
        console.log(result.code); // minified output: function add(n,d){return n+d}
        console.log(result.map); // source map
      }

      window.onload = () => {
        console.log('terser', typeof window.terser, typeof window.minify);
        minifyCode();
      };

    </script>
  </body>
 </body>
</html>
```
