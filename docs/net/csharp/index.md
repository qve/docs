# CSharp c#

编程语言有时可以划分为静态类型化语言和动态类型化语言。

- C#和 Java 经常被认为是静态化类型的语言
- 而 Python、Ruby 和 JavaScript 是动态类型语言。
- 动态语言在编译时不会对类型进行检查，而是在运行时识别对象的类型。这种方法有利有弊：代码编写起来更快、更容易，但无法获取编译器错误，只能通过单元测试和其他方法来确保应用正常运行。
- C#最初是作为纯静态语言创建的，但是 C#4 添加一个新关键字来支持这些动态功能：dynamic。

## dynamic object var

- var 实际上编译器抛给我们的语法糖，一旦被编译，编译器就会自动匹配 var 变量的实际类型，并用实际类型来替换该变量的声明，等同于我们在编码时使用了实际类型声明。
- dynamic 被编译后是一个 Object 类型，编译器编译时不会对 dynamic 进行类型检查。
- Object 在编译时进行类型检查

```csharp
// 动态转换
dynamic d1 = 7;
dynamic d2 = "a string";
int i = d1;
string str = d2;
```

## Dynamic 与 Json

```csharp

dynamic person = new ExpandoObject();  //ExpandoObject 为密封类
Func<int, string> doSomething = ID => ID.ToString();    //定义委托方法
person.GetID = doSomething;      //运行时动态添加方法
person.Name = "吴某某";        //运行时动态添加了属性Name
Console.WriteLine(person.Name + "  " + person.GetID(50)); // person.GetID(50) 方法调用

```

## ExpandoObject json

```csharp
dynamic obj = new System.Dynamic.ExpandoObject();
if(obj  is ExpandoObject){
  ((IDictionary<string, object>)obj).Add("Key", "Value");
}

///dynamic类型的数据对象是否存在某个属性, JSON 建议用 JsonQuick
public static bool IsPropertyExist(dynamic data, string propertyname)
{
    if (data is ExpandoObject)
      return ((IDictionary<string, object>)data).ContainsKey(propertyname);
    return data.GetType().GetProperty(propertyname) != null;
}
```

## CSharpCodeProvider 动态执行

- 提供对 C# 代码生成器和代码编译器的实例的访问权限
- [csharpcodeprovider](https://docs.microsoft.com/zh-cn/dotnet/api/microsoft.csharp.csharpcodeprovider?view=dotnet-plat-ext-6.0)

## 正则

```csharp

string _str="需要取出：{ab}"

// 语法
Match _mc = Regex.Match(_str,@"\{(.*?)\}", RegexOptions.IgnoreCase);
Console.WriteLine(_mc);
if (_mc.Groups.Count > 1)
{
  string _key = _mc.Groups[1].Value;
}

// 使用QucikBLL
 string _key2 =QuickBLL.Match(_str,@"\{(.*?)\}").Groups[1].Value;

```

## StringBuilder 字符串拼接

## Model 构造语法

```csharp
public class TestModel
{

  public string client;

  public TestModel() : this("Quick")
  {

  }

  public TestModel(string dbCode)
  {
    client =dbCode;
  }
}
```

## 调试断点 Debugger

- 如何在调试模式下，灵活配置断点，用于代码调试。

```csharp

public override void OnActionExecuting(ActionExecutingContext context)
{

#if DEBUG
  //  Debug.WriteLine(authType.ToString());
  // 调试断点
  Debugger.Break();
#endif
}
```
