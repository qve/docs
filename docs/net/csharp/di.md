# DI 函数注入

> 适用于 Core,Net5
> .NET DI 一般使用构造函数注入获取对象

## 注入函数

权重：`AddSingleton`→`AddTransient`→`AddScoped`

- AddSingleton 的生命周期：

项目启动-项目关闭 相当于静态类 只会有一个

- AddScoped 的生命周期：

请求开始-请求结束 在这次请求中获取的对象都是同一个

- AddTransient 的生命周期：

请求获取-（GC 回收-主动释放） 每一次获取的对象都不是同一个

## DI 注入

- 在 Startup.cs 的 Configure 配置注入

```csharp
public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        // 注入（DI）在同一个请求发起到结束，在服务容器中注册自定义中间件QuickSafe用户类
        services.AddScoped(typeof(QuickSafeFilterModel));

        //测试服务
        services.AddSingleton<ITestService,TestService>().

        #region  工作流配置
          services.AddWorkflow();
          services.AddWorkflowDSL();
        #endregion
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {

        // 扩展服务
        app.UseConfigureExtensions();
    }
}
```

- 第一种获取方式（不推荐使用）

```csharp

    /// <summary>
    ///  自定义服务扩展
    /// </summary>
    public static class ConfigureExtensions
    {
        /// <summary>
        ///  服务扩展
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseConfigureExtensions(this IApplicationBuilder app)
        {
            var services = new ServiceCollection();
            var provider = services.BuildServiceProvider();
            var _safe = provider.GetService<QuickSafeFilterModel>();
        }

        ///注入新服务
        private static IServiceProvider ConfigureServices()
        {
            //setup dependency injection
            IServiceCollection services = new ServiceCollection();
            //services.AddLogging();
            //services.AddWorkflow();
            //services.AddWorkflowDSL();

            var serviceProvider = services.BuildServiceProvider();

            return serviceProvider;
        }
    }

```

- 第二种获取方式（推荐使用）

注意： 使用`ServiceLocator.Instance.GetService<T>();`，只能获取`AddTransient`和`AddSingleton`注入的对象，而不能获取`AddScoped`（请求生命周期内唯一）注入的对象，不是不能获取，而是获取的和构造函数获取的不是相同对象，也就是说获取的对象没有共享.

```csharp

    /// <summary>
    ///  自定义服务扩展
    /// </summary>
    public static class ConfigureExtensions
    {

        /// <summary>
        /// 构造服务加载
        /// </summary>
        public static class ServiceLocator
        {
            public static IServiceProvider Instance { get; set; }
        }

        /// <summary>
        ///  服务扩展
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseConfigureExtensions(this IApplicationBuilder app)
        {

            //工作流注册
            ServiceLocator.Instance = app.ApplicationServices;
            var loader = ServiceLocator.Instance.GetService<IDefinitionLoader>();
            var result = loader.LoadDefinition("json 代码", Deserializers.Json);
            Console.WriteLine("LoadTest");
        }
    }

```

- Api 控制获取

```csharp
private ITestService _valueService;

public TestController(ITestService valueService)
{
    _valueService = valueService;
}
```

- 类服务获取

使用场景比如获取`IUnitOfWork`，`AddScoped`（请求生命周期内唯一）注入的对象

```csharp

public class SampleDomainService : IDomainService
{
        private IUnitOfWork _unitOfWork;

        public SampleDomainService(IHttpContextAccessor httpContextAccessor)
        {
            _unitOfWork = httpContextAccessor.HttpContext.RequestServices.GetService<IUnitOfWork>();
        }
}
```
