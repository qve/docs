# 基础

## sealed class 密封类

- 密封类在声明使用 sealed 修饰符，这样就可以防止该类被其它类继承。如果试图将一个密封类作为其它类的基类，C#将提示出错。理所当然，密封类不能同时又是抽象类，因为抽象总是希望被继承的。

- 密封类中不可能有派生类。如果密封类实例中存在虚成员函数，该成员函数可以转化为非虚的，函数修饰符 virtual 不再生效。

```csharp
public sealed class DbHelper{
    public string ConnectionString { get; set; }
    private DbProviderFactory providerFactory;
    public DbHelper(string connectionString, DbProviderType providerType)
    {}
}

```
